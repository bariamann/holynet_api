<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holynetlogin extends CI_Controller 

{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
		  $this->load->model('loginmodel');
		  $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		  $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
		  $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	public function index()
	{
		$this->load->view('loginview');	
	}
	public function login()
	{
			$user=$this->input->post('Name');
			$password=$this->input->post('password');
		
			$userData=$this->loginmodel->loginUser($user,$password);
			
			 if ($userData != null) {
				 		$name='';
						$id=0;
						$sessiondata = '';
						 foreach($userData as $row){
						 $id = (int)$row['Id'];
						 $name = $row["userName"];
						 }
				 		 $sessiondata = array('username'=>$name,'loginuser' => TRUE,'id'=>$id);
				 		 $this->session->set_userdata('data',$sessiondata);
						echo json_encode(array('Status' => 1,'Message' => "Successfully login.",'Data'=> $userData,'Session'=>$sessiondata));
						return;
			        }
					else
					{
					      echo json_encode(array('Status' => 0,'Message' => "UserName and Password Not matched"));
							return;
				     }		
	}
	
	public function logout()
	{
		$sessdata=$this->session->userdata('data');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('loginuser');
		$this->session->unset_userdata('id');
		$this->session->sess_destroy();
		redirect(base_url());
	}
	  protected function no_cache()
	 {
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
}

?>