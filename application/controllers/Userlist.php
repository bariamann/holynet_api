<?php
class Userlist extends CI_Controller
{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
	}
	public function index()
	{
			$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				//$this->load->view('header');
				$this->load->view('userlistview');
			}
			else
			{
				redirect('Holynetlogin');
			}
	}
	
	public function userprofile()
	{
		$this->load->view('userprofileview');
	}
}
?>
