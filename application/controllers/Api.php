<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');
require_once(APPPATH.'getid3/getid3/getid3.php');
class Api extends CI_Controller{


	private function showdefaultMessage($message){
		echo json_encode(array('Status' => 0,'Message' => $message));
		return;
	}

		/* Method to display error msg with status code
		   Created By: Manzz Baria
		*/
			private function showErrorMessageWithStatus($status,$message){
				echo json_encode(array('Status' => $status,'Message' => $message));
				return;
			}

		/* Method to display result with status code
		   Created By: Manzz Baria
		*/
			private function showResultWithStatus($status,$message,$data){
				echo json_encode(array('Status' => $status,'Message' => $message,'Data' => $data));
				return;
			}

		/*	Method to test method
			Created By: Manzz Baria
		*/
			public function getJsonArray(){
				$data=json_decode(file_get_contents('php://input'));
				//$this->output->set_header('Content-Type: application/json; charset=utf-8');
				$request = file_get_contents('php://input');
				$input = json_decode($request);
				//foreach($input as $row){
					echo json_encode(array('Status'=>$input->UserID,'input'=>$input->PhoneLogs));
			//	}

				return;



			}

		/*	Method to check latest version
			Created By: Manzz Baria
		*/
			public function checkVersion(){
$data=json_decode(file_get_contents('php://input'));
				$VersionCode = 0;
				$VersionName = 0;
				if (!empty($_REQUEST['VersionCode'])){
					$VersionCode = $_REQUEST['VersionCode'];}

				if (!empty($_REQUEST['VersionName'])){
					$VersionName = $_REQUEST['VersionName'];}

				if ($VersionName < '1.0')
				{
					echo json_encode(array('Status'=>0,'Message'=>'You need to update yout app version'));
					return;

				}
				echo json_encode(array('Status'=>1,'Message'=>'Your version is up-to-date'));

			}
		/*	Method to check latest version
			Created By: Manzz Baria
		*/
			public function test(){
				$data=json_decode(file_get_contents('php://input'));
			$this->load->model("ApiUserModel","model");
			$userArray = $this->model->getUserById(1,'en');
			echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userArray));
			}

		/*	Method to User Registration
				Created By: Manzz Baria
			*/
		public function userRegistration(){
				$data=json_decode(file_get_contents('php://input'));
				if($this->input->post()){

					$lang = 'en';

					if (!empty($_POST['lang'])){
						$lang = $_POST['lang'];
					}

					if (!empty($_POST['FullName'])){
						$FullName = $_POST['FullName'];
					}else{
						$this->showdefaultMessage("FullName is required");
						return;
					}

					if (!empty($_POST['UserName'])){
						$UserName = $_POST['UserName'];
					}else{
						$this->showdefaultMessage("UserName is required");
						return;
					}

					if (!empty($_POST['Email'])){
						$Email = $_POST['Email'];
					}else{
						$this->showdefaultMessage("Email is required");
						return;
					}

					if (!empty($_POST['Password'])){
						$Password = $_POST['Password'];
					}else{
						$this->showdefaultMessage("Password is required");
						return;
					}

					if (!empty($_POST['DeviceType'])){
						$DeviceType = $_POST['DeviceType'];
					}else{
						$this->showdefaultMessage("DeviceType is required");
						return;
					}
					if (!empty($_POST['DeviceToken'])){
						$DeviceToken = $_POST['DeviceToken'];
					}else{
						$this->showdefaultMessage("DeviceToken is required");
						return;
					}

					$this->load->model("ApiUserModel","model");
					$userExits = $this->model->isUserExists($Email,$UserName);
					if (!$userExits) {
						$userArray = $this->model->registerUser($FullName,$UserName,$Email,$Password,$DeviceType,$DeviceToken,$lang);
						if ($userArray == null) {

							$this->showdefaultMessage("Error while registration");
							return;

						}
						echo json_encode(array('Status'=>1,'Message'=>'Successfully registered','Data'=> $userArray));
					}else{
						$this->showErrorMessageWithStatus(2,"User already registered with same username or email");
						return;

					}
				}else{
						$this->showdefaultMessage("Please use POST method");

				}
			}

		/*	Method to User Login
			Created By: Manzz Baria
		*/
		public function userLogin(){
				$data=json_decode(file_get_contents('php://input'));
				if($this->input->post()){

					if (!empty($_POST['UserName'])){
						$UserName = $_POST['UserName'];
					}else{
						$this->showdefaultMessage("Username or Email is required");
						return;
					}
					if (!empty($_POST['Password'])){
						$Password = $_POST['Password'];
					}else{
						$this->showdefaultMessage("Password is required");
						return;
					}

					if (!empty($_POST['DeviceType'])){
						$DeviceType = $_POST['DeviceType'];
					}else{
						$this->showdefaultMessage("DeviceType is required");
						return;
					}
					if (!empty($_POST['DeviceToken'])){
						$DeviceToken = $_POST['DeviceToken'];
					}else{
						$this->showdefaultMessage("DeviceToken is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$userStatus = $this->model->getUserStatusBlock($UserName,$Password);
					if($userStatus){
						$this->showdefaultMessage("Sorry, Your account is blocked by holynet due to abuse content.");
						return;
					}

					$userStatus = $this->model->checkUserIsActive($UserName);
					if(!$userStatus){
						$this->showdefaultMessage("Please activate your account from link which sent to your email.");
						return;
					}
					$userArray = $this->model->userLogin($UserName,$Password,$DeviceType,$DeviceToken);
					if ($userArray == null) {
						$this->showdefaultMessage("Invalid UserName/email or password");
						return;
					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userArray));
				}else{
					$this->showdefaultMessage("Please use POST method");
				}
			}

	/*	Method to get ProfileTypes
			Created By: Manzz Baria
		*/
		public function getProfileTypes(){
$data=json_decode(file_get_contents('php://input'));
					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}

					$this->load->model("ApiProfileModel","model");
					$userArray = $this->model->getProfiles($lang);
					if ($userArray == null) {
						$this->showdefaultMessage("No records found");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userArray));

		}

		/*	Method to get Video Types
			Created By: Manzz Baria
		*/
		public function getVideoTypes(){
$data=json_decode(file_get_contents('php://input'));
					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}
					$this->load->model("ApiVideoModel","model");
					$userArray = $this->model->getVideoTypes($lang);
					if ($userArray == null) {
						$this->showdefaultMessage("No records found");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userArray));

		}

		/*	Method to create Video Types
			Created By: Manzz Baria
		*/
		public function CreateVideoType(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['NameEnglish'])){
						$NameEnglish = $_POST['NameEnglish'];
					}else{
						$this->showdefaultMessage("NameEnglish is required");
						return;
					}
					if (!empty($_POST['NameArabic'])){
						$NameArabic = $_POST['NameArabic'];
					}else{
						$this->showdefaultMessage("NameArabic is required");
						return;
					}
					$this->load->model("ApiVideoModel","model");
					$isSuccess = $this->model->createVideoType($NameEnglish,$NameArabic);
					if (!$isSuccess) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully added'));
			}else{
						$this->showdefaultMessage("Please use POST method");

				}

		}


		/*	Method to create profile Types
			Created By: Manzz Baria
		*/
		public function CreateProfileType(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['NameEnglish'])){
						$NameEnglish = $_POST['NameEnglish'];
					}else{
						$this->showdefaultMessage("NameEnglish is required");
						return;
					}
					if (!empty($_POST['NameArabic'])){
						$NameArabic = $_POST['NameArabic'];
					}else{
						$this->showdefaultMessage("NameArabic is required");
						return;
					}
					$this->load->model("ApiProfileModel","model");
					$isSuccess = $this->model->createProfileType($NameEnglish,$NameArabic);
					if (!$isSuccess) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully added'));
			}else{
						$this->showdefaultMessage("Please use POST method");

				}

		}


		/*	Method to get normal profile User list
			Created By: Manzz Baria
		*/
		public function getUsers(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}
					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}
					$IsVerified = 0;
					if (!empty($_REQUEST['IsVerified'])){
						$IsVerified = $_REQUEST['IsVerified'];
					}
					$this->load->model("ApiUserModel","model");
					$userArray = $this->model->getUsers($PageIndex,$lang,$IsVerified);
					if ($userArray == null) {
						$this->showdefaultMessage("No records found");
						return;

					}
					$totalPage = 0;
					$totalPage = $this->model->getTotalPageOfUsers($IsVerified);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage'=>$totalPage,
											"CurrentPage"=>$PageIndex+1,
											'Data'=> $userArray));

		}

		/*	Method to get abuse User list
			Created By: Nishit Patel
		*/
		public function getAbuseUsers(){
$data=json_decode(file_get_contents('php://input'));
			$PageIndex = 0;
			if (!empty($_REQUEST['PageIndex'])){
				$PageIndex = $_REQUEST['PageIndex'];
			}
			$lang = 'en';
			if (!empty($_REQUEST['lang'])){
				$lang = $_REQUEST['lang'];
			}
			$this->load->model("ApiUserModel","model");
			$userArray = $this->model->getAbuseUsers($PageIndex,$lang);
			if ($userArray == null) {
				$this->showdefaultMessage("No records found");
				return;
			}
			$totalPage = 0;
			$totalPage = $this->model->getAbuseTotalPageOfUsers();
			echo json_encode(array('Status'=>1,
					'Message'=>'Successfully',
					'TotalPage'=>$totalPage,
					"CurrentPage"=>$PageIndex+1,
					'Data'=> $userArray));

		}

		/*	Method delete User
			Created By: Manzz Baria
		*/
		public function deleteUser(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){
					if (!empty($_POST['UserId'])){
						$UserId = $_POST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->deleteUser($UserId);
					if (!$isSuccess) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}

		/*	Method change password
			Created By: Manzz Baria
		*/
		public function changePassword(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){
					if (!empty($_POST['UserId'])){
						$UserId = $_POST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}
					if (!empty($_POST['CurrentPassword'])){
						$CurrentPassword = $_POST['CurrentPassword'];
					}else{
						$this->showdefaultMessage("CurrentPassword is required");
						return;
					}
					if (!empty($_POST['NewPassword'])){
						$NewPassword = $_POST['NewPassword'];
					}else{
						$this->showdefaultMessage("NewPassword is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$isxists = $this->model->isCurrentPasswordMatched($UserId,$CurrentPassword);
					if (!$isxists) {
						$this->showdefaultMessage("wrong current password");
						return;

					}
					$this->model->changePassword($UserId,$NewPassword);
					echo json_encode(array('Status'=>1,'Message'=>'Successful changed'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}

	public function removeUserProfilePicture(){
$data=json_decode(file_get_contents('php://input'));
				if (!empty($_REQUEST['UserId'])){
							$UserId = $_REQUEST['UserId'];
				}else{
						$this->showdefaultMessage("UserId is required");
						return;
				}
				$profileImage ='';
				$profileThumb ='';
				$this->load->model("ApiUserModel","model");
				$isUpdated = $this->model->updateUserProfilePicture($UserId,$profileImage,$profileThumb);
				$userData = $this->model->getUserById($UserId,'en');
				echo json_encode(array('Status'=>1,
											'Message'=>'Successfully removed',
										'Data'=>$userData));
	}

	public function updateFullName(){
$data=json_decode(file_get_contents('php://input'));
						if (!empty($_REQUEST['UserId'])){
							$UserId = $_REQUEST['UserId'];
						}else{
							$this->showdefaultMessage("UserId is required");
							return;
						}

					$fullName ='-1'	;
					if (isset($_REQUEST['fullName'])){
						$fullName = $_REQUEST['fullName'];
					}
					$this->load->model("ApiUserModel","model");
					$isUpdated = $this->model->changeFullName($UserId,$fullName);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully updated'));
	}

		/*	Method to update User profile
			Created By: Manzz Baria
		*/
		public function updateUserProfile(){
$data=json_decode(file_get_contents('php://input'));
					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$fullName ='-1';
					$userName ='-1';
					$email ='-1';
					$mobileNumber ='-1';
					$faxNumber = '-1';
					$homeTown ='-1';
					$countryId = -1;
					$currentAddress ='-1';
					$profileTypeId = -1;


					if (isset($_REQUEST['fullName'])){
						$fullName = $_REQUEST['fullName'];
					}

					if (isset($_REQUEST['userName'])){
						$userName = $_REQUEST['userName'];
					}

					if (isset($_REQUEST['email'])){
						$email = $_REQUEST['email'];
					}

					if (isset($_REQUEST['mobileNumber'])){
						$mobileNumber = $_REQUEST['mobileNumber'];
					}

					if (isset($_REQUEST['homeTown'])){
						$homeTown = $_REQUEST['homeTown'];
					}

					if (isset($_REQUEST['countryId'])){
						$countryId = $_REQUEST['countryId'];
					}

					if (isset($_REQUEST['currentAddress'])){
						$currentAddress = $_REQUEST['currentAddress'];
					}

					if (isset($_REQUEST['faxNumber'])){
						$faxNumber = $_REQUEST['faxNumber'];
					}

					if (isset($_REQUEST['profileTypeId'])){
						$profileTypeId = $_REQUEST['profileTypeId'];
					}


					if(isset($_FILES['file']['name']))
					{
						$file=$_FILES['file']['name'];
						$time = time();
						$new_name = 'holynet_'.$time;
						$config['upload_path'] = './uploads/files/';
						$config['allowed_types']        = '*';
						$config['max_size']             = 0;
						$config['max_width']            = 5400;
						$config['max_height']           = 5400;
						$config['file_name'] = $new_name;

						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if ( ! $this->upload->do_upload('file'))
						{
							$error = array('error' => $this->upload->display_errors());
							$msg=$error['error'];
							$status=FALSE;
							echo json_encode(array('Status'=>0,'Message'=>$msg,'Data'=> null));

						}
						else
						{
							$image_path='./uploads/files/';
							$thumb_path='./uploads/files/thumb/';
							$data = array('upload_data' => $this->upload->data());
							$filename=$data['upload_data']['file_name'];
						}
						$data = $this->upload->data();
						$data=array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];

						$videofile=$_FILES["file"]["tmp_name"];
						$image="";
						$image.="c:/xampp/htdocs/holynet/uploads/files/thumb/";
						$image_path='/uploads/files/';
						$thumb_path='/uploads/files/thumb/';

						$s3URL ='';
						$s3Thumb ='';

						$imagefile = $filename;
						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
						$config['maintain_ratio'] = TRUE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$config['width'] = 300;
						$config['height'] = 200;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$image = $image.$imagefile;
						$s3URL = 'Holynet/ProfilePicture/'.$filename;
						$s3Thumb = 'Holynet/ProfilePicture/Thumb/'.$imagefile;

						$fileUrl = $filename;
						$thumbUrl = $imagefile;

						//Amazon s3 server upload video
						$this->load->library('s3');
						if ($this->s3->putObject($this->s3->inputFile($videofile), BUCKET, $s3URL))
						{
							if (file_exists($image))
							{
									$this->s3->putObject($this->s3->inputFile($image), BUCKET, $s3Thumb);
							}
							// get file size
							$info = $this->s3->getObjectInfo(BUCKET, $s3URL);
							$size = $info['size'];

							// Save file details in database
							$FileUrl = $image_path.$filename;
							$FileThumb = $thumb_path.$imagefile;

							$this->load->model("ApiUserModel","usermodel");
							$isUpdated = $this->usermodel->updateUserProfilePicture($UserId,$fileUrl,$thumbUrl);


						}


					}

					$this->load->model("ApiUserModel","model");
					$isUpdated = $this->model->updateUserProfile($UserId,$fullName,$userName,$email,$mobileNumber,$homeTown,$countryId,$currentAddress,$faxNumber,$profileTypeId);
					if (!$isUpdated) {
						$this->showdefaultMessage("Error at server, please try again later");
						return;

					}
					$userData = $this->model->getUserById($UserId,'en');
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully updated',
										'Data'=>$userData));

		}




		/*	Method Get Total count report
			Created By: Manzz Baria
		*/
		public function getTotalCountReport(){
$data=json_decode(file_get_contents('php://input'));
					$TotalImage = 0; $TotalAbuseImage = 0;
					$TotalVideo=0; $TotalAbuseVideo=0;
					$TotalPost=0; $TotalAbusePost=0;
					$TotalSpecialUser=0; $TotalNormalUser=0;
					$TotalAbuseUser=0;

					$this->load->model("ApiAbuseModel","abmodel");
					$TotalAbuseImage = $this->abmodel->countTotalAbuse(IMAGE_TYPE);
				  $this->load->model("ApiImageModel","imodel");
					$TotalImage = $this->imodel->countTotalImage();

					$this->load->model("ApiVideoModel","vmodel");
					$TotalVideo = $this->vmodel->countTotalVideo();
					$TotalAbuseVideo = $this->abmodel->countTotalAbuse(VIDEO_TYPE);

					$this->load->model("ApiPostModel","pmodel");
					$TotalPost = $this->pmodel->countTotalPost();
					$TotalAbusePost = $this->abmodel->countTotalAbuse(POST_TYPE);

					$this->load->model("ApiUserModel","model");
					// $isSpecial = -1 for both , 0 for normal and 1 for special
				    $TotalSpecialUser = $this->model->countTotalUser(1);
				    $TotalNormalUser = $this->model->countTotalUser(0);
				    $TotalAbuseUser = $this->model->countTotalAbuseUser();

					$report = array(
										'TotalImage'=>$TotalImage,
										'TotalAbuseImage'=>$TotalAbuseImage,
										"TotalVideo"=>$TotalVideo,
										"TotalAbuseVideo"=>$TotalAbuseVideo,
										'TotalPost'=> $TotalPost,
										'TotalAbusePost'=> $TotalAbusePost,
										'TotalUser'=> $TotalNormalUser + $TotalSpecialUser,
										'TotalAbuseUser'=> $TotalAbuseUser,
										'TotalSpecialUser'=> $TotalSpecialUser,
										'TotalNormalUser'=> $TotalNormalUser
									);

					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'Report'=> $report
											));
					return;

		}


		/*	Method Make user's profile as special
			Created By: Manzz Baria
		*/
		public function makeVerifiedProfile(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['UserId'])){
						$UserId = $_POST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->makeVerifiedProfile($UserId);
					if (!$isSuccess) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}

		public function sendActive($userName,$userId,$accessToken,$email){
			$this->load->model("ApiUserModel","model");
			$userName = "مرحبا";
			$isSuccess = $this->model->sendActivationEmail($userName,$userId,$accessToken,$email);
		}

		public function sendForget($email){
			$this->load->model("ApiUserModel","model");
			$isSuccess = $this->model->forgetPassword($email);
		}

		/*Method Forget password (send email for reset password)
			Created By: Manzz Baria
		*/
		public function forgetPassword(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['Email'])){
						$Email = $_POST['Email'];
					}else{
						$this->showdefaultMessage("Email is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->forgetPassword($Email);
					if (!$isSuccess) {
						$this->showdefaultMessage("Email address is not registered yet");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Email has been sent to your email address, please check you email for reset password'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}

		/*Method Reset password
			Created By: Manzz Baria
		*/
		public function resetPassword(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['validationToken'])){
						$validationToken = $_POST['validationToken'];
					}else{
						$this->showdefaultMessage("validationToken is required");
						return;
					}
					if (!empty($_POST['accessToken'])){
						$accessToken = $_POST['accessToken'];
					}else{
						$this->showdefaultMessage("accessToken is required");
						return;
					}
					if (!empty($_POST['newPassword'])){
						$newPassword = $_POST['newPassword'];
					}else{
						$this->showdefaultMessage("newPassword is required");
						return;
					}

					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->resetPassword($validationToken,$accessToken,$newPassword);
					if (!$isSuccess) {
						$this->showdefaultMessage("Session expired");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Your account successfully activated'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}


		/*Method Active holynet account
			Created By: Manzz Baria
		*/
		public function activeAccount(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['validationToken'])){
						$validationToken = $_POST['validationToken'];
					}else{
						$this->showdefaultMessage("validationToken is required");
						return;
					}
					if (!empty($_POST['accessToken'])){
						$accessToken = $_POST['accessToken'];
					}else{
						$this->showdefaultMessage("accessToken is required");
						return;
					}

					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->activeAccount($validationToken,$accessToken);
					if (!$isSuccess) {
						$this->showdefaultMessage("Session expired");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Your password successfully changed'));
			}else{
					$this->showdefaultMessage("Please use POST method");
				}
		}

		/*	Method remove user's from special and make it as normal profile
			Created By: Manzz Baria
		*/
		public function removeVerifledProfile(){
			$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['UserId'])){
						$UserId = $_POST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}
					$this->load->model("ApiUserModel","model");
					$isSuccess = $this->model->removeVerifledProfile($UserId);
					if (!$isSuccess) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful'));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}

	/*	Method add post
			Created By: Manzz Baria
	*/
	public function addPost(){
		$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					if (!empty($_POST['Description'])){
						$description = $_POST['Description'];
					}else{
						$this->showdefaultMessage("Description is required");
						return;
					}
					if (!empty($_POST['Privacy'])){
						$privacy = $_POST['Privacy'];
					}else{
						$this->showdefaultMessage("Privacy is required");
						return;
					}

					if (isset($_POST['isCommentable'])){
						$isCommentable = $_POST['isCommentable'];
					}else{
						$this->showdefaultMessage("isCommentable is required");
						return;
					}
					if (!empty($_POST['UserId'])){
						$userId = $_POST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$sharedIds = null;
					if (!empty($_POST['SharedIds'])){
						$sharedIds = $_POST['SharedIds'];
					}

					$this->load->model("ApiContentModel","model");
					$isSuccess = $this->model->addPost($description,$privacy,$isCommentable,$userId,$sharedIds);
					if ($isSuccess == null) {
						$this->showdefaultMessage("Error at server side, please try again later");
						return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful',"Data"=>$isSuccess));
			}else{
					$this->showdefaultMessage("Please use POST method");

				}

		}


/*	Method update post
			Created By: Manzz Baria
	*/
	public function updatePost(){
		$data=json_decode(file_get_contents('php://input'));
			if($this->input->post()){

					$description = ''; $privacy =-1; $isCommentable=-1; $SharedIds ='';
					if (!empty($_REQUEST['Description'])){
						$description = $_REQUEST['Description'];
					}

					if (!empty($_REQUEST['Privacy'])){
						$privacy = $_REQUEST['Privacy'];
					}

					if (isset($_REQUEST['isCommentable'])){
						$isCommentable = $_REQUEST['isCommentable'];
					}

					if (!empty($_REQUEST['SharedIds'])){
						$SharedIds = $_REQUEST['SharedIds'];
					}

					if (!empty($_REQUEST['UserId'])){
						$userId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}
					if (!empty($_REQUEST['PostId'])){
						$postId = $_REQUEST['PostId'];
					}else{
						$this->showdefaultMessage("PostId is required");
						return;
					}

					if ($description != '' || $privacy != -1 || $isCommentable != -1 || $SharedIds != '') {

						$this->load->model("ApiPostModel","model");
						$isSuccess = $this->model->updatePost($description,$privacy,$isCommentable,$SharedIds,$userId,$postId);
						if ($isSuccess == null) {
							$this->showdefaultMessage("Access denied");
							return;

						}
						echo json_encode(array('Status'=>1,'Message'=>'Successful','Data'=>$isSuccess));
						return;

					}else{
							$this->showdefaultMessage("Please provide atleast one parameter for update post");
							return;
					}



			}else{
					$this->showdefaultMessage("Please use POST method");
					return;

				}

		}

		/*	Method to search users
			Created By: Manzz Baria
		*/
		public function searchUsers(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0; $isVerified = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}
					// $isSpecial = -1 for both , 0 for normal and 1 for special
					if (!empty($_REQUEST['IsVerified']))
					{
						$isVerified = $_REQUEST['IsVerified'];
					}
					$lang = 'en';$Keyword = '';$Email='';$MobileNo='';$CountryId=-1;

					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}

					if (!empty($_REQUEST['Keyword'])){
						$Keyword = $_REQUEST['Keyword'];
					}
					if (!empty($_REQUEST['Email'])){
						$Email = $_REQUEST['Email'];
					}
					if (!empty($_REQUEST['MobileNo'])){
						$MobileNo = $_REQUEST['MobileNo'];
					}
					if (!empty($_REQUEST['CountryId'])){
						$CountryId = $_REQUEST['CountryId'];
					}


					$this->load->model("ApiUserModel","model");
					$userArray = $this->model->getSearchUsers($PageIndex,$lang,$isVerified,$Keyword,$Email,$MobileNo,$CountryId);
					if ($userArray == null) {
						$this->showdefaultMessage("No records found");
						return;
					}
					$totalPage = 0;
					$totalPage = $this->model->getTotalPageOfSearchUsers($isVerified,$Keyword, $Email,$MobileNo,$CountryId);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage'=>$totalPage,
											"CurrentPage"=>$PageIndex+1,
											'Data'=> $userArray));
					return;

		}


		/*	Method to get Post List
			Created By: Manzz Baria
		*/
		public function getPostList(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}

					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$this->load->model("ApiPostModel","model");
					$postArray = $this->model->getPostList($PageIndex,$UserId);
					if ($postArray == null) {
						$this->showdefaultMessage("No records found");
						return;
					}
					$totalPage = 0;
					$totalPage = $this->model->getTotalPageOfPostList($UserId);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage'=>$totalPage,
											"CurrentPage"=>$PageIndex+1,
											'Data'=> $postArray));
					return;

		}
		/*	Method to get Post details
			Created By: Manzz Baria
		*/
		public function getPostDetails(){
$data=json_decode(file_get_contents('php://input'));
					if (!empty($_REQUEST['PostId'])){
						$PostId = $_REQUEST['PostId'];
					}else{
						$this->showdefaultMessage("PostId is required");
						return;
					}
					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$this->load->model("ApiPostModel","model");
					$postArray = $this->model->getPostDetails($PostId,$UserId);
					if ($postArray == null) {
						$this->showdefaultMessage("No records found");
						return;
					}
					$postComments = null;$PageIndex = 0;$type = 3;
					$this->load->model("ApiCommentModel","cmtmodel");
					$postComments = $this->cmtmodel->getCommentList($PostId,$type,$PageIndex);

					$this->load->model("ApiAbuseModel","abmodel");
					$abuseComments = $this->abmodel->getAbuseReportList($PostId,$type,$PageIndex);

					$totalPage = 0;
					$totalPage = $this->model->getTotalPageOfPostList($UserId);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'Data'=> $postArray,
											'Comments'=>$postComments,
											'AbuseReport'=>$abuseComments
											));
					return;

		}

		/*	Method to get comment list
			Created By: Manzz Baria
		*/
		public function getCommentList(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}

					if (!empty($_REQUEST['fileId'])){
						$fileId = $_REQUEST['fileId'];
					}else{
						$this->showdefaultMessage("fileId is required");
						return;
					}
					if (!empty($_REQUEST['type'])){
						$type = $_REQUEST['type'];
					}else{
						$this->showdefaultMessage("type is required");
						return;
					}

					$postComments = null;
					$this->load->model("ApiCommentModel","cmtmodel");
					$postComments = $this->cmtmodel->getCommentList($fileId,$type,$PageIndex);
					$totalPage = $this->cmtmodel->getTotalPageOfCommentList($fileId,$type);
					$currentPage = $PageIndex+1;
					if ($postComments == null) {
						$this->showdefaultMessage("No records found");
						return;
					}
					$totalPage = 0;
					$totalPage = $this->cmtmodel->getTotalPageOfCommentList($fileId,$type);
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage' => $totalPage,
											'CurrentPage' => $currentPage,
											'Comments'=>$postComments
											));
					return;

		}


	/*	Method to delete profile type
			Created By: Manzz Baria
		*/
			public function deleteProfileType(){

$data=json_decode(file_get_contents('php://input'));
				if (!empty($_REQUEST['ProfileId'])){
					$ProfileId = $_REQUEST['ProfileId'];
				}else{
						$this->showdefaultMessage("ProfileId is required");
						return;
				}

				$this->load->model("ApiProfileModel","prflmodel");
				$isDeleted = $this->prflmodel->deleteProfileType($ProfileId);
				if (!$isDeleted) {
							$this->showdefaultMessage("Error at server side");
							return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful'));
					return;

			}
	/*	Method to delete video type
			Created By: Manzz Baria
		*/
			public function deleteVideoType(){

$data=json_decode(file_get_contents('php://input'));
				if (!empty($_REQUEST['VideoTypeId'])){
					$VideoTypeId = $_REQUEST['VideoTypeId'];
				}else{
						$this->showdefaultMessage("VideoTypeId is required");
						return;
				}

				$this->load->model("ApiVideoModel","vmodel");
				$isDeleted = $this->vmodel->deleteVideoType($VideoTypeId);
				if (!$isDeleted) {
							$this->showdefaultMessage("Error at server side");
							return;

					}
					echo json_encode(array('Status'=>1,'Message'=>'Successful'));
					return;

			}


		/*	Method to Upload file (upload image / video)
			Created By: Manzz Baria
		*/
		public function uploadTestFile(){
				$data=json_decode(file_get_contents('php://input'));
				$data ='';
				$Keyword = '';
				if (!empty($_REQUEST['Type'])){
					$Type = $_REQUEST['Type'];
				}else{
					$this->showdefaultMessage("Type is required");
					return;
				}
				if (!empty($_REQUEST['Title'])){
					$Title = $_REQUEST['Title'];
				}else{
					$this->showdefaultMessage("Title is required");
					return;
				}
				if (!empty($_REQUEST['Keyword'])){
					$Keyword = $_REQUEST['Keyword'];
				}else{
					//$this->showdefaultMessage("Keyword is required");
					//return;
				}
				if (!empty($_REQUEST['UserId'])){
					$UserId = $_REQUEST['UserId'];
				}else{
					$this->showdefaultMessage("UserId is required");
					return;
				}

				$Privacy = 1;
				$IsCommentable = 0;
				$TransLanguageId = 0;
				$TransTitle ='';
				$TransDescription ='';
				$SharedIds = '';
				$Duration ='';
				$PlayListName =null;
				$PlayListId =0;

				if (isset($_REQUEST['SharedIds'])){
						$SharedIds = $_REQUEST['SharedIds'];
				}

				if (isset($_REQUEST['TransLanguageId'])){
						$TransLanguageId = $_REQUEST['TransLanguageId'];
					}
					if (isset($_REQUEST['TransTitle'])){
							$TransTitle = $_REQUEST['TransTitle'];
					}
					if (isset($_REQUEST['TransDescription'])){
							$TransDescription = $_REQUEST['TransDescription'];
					}

					if (isset($_REQUEST['IsCommentable'])){
						$IsCommentable = (int)$_REQUEST['IsCommentable'];
					}


				if($Type == VIDEO_TYPE)
				{
					$Privacy = 0;
					$Description = "";
					$VideoTypeId = 0;
					$PlayListName = "";
					$PlayListId = 0;
					// Video
					if (isset($_REQUEST['Privacy'])){
						$Privacy = $_REQUEST['Privacy'];
					}
					if (isset($_REQUEST['Description'])){
						$Description = $_REQUEST['Description'];
					}else{
						//$this->showdefaultMessage("Description is required");
						//return;
					}
					if (isset($_REQUEST['VideoTypeId'])){
						$VideoTypeId = $_REQUEST['VideoTypeId'];
					}else{
						//$this->showdefaultMessage("VideoTypeId is required");
						//return;
					}



					if (isset($_REQUEST['PlayListName'])){
						$PlayListName = $_REQUEST['PlayListName'];
					}
					if (isset($_REQUEST['PlayListId'])){
						$PlayListId = $_REQUEST['PlayListId'];
					}

					if ($TransLanguageId != 0) {
						if ($TransTitle == '' && $TransDescription == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
					if ($PlayListName != null && $PlayListId == 0) {
						// create playlist and get PlayListId
						$this->load->model("ApiPlaylistModel","playmodel");
						$isExist = $this->playmodel->checkExistingPlaylist($PlayListName,$UserId);
						if ($isExist) {
							echo json_encode(array('Status'=>0,'Message'=>'play list already added!','Data'=> null));
							return;
						}else{
							$PlayListId = $this->playmodel->createPlayList($PlayListName,$UserId);

						}
					}
				}
				if($Type == IMAGE_TYPE){
					if ($TransLanguageId != 0) {
						if ($TransTitle == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
				}


				if(isset($_FILES['file']['name']))
				{
					$file=$_FILES['file']['name'];

					/* Uploading Content */
					$time = time();
					$new_name = 'holynet_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 5400;
					$config['max_height']           = 5400;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('file'))
					{
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						echo json_encode(array('Status'=>0,'Message'=>$msg,'Data'=> null));

					}
					else
					{
						$image_path='./uploads/files/';
						$thumb_path='./uploads/files/thumb/';
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					$filename=$data['upload_data']['file_name'];

					/* Create Thumbnail according to media type */

					$videofile=$_FILES["file"]["tmp_name"];
					$image="";
					$image.="c:/xampp/htdocs/holynet/uploads/files/thumb/";
					$image_path='/uploads/files/';
					$thumb_path='/uploads/files/thumb/';

					$s3URL ='';
					$s3Thumb ='';

					if($Type == 1)
					{
						// Videos create thumbnail

						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'];//.'/assets/thumb/'.$filevideo;
						$config['maintain_ratio'] = FALSE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$imagerand = $time;
						$imagefile = "";
						$imagefile.= "holynet_";
						$imagefile.=  $imagerand;
						$imagefile.=  ".jpg";

						$image.= "holynet_";
						$image.=  $imagerand;
						$image.=  ".jpg";
						$ffmpeg=  "c:\\ffmpeg\\bin\\ffmpeg";
						//$videofile=$_FILES["file"]["tmp_name"];
						$size= "375*250";
						for($num =1;$num<=3;$num++)
						{
							$getsecond=$num*2;
						}
						$cmd="$ffmpeg -i $videofile -an -ss $getsecond -s $size $image";

						/* duration generater*/
						$getID3 = new getID3;
						$file = $getID3->analyze($videofile);
						$Duration=$file['playtime_string'];

						if(!shell_exec($cmd))
						{
						}
						else
						{

						}

						$s3URL = 'Holynet/Videos/'.$filename;
						$s3Thumb = 'Holynet/Videos/Thumb/'.$imagefile;

					}else
					{
						// Images create thumbnail

						$imagefile = $filename;
						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
						$config['maintain_ratio'] = FALSE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$config['width'] = 200;
						$config['height'] = 200;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$image = $image.$imagefile;
						$s3URL = 'Holynet/Images/'.$filename;
						$s3Thumb = 'Holynet/Images/Thumb/'.$imagefile;

					}

					$fileUrl = $filename;
					$thumbUrl = $imagefile;

					//Amazon s3 server upload video
					$this->load->library('s3');

					if ($this->s3->putObject($this->s3->inputFile($videofile), BUCKET, $s3URL))
					{

						if (file_exists($image))
						{
								$this->s3->putObject($this->s3->inputFile($image), BUCKET, $s3Thumb);
						}

						// get file size
						$info = $this->s3->getObjectInfo(BUCKET, $s3URL);
						$size = $info['size'];

						// Save file details in database
						$FileUrl = $image_path.$filename;
						$FileThumb = $thumb_path.$imagefile;

						if ($Type == VIDEO_TYPE) {
							// video save in database
							$this->load->model("ApiVideoModel","vmodel");
							$fileId = $this->vmodel->saveVideo($Title,$Description,$thumbUrl,$fileUrl,$Privacy,$size,$Duration,$Keyword,$VideoTypeId,$IsCommentable,$PlayListId,$UserId,$SharedIds);

							// save translate title and description in database
							//echo "save video";
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(VIDEO_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->vmodel->getVideoDetails($fileId,'en',$UserId);
						}else if ($Type == IMAGE_TYPE) {
							// image save in database
							//$IsCommentable = 0;
							$this->load->model("ApiImageModel","imodel");
							$fileId = $this->imodel->saveImage($Title,$thumbUrl,$fileUrl,$Privacy,$size,$Keyword,$IsCommentable,$UserId,$SharedIds);
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(IMAGE_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->imodel->getImageDetails($fileId,'en',$UserId);
						}

						$this->load->helper("file");
						$pathVID = "c:/xampp/htdocs/holynet".$FileUrl;
						$pathThumb = "c:/xampp/htdocs/holynet".$FileThumb;
	echo json_encode(array('Status'=>1,'Message'=>'File successfully uploaded','Data'=>$data));
						//Delete file from local server
							try {

							 	if (file_exists($pathVID))
							 	{
							 		unlink($pathVID);
							 	}

									if (file_exists($pathThumb))
								 	{
								 		unlink($pathThumb);
								 	}


							} catch( Exception $e)
							{

							}


					} else {

						echo json_encode(array('Status'=>0,'Message'=>'File error to uploading'));
					}

				}else
				{
					$this->showdefaultMessage("Please provide valid file path");
					return;

				}

		}


		/*	Method to Upload file (upload image / video)
			Created By: Manzz Baria
		*/
		public function uploadFileThumb(){
				$data=json_decode(file_get_contents('php://input'));
				$data ='';
				$Keyword = '';
				if (!empty($_REQUEST['Type'])){
					$Type = $_REQUEST['Type'];
				}else{
					$this->showdefaultMessage("Type is required");
					return;
				}
				if (!empty($_REQUEST['Title'])){
					$Title = $_REQUEST['Title'];
				}else{
					$this->showdefaultMessage("Title is required");
					return;
				}
				if (!empty($_REQUEST['Keyword'])){
					$Keyword = $_REQUEST['Keyword'];
				}else{
					//$this->showdefaultMessage("Keyword is required");
					//return;
				}
				if (!empty($_REQUEST['UserId'])){
					$UserId = $_REQUEST['UserId'];
				}else{
					$this->showdefaultMessage("UserId is required");
					return;
				}

				$Privacy = 1;
				$IsCommentable = 0;
				$TransLanguageId = 0;
				$TransTitle ='';
				$TransDescription ='';
				$SharedIds = '';
				$Duration ='';
				$PlayListName =null;
				$PlayListId =0;

				if (isset($_REQUEST['SharedIds'])){
						$SharedIds = $_REQUEST['SharedIds'];
				}

				if (isset($_REQUEST['TransLanguageId'])){
						$TransLanguageId = $_REQUEST['TransLanguageId'];
					}
					if (isset($_REQUEST['TransTitle'])){
							$TransTitle = $_REQUEST['TransTitle'];
					}
					if (isset($_REQUEST['TransDescription'])){
							$TransDescription = $_REQUEST['TransDescription'];
					}

					if (isset($_REQUEST['IsCommentable'])){
						$IsCommentable = (int)$_REQUEST['IsCommentable'];
					}


				if($Type == VIDEO_TYPE)
				{
					$Privacy = 0;
					$Description = "";
					$VideoTypeId = 0;
					$PlayListName = "";
					$PlayListId = 0;
					// Video
					if (isset($_REQUEST['Privacy'])){
						$Privacy = $_REQUEST['Privacy'];
					}
					if (isset($_REQUEST['Description'])){
						$Description = $_REQUEST['Description'];
					}else{
						//$this->showdefaultMessage("Description is required");
						//return;
					}
					if (isset($_REQUEST['VideoTypeId'])){
						$VideoTypeId = $_REQUEST['VideoTypeId'];
					}else{
						//$this->showdefaultMessage("VideoTypeId is required");
						//return;
					}



					if (isset($_REQUEST['PlayListName'])){
						$PlayListName = $_REQUEST['PlayListName'];
					}
					if (isset($_REQUEST['PlayListId'])){
						$PlayListId = $_REQUEST['PlayListId'];
					}

					if ($TransLanguageId != 0) {
						if ($TransTitle == '' && $TransDescription == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
					if ($PlayListName != null && $PlayListId == 0) {
						// create playlist and get PlayListId
						$this->load->model("ApiPlaylistModel","playmodel");
						$isExist = $this->playmodel->checkExistingPlaylist($PlayListName,$UserId);
						if ($isExist) {
							echo json_encode(array('Status'=>0,'Message'=>'play list already added!','Data'=> null));
							return;
						}else{
							$PlayListId = $this->playmodel->createPlayList($PlayListName,$UserId);

						}
					}
				}
				if($Type == IMAGE_TYPE){
					if ($TransLanguageId != 0) {
						if ($TransTitle == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
				}


				if(isset($_FILES['file']['name']))
				{
					$file=$_FILES['file']['name'];

					/* Uploading Content */
					$time = time();
					$new_name = 'holynet_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 5400;
					$config['max_height']           = 5400;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('file'))
					{
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						echo json_encode(array('Status'=>0,'Message'=>$msg,'Data'=> null));

					}
					else
					{
						$image_path='./uploads/files/';
						$thumb_path='./uploads/files/thumb/';
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					$filename=$data['upload_data']['file_name'];

					/* Create Thumbnail according to media type */

					$videofile=$_FILES["file"]["tmp_name"];
					$image="";
					$image.="c:/xampp/htdocs/holynet/uploads/files/thumb/";
					$image_path='/uploads/files/';
					$thumb_path='/uploads/files/thumb/';

					$s3URL ='';
					$s3Thumb ='';

					if($Type == 1)
					{
						// Videos create thumbnail

						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'];//.'/assets/thumb/'.$filevideo;
						$config['maintain_ratio'] = FALSE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$imagerand = $time;
						$imagefile = "";
						$imagefile.= "holynet_";
						$imagefile.=  $imagerand;
						$imagefile.=  ".jpg";

						$image.= "holynet_";
						$image.=  $imagerand;
						$image.=  ".jpg";
						$ffmpeg=  "c:\\ffmpeg\\bin\\ffmpeg";
						//$videofile=$_FILES["file"]["tmp_name"];
						$size= "375*250";
						for($num =1;$num<=3;$num++)
						{
							$getsecond=$num*2;
						}
						$cmd="$ffmpeg -i $videofile -an -ss $getsecond -s $size $image";

						/* duration generater*/
						$getID3 = new getID3;
						$file = $getID3->analyze($videofile);
						$Duration=$file['playtime_string'];

						if(!shell_exec($cmd))
						{
						}
						else
						{

						}

						$s3URL = 'Holynet/Videos/'.$filename;
						$s3Thumb = 'Holynet/Videos/Thumb/'.$imagefile;

					}else
					{
						// Images create thumbnail

						$imagefile = $filename;
						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
						$config['maintain_ratio'] = TRUE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$config['width'] = 375;
						$config['height'] = 250;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$image = $image.$imagefile;
						$s3URL = 'Holynet/Images/'.$filename;
						$s3Thumb = 'Holynet/Images/Thumb/'.$imagefile;

					}

					$fileUrl = $filename;
					$thumbUrl = $imagefile;

					//Amazon s3 server upload video
					$this->load->library('s3');

					if ($this->s3->putObject($this->s3->inputFile($videofile), BUCKET, $s3URL))
					{

						if (file_exists($image))
						{
								$this->s3->putObject($this->s3->inputFile($image), BUCKET, $s3Thumb);
						}

						// get file size
						$info = $this->s3->getObjectInfo(BUCKET, $s3URL);
						$size = $info['size'];

						// Save file details in database
						$FileUrl = $image_path.$filename;
						$FileThumb = $thumb_path.$imagefile;

						if ($Type == VIDEO_TYPE) {
							// video save in database
							$this->load->model("ApiVideoModel","vmodel");
							$fileId = $this->vmodel->saveVideo($Title,$Description,$thumbUrl,$fileUrl,$Privacy,$size,$Duration,$Keyword,$VideoTypeId,$IsCommentable,$PlayListId,$UserId,$SharedIds);

							// save translate title and description in database
							//echo "save video";
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(VIDEO_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->vmodel->getVideoDetails($fileId,'en',$UserId);
						}else if ($Type == IMAGE_TYPE) {
							// image save in database
							//$IsCommentable = 0;
							$this->load->model("ApiImageModel","imodel");
							$fileId = $this->imodel->saveImage($Title,$thumbUrl,$fileUrl,$Privacy,$size,$Keyword,$IsCommentable,$UserId,$SharedIds);
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(IMAGE_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->imodel->getImageDetails($fileId,'en',$UserId);
						}

						$this->load->helper("file");
						$pathVID = "c:/xampp/htdocs/holynet".$FileUrl;
						$pathThumb = "c:/xampp/htdocs/holynet".$FileThumb;
		echo json_encode(array('Status'=>1,'Message'=>'File successfully uploaded','Data'=>$data));
						//Delete file from local server
							try {

								if (file_exists($pathVID))
								{
									unlink($pathVID);
								}

									if (file_exists($pathThumb))
									{
										unlink($pathThumb);
									}


							} catch( Exception $e)
							{

							}


					} else {

						echo json_encode(array('Status'=>0,'Message'=>'File error to uploading'));
					}

				}else
				{
					$this->showdefaultMessage("Please provide valid file path");
					return;

				}

		}

		/*	Method to Upload file (upload image / video)
			Created By: Manzz Baria
		*/
		public function uploadFile(){
				$data=json_decode(file_get_contents('php://input'));
				$data ='';
				$Keyword = '';
				if (!empty($_REQUEST['Type'])){
					$Type = $_REQUEST['Type'];
				}else{
					$this->showdefaultMessage("Type is required");
					return;
				}
				if (!empty($_REQUEST['Title'])){
					$Title = $_REQUEST['Title'];
				}else{
					$this->showdefaultMessage("Title is required");
					return;
				}
				if (!empty($_REQUEST['Keyword'])){
					$Keyword = $_REQUEST['Keyword'];
				}else{
					//$this->showdefaultMessage("Keyword is required");
					//return;
				}
				if (!empty($_REQUEST['UserId'])){
					$UserId = $_REQUEST['UserId'];
				}else{
					$this->showdefaultMessage("UserId is required");
					return;
				}

				$Privacy = 1;
				$IsCommentable = 0;
				$TransLanguageId = 0;
				$TransTitle ='';
				$TransDescription ='';
				$SharedIds = '';
				$Duration ='';
				$PlayListName =null;
				$PlayListId =0;

				if (isset($_REQUEST['SharedIds'])){
						$SharedIds = $_REQUEST['SharedIds'];
				}

				if (isset($_REQUEST['TransLanguageId'])){
						$TransLanguageId = $_REQUEST['TransLanguageId'];
					}
					if (isset($_REQUEST['TransTitle'])){
							$TransTitle = $_REQUEST['TransTitle'];
					}
					if (isset($_REQUEST['TransDescription'])){
							$TransDescription = $_REQUEST['TransDescription'];
					}

					if (isset($_REQUEST['IsCommentable'])){
						$IsCommentable = (int)$_REQUEST['IsCommentable'];
					}


				if($Type == VIDEO_TYPE)
				{
					$Privacy = 0;
					$Description = "";
					$VideoTypeId = 0;
					$PlayListName = "";
					$PlayListId = 0;
					// Video
					if (isset($_REQUEST['Privacy'])){
						$Privacy = $_REQUEST['Privacy'];
					}
					if (isset($_REQUEST['Description'])){
						$Description = $_REQUEST['Description'];
					}else{
						//$this->showdefaultMessage("Description is required");
						//return;
					}
					if (isset($_REQUEST['VideoTypeId'])){
						$VideoTypeId = $_REQUEST['VideoTypeId'];
					}else{
						//$this->showdefaultMessage("VideoTypeId is required");
						//return;
					}



					if (isset($_REQUEST['PlayListName'])){
						$PlayListName = $_REQUEST['PlayListName'];
					}
					if (isset($_REQUEST['PlayListId'])){
						$PlayListId = $_REQUEST['PlayListId'];
					}

					if ($TransLanguageId != 0) {
						if ($TransTitle == '' && $TransDescription == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
					if ($PlayListName != null && $PlayListId == 0) {
						// create playlist and get PlayListId
						$this->load->model("ApiPlaylistModel","playmodel");
						$isExist = $this->playmodel->checkExistingPlaylist($PlayListName,$UserId);
						if ($isExist) {
							echo json_encode(array('Status'=>0,'Message'=>'play list already added!','Data'=> null));
							return;
						}else{
							$PlayListId = $this->playmodel->createPlayList($PlayListName,$UserId);

						}
					}
				}
				if($Type == IMAGE_TYPE){
					if ($TransLanguageId != 0) {
						if ($TransTitle == '') {
							$this->showdefaultMessage("Translation is required");
							return;
						}
					}
				}


				if(isset($_FILES['file']['name']))
				{
					$file=$_FILES['file']['name'];

					/* Uploading Content */
					$time = time();
					$new_name = 'holynet_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 5400;
					$config['max_height']           = 5400;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('file'))
					{
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						echo json_encode(array('Status'=>0,'Message'=>$msg,'Data'=> null));

					}
					else
					{
						$image_path='./uploads/files/';
						$thumb_path='./uploads/files/thumb/';
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					$filename=$data['upload_data']['file_name'];

					/* Create Thumbnail according to media type */

					$videofile=$_FILES["file"]["tmp_name"];
					$image="";
					$image.="c:/xampp/htdocs/holynet/uploads/files/thumb/";
					$image_path='/uploads/files/';
					$thumb_path='/uploads/files/thumb/';

					$s3URL ='';
					$s3Thumb ='';

					if($Type == 1)
					{
						// Videos create thumbnail

						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'];//.'/assets/thumb/'.$filevideo;
						$config['maintain_ratio'] = FALSE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$imagerand = $time;
						$imagefile = "";
						$imagefile.= "holynet_";
						$imagefile.=  $imagerand;
						$imagefile.=  ".jpg";

						$image.= "holynet_";
						$image.=  $imagerand;
						$image.=  ".jpg";
						$ffmpeg=  "c:\\ffmpeg\\bin\\ffmpeg";
						//$videofile=$_FILES["file"]["tmp_name"];
						$size= "375*250";
						for($num =1;$num<=3;$num++)
						{
							$getsecond=$num*2;
						}
						$cmd="$ffmpeg -i $videofile -an -ss $getsecond -s $size $image";

						/* duration generater*/
						$getID3 = new getID3;
						$file = $getID3->analyze($videofile);
						$Duration=$file['playtime_string'];

						if(!shell_exec($cmd))
						{
						}
						else
						{

						}

						$s3URL = 'Holynet/Videos/'.$filename;
						$s3Thumb = 'Holynet/Videos/Thumb/'.$imagefile;

					}else
					{
						// Images create thumbnail

						list($width, $height) = getimagesize($videofile);
						$thumbWidth = 0;
						$thumbHeight = 0;
						$check = "";
						if($width > $height){
							//Landscape
							//$check = "width".$width."_".$height;
							$thumbWidth = 375;
							$thumbHeight = 0;
						}else{

							//Potrait
							//$check = "height".$width."_".$height;
							$thumbWidth = 0;
							$thumbHeight = 350;
						}

						$imagefile = $filename;
						$config['image_library'] = 'gd2';
						$config['source_image'] =  $data['upload_data']['full_path'];
						$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
						$config['maintain_ratio'] = TRUE;
						$config['overwrite'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = 0;
						$config['width'] = $thumbWidth;
						$config['height'] = $thumbHeight;
						$this->load->library('image_lib', $config);
						if($this->image_lib->resize())
						{

						}
						else
						{

						}
						$image = $image.$imagefile;
						$s3URL = 'Holynet/Images/'.$filename;
						$s3Thumb = 'Holynet/Images/Thumb/'.$imagefile;

					}

					$fileUrl = $filename;
					$thumbUrl = $imagefile;

					//Amazon s3 server upload video
					$this->load->library('s3');

					if ($this->s3->putObject($this->s3->inputFile($videofile), BUCKET, $s3URL))
					{

						if (file_exists($image))
						{
								$this->s3->putObject($this->s3->inputFile($image), BUCKET, $s3Thumb);
						}

						// get file size
						$info = $this->s3->getObjectInfo(BUCKET, $s3URL);
						$size = $info['size'];

						// Save file details in database
						$FileUrl = $image_path.$filename;
						$FileThumb = $thumb_path.$imagefile;

						if ($Type == VIDEO_TYPE) {
							// video save in database
							$this->load->model("ApiVideoModel","vmodel");
							$fileId = $this->vmodel->saveVideo($Title,$Description,$thumbUrl,$fileUrl,$Privacy,$size,$Duration,$Keyword,$VideoTypeId,$IsCommentable,$PlayListId,$UserId,$SharedIds);

							// save translate title and description in database
							//echo "save video";
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(VIDEO_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->vmodel->getVideoDetails($fileId,'en',$UserId);
						}else if ($Type == IMAGE_TYPE) {
							// image save in database
							//$IsCommentable = 0;
							$this->load->model("ApiImageModel","imodel");
							$fileId = $this->imodel->saveImage($Title,$thumbUrl,$fileUrl,$Privacy,$size,$Keyword,$IsCommentable,$UserId,$SharedIds);
							if ($TransLanguageId != 0) {
								$this->load->model("ApiTranslateModel","transmodel");
								$result = $this->transmodel->saveTranslate(IMAGE_TYPE,$fileId,$TransLanguageId,$TransTitle,$TransDescription);
							}
							$data = $this->imodel->getImageDetails($fileId,'en',$UserId);
						}

						$this->load->helper("file");
						$pathVID = "c:/xampp/htdocs/holynet".$FileUrl;
						$pathThumb = "c:/xampp/htdocs/holynet".$FileThumb;
		echo json_encode(array('Status'=>1,'Message'=>'File successfully uploaded','Data'=>$data));
						//Delete file from local server
							try {

								if (file_exists($pathVID))
								{
									unlink($pathVID);
								}

									if (file_exists($pathThumb))
									{
										unlink($pathThumb);
									}


							} catch( Exception $e)
							{

							}


					} else {

						echo json_encode(array('Status'=>0,'Message'=>'File error to uploading'));
					}

				}else
				{
					$this->showdefaultMessage("Please provide valid file path");
					return;

				}

		}


/*	Method to get Resource List (videos / Images)
	Created By: Manzz Baria
*/
	public function getResourceList(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}
					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}
					if (!empty($_REQUEST['Type'])){
						$Type = $_REQUEST['Type'];
					}else{
						$this->showdefaultMessage("Type is required");
						return;
					}

					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$myUserId = 0;
					if (!empty($_REQUEST['myUserId'])){
						$myUserId = $_REQUEST['myUserId'];
					}

					$Array = null;
					$totalPage = 0;


					if ($Type == 1) {
						$this->load->model("ApiVideoModel","vmodel");
						$Array = $this->vmodel->getVideoList($PageIndex,$UserId,$lang,$myUserId);
						$totalPage = $this->vmodel->getTotalPageOfVideoList($UserId);
					}else if($Type == 2){
						$this->load->model("ApiImageModel","imodel");
						$Array = $this->imodel->getImageList($PageIndex,$UserId,$lang,$myUserId);
						$totalPage = $this->imodel->getTotalPageOfImageList($UserId);
					}
					if ($Array == null) {
						$this->showdefaultMessage("No records found");
						return;
					}

					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage'=>$totalPage,
											"CurrentPage"=>$PageIndex+1,
											'Data'=> $Array));
					return;

	}

		/*	Method to get resource details
			Created By: Manzz Baria
		*/
		public function getResourceDetails(){
$data=json_decode(file_get_contents('php://input'));
					if (!empty($_REQUEST['ResourceId'])){
						$ResourceId = $_REQUEST['ResourceId'];
					}else{
						$this->showdefaultMessage("ResourceId is required");
						return;
					}
					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}

					if (!empty($_REQUEST['Type'])){
						$Type = $_REQUEST['Type'];
					}else{
						$this->showdefaultMessage("Type is required");
						return;
					}

					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex'])){
						$PageIndex = (int)$_REQUEST['PageIndex'];
					}

					$ArrayDetails = null;
					$ArrayComments = null;
					$ArrayAbuseReport = null;


					if ($Type == VIDEO_TYPE) {

						$this->load->model("ApiVideoModel","vmodel");
						$ArrayDetails = $this->vmodel->getVideoDetails($ResourceId,$lang,$UserId);

					}else if($Type == IMAGE_TYPE){

						$this->load->model("ApiImageModel","imodel");
						$ArrayDetails = $this->imodel->getImageDetails($ResourceId,$lang,$UserId);

					}else if($Type == POST_TYPE){

						$this->load->model("ApiPostModel","pmodel");
						$ArrayDetails = $this->pmodel->getPostDetails($ResourceId,$UserId);

					}

					$this->load->model("ApiCommentModel","cmtmodel");
					$ArrayComments = $this->cmtmodel->getCommentList($ResourceId,$Type,$PageIndex);
					$totalCommentsPage = $this->cmtmodel->getTotalPageOfCommentList($ResourceId,$Type);
					$commentsObject = array('TotalPage'=>$totalCommentsPage,'CurrentPage'=>$PageIndex+1,'Comments'=>$ArrayComments);

					$this->load->model("ApiAbuseModel","abmodel");
					$ArrayAbuseReport = $this->abmodel->getAbuseReportList($ResourceId,$Type,$PageIndex);
					$totalAbusePage = $this->abmodel->getTotalAbuseReport($ResourceId,$Type);
					$abusesObject = array('TotalPage'=>$totalAbusePage,'CurrentPage'=>$PageIndex+1,'AbuseReport'=>$ArrayAbuseReport);


					if ($ArrayDetails == null) {
						$this->showdefaultMessage("No records found");
						return;
					}

					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'Data'=> $ArrayDetails,
											'Comments'=>$commentsObject,
											'AbuseReport'=>$abusesObject
											));
					return;

		}

		/*	Method to delete resource
			Created By: Manzz Baria
		*/
		public function deleteResource(){
$data=json_decode(file_get_contents('php://input'));
					if (!empty($_REQUEST['ResourceId'])){
						$ResourceId = $_REQUEST['ResourceId'];
					}else{
						$this->showdefaultMessage("ResourceId is required");
						return;
					}
					if (!empty($_REQUEST['Type'])){
						$Type = $_REQUEST['Type'];
					}else{
						$this->showdefaultMessage("Type is required");
						return;
					}

					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$this->load->model("ApiContentModel","cmodel");
					$isAccess = $this->cmodel->isAccessResource($ResourceId,$Type,$UserId);
					if (!$isAccess) {
						$this->showdefaultMessage("Access denied");
						return;
					}
					$isDeleted = $this->cmodel->deleteResource($ResourceId,$Type,$UserId);
					if (!$isDeleted) {
						$this->showdefaultMessage("Unable to delete, try again later");
						return;
					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully deleted'));

		}

		/*	Method to update resource
			Created By: Manzz Baria
		*/
		public function updateResource(){
$data=json_decode(file_get_contents('php://input'));
					if (!empty($_REQUEST['Type'])){
						$Type = $_REQUEST['Type'];
					}else{
						$this->showdefaultMessage("Type is required");
						return;
					}
					if (!empty($_REQUEST['ResourceId'])){
						$ResourceId = $_REQUEST['ResourceId'];
					}else{
						$this->showdefaultMessage("ResourceId is required");
						return;
					}

					if (!empty($_REQUEST['UserId'])){
						$UserId = $_REQUEST['UserId'];
					}else{
						$this->showdefaultMessage("UserId is required");
						return;
					}

					$data = '';
					$title ='-1';
					$Keyword = '-1';
					$description ='-1';
					$videoTypeId =-1;
					$IsCommentable =-1;
					$playlistId =-1;
					$TransLanguageId =-1;
					$TransTitle ='-1';
					$TransDescription ='-1';
					$privacy = 1;
					$SharedIds = '-1';

					if (isset($_REQUEST['SharedIds'])){
						$SharedIds = $_REQUEST['SharedIds'];
					}

					if (isset($_REQUEST['Privacy'])){
						$privacy = $_REQUEST['Privacy'];
					}

					if (isset($_REQUEST['Title'])){
						$title = $_REQUEST['Title'];
					}
					if (isset($_REQUEST['Keyword'])){
						$Keyword = $_REQUEST['Keyword'];
					}
					if (isset($_REQUEST['Description'])){
						$description = $_REQUEST['Description'];
					}
					if (isset($_REQUEST['VideoTypeId'])){
						$videoTypeId = $_REQUEST['VideoTypeId'];
					}
					if (isset($_REQUEST['IsCommentable'])){
						$IsCommentable = (int)$_REQUEST['IsCommentable'];
					}

					if (isset($_REQUEST['PlayListId'])){
						$playlistId = $_REQUEST['PlayListId'];
					}
					if (isset($_REQUEST['TransLanguageId'])){
						$TransLanguageId = $_REQUEST['TransLanguageId'];
					}
					if (isset($_REQUEST['TransTitle'])){
							$TransTitle = $_REQUEST['TransTitle'];
					}
					if (isset($_REQUEST['TransDescription'])){
							$TransDescription = $_REQUEST['TransDescription'];
					}

					$this->load->model("ApiContentModel","cmodel");
					$isAccess = $this->cmodel->isAccessResource($ResourceId,$Type,$UserId);
					if (!$isAccess) {
						$this->showdefaultMessage("Access denied");
						return;
					}

					$this->load->model("ApiTranslateModel","transmodel");
					$this->transmodel->updateTranslate($Type,$ResourceId,$TransLanguageId,$TransTitle,$TransDescription);

					if ($Type == VIDEO_TYPE) {
						$this->load->model("ApiVideoModel","videomodel");
						$isAccess = $this->videomodel->updateVideo($title,$description,$Keyword,$IsCommentable,$videoTypeId,$playlistId,$UserId,$ResourceId,$SharedIds,$privacy);
						if (!$isAccess) {
							$this->showdefaultMessage("Access denied");
							return;
						}
						$data = $this->videomodel->getVideoDetails($ResourceId,'en',$UserId);
						echo json_encode(array('Status'=>1,'Message'=>'Successfully updated','Data'=>$data));
						return;

					}else if($Type == IMAGE_TYPE){
						$this->load->model("ApiImageModel","imagemodel");
						$isAccess = $this->imagemodel->updateImage($title,$Keyword,$IsCommentable,$UserId,$ResourceId,$SharedIds,$privacy);
						if (!$isAccess) {
							$this->showdefaultMessage("Access denied");
							return;
						}
						$data = $this->imagemodel->getImageDetails($ResourceId,'en',$UserId);
						echo json_encode(array('Status'=>1,'Message'=>'Successfully updated','Data'=>$data));
						return;
					}else if($Type == POST_TYPE){
						$this->load->model("ApiPostModel","postmodel");
						$isAccess = $this->postmodel->updatePost($description,$privacy,$IsCommentable,$SharedIds,$UserId,$ResourceId);
						if (!$isAccess) {
							$this->showdefaultMessage("Access denied");
							return;
						}
						$data = $this->postmodel->getPostDetails($ResourceId,$UserId);
						echo json_encode(array('Status'=>1,'Message'=>'Successfully updated','Data'=>$data));
						return;
					}


		}


			/*********** Language Api  ***********/

			/* Method to get Language list
				 Created By: Nishit Patel
			*/
			public function getLanguageList(){
				$data=json_decode(file_get_contents('php://input'));
				$this->load->model("ApiLanguageModel","model");
				$languageList = $this->model->getLanguages();
				if ($languageList == null) {
						$this->showdefaultMessage("Language data not found");
						return;
				}
				echo json_encode(array('Status'=>1,'Message'=>'Success','Data'=>$languageList));
			}
			/* Method to Add new language
					Created By: Nishit Patel
			*/
			public function addLanguage(){
$data=json_decode(file_get_contents('php://input'));
					$Name = "";
					if (!empty($_REQUEST['Name'])){
						$Name = $_REQUEST['Name'];
					}else{
						$this->showdefaultMessage("Please provide Language Title");
						return;
					}

					$this->load->model("ApiLanguageModel","model");
					$isExist = $this->model->checkIsLanguageExsist($Name);
					if ($isExist) {
						echo json_encode(array('Status'=>0,'Message'=>'Language already exist or suggested with same name','Data'=> null));
						return;
					}else{
						$Id = $this->model->addNewLanguage($Name);
					   echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
					}

			}
			/* Method to Update exsisting language
					Created By: Nishit Patel
			*/
			public function updateLanguage(){
$data=json_decode(file_get_contents('php://input'));
					$Name = "";
					$Id = 0;
					if (!empty($_REQUEST['Name'])){
						$Name = $_REQUEST['Name'];
					}else{
						$this->showdefaultMessage("Please provide Language Title");
						return;
					}

					if (!empty($_REQUEST['Id'])){
						$Id = $_REQUEST['Id'];
					}else{
						$this->showdefaultMessage("Please provide Language Id");
						return;
					}

					$this->load->model("ApiLanguageModel","model");
					$isExist = $this->model->checkIsLanguageExsistforUpdate($Id,$Name);
					if ($isExist) {
						echo json_encode(array('Status'=>0,'Message'=>'Language name already exist with different Id','Data'=> null));
						return;
					}else{
						$updatedData = $this->model->updateExistsLanguage($Id,$Name);
						if($updatedData){
							echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> array('Id' => (int)$Id,'Language'=> $Name)));
							return;
						}else{
							echo json_encode(array('Status'=>0,'Message'=>'Sorry, Data not updated!','Data'=> null));
							return;
						}

					}

			}

			/* Method to delete existing language
					Created By: Nishit Patel
			*/
			public function deleteLanguage(){
				$data=json_decode(file_get_contents('php://input'));
				$Id = 0;
				if (!empty($_REQUEST['Id'])){
					$Id = (int)$_REQUEST['Id'];
				}else{
					$this->showdefaultMessage("Please provide Language Id");
					return;
				}

				$this->load->model("ApiLanguageModel","model");
				$isExist = $this->model->deleteExistsLanguage($Id);
				if($isExist){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> null));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, Not able to delete!','Data'=> null));
				}

			}
			/*********** End Language APi **********/


			/*********** Country Api  ***********/
			/* Method to get Language list
				 Created By: Nishit Patel
			*/
			public function getCountryList(){
				$data=json_decode(file_get_contents('php://input'));
				$this->load->model("ApiCountryModel","model");
				$languageList = $this->model->getCountry();
				if ($languageList == null) {
						$this->showdefaultMessage("Language data not found");
						return;
				}
				echo json_encode(array('Status'=>1,'Message'=>'Success','Data'=>$languageList));
			}

			/* Method to Add new language
					Created By: Nishit Patel
			*/
			public function addCountry(){
$data=json_decode(file_get_contents('php://input'));
					$NameEnglish = "";
					$NameArabic = "";
					if (!empty($_REQUEST['NameEnglish'])){
						$NameEnglish = $_REQUEST['NameEnglish'];
					}else{
						$this->showdefaultMessage("Please provide Title in English");
						return;
					}

					if (!empty($_REQUEST['NameArabic'])){
						$NameArabic = $_REQUEST['NameArabic'];
					}else{
						$this->showdefaultMessage("Please provide Title in English");
						return;
					}

					$this->load->model("ApiCountryModel","model");
					$isExist = $this->model->checkIsCountryExsist($NameEnglish,$NameArabic);
					if ($isExist) {
						echo json_encode(array('Status'=>0,'Message'=>'Country already exists or suggested with same name','Data'=> null));
						return;
					}else{
						$Id = $this->model->addNewCountry($NameEnglish,$NameArabic);
					   echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
					}

			}

			/* Method to Update exsisting language
					Created By: Nishit Patel
			*/
			public function updateCountry(){
$data=json_decode(file_get_contents('php://input'));
					$Id = 0;
					$NameEnglish = "";
					$NameArabic = "";
					if (!empty($_REQUEST['NameEnglish'])){
						$NameEnglish = $_REQUEST['NameEnglish'];
					}else{
						$this->showdefaultMessage("Please provide Title in English");
						return;
					}

					if (!empty($_REQUEST['NameArabic'])){
						$NameArabic = $_REQUEST['NameArabic'];
					}else{
						$this->showdefaultMessage("Please provide Title in Arabic");
						return;
					}

					if (!empty($_REQUEST['Id'])){
						$Id = $_REQUEST['Id'];
					}else{
						$this->showdefaultMessage("Please provide Country Id");
						return;
					}

					$this->load->model("ApiCountryModel","model");
					$isExist = $this->model->checkIsCountryExsistforUpdate($Id,$NameEnglish,$NameArabic);
					if ($isExist) {
						echo json_encode(array('Status'=>0,'Message'=>'Country name already exist with different Id','Data'=> null));
						return;
					}else{
						$updatedData = $this->model->updateExistsCountry($Id,$NameEnglish,$NameArabic);
						if($updatedData){
							echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> array('Id' => (int)$Id,'NameEnglish'=> $NameEnglish,'NameArabic'=> $NameArabic)));
							return;
						}else{
							echo json_encode(array('Status'=>0,'Message'=>'Sorry, Data not updated!','Data'=> null));
							return;
						}

					}

			}

			/* Method to delete existing Country
					Created By: Nishit Patel
			*/
			public function deleteCountry(){
				$data=json_decode(file_get_contents('php://input'));
				$Id = 0;
				if (!empty($_REQUEST['Id'])){
					$Id = (int)$_REQUEST['Id'];
				}else{
					$this->showdefaultMessage("Please provide Country Id");
					return;
				}

				$this->load->model("ApiCountryModel","model");
				$isExist = $this->model->deleteExistsCountry($Id);
				if($isExist){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> null));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, Not able to delete!','Data'=> null));
				}

			}
			/*********** End Country API **********/

			/*********** Start Favourites API **********/

			/* Method to add content into your favourite List
				Created By: Nishit Patel
			*/
			public function addToFavourite(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$fileId = 0;
				$type = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['fileId'])){
					$fileId = (int)$_REQUEST['fileId'];
				}else{
					$this->showdefaultMessage("Please provide file Id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				$this->load->model("ApiFavouriteModel","model");
				$favouriteId = $this->model->checkIsFavouriteExsist($userId,$fileId,$type);
				if ($favouriteId > 0) {
					$isExist = $this->model->deleteExistsFavourite($favouriteId);
					if($isExist){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully deleted','Data'=> null));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'Sorry, Not able to delete!','Data'=> null));
						return;
					}
				}else{
					$Id = $this->model->addFavourite($userId,$fileId,$type);
					if($Id != null){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully Added','Data'=> $Id));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'Sorry, Not able to add!','Data'=> $Id));
						return;
					}
				}

			}

			/* Method to remove content from your favourite list
				Created By: Nishit Patel
			*/
			public function removeFromFavourite(){
$data=json_decode(file_get_contents('php://input'));
				$Id = 0;

				if (!empty($_REQUEST['Id'])){
					$Id = (int)$_REQUEST['Id'];
				}else{
					$this->showdefaultMessage("Please provide favourite Id");
					return;
				}

				$this->load->model("ApiFavouriteModel","model");
				$isExist = $this->model->deleteExistsFavourite($Id);
				if($isExist){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> null));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, Not able to delete!','Data'=> null));
				}

			}

			/* Method to get Favourite Tubes for User
				 Created By: Nishit Patel
			*/
			public function getFavouriteTubes(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$totalPage = 0;
				$this->load->model("ApiFavouriteModel","model");
				$totalPage = $this->model->getTotalPagesForFavouritesUsers($userId);
				$result = $this->model->getFavouritesTubesByUserId($userId,$pageIndex);
				if($result != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $result));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, No data found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}

			/* Method to get Favourite Tubes for User
				 Created By: Nishit Patel
			*/
			public function getFavouriteContent(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;
				$type = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				if(!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide content type");
					return;
				}

				$totalPage = 0;
				$this->load->model("ApiFavouriteModel","model");
				$totalPage = $this->model->getTotalPagesForFavouritesContent($userId,$type);
				$result = $this->model->getFavouritesContentByUserIdType($userId,$pageIndex,$type);
				if($result != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $result));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, No data found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}

			/* Method to Get Favourite List
			 	Created By: Nishit Patel
			*/
			public function getFavouriteUsersByUserId(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiFavouriteModel","model");

				$totalPage = 0;
				$totalPage = $this->model->getTotalPagesForFavouritesUsers($userId);
				$playList = $this->model->getFavouriteUsers($userId,$pageIndex);
				if($playList){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $playList));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data not found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}

			/* Method to Get Favourite List
				Created By: Nishit Patel
			*/
			public function getFollowersUsersByUserId(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiFavouriteModel","model");

				$totalPage = 0;
				$totalPage = $this->model->getTotalPagesForFollowersUsers($userId);
				$playList = $this->model->getFollowersUsers($userId,$pageIndex);
				if($playList){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $playList));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data not found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}


			/*********** End Favourites API **********/


			/*********** Start Rating API **********/
			/* Method to add Rating to Video
				Created By: Nishit Patel
			*/
			public function addRatingToVideo(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$videoId = 0;
				$rate = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['videoId'])){
					$videoId = (int)$_REQUEST['videoId'];
				}else{
					$this->showdefaultMessage("Please provide video Id");
					return;
				}

				if (!empty($_REQUEST['rate'])){
					$rate = (int)$_REQUEST['rate'];
				}else{
					$this->showdefaultMessage("Please provide rate for video");
					return;
				}

				$this->load->model("ApiRateModel","model");
				$isExist = $this->model->checkForExistingRating($videoId,$userId,$rate);
				$TotalRate = $this->model->getTotalRatingForFileId($videoId);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'Already rated same video.','TotalRate'=>$TotalRate,'Data'=> null));
					return;
				}else{
					$Id = $this->model->addRatingToVideo($videoId,$userId,$rate);
					$TotalRate = $this->model->getTotalRatingForFileId($videoId);
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalRate'=>$TotalRate,'Data'=> $Id));
				}

			}

			/* Method to update rating to video
				Created By: Nishit Patel
			*/
			public function updateRatingToVideo(){
$data=json_decode(file_get_contents('php://input'));
				$rateId = 0;
				$rate = 0;

				if (!empty($_REQUEST['rateId'])){
					$rateId = (int)$_REQUEST['rateId'];
				}else{
					$this->showdefaultMessage("Please provide rate Id");
					return;
				}

				if (!empty($_REQUEST['rate'])){
					$rate = (int)$_REQUEST['rate'];
				}else{
					$this->showdefaultMessage("Please provide rate for video");
					return;
				}

				$this->load->model("ApiRateModel","model");
				$isUpdate = $this->model->updateExistsRating($rateId,$rate);
				$videoId = $this->model->getVideoIdFromRateId($rateId);
				$TotalRate = $this->model->getTotalRatingForFileId($videoId);
				if ($isUpdate) {
					echo json_encode(array('Status'=>1,'Message'=>'Success, Video Rating uploaded','TotalRate'=>$TotalRate));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Not able to update video rating','TotalRate'=>$TotalRate));
					return;
				}

			}

			/* Method to delete rate video
				Created By: Nishit Patel
			*/
			public function deleteRatedVideo(){
				$data=json_decode(file_get_contents('php://input'));
				$rateId = 0;

				if (!empty($_REQUEST['rateId'])){
					$rateId = (int)$_REQUEST['rateId'];
				}else{
					$this->showdefaultMessage("Please provide rate Id");
					return;
				}
				$this->load->model("ApiRateModel","model");
				$videoId = $this->model->getVideoIdFromRateId($rateId);
				$isUpdate = $this->model->deleteExistingRate($rateId);
				$TotalRate = $this->model->getTotalRatingForFileId($videoId);
				if ($isUpdate) {
					echo json_encode(array('Status'=>1,'Message'=>'Success, Video Rating deleted','TotalRate'=>$TotalRate));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Not able to delete video rate','TotalRate'=>$TotalRate));
					return;
				}

			}


			/* Method to get list of user who rate specific video
				Created By: Manzz Baria
			*/
			public function whoRatedVideo(){
$data=json_decode(file_get_contents('php://input'));
				$videoId = 0;
				$pageIndex = 0;
				$totalPage =0;
				$userId = 0;
				if (!empty($_REQUEST['videoId'])){
					$videoId = (int)$_REQUEST['videoId'];
				}else{
					$this->showdefaultMessage("Please provide video Id ");
					return;
				}
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				$this->load->model("ApiRateModel","model");
				$whoRated = $this->model->whoRatedVideo($pageIndex,$videoId,$userId);
				$totalPage = $this->model->getTotalPageOfwhoRatedVideo($videoId);
				if ($whoRated != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Success',
						'TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data' =>$whoRated));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no one rate this file'));
					return;
				}

			}

			/*********** End Rating API **********/

			/*********** Start Comment API **********/
			/* Method to add comment for file
				Created By: Nishit Patel
			*/
			public function addComments(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$fileId = 0;
				$type = 0;
				$comment = "";

				if (!empty($_POST['userId'])){
					$userId = (int)$_POST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_POST['fileId'])){
					$fileId = (int)$_POST['fileId'];
				}else{
					$this->showdefaultMessage("Please provide file Id");
					return;
				}

				if (!empty($_POST['type'])){
					$type = (int)$_POST['type'];
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_POST['comment'])){
					$comment = $_POST['comment'];
				}else{
					$this->showdefaultMessage("Please provide Comments");
					return;
				}

				$this->load->model("ApiCommentModel","model");
				$isExist = $this->model->checkForExistingComment($userId,$fileId,$type,$comment);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'comment already added!','Data'=> null));
					return;
				}else{
					$Id = $this->model->addComment($userId,$fileId,$type,$comment);
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
				}


			}

			/* Method to update comment for file
				Created By: Nishit Patel
			*/
			public function updateComments(){
				$data=json_decode(file_get_contents('php://input'));
				$commentId = 0;
				$comment = "";

				if (!empty($_POST['commentId'])){
					$commentId = (int)$_POST['commentId'];
				}else{
					$this->showdefaultMessage("Please provide comment Id");
					return;
				}

				if (!empty($_POST['comment'])){
					$comment = $_POST['comment'];
				}else{
					$this->showdefaultMessage("Please provide Comments");
					return;
				}

				$this->load->model("ApiCommentModel","model");
				$isUpdated = $this->model->updateComment($commentId,$comment);
				if ($isUpdated != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfully editted comment!','Data'=> $isUpdated));
					return;
				}else{
					echo json_encode(array('Status'=>1,'Message'=>'Sorry, enable to edit comment!','Data'=> $isUpdated));
					return;
				}

			}

			/* Method to update comment for file
				Created By: Nishit Patel
			*/
			public function deleteComments(){
				$data=json_decode(file_get_contents('php://input'));
				$commentId = 0;

				if (!empty($_POST['commentId'])){
					$commentId = (int)$_POST['commentId'];
				}else{
					$this->showdefaultMessage("Please provide comment Id");
					return;
				}

				$this->load->model("ApiCommentModel","model");
				$isDeleted = $this->model->deleteCommentForFile($commentId);
				if ($isDeleted) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfully deleted comment!','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, enable to delete comment!','Data'=> null));
					return;
				}

			}

			/* Method to update comment for file
				Created By: Nishit Patel
			*/
			public function deleteCommentsForFile(){
				$data=json_decode(file_get_contents('php://input'));
				$fileId = 0;
				$type = 0;

				if (!empty($_POST['fileId'])){
					$fileId = (int)$_POST['fileId'];
				}else{
					$this->showdefaultMessage("Please provide file Id");
					return;
				}

				if (!empty($_POST['type'])){
					$type = (int)$_POST['type'];
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				$this->load->model("ApiCommentModel","model");
				$isdelete = $this->model->deleteCommentsByFileId($fileId,$type);
				if ($isdelete) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfully deleted comments!','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Sorry, enable to delete comment!','Data'=> null));
					return;
				}

			}
			/*********** End Comment API **********/


			/*********** Start PlayList API **********/
			/* Method to get Playlist According UserId
					Created By: Nishit Patel
			*/
			public function getPlaylist(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				$PageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$PageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiPlaylistModel","model");
				$result = $this->model->getPlayList($userId,$PageIndex);

				$totalPages = $this->model->getTotalPagesForGetPlayList($userId);

				if($result != null){
					echo json_encode(array('Status'=>1,'Message'=>'Success','TotalPage'=>$totalPages,'CurrentPage'=>$PageIndex+1,'Data'=> $result));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'No Playlist found','TotalPage'=>$totalPages,'CurrentPage'=>$PageIndex+1,'Data'=> null));
					return;
				}

			}

			/* Method to get Playlist Title and Id According UserId
					Created By: Manzz Baria
			*/
			public function getPlaylistTitle(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				$this->load->model("ApiPlaylistModel","model");
				$result = $this->model->getPlaylistTitle($userId);

				if($result != null){
					echo json_encode(array('Status'=>1,'Message'=>'Success','Data'=> $result));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'No Playlist found','Data'=> null));
					return;
				}

			}

			/* Method to add new playlist
				 Created By: Nishit Patel
			*/
			public function addPlaylist(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$playListName = "";

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['name'])){
					$playListName = $_REQUEST['name'];
				}else{
					$this->showdefaultMessage("Please provide playlist name");
					return;
				}

				$this->load->model("ApiPlaylistModel","model");
				$isExist = $this->model->checkExistingPlaylist($playListName,$userId);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'play list already added!','Data'=> null));
					return;
				}else{
					$Id = $this->model->addPlayList($playListName,$userId);
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
				}
			}

			/* Method to update existing playlist
				 Created By: Nishit Patel
			*/
			public function updatePlaylist(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$playListId = 0;
				$playListName = "";

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['playListId'])){
					$playListId = (int)$_REQUEST['playListId'];
				}else{
					$this->showdefaultMessage("Please provide play list Id");
					return;
				}

				if (!empty($_REQUEST['name'])){
					$playListName = $_REQUEST['name'];
				}else{
					$this->showdefaultMessage("Please provide playlist name");
					return;
				}

				$this->load->model("ApiPlaylistModel","model");
				$isExist = $this->model->checkExistingPlaylistForUpdate($playListId,$playListName,$userId);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'play list name already existing with different id!','Data'=> null));
					return;
				}else{
					$isUpdated = $this->model->updatePlayList($playListId,$playListName);
					if($isUpdated != null){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully updated','Data'=> $isUpdated));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'no abel to update','Data'=> $isUpdated));
						return;
					}
				}
			}

			/* Method to update existing playlist
				 Created By: Nishit Patel
			*/
			public function deletePlaylist(){
				$data=json_decode(file_get_contents('php://input'));
				$playListId = 0;

				if (!empty($_REQUEST['playListId'])){
					$playListId = (int)$_REQUEST['playListId'];
				}else{
					$this->showdefaultMessage("Please provide play list Id");
					return;
				}

				$this->load->model("ApiPlaylistModel","model");
				$isDelete = $this->model->deletePlayList($playListId);
				if($isDelete){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully deleted','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no abel to delete','Data'=> null));
					return;
				}
			}

		/*	Method to get video list for specific playlist
			Created By: Manzz Baria
		*/
		public function getVideoListForPlayList(){
$data=json_decode(file_get_contents('php://input'));
					$PageIndex = 0;
					if (!empty($_REQUEST['PageIndex']))
					{
						$PageIndex = $_REQUEST['PageIndex'];
					}
					if (!empty($_REQUEST['PlayListId'])){
						$PlayListId = $_REQUEST['PlayListId'];
					}else{
						$this->showdefaultMessage("PlayListId is required");
						return;
					}
					$userId = 0;
					if (!empty($_REQUEST['userId'])){
						$userId = $_REQUEST['userId'];
					}

					$lang = 'en';
					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}
					$Array = null;
					$totalPage = 0;

					$this->load->model("ApiVideoModel","vmodel");
					$Array = $this->vmodel->getVideoListForPlaylist($PageIndex,$PlayListId,$lang,$userId);
					$totalPage = $this->vmodel->getTotalPageOfVideoListForPlaylist($PlayListId);

					if ($Array == null) {
						$this->showdefaultMessage("No records found");
						return;
					}
					echo json_encode(array('Status'=>1,
											'Message'=>'Successfully',
											'TotalPage'=>$totalPage,
											"CurrentPage"=>$PageIndex+1,
											'Data'=> $Array));
					return;

	}


			/*********** End PlayList API **********/

			/*********** Start Admin User Profile API **********/

			public function getUserActivity($fileId,$type,$userId){
				$this->load->model("ApiUserModel","model");
				$userData = $this->model->getUserActivity($fileId,$type,$userId);
				echo json_encode($userData);
			}

			public function getUserProfile(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$myUserId = -1;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				if (!empty($_REQUEST['myUserId'])){
					$myUserId = (int)$_REQUEST['myUserId'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiUserModel","model");
				$userData = $this->model->getUserProfile($userId,$pageIndex,$myUserId);

				if($userData != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no data found','Data'=> null));
					return;
				}

			}
			/*********** End Admin User Profile API **********/

			/*********** Start Abuse Report API **********/
			/*	Method to get Resource List (videos / Images)
				Created By: Manzz Baria
			*/
			public function getAbuseList(){
$data=json_decode(file_get_contents('php://input'));
				$type = 0;
				$pageIndex = 0;

				if (!empty($_REQUEST['PageIndex'])){
					$pageIndex = $_REQUEST['PageIndex'];
				}
				$lang = 'en';
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				if (!empty($_REQUEST['Type'])){
					$type = $_REQUEST['Type'];
				}else{
					$this->showdefaultMessage("Type is required");
					return;
				}

				$Array = null;
				$totalPage = 0;

				$this->load->model("ApiAbuseModel","amodel");


				if($type == POST_TYPE){
					$Array = $this->amodel->getAbusedPostList($type,$pageIndex);
					$totalPage = $this->amodel->getTotalPagesForPostAbusedContent($type);
				}else if($type == VIDEO_TYPE){
					$Array = $this->amodel->getAbousedContentList($type,$lang,$pageIndex);
					$totalPage = $this->amodel->getTotalPagesForAbusedContent($type);
				}else if($type == IMAGE_TYPE){
					$userId=0;
					$Array = $this->amodel->getAbuseImageList($pageIndex,$userId);
					$totalPage = $this->amodel->getTotalPagesForImageontent($type);
				}

				if ($Array == null) {
					$this->showdefaultMessage("No records found");
					return;
				}

				echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $Array));

				return;

			}

			/*	Method to get resource details
				Created By: Manzz Baria
			*/
			public function getAbuseDetails(){
$data=json_decode(file_get_contents('php://input'));
				if (!empty($_REQUEST['ResourceId'])){
					$ResourceId = $_REQUEST['ResourceId'];
				}else{
					$this->showdefaultMessage("ResourceId is required");
					return;
				}
				$lang = 'en';
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}

				if (!empty($_REQUEST['Type'])){
					$Type = $_REQUEST['Type'];
				}else{
					$this->showdefaultMessage("Type is required");
					return;
				}

				$userId= 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				$PageIndex= 0;
				if (!empty($_REQUEST['pageIndex'])){
					$PageIndex = (int)$_REQUEST['pageIndex'];
				}

				$ArrayDetails = null;
				$ArrayComments = null;
				$ArrayAbuseReport = null;


				if ($Type == VIDEO_TYPE) {
					$this->load->model("ApiVideoModel","vmodel");
					$ArrayDetails = $this->vmodel->getVideoDetails($ResourceId,$lang,$userId);
				}else if($Type == IMAGE_TYPE){
					$this->load->model("ApiImageModel","imodel");
					$ArrayDetails = $this->imodel->getImageDetails($ResourceId,$lang,$userId);
				}else if($Type == POST_TYPE){
					$this->load->model("ApiPostModel","pmodel");
					$ArrayDetails = $this->pmodel->getPostDetails($ResourceId,$userId);
				}

				//$this->load->model("ApiCommentModel","cmtmodel");
				//$ArrayComments = $this->cmtmodel->getCommentList($ResourceId,$Type,$PageIndex);

				$this->load->model("ApiAbuseModel","abmodel");
				$ArrayAbuseReport = $this->abmodel->getAbuseReportList($ResourceId,$Type,$PageIndex);
				$totalAbusePages = $this->abmodel->getTotalAbuseReport($ResourceId,$Type);

				$abusesObject = array('TotalPage'=>$totalAbusePages,'CurrentPage'=>$PageIndex+1,'AbuseReport'=>$ArrayAbuseReport);
				if ($ArrayDetails == null) {
					$this->showdefaultMessage("No records found");
					return;
				}

				echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $ArrayDetails,'AbuseReport'=>$abusesObject));
				return;

			}

			/*********** End Abuse Report API **********/


			/*********** Start Like - Dislike API **********/
			/* Method to add like/dislike to hoynet contents
				 Created By: Nishit Patel
			*/
			public function addLikesToConetent(){
$data=json_decode(file_get_contents('php://input'));
				$fileId = 0;
				$type = 0;
				$status = 0;
				$userId = 0;

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for file Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for file type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_REQUEST['status'])){
					$status = $_REQUEST['status'];
					if(!is_numeric($status)){
						$this->showdefaultMessage("Provide integer value for status");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide content status");
					return;
				}

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				$this->load->model("ApiLikeModel","lmodel");
				$isExist = $this->lmodel->checkIsLikeDislikeExists($status,$fileId,$type,$userId);
				if($isExist > 0){
					$isUpdate = $this->lmodel->deleteLikeDislikeHolyNets($isExist);

					$totalLikes = $this->lmodel->getTotalLike($fileId,$type);
					$totalDislikes = $this->lmodel->getTotalDislike($fileId,$type);
					$data = array("TotalLikes"=>$totalLikes,"TotalDislike"=>$totalDislikes);

					if ($isUpdate) {
						echo json_encode(array('Status'=>1,'Message'=>'Deleted!','Data'=> $data));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'not able to delete','Data'=> $data));
					}
				}
				$isStatusChanged = $this->lmodel->checkStatusIsChanged($status,$fileId,$type,$userId);
				if($isStatusChanged > 0){
					$isUpdate = $this->lmodel->updateLikeDislikeHolyNets($isStatusChanged,$status,$fileId,$type,$userId);

					$totalLikes = $this->lmodel->getTotalLike($fileId,$type);
					$totalDislikes = $this->lmodel->getTotalDislike($fileId,$type);
					$data = array("TotalLikes"=>$totalLikes,"TotalDislike"=>$totalDislikes);

					if ($isUpdate) {
						echo json_encode(array('Status'=>1,'Message'=>'Updated!','Data'=> $data));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'not able to update','Data'=> $data));
					}
				}

				$Id = $this->lmodel->addLikeDislikeHolyNets($status,$fileId,$type,$userId);
				$totalLikes = 0;
				$totalDislikes = 0;

				$totalLikes = $this->lmodel->getTotalLike($fileId,$type);
				$totalDislikes = $this->lmodel->getTotalDislike($fileId,$type);
				$data = array("TotalLikes"=>$totalLikes,"TotalDislike"=>$totalDislikes);

				if($Id != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $data));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Not able to perform','Data'=> $data));
					return;
				}

			}

			/* Method to delete like/dislike holynet contents
				 Created By: Nishit patel
			*/
			public function deleteLikesToConetent(){
$data=json_decode(file_get_contents('php://input'));
				$Id = 0;

				if (!empty($_REQUEST['Id'])){
					$Id = $_REQUEST['Id'];
					if(!is_numeric($Id)){
						$this->showdefaultMessage("Provide integer value for Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide Id");
					return;
				}

				$this->load->model("ApiLikeModel","lmodel");
				$isUpdate = $this->lmodel->deleteLikeDislikeHolyNets($Id);
				if ($isUpdate) {
					echo json_encode(array('Status'=>1,'Message'=>'Deleted!','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'not able to delete','Data'=> null));
				}

			}

			/* Method to get like/dislike  users like for hoynet contents
				 Created By: Nishit Patel
			*/
			public function getLikesDislikesUsers(){
$data=json_decode(file_get_contents('php://input'));
				$fileId = 0;
				$type = 0;
				$status = 0;
				$pageIndex = 0;
				$userId = 0;

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for file Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for file type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_REQUEST['status'])){
					$status = $_REQUEST['status'];
					if(!is_numeric($status)){
						$this->showdefaultMessage("Provide integer value for status");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide content status");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
					if(!is_numeric($status)){
						$this->showdefaultMessage("Provide integer value for page index");
						return;
					}
				}

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				$this->load->model("ApiLikeModel","lmodel");
				$usersList = $this->lmodel->getLikesDislikesUsersList($fileId,$type,$status,$pageIndex,$userId);
				if ($usersList != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Success','Data'=> $usersList));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'No data found','Data'=> null));
				}

			}

			/*********** End Like - Dislike API **********/

			/*********** Start Abuse Report API **********/
			/* Method to report abuse for holynet content
				 Created By: Nishit Patel
			*/
			public function addAbuseReport(){
				$data=json_decode(file_get_contents('php://input'));
				$fileId = 0;
				$type = 0;
				$reason = "";
				$userId = 0;

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for file Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for file type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (isset($_REQUEST['reason'])){
					$reason = $_REQUEST['reason'];
				}else{
					$this->showdefaultMessage("Please provide reason");
					return;
				}

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				$this->load->model("ApiAbuseModel","amodel");

				$count = $this->amodel->getIsAllowToReportAbuse($fileId,$type);
				if($count >= 3){
					echo json_encode(array('Status'=>0,'Message'=>'This content is already approved by admin.','Data'=> null));
					return;
				}

				$isExist = $this->amodel->checkIsAbuseExists($reason,$fileId,$type,$userId);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'already added!','Data'=> null));
					return;
				}else{
					$isAlreadyReported = $this->amodel->checkIsUserAlreadyReportedAbuse($fileId,$type,$userId);
					if($isAlreadyReported){
						echo json_encode(array('Status'=>0,'Message'=>'You already reported abuse!','Data'=> null));
						return;
					}
					$Id = $this->amodel->addAbuseReportForHolyNetContent($reason,$fileId,$type,$userId);
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
					return;
				}
			}

			/* Method to update abuse report for holynet content
				 Created By: Nishit Patel
			*/
			public function updateAbuseReport(){
				$data=json_decode(file_get_contents('php://input'));
				$abuseId = 0;
				$reason = "";

				if (!empty($_REQUEST['abuseId'])){
					$abuseId = $_REQUEST['abuseId'];
					if(!is_numeric($abuseId)){
						$this->showdefaultMessage("Provide integer value for abuse Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide abuse id");
					return;
				}

				if (isset($_REQUEST['reason'])){
					$reason = $_REQUEST['reason'];
				}else{
					$this->showdefaultMessage("Please provide reason");
					return;
				}

				$this->load->model("ApiAbuseModel","amodel");
				$isUpdate = $this->amodel->updateAbuseReportHolyNetContent($abuseId,$reason);
				if ($isUpdate) {
					echo json_encode(array('Status'=>1,'Message'=>'succesffully updated!','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'unable to update','Data'=> null));
					return;
				}
			}

			/* Method to delete abuse report for holynet content
				 Created By: Nishit Patel
			*/
			public function deleteAbuseReport(){
				$data=json_decode(file_get_contents('php://input'));
				$abuseId = 0;

				if (!empty($_REQUEST['abuseId'])){
					$abuseId = $_REQUEST['abuseId'];
					if(!is_numeric($abuseId)){
						$this->showdefaultMessage("Provide integer value for abuse Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide abuse id");
					return;
				}

				$this->load->model("ApiAbuseModel","amodel");
				$isUpdate = $this->amodel->deleteAbuseReportHolyNetContent($abuseId);
				if ($isUpdate) {
					echo json_encode(array('Status'=>1,'Message'=>'succesffully deleted!','Data'=> null));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'unable to delete','Data'=> null));
					return;
				}
			}
			/*********** End Abuse Report API **********/

			/*********** Start Watched Video API **********/
			/* Method to add watched content
				 Created By: Nishit Patel
			*/
			public function addWatchedContent(){
				$data=json_decode(file_get_contents('php://input'));
				$userId=0;
				$fileId=0;
				$type=0;

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for File Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for File type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}


				$this->load->model("ApiWatchedModel","wmodel");
				$isExist = $this->wmodel->checkIsWatchedVideoExist($fileId,$type,$userId);
				if ($isExist) {
					echo json_encode(array('Status'=>0,'Message'=>'already watched!','Data'=> null));
					return;
				}else{
					$Id = $this->wmodel->addWatchedFile($fileId,$type,$userId);
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $Id));
					return;
				}


			}

			/* Method to add watched content
				 Created By: Nishit Patel
			*/
			public function getWatchedUsers(){
				$data=json_decode(file_get_contents('php://input'));
				$fileId=0;
				$type=0;
				$pageIndex=0;
				$userId=0;

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for File Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for File type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				$this->load->model("ApiWatchedModel","wmodel");
				$watchedData = $this->wmodel->getWatchedUsersList($fileId,$type,$pageIndex,$userId);

				$totalPage = $this->wmodel->getTotalPagesWatchedUser($fileId,$type);

				if ($watchedData != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfull','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $watchedData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data Not found','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> null));
					return;
				}

			}
			/*********** End Watched Video API **********/

			/*********** Start History API **********/
			/* Method to get watched content by user
				 Created By: Nishit Patel
			*/
			public function getHistoryForWatchedContent(){
				$data=json_decode(file_get_contents('php://input'));
				$fileId=0;
				$type=0;
				$userId=0;
				$pageIndex=0;

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}

				$this->load->model("ApiWatchedModel","wmodel");
				$watchedData = $this->wmodel->getWatchedContent($userId,$pageIndex);

				$totalPage = $this->wmodel->getTotalPageWatchedContent($userId);

				if ($watchedData != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfull','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $watchedData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data Not found','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> null));
					return;
				}
			}

			/* Method to get liked content by user
				 Created By: Nishit Patel
			*/
			public function getHistoryForLikedContent(){
$data=json_decode(file_get_contents('php://input'));
				$status=0;
				$userId=0;
				$pageIndex=0;

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				if (!empty($_REQUEST['status'])){
					$status = $_REQUEST['status'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for status");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide status");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}

				$this->load->model("ApiLikeModel","imodel");
				$likedData = $this->imodel->getLikedContentByUser($userId,$status,$pageIndex);
				$totalPage = $this->imodel->getTotalPageLikedContent($userId,$status);

				if ($likedData != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfull','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $likedData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data Not found','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> null));
					return;
				}

			}

			/* Method to get liked content by user
				 Created By: Nishit Patel
			*/
			public function getNotificationForLikedContent(){
				$data=json_decode(file_get_contents('php://input'));
				$status=0;
				$userId=0;
				$pageIndex=0;

				if (!empty($_REQUEST['userId'])){
					$userId = $_REQUEST['userId'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for user Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide user id");
					return;
				}

				if (!empty($_REQUEST['status'])){
					$status = $_REQUEST['status'];
					if(!is_numeric($userId)){
						$this->showdefaultMessage("Provide integer value for status");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide status");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}

				$this->load->model("ApiLikeModel","imodel");
				//$likedData = $this->imodel->getLikedContentByUser($userId,$status,$pageIndex);
				//$totalPage = $this->imodel->getTotalPageLikedContent($userId,$status);

				$likedData = $this->imodel->getNotificationLikedContentByUser($userId,$status,$pageIndex);
				$totalPage = $this->imodel->getNotificationTotalPageLikedContent($userId,$status);

				if ($likedData != null) {
					echo json_encode(array('Status'=>1,'Message'=>'Successfull','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $likedData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data Not found','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> null));
					return;
				}

			}

			/*********** End History API **********/

			/*********** Start Related Content API **********/
			public function getRelatedContent(){
				$data=json_decode(file_get_contents('php://input'));
				$fileId=0;
				$type=0;
				$userId = 0;
				$pageIndex=0;

				if (!empty($_REQUEST['fileId'])){
					$fileId = $_REQUEST['fileId'];
					if(!is_numeric($fileId)){
						$this->showdefaultMessage("Provide integer value for file Id");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = $_REQUEST['type'];
					if(!is_numeric($type)){
						$this->showdefaultMessage("Provide integer value for file type");
						return;
					}
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				$totalPage = 1;
				if ($type == VIDEO_TYPE) {
					$this->load->model("ApiVideoModel","vmodel");
					$Array = $this->vmodel->getRelatedVideos($fileId,$pageIndex,$userId);
				}else if($type == IMAGE_TYPE){
					$this->load->model("ApiImageModel","imodel");
					$Array = $this->imodel->getRelatedImageList($fileId,$pageIndex);
				}else if($type == POST_TYPE){
					$this->load->model("ApiPostModel","pmodel");
					$Array = $this->pmodel->getRelatedPostList($fileId,$pageIndex);
				}
				if ($Array == null) {
					$this->showdefaultMessage("No records found");
					return;
				}

				echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $Array));

			}


			public function mergedArray(){
				$input = array("a" => "green", "red", "b" => "green", "blue", "red");
				$result = array_unique($input);
				print_r($result);
			}

			/*********** End Related Video API **********/

			/*********** Start Newsfeed API **********/
			/* Method to get news feed
				 Created By: Nishit Patel
			*/
			public function getNewsfeed(){
				$data=json_decode(file_get_contents('php://input'));
				$userId=0;
				$pageIndex=0;
				$lang = "en";

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = $_REQUEST['pageIndex'];
				}
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}

				$this->load->model("ApiNewsFeedModel","nfmodel");
				$totalPage = 0;//$this->nfmodel->getTotalPagePopularContents($userId);

				if($userId == 0){
					$totalPage = $this->nfmodel->getTotalPagePopularContents($userId);
				}else{
					$totalPage = $this->nfmodel->getTotalPageFavouritesNewsFeedForUser($userId);
				}

				$Data = null;
				if($userId == 0){
					$Data = $this->nfmodel->getPopularContents($userId,$lang,$pageIndex);
				}else{
					$Data = $this->nfmodel->getFavouritesNewsFeedForUser($userId,$lang,$pageIndex);
					if($Data == null ){
						$totalPage = $this->nfmodel->getTotalPagePopularContents($userId);
						$Data = $this->nfmodel->getPopularContents($userId,$lang,$pageIndex);
					}
				}

				if($Data != null){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $Data));
						return;
				}else{
						echo json_encode(array('Status'=>0,'Message'=>'Sorry, Newsfeed not found','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
						return;
				}

			}

			public function sampleVideo($fileId){
				$data=json_decode(file_get_contents('php://input'));
				$this->load->model("ApiVideoModel","vmodel");
				$lang = "en";
				$favrouiteTubes = $this->vmodel->getVideoDetails($fileId,$lang);
				echo json_encode($favrouiteTubes);
			}

			/*********** End Newsfeed API **********/

			/*********** Start App User Profile API **********/
			/* Method to get User detail for App
				 Created By: Nishit Patel
			*/
			public function getAppUserDetail(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				$lang = "en";
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$loginUserId = 0;
				if (!empty($_REQUEST['loginUserId'])){
					$loginUserId = (int)$_REQUEST['loginUserId'];
				}

				$this->load->model("ApiUserModel","model");
				$userData = $this->model->getUserProfileForApp($userId,$lang,$pageIndex,$loginUserId);

				if($userData != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no data found','Data'=> null));
					return;
				}

			}

			/* Method to get User detail for App
				 Created By: Nishit Patel
			*/
			public function getAppUserNewsFeeds(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				$lang = "en";
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}
				$this->load->model("ApiNewsFeedModel","nfmodel");
				$newFeeds = $this->nfmodel->getUserNewsFeed($userId,$lang,$pageIndex);
				if($newFeeds != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $newFeeds));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no data found','Data'=> null));
					return;
				}

			}

			/*********** End App User Profile API **********/

			/*********** Start Search tubes API **********/
			/* Method to get User detail for App
				 Created By: Nishit Patel
			*/
			public function getKeywordSearchResult(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					//$this->showdefaultMessage("Please provide user Id");
					//return;
				}
				$lang = "en";
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}
				$keyword = "";
				if (!empty($_REQUEST['keyword'])){
					$keyword = $_REQUEST['keyword'];
				}else{
					$this->showdefaultMessage("Please provide search keyword");
					return;
				}
				$type = 0;
				if (!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide content type");
					return;
				}

				$this->load->model("ApiSearchModel","smodel");
				$userData = $this->smodel->getSearchContentByKeyword($userId,$lang,$pageIndex,$keyword,$type);
				$totalPage = 0;
				if ($type == VIDEO_TYPE) {
		      $this->load->model("ApiVideoModel","vmodel");
		      $totalPage = $this->vmodel->getTotalPagesVideoBySearchKeyword($keyword,$lang,$pageIndex,$userId);
		    }
		    if($type == IMAGE_TYPE){
		      $this->load->model("ApiImageModel","imodel");
		      $totalPage = $this->imodel->getTotalPageSearchImagedByKeyword($userId,$keyword,$lang,$pageIndex);
		    }
		    if($type == POST_TYPE){
		      $this->load->model("ApiPostModel","pmodel");
		      $totalPage = $this->pmodel->getTotalPageSearchPostByKeyword($userId,$keyword,$lang,$pageIndex);
		    }
		    if($type == USER_TYPE){
		      $this->load->model("ApiUserModel","usermodel");
		      $totalPage = $this->usermodel->getTotalPageSearchUsersKeyword($userId,$keyword,$lang,$pageIndex);
		    }
		    if($type == PLAYLIST_TYPE){
		      $this->load->model("ApiPlaylistModel","playmodel");
		      $totalPage = $this->playmodel->getTotalPageSearchPlayListByKeyword($userId,$keyword,$lang,$pageIndex);
		    }

				if($userData != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> $userData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no data found for keyword : '.$keyword,'TotalPage'=>$totalPage,'CurrentPage'=>$pageIndex+1,'Data'=> null));
					return;
				}

			}

			public function getTotalSearchPlaylist(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				$lang = "en";
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}
				$keyword = "";
				if (!empty($_REQUEST['keyword'])){
					$keyword = $_REQUEST['keyword'];
				}else{
					$this->showdefaultMessage("Please provide search keyword");
					return;
				}
				$type = 0;
				if (!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide content type");
					return;
				}
			}

			/* Method to get User detail for App
				 Created By: Nishit Patel
			*/
			public function getAdvanceSearchResult(){
$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				$lang = "en";
				if (!empty($_REQUEST['lang'])){
					$lang = $_REQUEST['lang'];
				}
				$pageIndex = 0;
				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}
				$this->load->model("ApiNewsFeedModel","nfmodel");
				$newFeeds = $this->nfmodel->getUserNewsFeed($userId,$lang,$pageIndex);
				if($newFeeds != null){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','Data'=> $userData));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'no data found','Data'=> null));
					return;
				}

			}

			/*********** End Search tubes API **********/

			/*********** Start Share content API **********/
			/* Method to share holynet content with other users of holynet
				 Created By: Nishit Patel
			*/
			public function shareContentWithOthers(){
				$data=json_decode(file_get_contents('php://input'));
					$fileId = 0;
					$type = 0;
					$userId = 0;
					$sharedIds = "";

					if (!empty($_REQUEST['userId'])){
						$userId = (int)$_REQUEST['userId'];
					}else{
						$this->showdefaultMessage("Please provide user Id");
						return;
					}

					if (!empty($_REQUEST['fileId'])){
						$fileId = (int)$_REQUEST['fileId'];
					}else{
						$this->showdefaultMessage("Please provide file Id");
						return;
					}

					if (!empty($_REQUEST['type'])){
						$type = (int)$_REQUEST['type'];
					}else{
						$this->showdefaultMessage("Please provide file type");
						return;
					}

					if (!empty($_REQUEST['sharedId'])){
						$sharedIds = $_REQUEST['sharedId'];
					}else{
						$this->showdefaultMessage("Please provide shared user id");
						return;
					}
					$this->load->model("ApiShareModel","shmodel");
					$isShared = $this->shmodel->share($userId,$fileId,$type,$sharedIds);
					$totalShared = $this->shmodel->getTotalShare($fileId,$type);
					if($isShared){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully shared','TotalShared'=>$totalShared));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'no able to shared','TotalShared'=>$totalShared));
						return;
					}
			}
			/* Method to get shared content by user id
				 Created By: Nishit Patel
			*/
			public function getSharedContentByUser(){
$data=json_decode(file_get_contents('php://input'));
					$userId = 0;
					$lang = "en";
					$PageIndex = 0;

					if (!empty($_REQUEST['userId'])){
						$userId = (int)$_REQUEST['userId'];
					}else{
						$this->showdefaultMessage("Please provide user Id");
						return;
					}

					if (!empty($_REQUEST['lang'])){
						$lang = $_REQUEST['lang'];
					}

					if (!empty($_REQUEST['pageIndex'])){
						$PageIndex = (int)$_REQUEST['pageIndex'];
					}

					$this->load->model("ApiShareModel","shmodel");
					$sharedData = $this->shmodel->getSharedContent($userId,$lang,$PageIndex);
					$totalPage = $this->shmodel->getTotalPageForSharedContent($userId);
					if($sharedData != null){
						echo json_encode(array('Status'=>1,'Message'=>'Successfully shared','TotalPage'=>$totalPage,"CurrentPage"=>$PageIndex+1,'Data'=>$sharedData));
						return;
					}else{
						echo json_encode(array('Status'=>0,'Message'=>'No shared content found','TotalPage'=>$totalPage,"CurrentPage"=>$PageIndex+1,'Data'=>null));
						return;
					}
			}
			/*********** End Share content API **********/
			/*********** Start Reject content API **********/
			/* Method to reject content by Admin
				 Created By: Nishit Patel
			*/
			public function rejectContent(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$reason = "";
 				$fileId = 0;
				$type = 0;
				$fileIdstr = "";

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['fileId'])){
					$fileIdstr = $_REQUEST['fileId'];
				}else{
					$this->showdefaultMessage("Please provide file Id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				if (!empty($_REQUEST['reason'])){
					$reason = $_REQUEST['reason'];
				}else{
					$this->showdefaultMessage("Please provide reject reason");
					return;
				}
				$this->load->model("ApiAbuseModel","abusemodel");
				$this->load->model("ApiContentModel","contentmodel");
				$this->load->model("ApiNotificationModel","notymodel");
				$rejectId = 0;

				$fileIds = explode(',', $fileIdstr);
				foreach ($fileIds as $fileId)
				{
					$this->notymodel->sendNotificationForRejection($fileId,$type,$reason);
					//$rejectId = $this->abusemodel->rejectContent($userId,$fileId,$type,$reason);
					$isReject = $this->contentmodel->deleteResource($fileId,$type,$userId);
					if ($isReject) {

						//echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					//	return;
					}else{
						//echo json_encode(array('Status'=>0,'Message'=>'Access denied'));
					//	return;
					}

				}
				echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
				return;

				/*if($rejectId > 0 && $rejectId != 1){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					return;
				}else if($rejectId  == 1){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully Updated'));
					return;
				}else if($rejectId  == 0){
					echo json_encode(array('Status'=>0,'Message'=>'Already rejected'));
					return;
				}*/

			}

			/* Method to block the user by Admin
				 Created By: Nishit Patel
			*/
			public function blockUsers(){
				$data=json_decode(file_get_contents('php://input'));
					$status = 0;
					$fileIdstr = "";

					if (!empty($_REQUEST['userId'])){
						$fileIdstr = $_REQUEST['userId'];
					}else{
						$this->showdefaultMessage("Please provide user Id(s)");
						return;
					}

					if (isset($_REQUEST['status'])){
						$status = (int)$_REQUEST['status'];
					}else{
						$this->showdefaultMessage("Please provide user status");
						return;
					}

					$this->load->model("ApiAbuseModel","abusemodel");
					$rejectId = 0;

					$fileIds = explode(',', $fileIdstr);
					foreach ($fileIds as $fileId)
					{
						$rejectId = $this->abusemodel->blockUsers($fileId,$status);
					}
					echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					return;
			}

			/* Method to approved abuse content
				 Created By: Nishit Patel
			*/
			public function approvedContent(){
				$data=json_decode(file_get_contents('php://input'));
 				$fileIdstr = "";
				$type = 0;

				if (!empty($_REQUEST['fileId'])){
					$fileIdstr = $_REQUEST['fileId'];
				}else{
					$this->showdefaultMessage("Please provide file Id");
					return;
				}

				if (!empty($_REQUEST['type'])){
					$type = (int)$_REQUEST['type'];
				}else{
					$this->showdefaultMessage("Please provide file type");
					return;
				}

				$this->load->model("ApiAbuseModel","abusemodel");
				$fileIds = explode(',', $fileIdstr);
				$result = false;
				foreach ($fileIds as $fileId)
				{
					$result = $this->abusemodel->approvedContentByAdmin($fileId,$type);
				}
				//echo json_encode($result);
				if($result){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'due to server error try again later'));
					return;
				}

			}

			/* Method to approved abuse users
				 Created By: Nishit Patel
			*/
			public function approvedUsers(){
				$data=json_decode(file_get_contents('php://input'));
 				$userIdstr = "";
				$type = 0;

				if (!empty($_REQUEST['userId'])){
					$userIdstr = $_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id(s)");
					return;
				}

				$this->load->model("ApiAbuseModel","abusemodel");
				$fileIds = explode(',', $userIdstr);
				$result = false;
				foreach ($fileIds as $fileId)
				{
					$result = $this->abusemodel->approvedUsersByAdmin($fileId);
				}
				//echo json_encode($result);
				if($result){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'due to server error try again later'));
					return;
				}

			}


			/*********** End Reject content API **********/

			/*********** Start User search API **********/
			/* Method to get user by search keyword
				Created By: Nishit Patel
			*/
			public function getUserListBySearchName(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;
				$keyword = "";

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}else{
					$this->showdefaultMessage("Please provide user Id");
					return;
				}

				if (!empty($_REQUEST['keyword'])){
					$keyword = $_REQUEST['keyword'];
				}else{
					$this->showdefaultMessage("Please provide search keyword");
					return;
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiUserModel","model");

				$totalPage = 0;
				$totalPage = $this->model->getTotalPagesForUserByName($userId,$keyword);
				$playList = $this->model->getUsersByName($userId,$keyword,$pageIndex);
				if($playList){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $playList));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data not found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}
			/*********** End User search API **********/

			/*********** Start Popular user list API **********/
			/* Method to get popular user List
				 Created By: Nishit Patel
			*/
			public function getPopularUsers(){
				$data=json_decode(file_get_contents('php://input'));
				$userId = 0;
				$pageIndex = 0;

				if (!empty($_REQUEST['userId'])){
					$userId = (int)$_REQUEST['userId'];
				}

				if (!empty($_REQUEST['pageIndex'])){
					$pageIndex = (int)$_REQUEST['pageIndex'];
				}

				$this->load->model("ApiUserModel","model");
				$totalPage = 0;
				$totalPage = $this->model->getTotalPagesPopularUsers($userId,$pageIndex);
				$userLists = $this->model->getPopularUsers($userId,$pageIndex);
				if($userLists){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> $userLists));
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Data not found!','TotalPage'=>$totalPage,"CurrentPage"=>$pageIndex+1,'Data'=> null));
				}

			}
			/*********** End Popular user list API **********/

			/* Send sample notification
				 Created By: Nishit Patel
			*/
			public function sendTestNotification($userId,$type){
				$this->load->model("ApiNotificationModel","notificationmodel");
				$this->notificationmodel->sendSampleNotification($userId,$type);
			}

			/* Method to test iOS push notification
				 Created By: Nishit Patel
			*/
			public function sendIphoneNotification($deviceToken){
				//send notification
		    $this->load->model("ApiNotificationModel","notificationmodel");
				$devices[] = $deviceToken;
				$devices[] = "341a7d0f615f32cd7efe25d435cd799cde312f252647cabc1df350f7994cc89d";
				$devices[] = "341a7d0f615f32cd7efe25d435cd799cde312f252647cabc1df350f7994cc89d";
				$devices[] = "4b2333278b702639c231d70554cc1cf23bf38d57be8fcb23eac24b3499d114a0";
				$devices[] = "13b441b5a31d34bdee29ac5c2baa5c0325f98ffae67dc9acf208bba047c3b477";
				if(IS_APN_DEVELOPMENT){
						$this->notificationmodel->sendNotificationToIphoneDevice($devices,"Development","APN Testing Message","","","","","","");
				}else{
						echo "production</BR>";
						$this->notificationmodel->sendNotificationToDistributorIphoneDevice($devices,"Distribution","APN Testing Message","","","","","","");
				}
			}

			public function sendLikeNotification($fileId,$type,$userId,$status){
				$this->load->model("ApiNotificationModel","notificationmodel");
		    $this->notificationmodel->sendNotificationForLikeDislikeContent($fileId,$type,$userId,$status);
			}


			/* Method to update device token for Mobile users
				 Created By: Nishit Patel
			*/
			public function updateDeviceToken(){
				$deviceType = 0;
				$userId = 0;
				$deviceToken = "";

				if (empty($_REQUEST['userId'])){
					$this->showdefaultMessage("Please provide user Id");
					return;
				}
				$userId = (int)$_REQUEST['userId'];

				if (empty($_REQUEST['deviceType'])){
					$this->showdefaultMessage("Please provide device type");
					return;
				}
				$deviceType = (int)$_REQUEST['deviceType'];

				if (empty($_REQUEST['deviceToken'])){
					$this->showdefaultMessage("Please provide device token");
					return;
				}
				$deviceToken = $_REQUEST['deviceToken'];

				$this->load->model("ApiUserModel","usermodel");
				$status = $this->usermodel->updateDeviceToken($deviceType,$userId,$deviceToken);
				if($status){
					echo json_encode(array('Status'=>1,'Message'=>'Successfully'));
					return;
				}else{
					echo json_encode(array('Status'=>0,'Message'=>'Server error, please try again later'));
					return;
				}

			}



}
?>
