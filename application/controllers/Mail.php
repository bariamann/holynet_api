<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller 
{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
		  $this->load->model('loginmodel');
		   $this->load->library('email');
		   $this->no_cache();
	}
	public function index()
	{
		$this->load->view('loginview');	
	}
	
	public function login()
	{
			$user=$this->input->post('inputUser');
			$password=$this->input->post('inputPassword');
		
			$data=$this->loginmodel->checkadmin($user,$password);
			// echo $data['id'];
			if($data['flag']==1) 
			{
				 $sessiondata = array('username'=>$user,'loginuser' => TRUE,'id'=>$data['id']);
				 $this->session->set_userdata('data',$sessiondata);
				 echo 1;
			}
			else if($data['flag']==0)
			{
				echo 0;
			}
	}
	
	public function redirecting()
	{
			$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				echo 1;
			}
			else
			{
				echo 0;
			} 
	}
	public function logout()
	{
		$sessdata=$this->session->userdata('data');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('loginuser');
		$this->session->sess_destroy();
		redirect(base_url());
	}
	public function check_email()
	{
		 $email=$this->input->post('email');	
		 $data=$this->loginmodel->chk_email($email);
		 if($data['flag']==1)
		 {
			
			$status=1;
			$pwd=$data['password'];	 
			$config = Array( 
					'protocol' => 'smtp', 
					'smtp_host' => 'ssl://smtp.googlemail.com', 
					'smtp_port' => 465,
					'smtp_user' => 'singhdeepikad2009@gmail.com', 
					'smtp_pass' => '18nov1989',
					 'mailtype' => 'html',
					'charset' => 'iso-8859-1',
				    'wordwrap' => TRUE ); 
					
					 $this->email->initialize($config);
						$this->load->library('email', $config); 
					$this->email->set_newline("\r\n");
					$this->email->from('singhdeepikad2009@gmail.com', 'deepika singh');
					$this->email->to($email);
					$this->email->subject('Forgot Password'); 
					$this->email->message($pwd);
					if (!$this->email->send()) {
					//	show_error($this->email->print_debugger());
						$msg='Sorry Unable to send email...'; }
					else {
						
						$msg='Your e-mail has been sent!';
					}
				
		 }
		 else
		 {
			$msg="Email Id does not match";
			$status=0;	
			$pwd="";
		 }
		$data_value=array($msg,$status,$pwd);
		echo json_encode($data_value);
	}
	 protected function no_cache()
	 {
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
	
}
