<?php
class Videotype extends CI_Controller
{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
		  $this->load->model('videotypemodel');
	}
	public function index()
	{
			$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				//$this->load->view('header');
				$this->load->view('videotypeview');
			}
			else
			{
				redirect('Holynetlogin');
			}
	}
	
	public function displayVideotype()
	{
		//$this->load->model('videotypemodel');
		$this->videotypemodel->displayVideoType();
	}
	
	public function editVideotype($id)
	{
		$this->db->select('*');
		$this->db->from('videotypemaster');
		$this->db->where('Id',$id);
		$query = $this->db->get();
		$result = $query->result();
		echo json_encode(array("Status"=>1,"Data"=>$result));
	}
	public function updateVideotype($id)
	{
		$nameenglish=$this->input->post('NameEnglish');
		$namearabic=$this->input->post('NameArabic');
		$sql="UPDATE videotypemaster set nameEnglish='".$nameenglish."' , nameArabic='".$namearabic."'  where Id=".$id;
		
			$this->db->query($sql);
			if($this->db->affected_rows()>0)
			{
				echo json_encode(array("Status"=>1,"Message"=>"Video Type Updated Successfully"));
			}
	}
	
	public function displayProfiletype()
	{
		//$this->load->model('videotypemodel');
		$this->videotypemodel->displayProfileType();
	}
	
	public function editProfiletype($id)
	{
		$this->db->select('*');
		$this->db->from('profiletypemaster');
		$this->db->where('Id',$id);
		$query = $this->db->get();
		$result = $query->result();
		echo json_encode(array("Status"=>1,"Data"=>$result));
	}
	public function updateProfiletype($id)
	{
		$nameenglish=$this->input->post('NameEnglish');
		$namearabic=$this->input->post('NameArabic');
		$sql="UPDATE profiletypemaster set nameEnglish='".$nameenglish."' , nameArabic='".$namearabic."'  where Id=".$id;
		
			$this->db->query($sql);
			if($this->db->affected_rows()>0)
			{
				echo json_encode(array("Status"=>1,"Message"=>"Profile Type Updated Successfully"));
			}
	}
	
	public function displayLanguage()
	{
		//$this->load->model('videotypemodel');
		$this->videotypemodel->DisplayLanguage();
	}
	
	public function displayCountry()
	{
		//$this->load->model('videotypemodel');
		$this->videotypemodel->DisplayCountry();
	}
	public function editLanguage($id)
	{
		$this->db->select('*');
		$this->db->from('languagemaster');
		$this->db->where('Id',$id);
		$query = $this->db->get();
		$result = $query->result();
		echo json_encode(array("Status"=>1,"Data"=>$result));
	}
	
	public function editCountry($id)
	{
		$this->db->select('*');
		$this->db->from('countrymaster');
		$this->db->where('Id',$id);
		$query = $this->db->get();
		$result = $query->result();
		echo json_encode(array("Status"=>1,"Data"=>$result));
	}
	public function updateLangugae($id)
	{
		$language=$this->input->post('Language');
	
		$sql="UPDATE languagemaster set name='".$language."' where Id=".$id;
		
			$this->db->query($sql);
			if($this->db->affected_rows()>0)
			{
				echo json_encode(array("Status"=>1,"Message"=>"Language Updated Successfully"));
			}
	}
	
	public function updateCountry($id)
	{
		$name_en=$this->input->post('Name_en');
		$name_arb=$this->input->post('Name_arb');
	
		$sql="UPDATE countrymaster set nameEnglish='".$name_en."' , nameArabic='".$name_arb."' where Id=".$id;
		
			$this->db->query($sql);
			if($this->db->affected_rows()>0)
			{
				echo json_encode(array("Status"=>1,"Message"=>"Country Updated Successfully"));
			}
	}
}
?>
