<?php
class Rejectedreport extends CI_Controller
{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
	}
	public function index()
	{
			$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				//$this->load->view('header');
				$this->load->view('rejectlistview');
			}
			else
			{
				redirect('Holynetlogin');
			}
	}
	public function getrejectpost()
	{
		$this->db->select('*');
		$this->db->from('rejectcontentmaster');
		$this->db->where('type',3);
		$query = $this->db->get();
		$result = $query->result();
		if($result > 0)
		{
			echo json_encode(array("Status"=>1,"Data"=>$result));
		}
		else
		{
			echo json_encode(array("Status"=>0,"Message"=>"No Result found"));
		}
	}
		public function viewabuseimage()
	{
		$this->load->view('abuseimageview');
	}
	public function viewabusedetail()
	{
		$this->load->view('abusevideoview');
	}
}
?>
