<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/* CONSTANT CODE FOR PROJECT
	Created By: Nishit Patel
*/

define('SERVER_PATH','http://dev.mobileartsme.com/holynet/');
define('S3_PATH','https://s3.amazonaws.com/devemobilesrts/Holynet/');
define('VIDEO_URL',S3_PATH.'Videos/');
define('VIDEO_THUMB_URL',S3_PATH.'Videos/Thumb/');
define('IMAGE_URL',S3_PATH.'Images/');
define('IMAGE_THUMB_URL',S3_PATH.'Images/Thumb/');
define('PROFILE_URL',S3_PATH.'ProfilePicture/');
define('PROFILE_THUMB_URL',S3_PATH.'ProfilePicture/Thumb/');

define('DELETE_VIDEO_URL','Holynet/Videos/');
define('DELETE_VIDEO_THUMB_URL','Holynet/Videos/Thumb/');
define('DELETE_IMAGE_URL','Holynet/Images/');
define('DELETE_IMAGE_THUMB_URL','Holynet/Images/Thumb/');
define('DELETE_PROFILE_URL','Holynet/ProfilePicture/');
define('DELETE_PROFILE_THUMB_URL','Holynet/ProfilePicture/Thumb/');


define('BUCKET','devemobilesrts');

define('ENCRYPTION_KEY','holynet_key');

define('IS_DEVELOPMENT',TRUE);
define('VIDEO_TYPE',1);
define('IMAGE_TYPE',2);
define('POST_TYPE',3);
define('USER_TYPE',4);
define('PLAYLIST_TYPE',5);

define('LIKE',1);
define('DISLIKE',2);

define('REJECT_NOTIFICATION',1);
define('SHARE_NOTIFICATION',2);
define('LIKE_DISLIKE_NOTIFICATION',3);
define('FOLLOWING_NOTIFICATION',4);
define('COMMENT_NOTIFICATION',5);

define('ANDROID_NOTIFICATION',1);
define('IOS_NOTIFICATION',2);

define('APPROVED_LIMIT',3);

define('IS_APN_DEVELOPMENT',FALSE);

define('DEBUG_APN_WITH_SAMPLE',FALSE);

define('SUPER_USER_ID',81);
