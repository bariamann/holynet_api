<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome to Tannourine</title>

  <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
  
<link rel="stylesheet" href="<?=base_url('assets');?>/datingcss/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"  />
<link rel="stylesheet" href="<?=base_url('assets');?>/datingcss/css/style.css">
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js"></script>

<script src="<?=base_url('assets');?>/datingjs/js/fileinput.min.js" type="text/javascript"></script>
<script src="<?=base_url('assets');?>/datingjs/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets');?>/datingjs/js/ajaxFileUpload.js"></script>
<link href="<?php echo base_url('assets')?>/datingcss/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="http://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets')?>/datingjs/js/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url('assets')?>/datingjs/css/datepicker.css" rel="stylesheet">



    <!-- Custom CSS -->
    <style>
    
    body
    {
        padding-top: 70px;
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('Tannourine');?>">Tannourine</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Settings <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                         <li><a href="<?php echo base_url('Category');?>">New Category</a></li>
                        <li><a href="<?php echo base_url('NotificationTune');?>">Notification Tune</a></li>
                      </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('Tannourine');?>">New Notification</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('History');?>">Existing Notification</a>
                    </li>
                   <!-- <li>
                        <a href="#">Resource Master</a>
                    </li>-->
                    <li>
                        <a href="<?php echo base_url('User');?>">Users</a>
                    </li>
                    
                </ul>
                
                  <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
							 <li><a href="<?php echo base_url('login/logout');?>">Logout</a></li>
							 <li><a href="<?php echo base_url('login/logout');?>">Change Password</a></li>
							</ul>
                  	  </li>
                  </ul>
            </div>
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    

    <!-- Page Content -->
    
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
