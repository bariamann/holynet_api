
<nav class="w3-sidenav w3-collapse w3-white w3-animate-left w3-large" style="z-index:3;width:300px;" id="mySidenav">
<ul class="w3-navbar w3-black w3-center">

  <li class="navimg">
    <a style="color:#ffffff;padding-left:40px;">
    	<?php
		$sesscheck=$this->session->userdata('data');
			if($sesscheck['loginuser']==1)
			{
				echo "Welcome,".$sesscheck['username'];
			}
		?>

	</a>
  </li>
</ul>

<div id="nav01">
 <div class="w3-container  w3-theme w3-lobster" style="padding-left:5px;">
   <table style="width:100%">
     <tr>
       <td style="vertical-align:middle; padding-left:10px; width:30%;"><img src="<?php echo base_url()?>assets/images/ic_luncher_icon.png" height="50px"></td>
       <td style="vertical-align:middle; width:70%;"><h1 style="color:#FFFFFF; height:50px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
    font-size: 34px;">Holynet</h1></td>
     </tr>
   </table>
  </div>
  <a href="javascript:void(0)" onClick="w3_close()" class="w3-text-teal w3-hide-large w3-closenav w3-large">Close X</a>
  <a href="<?php echo  base_url()."Holynetlogin/logout";?>" class="w3-text-teal w3-hide-large w3-closenav w3-large"> <span><i class="fa fa-cog"></i></span> Logout</a>
  <a href="<?php echo  base_url()."Admindashboard";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-tachometer"></i></span> Dashboard</a>
  <a href="<?php echo  base_url()."Userlist";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-users"></i></span> Users</a>
  <a href="<?php echo  base_url()."Videolist";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-video-camera"></i></span> Videos</a>
   <a href="<?php echo  base_url()."Imagelist";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-picture-o"></i></span> Images</a>
   <a href="<?php echo  base_url()."Postlist";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-comments"></i></span> Posts</a>
  <a href="<?php echo  base_url()."Abusereport";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-newspaper-o"></i></span> Abused Content</a>
   <a href="<?php echo  base_url()."Abuseuser";?>" style="color:black" class="w3-padding"> <span><i class="fa fa-newspaper-o"></i></span> Abused User</a>
    <a class="w3-padding" href="<?php echo  base_url()."Videotype";?>"style="color:black"> <span><i class="fa fa-cog"></i></span>  Settings </a>
	<div class="w3-dropdown-hover">
    <a class="w3-padding" href="javascript:void(0)" style="color:black"><i class="fa fa-users "></i>  Account <i class="fa fa-caret-down"></i></a>
    <div class="w3-dropdown-content w3-white w3-card-4">
      <a href="<?php echo  base_url()."ChangePassword";?>" style="color:black">Change Password</a>
    </div>
  </div>


 <!--  <div class="w3-dropdown-content w3-white w3-card-4">
   <div class="w3-dropdown-hover">

      <a href="javascript:void(0)" style="color:black">  Profile Type</a>
      <a href="javascript:void(0)" style="color:black"> Language</a>
      <a href="javascript:void(0)" style="color:black"> Country</a>
	   <a href="<?php echo  base_url()."Videotype";?>" style="color:black"> Video Types</a>

</div>-->
<div id="nav02"></div>

<div id="nav03"></div>
</nav>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function()
{
//alert(234);
    $("[href]").each(function() {
    if (this.href == window.location.href)
	{
        $(this).addClass("w3-light-grey");
     }
    });
});
</script>
