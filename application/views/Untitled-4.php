<body class="padTop53" >

    <!-- MAIN WRAPPER -->
    	<div id="wrap">
        
        <!-- HEADER SECTION -->
        <div id="top">

        <nav class="navbar navbar-inverse navbar-fixed-top" style="padding-top: 10px;">
      <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!--MAIN SALON LOGO SECTION -->
                <header class="navbar-header">
                    <a href="index.html" class="navbar-brand">
                    Private Document Management System
                    </a>
                </header>
                
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right">
                    
                   	<li><span style='color:blue;font-size:1.2em;font-weight:bold'><?php $sesscheck=$this->session->userdata('data');	
									if($sesscheck['loginuser']==1)
									{
										echo $sesscheck['username'];//$sesscheck['username'];
									}
					?></span></li>
                    
                    <!--ADMIN SETTINGS SECTIONS -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp;<i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                           <li>
                             <a href="<?php echo base_url('Adminloginpage/logout');?>"><i class="icon-signout"></i> Logout </a>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

         </nav>

        </div>
        <!-- END HEADER SECTION -->
        
        <!-- MENU SECTION -->
       <?php $this->load->view('leftmenu');?>
        <!--END MENU SECTION -->

        <!--PAGE CONTENT -->
        <div id="content">
             
            <div class="inner col-lg-12" style="min-height:500px;">
                <div class="row ">
                    <div class="col-lg-12">
                        <h3>Manage Files</h3>
                    </div>
                </div>
                <hr />
               	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" align="right" >
							<b><span id="successmsg" style="text-align:justify:font-weight:1.8em;color:#FF0000;"></span></b>
            <button class="btn btn-default" data-toggle="modal" data-target="#Filemodal">Add New</button>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content" style="padding-top:10px">
                                    <div class="tab-pane active" id="sectionLIST">
                                        <div class="row">
                                            <div class="col-md-10 col-xs-offset-1" >
                                                <div class="table-responsive">
         										 <table class="table table-striped table-bordered table-hover" id="file-table">
                                                    <thead>
                                                      		  <th>Description</th>
															  <th>Group Name</th>
															  <th>File</th>
           												     <th style="display:none"></th>
															 <th style="display:none"></th>                      
                                                    </thead>
                                                    <tbody id="file-body"> 

                                                    </tbody>
													</table>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>


   								 <div class="modal fade" id="Filemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                                    <div class="modal-content">
									<form method="post" id="formaddvideos" name="addvideos" class="form-horizontal" enctype="multipart/form-data">
                                   
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="H2">New File</h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Description </label>
                                    <div class="col-md-6">
									<textarea class="form-control" placeholder="Enter Description" name="Description" id="Description" onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()"></textarea>
            					
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Select Group </label>
                                    <div class="col-md-6">
									<?php
											$this->db->select('*');
											$this->db->from('groupmaster');
											$query=$this->db->get();
											$result=$query->result();
									?>
                                        <select  name="GroupId" class="form-control" id="GroupId" onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()">
										<option value="none" selected="selected">---------Select GroupName-------</option>
											<?php
											foreach($result as $row)
											{
											?>
												<option value="<?php echo $row->Id;?>"><?php echo $row->Name;?></option>
											<?php
											}
											?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                        <label class="control-label col-lg-4" id="label">Upload File</label>
                        <div class="col-lg-8">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-group">
								   
                                        <input type="file" id="file" name="file" onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()"/>
                         	   </div>
                        	</div>
							
							<input type="hidden" id="UserId" value="1" name="UserId">
							 <div class="row"  style="padding-bottom:3px;"   >
								 		<div class="col-sm-12">
                        						<div id="videoPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
                  	  </div>
                        </div> <label class="control-label"  id="displaymsg"></label>
                                        </div>
										
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <span id="savebutton">  <button type="button" class="btn btn-primary" name="save" id="save">Save</button></span>
                                        </div>
                                        </form>
										<div id="loader-icon" style="display:none;"><img src="<?php echo base_url()?>images/loading.gif" width="30%" height="30%"/></div>
                                    </div>
                        </div>
</div>
                                </div>
                            </div>
                        </div>                                        
                    </div>
                </div>
            </div>
        </div>
        <!--END PAGE CONTENT -->

        
    	</div>
	<script src="<?php echo base_url('assets/plugins/jquery-2.0.3.min.js')?>"></script>
	  <script>
    	//Code To Perform Insert
	$(document).ready(function ()
		{
				 filluser();
			$('#file-table').dataTable();
			$('#Filemodal').on('hidden.bs.modal', function () 
			{
				$('.modal-body').find('textarea,input,select').val('');
				$('#displaymsg').html('');
				$('#loader-icon').hide();
				$('#save').removeClass('disabled');
				
			});
		/*	$("#fileupload").on('submit',(function(e) {
			alert(4);
				e.preventDefault();
				$.ajax({
					url: "http://dev.mobileartsme.com/private_documents/Apis/uploadContent?",
					type: "POST",
					data:  new FormData(this),
					mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
					success: function(Data)
					{
						alert(2332);
					},
					error: function(data) 
					{
						alert(data);
					}           
			   });
   		 }));*/
			
			$('#save').click(function()
			{
				var desc=$('#Description').val();
				var groupname=$("#GroupId").val();
				var file=$('#file').val();
				var userid=1;
				
				if(desc=="")
				{
					$('#displaymsg').html("<span class='alert-danger'>Please Enter Description cannot be blank</span>");
					return false;
				}
				if(groupname=="none")
				{
					$('#displaymsg').html("<span class='alert-danger'>Please Select GroupName</span>");
					return false;
				}	
				if(file=="")
				{
					$('#displaymsg').html("<span class='alert-danger'>Please Select File to Upload</span>");
					return false;
				}	
				if(desc!="" || groupname!="" || file!="")
				{
						$('#displaymsg').html("");
				}
				if($('#file').val()) 
				{
					$('#loader-icon').show();
					$('#save').addClass('disabled');
				}
				var formdata = new FormData($('#formaddvideos')[0]);
				//alert(formdata);
				$.ajax({
					url: "http://dev.mobileartsme.com/private_documents/Apis/uploadContent?",
					type: "POST",
					data:  formdata,
					mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
					success: function(Data)
					{
						//alert(2332);
						var obj = $.parseJSON(Data);
						//alert(obj['Message']);
						$("#Filemodal").modal('toggle');
						$('#successmsg').html(obj['Message']).fadeIn('slow');
						$('#successmsg').delay(2000).fadeOut('slow');
						filluser();
						
					},
					error: function(data) 
					{
						//var obj = $.parseJSON(data);
						//alert(obj['Message']);
					}           
			   });
			});
		
		});
		//code to Perform delete
		function deletefile(a)
		{
			var fileid=a;
			alert(fileid);
			if (confirm("Are you sure you want to Delete Group!") == true) 
			{
				$.ajax({
					url : "http://dev.mobileartsme.com/private_documents/Apis/deleteContent?ContentId="+fileid+"",
					type : "get",
					success:function(Data)
					{
						
						var obj = $.parseJSON(Data);
						//alert(obj['Message']);
						//$("#Filemodal").modal('toggle');
						$('#successmsg').html(obj['Message']).fadeIn('slow');
						$('#successmsg').delay(2000).fadeOut('slow');
						filluser();
						
					},
					error:function()
					{
						alert('error');
					}
					});	
			}
		}
    	//Code to Fill User Table 
     	function filluser()
     	{
     		$.ajax({
				url : "http://dev.mobileartsme.com/private_documents/Apis/getAllContents?PageIndex=0",
				type : "post",
				success:function(data)
				{
					
					var obj= $.parseJSON(data);
					//alert(obj);
					var userlist=obj['Data'];
					$('#file-body').html('');
					for(var i =0 ;i<userlist.length;i++)
					{
						var $raw= $("<tr>");
						$raw.append("<td>"+userlist[i]['Description']+"</td>")	;
						$raw.append("<td>"+userlist[i]['GroupName']+"</td>")	;
						$raw.append("<td><a href="+userlist[i]['URL']+"><img src="+userlist[i]['Thumb']+" weight='100px' height='90px'></a></td>")	;
						$raw.append("<td><a href='#'><center><button type='button' class='btn btn-success btn-circle' onclick='editfile("+userlist[i]['Id']+")'><span class='glyphicon glyphicon-pencil'></span></button></center></td>");
						$raw.append("<td><center><button type='button' onclick='deletefile("+userlist[i]['Id']+")' class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash'></span></button></center></td>");
						$raw.append("</tr>");
						$("#file-body").append($raw);
					}
					
				},
				error:function()
				{
					alert('error');	
				}
			});	
			
			$('#file-table').dataTable({
    				"bFilter":false,
    				"bLengthChange": false,
				"aoColumns": [
					{"bSearchable": true}, 
					{"bSearchable": true}
				]
				});
     	}
		
		function editfile(id)
		{
			$.ajax({
						type :  "get",
						datatype : "JSON",
						url: "<?php echo site_url('Managefile/viewrecord');?>/"+id,
						success:function(data)
						{
								var d = JSON.parse(data);
								//alert(d);
							$.each(d, function (key, value) 
							{
								callmodal(id,value.Description,value.GroupId);
							});
						}
			});
		}
		function callmodal(id,Description,groupid)
		 {
			$("#Filemodal").modal('show');
			$("#H2").html('<B style="color:#0197d8;">Edit</B>');
	
			$('#Description').val(Description);
			$('#GroupId').val(groupid);
			$('.fileupload').hide();
			$('#label').hide();
			$('#save').hide();
			$('#savebutton').html('<button type="button" class="btn btn-primary" id="edit" name="Edit">Save</button>');
			
			$('#edit').click(function()
			{
				alert(34535);
				var Description=$('#Description').val();
				var groupid=$('#GroupId').val();
				
				/*if(name=="")
				{
					$("#displaymsg").html("<span class='alert-danger'>Please Enter GroupName</span>");
					return false;
				}
				*/
					$.ajax({
								type :  "POST",
								datatype : "JSON",
								url: "http://dev.mobileartsme.com/private_documents/Apis/updateContent?Description="+Description+"&GroupId="+groupid+"&ContentId="+id+"",
								success:function(data)
								{
											var d = JSON.parse(data);
											//alert(d.Status);
										if(d.Status==1)
										{	
											//alert("File Updated Successfully");
											$("#Filemodal").modal('toggle');
											$('#successmsg').html("File Detail Updated Successfully").fadeIn('slow');
											$('#successmsg').delay(2000).fadeOut('slow');
											filluser();
										}
								}
						});
				});
		}

		
        </script>
	
	<script src="<?php echo base_url('assets/plugins/validationengine/js/jquery.validationEngine.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/validationengine/js/languages/jquery.validationEngine-en.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/validationInit.js')?>"></script>
	<script>
		$(function() { formValidation(); });
	</script>