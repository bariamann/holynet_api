<head>

      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
	 <title>Approve/Reject Videos</title>
 </head> 
<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="col-sm-12 form-box">
					<div class="form-top">
							<div class="form-top-left"><h3 style="color:#0197d8;">Approve / Reject Videos</h3></div>
							
							</div>
								<div class="form-bottom col-lg-12"><div class="table-responsive col-lg-12">
									<div class="col-lg-12 text-center"></div>
								</div>
									<div class='row'>
											<div class='col-sm-2' style='background-color: #f1f1f1;height: 100%;'>
													<div class="col-sm-12"  id="categorymenu">
														
													</div>
													</div><div id="pagination" align="center" class='col-sm-10'></div><div class='col-sm-10'><div id='display'></div></div>
											</div>
									</div>
					</div>
			</div>
	</div>
</div>

		
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"> 
			<span id="header"> </span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="formdata">
		 <div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Title</label>
			                        	<input type="text" name="title" id="title" placeholder="New Video Title" class="form-first-name form-control">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Video Description</label>
			                        	<textarea name="description" id="description" placeholder="New Video Description" 
			                        				class="form-about-yourself form-control"></textarea>
			                        </div>
			                        <div class="form-group">
									<?php
											$this->db->select('*');
											$this->db->from('categorymaster');
											$this->db->where('Status',1);
											$query=$this->db->get();
											$result=$query->result();
											//print_r($result);
									
								?>
			                        	<select class="form-email form-control" id="category" name="category">
											<option value="none" selected="selected">-------------------------Select New Category-------------------------</option>
											<?php
											foreach($result as $row)
											{
											?>
												<option value="<?php echo $row->Id;?>"><?php echo $row->Name;?></option>
											<?php
											}
											?>
										</select>
			                        </div>
									 <div class="form-group">
									 <input type="text" name="hashtag" id="hashtag" placeholder="Edit Hashtag" class="form-first-name form-control">
									 </div>
									<div align="right">
			                        <input type="submit" class="btn btn-primary" value="Approve Video" id="Updatevideo" name="Updatevideo">
									<input type="submit" class="btn btn-primary" value="Cancel" id="Cancel" name="cancel">
			                    </div>
								
        </div>
		 <div class="alert alert-danger" id="warning">
					<strong id="error"></strong>
		</div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
var id;
var pageindex1=0;
$(document).ready(function() 
{
	$('.alert').hide();
	getcategory();
 });

function getcategory()
{
	$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "http://dev.mobileartsme.com/holytube/Apis/getCategories",
						success:function(data)
						{
								var d = JSON.parse(data);
								if(d.Status==1)
								{
									str='';
									str1='';
									var i=1;
									//const j=0;
									$.each(d.Data, function (key, value) 
									{
										catid=value.Id;
										name=value.Name;
										//alert(name);
		str+='<ul class="nav nav-pills nav-stacked"><li><a  href="#" id="buttons'+catid+'" name="buttons[]" class="anchor" onclick="getvideos('+catid+')">'+value.Name+'</a></li></ui>';
											i++;
											
									});
											$('#categorymenu').html(str);
											$('#buttons1').click();
											$('#buttons1').css({'color':'#ff1a1a','text-align':'center','font-weight': 'bold','background':'#FFFFFF'});	
								}
						}
			});
}
function getvideos(videoid,pageindex,name) 
{
	$('.anchor').click(function()
	{
			$('.anchor').removeAttr('style');
			$('#buttons'+videoid+'').css({'color':'#ff1a1a','text-align':'center','font-weight': 'bold','background':'#FFFFFF'});	
	});
	
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "http://dev.mobileartsme.com/holytube/Apis/getPendingVideos?CategoryId="+videoid+"&pageindex="+pageindex1+"",
			crossDomain: true,
            data: "",
			success:function(responseData,textStatus,jqXHR)
			{
				var d = JSON.parse(responseData);
				//alert(d.Status);
				if(d.Status==0)
				{
					$('#display').html('<div id="four-columns" class="grid-container" style="border:thick;font-size:1.8em;color:#ff1a1a"><center>No Videos Available</center></div>');
				}
				else
				{
					//alert(d.Status);
					str='';
					count=0;
					totalnopage=d.TotalPage;
						str+="<br/><div id='four-columns' class='grid-container'><ul class='rig columns-4'>"
								$.each(d.Data, function (key, value) 
								{
									id=value.Id;
									var title=value.Title;
									var desc=value.Description;
									var dur=value.Duration;
									var uplaoddate=value.UploadDate;
									var url=value.FileURL;
									var thmub=value.FileThumb;
									var userid=value.UserId;
									var username=value.UserName;
									
								
									str+='<li><div id="overlaytop"></div><a href='+value.FileURL+'><img src='+value.FileThumb+' style="padding-top:0px;height:150px"></a><div><h6 style="color:#0197d8;">'+value.Title+'</h6><div>Submitted Date:'+value.UploadDate+'</div><div>Duration: '+value.Duration+'</div><div>'+value.Description+'</div>';
					str+='<div style="border-top:solid 1px #d2d2d2;padding-top:3px;padding-bottom:5px;"> <div class="row"><div class="col-sm-6"><a class="btn btn-xs" style="background-color:#00CC66;height:20px;width:100%" id="Approve" href="javascript:void(0)" onclick="ConfirmApprove('+value.Id+')" title="Approve" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-ok" style="color:#FFFFFF;text-align:left;"></span></a></div> <div class="col-sm-6"><a class="btn btn-danger btn-xs" style="height:20px;width:100%" id="delete" href="javascript:void(0)" onclick="ConfirmDelete('+value.Id+')" title="Reject" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-remove"></span></a></div></div></div></li>';
										count++;
										counting=count;
										
									if(count!=counting)
									{	
										str+='</ul></div><li></li>';
										count=0;
									}	
								});
				
									$('#display').html(str);
									str1='';
									if(d.TotalPage>1)
										{
											str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66">Previous</span></div>';
											str1+='<div  class="col-sm-6"><span id="next" style="color:#00CC66">Next</span></div>';
										
												$('#pagination').html(str1);
										
										$('#prev').click(function()
										{
											pageindex1--;
											if(pageindex1>=0)
											{
												getvideos(videoid,pageindex1);
											}
										});
										
										$('#next').click(function()
										{	
											pageindex1++;
											if(totalnopage>pageindex1)
											{
												
												getvideos(videoid,pageindex1);
											}
											else
											{
											
											}
											
										});
									}
				}
			}
			});
}
							
function ConfirmApprove(id)
{
//alert(id);
			$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/viewrecord');?>/"+id,
						success:function(data)
						{
								var d = JSON.parse(data);
								//alert(d);
							$.each(d, function (key, value) 
							{
							
								callmodal(id,value.Title,value.Description,value.CategoryId,value.Keywords);
							});
						}
			});
			
}

function callmodal(id,tit,desc,cat,keyword)
 {
	$("#myModal").modal('show');
	$("#header").html('<B style="color:#0197d8;">Approve'+" "+tit+'</B>');
	$('#title').val(tit);
	$('#description').val(desc);
	$('#category').val(cat);
	$('#hashtag').val(keyword);
 	$('#Updatevideo').click(function()
	{
		var title=$('#title').val();
		var description=$('#description').val();
		var category=$('#category').val();
		var keyword=$('#hashtag').val();
		
		if(title=="")
		{
			$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Video Title");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
		}
		if(description=="")
		{
			$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Video Decsription");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
		}
		if(category=="none")
		{
			$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please Select Video Category");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
		}
		if(keyword=="")
		{
			$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Video keyword");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
		}

	
			$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/approveVideo');?>/"+id,
						data : {title:title,description:description,category:category,keyword:keyword},
						success:function(data)
						{
								if(data==1)
								{
									$("#myModal").modal('hide');
									$('.alert-success') .fadeIn( function() 
								  	 {
										$('.alert-success').show();
										$("#Message").html("Video Approved");
										  setTimeout( function()
										  {
											$('.alert-success').fadeOut("fast");
											$('.alert-success').hide();
										  }, 2000);
										 });
									location.reload();
								}
						}
				});
		});
		
		$('#Cancel').click(function()
		{
			$("#myModal").modal('hide');
		});
}

function ConfirmDelete(id)
{
	//alert(id);
	$("#myModal").modal('show');
	$('.form-group').hide();
	$('#header').html("Are you sure you want to Reject Category");
	$('#formdata').html('<input type="button" class="btn btn-primary" value="Reject" id="deletevideo" name="video"> <input type="button" class="btn btn-primary" value="Cancel" id="cancelvideo" name="cancelvideo">');
	$('#deletevideo').click(function()
	{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/deletevideo');?>/"+id,
						success:function(data)
						{
								if(data==1)
								{
									$("#myModal").modal('hide');
									$('.alert-success') .fadeIn( function() 
									   {
											$('.alert-success').show();
											$("#Message").html("Video Rejected");
											  setTimeout( function()
											  {
												$('.alert-success').fadeOut("fast");
												location.reload();
											  }, 2000);
									   });
								}
						}
			});
	});
	$('#cancelvideo').click(function()
	{
			location.reload();
	});
}
</script>
