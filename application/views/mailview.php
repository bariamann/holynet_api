<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    
<title>Tannourine Login</title>
<style>

body, html 
{
    height: 100%;
}

.card-container.card 
{
    width: 350px;
    padding: 40px 40px;
}

.btn 
{
    font-weight: 700;
    height: 36px;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    cursor: default;
}

/*
 * Card component
 */
.card 
{
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 25px 30px;
    margin: 0 auto 25px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.profile-img-card 
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}

/*
 * Form styles
 */
.profile-name-card
 {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    min-height: 1em;
}

.reauth-email 
{
    display: block;
    color: #404040;
    line-height: 2;
    margin-bottom: 10px;
    font-size: 14px;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin #inputUser,
.form-signin #inputPassword 
{
    direction: ltr;
    height: 44px;
    font-size: 16px;
}

.form-signin input[type=email],
.form-signin input[type=password],
.form-signin input[type=text],
.form-signin button 
{
    width: 100%;
    display: block;
    margin-bottom: 10px;
    z-index: 1;
    position: relative;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin .form-control:focus {
    border-color: rgb(104, 145, 162);
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
}

.btn-signin {
    /*background-color: #4d90fe; */
    background-color: rgb(104, 145, 162);
    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
    padding: 0px;
    font-weight: 700;
    font-size: 14px;
    height: 36px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: none;
    -o-transition: all 0.218s;
    -moz-transition: all 0.218s;
    -webkit-transition: all 0.218s;
    transition: all 0.218s;
}
.forgot-password {
    color: rgb(104, 145, 162);
}

.forgot-password:hover,
.forgot-password:active,
.forgot-password:focus{
    color: rgb(12, 97, 33);
}
</style>
</head>

<body>
 <!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container" id="login">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="<?php echo base_url('images/login_icon.png');?>" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="inputUser" class="form-control" placeholder="Username" required autofocus>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <div id="remember" class="checkbox">
                    <label id="displaymsg" class="forgot-password">
                       
                    </label>
                </div>
                <input type="button" class="btn btn-lg btn-primary btn-block btn-signin" type="submit" id="Loginbtn" value="Sign in"/>
            </form><!-- /form -->
            <div class="forgot-password" style="padding-top:10px;">
			<a href="#" id="forgotpwd">Forgot Password </a>
           </div>    
           
        </div><!-- /card-container -->
    </div><!-- /container -->
	
	<div class="container">
        <div class="card card-container" id="forgotpassword">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="<?php echo base_url('images/forgot_password.png');?>" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-forgotpassword">
                <span id="reauth-email" class="reauth-email"></span>
				<div style="padding-bottom:15px;">
                <input type="text" id="email" class="form-control" placeholder="Enter Email" required autofocus>
				</div>
				
                <input type="button" class="btn btn-primary btn-block" type="submit" id="btn_forgotpwd" value="Send Password"/>
				 <input type="button" class="btn btn-primary btn-block" type="submit" id="btn_login" value="Login"/>
				
            </form><!-- /form -->
            <div id="displaymsg" class="forgot-password" style="padding-top:10px;">
           </div>    
           
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() {
    // DOM ready
	$('#login').show();
	$('#forgotpassword').hide();
	 "use strict";
	var options = {
	  'btn-error': '<i class="fa fa-remove"></i>',
	  'msg-error': 'Wrong login credentials!',
	  'useAJAX': true,
	  };
	$('#Loginbtn').click(function()
	{	
		//alert("clicked");
		document.getElementById('displaymsg').style.visibility="visible";
    	$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('Mail/login');?>",
						data	: {inputUser: $('#inputUser').val(),inputPassword: $('#inputPassword').val()},
						success:function(response)
						{
						//	alert(response);
							if(response==1)
							{
								$('#Loginbtn').addClass('disabled');
								document.getElementById('displaymsg').style.visibility="hidden";
								window.location.href = '<?php echo base_url('Tannourine');?>';
							}
							else if(response==0)
							{
								$('#displaymsg').addClass('error').html(options['btn-error']);
  								$('#displaymsg').addClass('show error').html(options['msg-error']);
								
								$('#displaymsg').click(function()
								{
									document.getElementById('displaymsg').style.visibility="hidden";
									document.getElementById('chnagepwd').style.visibility="visible";
								});
							}
							
						}
				});
	});
	
	$('#forgotpwd').click(function()
	{
		$('#login').hide();
		$('#forgotpassword').show();
	});
	
	
	$('#btn_login').click(function()
	{
			window.location.href = '<?php echo base_url('Login');?>'; 
                        
	});
    $('#btn_forgotpwd').click(function()
	{
        var email=$('#email').val();
         var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(email=="")
        {
            alert("Please Enter Email");
            return false;
        }
       else if(!filter.test(email))
		{
            alert("Please Enter valid Email");
            return false;
        }
		$.ajax({
                type :  "POST",
				datatype : "JSON",
				url: "<?php echo site_url('login/check_email');?>",
				data	: {email},
				success:function(response)
				{
                    var res=JSON.parse(response);
                    if(res[1]==1)
                    {
                       alert(res[0]);
                    } 
                    else {
                        alert(res[0]);
                    }               
                }
        });
                        
	});
});

</script>
</html>
