<!DOCTYPE html>
<html>
<title>Holynet-Userprofile</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background:white;
width:300px;
height:60%;
border:0px solid #000;
z-index: 9002;
overflow-y: scroll;
overflow-x:hidden;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3 id="username" style="text-transform:capitalize;"></h3>
				</div>
				<div class="w3-row-padding"><p></p>
 				<div class="row">
					<div class="col-sm-8" id="tableuserlist">   
					</div>
 					
					<div class="col-sm-4" id="tabletotal">
						
					</div>
					
 			</div>
				<div id="tableplaylist">
						
					</div>
			<div id='loadingmessage' style='display:none'>
  <center><img src='<?php echo base_url()?>/loading.gif' width="10%" height="10%"/></center>
</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			<p></p>
			<div class="w3-row">
<ul class="w3-navbar w3-white">
	  <!--<li  id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Playlist');" id='ptype'>Total Playlist</a></li>-->
  <li id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Posts');" id='ptype'>Total Posts</a></li>
  <li id="proftype1"><a href="#" class="tablink" onClick="openCity(event, 'Images');" id='languages'>Total Images</a></li>
  <li><a href="#" class="tablink" onClick="openCity(event, 'Videos');" id='countries'>Total Videos</a></li>

</ul>

</div>
<!--<div id="Playlist" class="w3-container w3-border city">
<h4>Playlist</h4>
	<hr>
 <div class="row" id="Playlists"></div>
</div>
-->
<div id="Posts" class="w3-container w3-border city">
    <h4>Post</h4>
	<hr>
<div class="row" id="Postlist">
</div>
</div>

<div id="Images" class="w3-container w3-border city">
  <h4>Images</h4>
	<hr>
	<div class="row" id="Imagelist">
 </div>
</div>

<div id="Videos" class="w3-container w3-border city">
  <h4>Videos</h4>
	<hr>
 <div class="row w3-margin-bottom" id="Videolist">
 </div>
</div>

			</div>
		</div>
  </div>


<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>

var pageindex1=0;


var userid = getParameterByName('Userid');

function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}
$(document).ready(function(event) 
{
			Userlist(pageindex1);
			
			openCitys('Posts');
			$('#proftype').addClass('redborded');
			$('#ptype').css("color", "#ffffff");
			$('#proftype1').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#languages').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#videos').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#countries').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
	
});

function Userlist(pageindex)
{
	$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getUserProfile?userId="+userid+"&pageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var str1='';
						var str2='',str3='',playlist='',post='',image='',video='';
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
							
						if(obj.Status==1)
						{
								$.each(obj.Data, function (key, value) 
								{
									$('#username').html(value.UserName);
									
										if(value.ProfileThumbImage=="null" || value.ProfileThumbImage=="")
										{
											$('#username').append('<img src="<?php echo base_url()?>/img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">');
										}
										else
										{
											$('#username').append('<span id="image'+value.UserId+'"><img src="'+value.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px" onError="doSomething('+value.UserId+');"></span>');
										}
								//	$('#username').append(str1);
									str+='<table class="w3-table w3-striped w3-bordered w3-border" width="100%"><thead class="w3-red"><tr><th>User Detail</th></tr></thead><tr><td style="font-size:0.8em;">Full Name : '+value.FullName+'</td></tr> <tr><td style="width:100%;font-size:0.8em;">HomeTown : '+value.HomeTown+'</td></tr><tr><td style="width:100%;font-size:0.8em;"> CurrentAddress : '+value.CurrentAddress+'</td></tr><tr><td style="width:100%;font-size:0.8em;">Mobile No. : '+value.MobileNumber+'</td></tr><tr><td style="width:100%;font-size:0.8em;"> FaxNumber : '+value.FaxNumber+'</td></tr><tr><td style="width:100%;font-size:0.8em;"> Email : '+value.Email+'</td></tr><tr><td style="width:100%;font-size:0.8em;">IsVerified : '+value.IsVerified+'';
									if(value.IsVerified==true)
									{
										str+='<i class="fa fa-check" style="color:green" aria-hidden="true"></i>';
									}
									str+='</td></tr><tr><td style="width:100%;font-size:0.8em;">RegistrationDate : '+value.RegistrationDate+'</td></tr><tr><td style="width:100%;font-size:0.8em;">Country : '+value.Country.Country+'</td></tr><tr><td style="width:100%;font-size:0.8em;">ProfileType : '+value.ProfileType.ProfileTitle+'</td></tr></table>';
									
									str2+='<table class="w3-table w3-striped w3-bordered w3-border" width="100%"><thead class="w3-red"><tr><th>Total Records</th></tr></thead> <tr><td style="font-size:0.8em;">TotalVideos : '+value.Totals.TotalVideos+'</td></tr> <tr><td style="width:100%;font-size:0.8em;">TotalImages : '+value.Totals.TotalImages+'</td></tr><tr><td style="width:100%;font-size:0.8em;">TotalPosts : '+value.Totals.TotalPosts+'</td></tr><tr><td style="width:100%;font-size:0.8em;">TotalPlaylist : '+value.Totals.TotalPlaylists+'</td></tr></table>';	
						
						
					/*		if(value.PlayList.PlayList==null)
								{
									playlist+='<div class="col-sm-12"><p></p><div class="w3-container w3-card-2 w3-white w3-margin-bottom w3-center">No Playlist Created</div></div>';							
								}
							*/
								if(value.Posts.Posts==null)
								{
										post+='<div class="col-sm-12"><p></p><div class="w3-container w3-card-2 w3-white w3-margin-bottom w3-center">No Post Created</div></div>';							
								}
								if(value.Images.Images==null)
								{
										image+='<div class="col-sm-12"><p></p><div class="w3-container w3-card-2 w3-white w3-margin-bottom w3-center">No Images Uploaded</div></div>';							
								}
								if(value.Videos.Videos==null)
								{
										video+='<div class="col-sm-12"><p></p><div class="w3-container w3-card-2 w3-white w3-margin-bottom w3-center">No Videos Uploaded</div></div>';							
								}
								
	
				/*	playlist+='<p></p><div id="pagination" class="row" align="center"></div>';	
					//alert(totalnopage);	
				$.each(value.PlayList.PlayList, function (keys, values) 
				{
					playlist+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><h5 style="text-transform:capitalize;">'+values.Name+'<span  class="w3-right w3-opacity">'+values.CreatedDate+'</span></h5><hr><p style="font-size: 0.865em; line-height: 1em; ">Total Videos<span  class="w3-right" style="color:red">'+values.Total.TotalVideos+'</span></p><p style="font-size: 0.865em; line-height: 1em; ">Total Favourites<span  class="w3-right"  style="color:red">'+values.Total.TotalFavourites+'</span></p></div></div>';
												
				});
					playlist+='</div></div>';
					$('#Playlists').html(playlist);
					var str1='';
								if(value.PlayList.TotalPage>1)
								{
										str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
										str1+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
													$('#pagination').html(str1);	
													$('#prev').click(function()
														{	
															if(pageindex>0)
															{
																pageindex--;
																Userlist(pageindex);
															}
															
														});
														
														$('#next').click(function()
														{	
																pageindex++	;
															
															if(value.PlayList.TotalPage>pageindex)
															{
																Userlist(pageindex);
															}
															if(value.PlayList.TotalPage==pageindex)
															{
																pageindex--;
																Userlist(pageindex);
																document.getElementById('next').style.display='none'; 
															}
														});
									}
	*/
	
	post+='<p></p><div id="pagination1" class="row" align="center"></div>';				
	$.each(value.Posts.Posts, function (postkey, postval) 
	{
	 	var desc=postval.Description;
								
	post+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><a href="<?php echo base_url();?>Postlist/postdetail?PostId='+postval.PostId+'&UserId='+postval.User.UserId+'"><h5 style="text-transform:capitalize;">'+postval.User.UserName+'</a><span  class="w3-right w3-opacity">'+postval.SubmitedDate+'</span></h5><hr>';
									if(desc.length >65)
									{
										 var shortdesc=desc.substring(0,45);
										 post+='<p><span  class="w3-opacity">'+shortdesc+'...</span></p></div></div>';
									}
									else
									{
										 post+='<p><span  class="w3-opacity">'+desc+'</span></p></div></div>';
									}
									
		});
			
			$('#Postlist').html(post);
		
				var poststr='';
				if(value.Posts.TotalPage>1)
				{
						poststr+='<div  class="col-sm-6"><span id="postprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
						poststr+=' <div  class="col-sm-6"><span id="postnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
							
							$('#pagination1').html(poststr);
								
									$('#postprev').click(function()
										{	
											if(pageindex>0)
											{
												pageindex--;
												Userlist(pageindex);
											}
											
										});
										
										$('#postnext').click(function()
										{	
											pageindex++;
											if(value.Posts.TotalPage>pageindex)
											{
												postlist(pageindex);
											}
											if(value.Posts.TotalPage==pageindex)
											{
												pageindex--;
												postlist(pageindex);
												document.getElementById('postnext').style.display='none'; 
											}
										});
					}	
				
	image+='<p></p><div id="imagepagination" class="row" align="center"></div>';	
							
	$.each(value.Images.Images, function (imagekey, imageval) 
	{
		image+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><h5>'+imageval.User.UserName+'<span  class="w3-right w3-opacity">'+imageval.SubmitedDate+'</span></h5><hr class="w3-clear"><p><a href="<?php echo base_url();?>Imagelist/imagedetail?ImageId='+imageval.ImageId+'&UserId='+imageval.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span  class="w3-opacity w3-text-grey">'+imageval.title+'</span></p><div class="w3-margin-bottom"><center><span id="errimage'+value.ImageId+'"><img src="'+imageval.thumbUrl+'" width="100%" height="150px" onError="dosolveerror('+value.ImageId+');"></span></center></a></div></div></div>';
									
	});
		$('#Imagelist').html(image);		
		
		var imagestr='';
				if(value.Images.TotalPage>1)
				{
					imagestr+='<div  class="col-sm-6"><span id="Imagesprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
						imagestr+=' <div  class="col-sm-6"><span id="Imagesnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
						$('#imagepagination').html(imagestr);	
						
									$('#Imagesprev').click(function()
										{	
											if(pageindex>0)
											{
												pageindex--;
												Userlist(pageindex);
											}
											
										});
										
										$('#Imagesnext').click(function()
										{	
											pageindex++;
											if(value.Images.TotalPage>pageindex)
											{
												Userlist(pageindex);
											}
											if(value.Images.TotalPage==pageindex)
											{
												pageindex--;
												Userlist(pageindex);
												document.getElementById('Imagesnext').style.display='none'; 
											}
										});
					}			
				
					video+='<p></p><div id="Videospagination" class="row" align="center"></div>';	
					$.each(value.Videos.Videos, function (videokey, videoval) 
					{
							 var desc=videoval.Description;
					
							video+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round" w3-margin-bottom"><h5>'+videoval.User.UserName+'<span class="w3-right w3-opacity">'+videoval.SubmitedDate+'</span></h5><hr class="w3-clear"><p><a href="<?php echo base_url();?>Videolist/videodetail?VideoId='+videoval.VideoId+'&UserId='+videoval.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span  class="w3-opacity  w3-text-grey">'+videoval.title+'</span></p><div class="w3-margin-bottom"><center><span id="errvideo'+value.VideoId+'"><img src="'+videoval.thumbUrl+'" width="100%" height="150px" onError="videoerror('+value.VideoId+');"></span></center></a></div>';
													
													if(desc.length > 70)
													{
														 var shortdesc=desc.substring(0,35);
														 video+='<p> <h6 class="w3-opacity">'+ shortdesc +'...</h6></p><div></div></div></div>';
													}
													else
													{
														 video+='<p> <h6 class="w3-opacity">'+ desc+'</h6></p><div></div></div></div>';
													}
													
													
					});
						$('#Videolist').html(video);
						
						var videostr='';
						
								if(value.Videos.TotalPage>1)
								{
										videostr+='<div  class="col-sm-6"><span id="Videosprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
										videostr+=' <div  class="col-sm-6"><span id="Videosnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
													$('#Videospagination').html(videostr);	
													$('#Videosprev').click(function()
														{	
															if(pageindex>0)
															{
																pageindex--;
																Userlist(pageindex);
															}
															
														});
														
														$('#Videosnext').click(function()
														{	
															pageindex++;
															if(value.Videos.TotalPage>pageindex)
															{
																Userlist(pageindex);
															}
															if(value.Videos.TotalPage==pageindex)
															{
																pageindex--;
																Userlist(pageindex);
																document.getElementById('nexVideosnextt').style.display='none'; 
															}
														});
									}
								
								});
								
						}
						
						$('#tableuserlist').html(str);
						$('#tabletotal').html(str2);
					},
					error:function()
					{
						alert('error');
					}
			});

}
function postlist(pageindex)
{
var post='';
$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getUserProfile?userId="+userid+"&pageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						post+='<p></p><div id="pagination1" class="row" align="center"></div>';	
						
						$.each(obj.Data, function (key, value) 
						{
						$.each(value.Posts.Posts, function (postkey, postval) 
						{
							var desc=postval.Description;
													
						post+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><a href="<?php echo base_url();?>Postlist/postdetail?PostId='+postval.PostId+'&UserId='+postval.User.UserId+'"><h5 style="text-transform:capitalize;">'+postval.User.UserName+'</a><span  class="w3-right w3-opacity">'+postval.SubmitedDate+'</span></h5><hr>';
														if(desc.length >65)
														{
															 var shortdesc=desc.substring(0,45);
															 post+='<p><span  class="w3-opacity">'+shortdesc+'...</span></p></div></div>';
														}
														else
														{
															 post+='<p><span  class="w3-opacity">'+desc+'</span></p></div></div>';
														}
														
							});
							
								$('#Postlist').html(post);
								var poststr='';
				if(value.Posts.TotalPage>1)
				{
						poststr+='<div  class="col-sm-6"><span id="postprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
						poststr+=' <div  class="col-sm-6"><span id="postnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
							
							$('#pagination1').html(poststr);
								
									$('#postprev').click(function()
										{	
											if(pageindex>0)
											{
												pageindex--;
												postlist(pageindex);
											}
											
										});
										
										$('#postnext').click(function()
										{	
											pageindex++;
											if(value.Posts.TotalPage>pageindex)
											{
												postlist(pageindex);
											}
											if(value.Posts.TotalPage==pageindex)
											{
												pageindex--;
												postlist(pageindex);
												document.getElementById('postnext').style.display='none'; 
											}
										});
					}	
									});
					}
		});
}

function imagelist(pageindex)
{
var image='';
$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getUserProfile?userId="+userid+"&pageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						post+='<p></p><div id="pagination1" class="row" align="center"></div>';	
						
						$.each(obj.Data, function (key, value) 
						{
							image+='<p></p><div id="imagepagination" class="row" align="center"></div>';	
							$.each(value.Images.Images, function (imagekey, imageval) 
							{
								image+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><h5>'+imageval.User.UserName+'<span  class="w3-right w3-opacity">'+imageval.SubmitedDate+'</span></h5><hr class="w3-clear"><p><a href="<?php echo base_url();?>Imagelist/imagedetail?ImageId='+imageval.ImageId+'&UserId='+imageval.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span  class="w3-opacity w3-text-grey">'+imageval.title+'</span></p><div class="w3-margin-bottom"><center><span id="errimage'+value.ImageId+'"><img src="'+imageval.thumbUrl+'" width="100%" height="150px" onError="dosolveerror('+value.ImageId+');"></span></center></a></div></div></div>';
															
							});
							
								$('#Imagelist').html(image);		
		
		var imagestr='';
				if(value.Images.TotalPage>1)
				{
					imagestr+='<div  class="col-sm-6"><span id="Imagesprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
						imagestr+=' <div  class="col-sm-6"><span id="Imagesnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
						$('#imagepagination').html(imagestr);	
						
									$('#Imagesprev').click(function()
										{	
											if(pageindex>0)
											{
												pageindex--;
												imagelist(pageindex);
											}
											
										});
										
										$('#Imagesnext').click(function()
										{	
											pageindex++;
											if(value.Images.TotalPage>pageindex)
											{
												imagelist(pageindex);
											}
											if(value.Images.TotalPage==pageindex)
											{
												pageindex--;
												imagelist(pageindex);
												document.getElementById('Imagesnext').style.display='none'; 
											}
										});
						}	
						});
					}
		});
}

function videolist(pageindex)
{
var video='';
$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getUserProfile?userId="+userid+"&pageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						video+='<p></p><div id="Videospagination" class="row" align="center"></div>';		
						$.each(obj.Data, function (key, value) 
						{
							$.each(value.Videos.Videos, function (videokey, videoval) 
							{
								 var desc=videoval.Description;
					
							video+='<div class="col-sm-4"><p></p><div class="w3-container w3-card-2 w3-white w3-round" w3-margin-bottom"><h5>'+videoval.User.UserName+'<span class="w3-right w3-opacity">'+videoval.SubmitedDate+'</span></h5><hr class="w3-clear"><p><a href="<?php echo base_url();?>Videolist/videodetail?VideoId='+videoval.VideoId+'&UserId='+videoval.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span  class="w3-opacity  w3-text-grey">'+videoval.title+'</span></p><div class="w3-margin-bottom"><center><span id="errvideo'+value.VideoId+'"><img src="'+videoval.thumbUrl+'" width="100%" height="150px" onError="videoerror('+value.VideoId+');"></span></center></a></div>';
													
													if(desc.length > 70)
													{
														 var shortdesc=desc.substring(0,35);
														 video+='<p> <h6 class="w3-opacity">'+ shortdesc +'...</h6></p><div></div></div></div>';
													}
													else
													{
														 video+='<p> <h6 class="w3-opacity">'+ desc+'</h6></p><div></div></div></div>';
													}												
							});
						$('#Videolist').html(video);
						
						var videostr='';
						
								if(value.Videos.TotalPage>1)
								{
										videostr+='<div  class="col-sm-6"><span id="Videosprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
										videostr+=' <div  class="col-sm-6"><span id="Videosnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
													$('#Videospagination').html(videostr);	
													$('#Videosprev').click(function()
														{	
															if(pageindex>0)
															{
																pageindex--;
																videolist(pageindex);
															}
															
														});
														
														$('#Videosnext').click(function()
														{	
															pageindex++;
															if(value.Videos.TotalPage>pageindex)
															{
																videolist(pageindex);
															}
															if(value.Videos.TotalPage==pageindex)
															{
																pageindex--;
																videolist(pageindex);
																document.getElementById('nexVideosnextt').style.display='none'; 
															}
														});
									}
								});
					}
		});
}
function doSomething(id)
{
	var str='';
	str='<img src="<?php echo base_url()?>/img_avatar3.png"  class="w3-left w3-circle w3-margin-right" style="width:30px">';
	//alert(str);
$('#image'+id).html(str);
}

function dosolveerror(id)
{
	var str='';
	str='<img src="<?php echo base_url()?>error.png"  width="100%" height="150px">';
	//alert(str);
$('#errimage'+id).html(str);
}

function videoerror(id)
{
	var str='';
	str='<img src="<?php echo base_url()?>error.png"  width="100%" height="150px">';
	//alert(str);
$('#errvideo'+id).html(str);
}
function getParameterByName(name) 
   {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>