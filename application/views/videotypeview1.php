<!DOCTYPE html>
<html>
<title>Holynet-Settings</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link href="<?php echo base_url('assets')?>/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background: teal;
width:400px;
height:100px;
border:2px solid #000;
z-index: 9002;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;

}

.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Admindashboard/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	  
  

  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>Setting</h3>
				</div>
				</div>
				
			</div>
			<div class="w3-border">
<ul class="w3-navbar w3-white">
  <li id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Profile');" id='ptype'>Profile Types</a></li>
  <li id="proftype1"><a href="#" class="tablink" onClick="openCity(event, 'Language');" id='languages'>Language</a></li>
  <li><a href="#" class="tablink" onClick="openCity(event, 'Country');" id='countries'>Country</a></li>
  <li><a href="#" class="tablink" onClick="openCity(event, 'Video');" id='videos'>Video Types</a></li>
</ul>

<div id="Profile" class="w3-container w3-border city">

 <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Profile Type<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id02').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="profilesuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displayprofile" class="w3-container">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id02" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id02').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addprofile">Add Profile Type</h4>
    </header>
    <div class="w3-container">
     <p>
			<form class="w3-container">
			
			<p>
			<input type="text" name="peng" id="peng"  class="w3-input" placeholder="Enter Name in English"></p>
			
			<p><input type="text" name="parabic" id="parabic"  class="w3-input" placeholder="Enter Name in Arabic"></p>
			<p><h5 style="color:red;" id="profilemsg"></h5></p>
			<p>
			<input type="button" name="profiletype" id="profiletype" value="Save" class="w3-btn w3-blue"/> 
			<input type="button" name="profiletypeupdate" id="profiletypeupdate" value="Update" class="w3-btn w3-blue"/> 
			</p>
			
			</form>
	</p>
    </div>
 </div>
</div>
</div>

<div id="Language" class="w3-container w3-border city">
  <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Language<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id03').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="langugaesuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displaylanguage" class="w3-container" style="width:80%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id03" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id03').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addlanguage">Add New Language</h4>
    </header>
    <div class="w3-container">
     <p>
			<form class="w3-container">
			
			<p>
				<input type="text" name="lang" id="lang"  class="w3-input" placeholder="Enter New Language">
			</p>
			<p><h5 style="color:red;" id="languagemsg"></h5></p>
			<p>
			<input type="button" name="language" id="language" value="Save" class="w3-btn w3-blue"/> 
			<input type="button" name="languageupdate" id="languageupdate" value="Update" class="w3-btn w3-blue"/> 
			</p>
			
			</form>
	</p>
    </div>
 </div>
</div>
</div>


<div id="Country" class="w3-container w3-border city">
   <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Country<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id04').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="countrysuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displaycountry" class="w3-container" style="width:80%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id04" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id04').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addcountry">Add New Country</h4>
    </header>
    <div class="w3-container">
     <p>
			<form class="w3-container">
			
			<p>
				<input type="text" name="lang" id="lang"  class="w3-input" placeholder="Enter New Language">
			</p>
			<p><h5 style="color:red;" id="languagemsg"></h5></p>
			<p>
			<input type="button" name="language" id="language" value="Save" class="w3-btn w3-blue"/> 
			<input type="button" name="languageupdate" id="languageupdate" value="Update" class="w3-btn w3-blue"/> 
			</p>
			
			</form>
	</p>
    </div>
 </div>
</div>
</div>

<div id="Video" class="w3-container w3-border city">
	<p>
		 <div class="row">
			  <div class="col-sm-8">
				<div  class="w3-card-4">
					<div class="w3-container w3-teal">
					  <h4>Video Type<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id01').style.display='block'" >+</a></span></h4>
					</div>
						<p id="successmsg" style="color:red;background-color:#FFFFCC;"></p>
							<div id="displayvideo" class="w3-container">
								<p></p>
							</div>

				</div>
			</div>
		</div>
	</p>
</div>

<div id="id01" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id01').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addvideo">Add Video Type</h4>
    </header>
   	 <div class="w3-container">
    	 <p>
			<form class="w3-container">
			
			<p>
			<input type="text" name="neng" id="neng"  class="w3-input" placeholder="Enter Name in English"></p>
			
			<p><input type="text" name="narabic" id="narabic"  class="w3-input" placeholder="Enter Name in Arabic"></p>
			<p><h5 style="color:red;" id="msg"></h5></p>
			<p>
			<input type="button" name="videotype" id="videotype" value="Save" class="w3-btn w3-blue"/> 
			<input type="button" name="videotypeupdate" id="videotypeupdate" value="Update" class="w3-btn w3-blue"/> 
			</p>	
			</form>
		</p>
    </div>
  </div>
</div>

<div id='loadingmessage' style='display:none'>
  <center><img src='loading.gif' width="10%" height="10%"/></center>
</div>
<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
  </div>
</div>
<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-white" align="center" id="textdisplay"></div>
<div class="col-sm-2"></div><div class="col-sm-8"  align="center"><br><button class="w3-btn w3-blue" id="ok">OK</button> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="cancel">Cancel</a> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="okcan">OK</a></div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">FORUM</a> |
  <a href="/about/default.asp" target="_top">ABOUT</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}

$(document).ready(function(event) 
{	
	openCitys('Profile');
	
			$('#proftype').addClass('redborded');
			$('#ptype').css("color", "#ffffff");
			$('#proftype1').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#languages').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#videos').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#countries').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
	 displayvideotype();
	// displayprofiletype();
	// displaylanguage();
	$('#videotypeupdate').hide();
	$('#profiletypeupdate').hide();
	$('#languageupdate').hide();
	
	//$('#ptype').click();
		$('#videotype').click(function()
		{
			var eng=$('#neng').val();
			var arb=$('#narabic').val();
			
			if(eng=="")
			{
				$('#msg').html("Plese Enter Name in English").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
			if(arb=="")
			{
				$('#msg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
			$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/CreateVideoType?",
					type : "POST",
					data:
					{
						NameEnglish : eng,	
						NameArabic : arb
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							$('#neng').val('');
							$('#narabic').val('');
							document.getElementById('id01').style.display='none';
							$('#successmsg').html("Video Type " + obj.Message).fadeIn('slow');
							$('#successmsg').delay(1000).fadeOut('slow');
							displayvideotype();
						}
					},
					error: function()
					{
						alert('error');
					}
				});
		});
	
});

function displayvideotype()
{
		$.get( "<?php echo base_url(); ?>Videotype/displayVideotype", function( data ) {
	  $("#displayvideo").html(data);
	  $('#listoutlet').DataTable({
		"language": {
            "zeroRecords": "No VideoType Found",
            "infoEmpty": "No VideoType Found",
        },
		});
		});
}

function editvideotype(id)
{
	alert(id);
$.ajax({
			url : "<?php echo base_url(); ?>Videotype/editVideotype/"+id,
			type : "GET",
			success:function(response)
			{
				$('#videotype').hide();
				$('#videotypeupdate').show();
				var obj = JSON.parse(response);
						if(obj.Status==1)
						{
							$('#id01').show();
							$('#addvideo').html("Edit Video Type");
							$.each(obj.Data, function (key, value) 
							{
									$('#neng').val(value.nameEnglish);
									$('#narabic').val(value.nameArabic);
							alert(value.Id);
								updateVideotype(value.Id,value.nameEnglish,value.nameArabic);
							});
							
									
						}
			},
			error: function()
			{
				alert('error');
			}
		});
}

function updateVideotype(id)
{
alert(id);
	$('#videotypeupdate').click(function()
	{
			var eng=$('#neng').val();
			var arb=$('#narabic').val();
			
			if(eng=="")
			{
				$('#msg').html("Plese Enter Name in English").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
			if(arb=="")
			{
				$('#msg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
				
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/updateVideotype/"+id,
			type : "POST",
			data:
			{
				NameEnglish : eng,	
				NameArabic : arb
			},
			success:function(response)
			{
			
				var obj = JSON.parse(response);
				if(obj.Status==1)
				{
					$('#id01').hide();
					popup();
					$('#ok').hide();
					$('#cancel').hide();
					$('#textdisplay').html(obj.Message);
					$('#okcan').click(function()
					{
						displayvideotype();
						$('#neng').val('');
						$('#narabic').val('');
						$('#videotypeupdate').hide();	
						$('#videotype').show();	
					});
				}
				else
				{
					$('#id01').hide();
					popup();
					$('#textdisplay').html("No Update");
					displayvideotype();
				}	
			}
		});
	});
}
function ConfirmDelete(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Video type");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
				url : "http://dev.mobileartsme.com/holynet/Api/deleteVideoType?VideoTypeId="+id,
					type : "POST",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displayvideotype();
						}
					}	
			});
		});
}
function ConfirmDeleteProfile(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Profile type");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
				url : "http://dev.mobileartsme.com/holynet/Api/deleteProfileType?ProfileId="+id,
					type : "POST",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displayprofiletype();
							
						}
					}	
			});
		});
}
function ConfirmDeleteLanguage(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Language");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
					url : "http://52.203.171.109/holynet/api/deleteLanguage?Id="+id,
					type : "GET",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#textdisplay').html(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displaylanguage();	
						}
					}	
			});
		});
}

</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

