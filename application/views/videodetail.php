<!DOCTYPE html>
<html>
<title>Holynet-Videolist</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background: teal;
width:400px;
height:100px;
border:2px solid #000;
z-index: 9002;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	  
  

  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal w3-margin-bottom">
					<h3>Video List</h3>
				</div>
				</div>
				<div id="pagination" class="row" align="center"></div>  
			</div>
			<div class="w3-row">
 			<div id="contentdisplay">
			</div>
			</div>
			</div>
		</div>
  </div>
</div>
<div id='loadingmessage' style='display:none'>
  <center><img src='loading.gif' width="10%" height="10%"/></center>
</div>

<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-white" align="center">Are you sure you want to Delete  User Record</div>
<div class="col-sm-2"></div><div class="col-sm-8"  align="center"><br><button class="w3-btn w3-blue" id="ok">OK</button> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue">Cancel</a></div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
var totalnopage=0;
var pageindex1=0;

$(document).ready(function(event) 
{
	postlist(pageindex1);
});
function postlist(pageindex)
{
	
$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getResourceList?Type=1&UserId=-1&PageIndex="+pageindex+"&lang=en",
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
					
						var obj = JSON.parse(response);
						var str='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
							totalnopage=obj.TotalPage;
							var myString = 'popUpDiv';
          						str+='<div class="row">';
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
									var desc=value.Description;
									
										str+='<p></p><div class="col-sm-6"><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%"><br>';
										if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
										{
											str+='<img src="img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
										}
										else
										{
											str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';
										}
								
									str+='<span class="w3-right w3-opacity">'+value.SubmitedDate+'</span>  <a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;text-transform:capitalize;"><h5  class="w3-text-grey">'+value.User.UserName+'</h5></a><div class="w3-card-4"> <div class="w3-container w3-text-black"><p><h5><a href="<?php echo base_url();?>Videolist/videodetail?VideoId='+value.VideoId+'&UserId='+value.User.UserId+'" style="color:black;text-decoration:none;text-transform:capitalize;">'+value.title+'</a></h5></p></div><div class="w3-display-container">  <a href="<?php echo base_url();?>Videolist/videodetail?VideoId='+value.VideoId+'&UserId='+value.User.UserId+'" style="color:black;text-decoration:none;"><span id="image'+value.VideoId+'"><img  id="my-video'+value.VideoId+'" src="'+value.thumbUrl+'" width="100%" height="220px" onError="doSomething('+value.VideoId+');"></span></a> <div class="w3-display-bottomright w3-container w3-text-black  w3-opacity"><h5><a href="<?php echo base_url();?>Videolist/videodetail?VideoId='+value.VideoId+'&UserId='+value.User.UserId+'" style="color:black;text-decoration:none;">'+value.duration+'</h5></div> <div class="w3-container w3-light-grey w3-margin-bottom"> ';
									
									
									if(desc.length >65)
									{
										 var shortdesc=desc.substring(0,50);
										 str+=' <p></p><p><h6>'+shortdesc+'...</h6></p> </a></div> </div></div>  </div> </div>';
									}
									else
									{
										 str+='<p></p><p><h6>'+desc+'</h6></p> </a></div> </div></div>  </div> </div>';
									}
									
								});
								var str1='';
								if(obj.TotalPage>1)
								{
									str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div><p></p>';
									str1+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#pagination').html(str1);	
									$('#prev').click(function()
										{
											//alert(totalnopage);
											//alert(pageindex1);	
											if(pageindex1>0)
											{
												pageindex1--;
												postlist(pageindex1);
											}
											
										});
										
										$('#next').click(function()
										{	
											pageindex1++;
											if(totalnopage>pageindex1)
											{
												postlist(pageindex1);
											}
											if(totalnopage==pageindex1)
											{
												pageindex1--;
												postlist(pageindex1);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
						}
								str+='</div>';
								$('#contentdisplay').html(str);
						//$('#London').append($('#close'));
					},
					error:function()
					{
						alert('error');
					}
			});
}
function doSomething(id)
{
	var str='';
	str='<img src="error.png"  width="100%" height="220px">';
	//alert(str);
$('#image'+id).html(str);
}
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

