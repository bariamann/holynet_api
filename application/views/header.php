<!DOCTYPE html>
<html>
<title>Holynet</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
}
</style>
<body>

<nav class="w3-sidenav w3-collapse w3-white w3-animate-left w3-large" style="z-index:3;width:300px;" id="mySidenav">
<ul class="w3-navbar w3-black w3-center">
  <li class="navimg">
    <a href="javascript:void(0)" onClick="openNav('nav01')">
    <i class="fa fa-bars w3-xlarge"></i></a></li>
 
  <li class="navimg">
    <a style="color:#ffffff;">
    	<?php
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				echo "Welcome,".$sesscheck['username'];
			}
		?>
	
	</a>
  </li>
</ul>

<div id="nav01">
 <div class="w3-container  w3-theme w3-lobster"> <h3 class="display-3">HOLYNET</h3></div>
  <a href="javascript:void(0)" onClick="w3_close()" class="w3-text-teal w3-hide-large w3-closenav w3-large">Close �</a>
  <a href="#" class="w3-light-grey">Dashboard</a>
  <a href="#">Video Detail</a>
  <a href="#">Abused Report</a>
  <a href="#">Playlist Detail</a>
  <a href="#">Verified User</a>
   <a href="tryw3css_templates_black.htm">Image Detail</a>
   <a href="tryw3css_examples_album.htm">Post Detail</a>
</div>

<div id="nav02">
 <div class="w3-container  w3-theme w3-lobster"> <h3>HOLYNET</h3></div>
  <a target="_blank" href="tryw3css_templates_black.htm">Image Detail</a>
  <a target="_blank" href="tryw3css_examples_album.htm">Post Detail</a>
  <a target="_blank" href="tryw3css_examples_blog.htm"></a>
</div>

<div id="nav03">
  <div class="w3-container w3-border-bottom">
    <h1 class="w3-text-theme">W3.CSS</h1>
  </div>
  <ul class="w3-ul w3-large">
   <li class="w3-padding-16">Smaller and faster</li>
   <li class="w3-padding-16">Esier to use</li>
   <li class="w3-padding-16">Esier to learn</li>
   <li class="w3-padding-16">CSS only</li>
   <li class="w3-padding-16">Speeds up apps</li>
   <li class="w3-padding-16">CSS equality for all</li>
   <li class="w3-padding-16">PC Laptop Tablet Mobile</li>
  </ul>
</div>
</nav>


