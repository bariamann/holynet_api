<!DOCTYPE html>
<html>
<title>Holynet-abusevideodetail</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<link href="http://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background:white;
width:300px;
height:60%;
border:0px solid #000;
z-index: 9002;
overflow-y: scroll;
overflow-x:hidden;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
span.stars, span.stars span {
    display: block;
    background: url(../stars.png) 0 -16px repeat-x;
    width: 80px;
    height: 16px;
}

span.stars span {
    background-position: 0 0;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
  <?php
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$id=$sesscheck['id'];  
			}
  ?>
<input type="hidden" name="userid" id="userid" value="<?php echo $id;?>" />
  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					
				</div>
				</div>
				
			</div>
			<div class="w3-row">
 			<div id="contentdisplay">
				
			</div>
			<div id="contenttab">
				<div class="w3-border">
							<ul class="w3-navbar w3-white">
							  <li id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Profile');" id='ptype'>Abused Report</a></li>
							</ul>
							<div id="Profile" class="w3-container w3-border city">
							  <p>
								<div class="row">
									<div class="col-sm-12">
										
											<div class="w3-row" style="width:100%">
											  <h4>Abuse Report</h4>
											    <hr>
											</div>
													<p></p>
												<div class="row"  id="displaylanguage"></div>
									</div>
								</div>
							</p>
							</div>
						</div></div>
			</div>
			</div>
		</div>
  </div>
</div>
<div id='loadingmessage' style='display:none'>
  <center><img src='<?php echo base_url()?>loading.gif' width="10%" height="10%"/></center>
</div>

<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-black w3-large" align="center"><span  id="liketit">Likes </span><a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-tiny w3-blue w3-right">x</a></div>
<div class="col-sm-2"></div><div class="row"><div id="displaylike"></div>
</div>
</div>
</div>
<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNEY</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p id="abusemsg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <span id="ok"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
<script>
var pageindex1=0;

function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}
var videoid = getParameterByName('resourceid');
var userid = getParameterByName('UserId');
$(document).ready(function(event) 
{
	videolistdetail(pageindex1);
	openCitys('Profile');
	
			$('#proftype').addClass('redborded');
			$('#ptype').css("color", "#ffffff");
});
function videolistdetail(pageindex)
{
	
$.ajax({
					url : "<?php echo base_url()?>Api/getAbuseDetails?ResourceId="+videoid+"&Type=1&PageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
						
								
						var myString = 'popUpDiv';
						
								$.each(obj.Data, function (key, value) 
								{
									str+='<div class="w3-container w3-teal w3-margin-bottom"><h4 style="text-transform:capitalize;">'+value.title+'<div class="w3-right"><button type="button" class="w3-btn w3-red w3-margin-bottom" onclick="rejectvideo('+value.VideoId+')">REJECT VIDEO</button></div></h4></div><div class="w3-row-padding"><div class="w3-twothird"><video width="100%" height="370px" controls preload="auto" style="width:100%; height: 370px !important;overflow: hidden; background:transparent url('+value.thumbUrl+') no-repeat 0 0; -webkit-background-size:cover;  -moz-background-size:cover;  -o-background-size:cover;   background-size:cover; " onError="doSomething('+value.VideoId+');"><source src="'+value.fileUrl+'"/></video><p></p><div id="contenttabs"></div></div><div class="w3-third"><table class="w3-table w3-striped w3-bordered w3-border"><thead class="w3-teal"><th style="width:30%;text-transform:capitalize;">'+value.title+'</th><th></th></thead> <tr><td style="width:100%;font-size:0.8em;">'+value.Description+'</td><td></td</tr> <tr><td style="width:100%;font-size:0.8em;">Video Size : '+value.size+'</td><td></td></tr><tr><td style="width:100%;font-size:0.8em;">Video Duration : '+value.duration+'</td><td></td></tr><tr><td style="width:100%;font-size:0.8em;">Posted Date : '+ value.SubmitedDate+'</td><td></td></tr><tr><td style="width:100%;font-size:0.8em;">Video TypeName : '+value.VideoType.videoTypeName+'</td><td></td></tr><tr><td style="width:100%;font-size:0.8em;">KeyWord : '+ value.keyword+'</td><td></td></tr></table><p></p><table class="w3-table w3-striped w3-bordered w3-border"> <thead class="w3-teal"><th style="width:100%">';
										if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
										{
											str+='<img src="<?php echo base_url()?>/img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
										}
										else
										{
											str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';
										}
										 str+='<a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.UserId+'" class="w3-text-white" style="text-decoration:none;text-transform:capitalize;">'+value.User.UserName+'</a></th></thead> <tr><td style="width:100%;font-size:0.8em;"> PlayList : '+value.PlayList.playListName+'</td></tr> <tr><td><div class="w3-row-padding w3-left" style="margin:0 -16px"><button type="button" id="like" class="btn-xs w3-btn w3-theme-d1 w3-margin-bottom"><i class="fa fa-thumbs-up"></i> '+ value.Total.TotalLikes+'</button> <button type="button" id="dislike" class="btn-xs w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-thumbs-down"></i> '+ value.Total.TotalDislike+'</button>  <button type="button" class="btn-xs w3-btn w3-theme-d2 w3-margin-bottom" id="comments"><i class="fa fa-comments"></i> '+ value.Total.TotalComments+'</button> <button type="button" class="btn-xs w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-share-alt"></i> '+ value.Total.TotalShared+'</button> <button type="button" class="btn-xs w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-newspaper-o"></i> '+ value.Total.TotalAbuseReport+'</button> <button type="button" class="btn-xs w3-btn w3-theme-d2 w3-margin-bottom" id="rating"><i class="fa fa-star-o"></i> '+ value.Total.TotalRate+'</button></div></td></tr></table>  </div></div>';
								});
								$('#contentdisplay').html(str);
								$('#contenttabs').append($('#contenttab'));
								
								$('#like').click(function()
								{
									$.ajax({
												url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+videoid+"&type=1&status=1&pageIndex=0",
												type : "GET",
												beforeSend: function()
												{
													$('#loadingmessage1').show();
												},
												complete: function()
												{
													$('#loadingmessage1').hide();
												},
												success:function(response)
												{
													popup();
													var obj = JSON.parse(response);
													var str='';
													$('#loadingmessage1').hide();
													$('#liketit').html("Likes")
													if(obj.Status==0)
													{
														str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Likes</center></p></div></div></div>';
													}
													else
													{
														$.each(obj.Data, function (key, value) 
														{
														
														str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
															if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
															{
															str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
															}
															else
															{
								str+='<img src="<?php echo base_url()?>'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
															}
														str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black"">'+value.User.UserName+'</a></span> </p></div></div></div>';
														});
													}
													$('#displaylike').html(str);
												}
												
											});
								});
								
								$('#dislike').click(function()
								{
									$.ajax({
												url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+videoid+"&type=1&status=2&pageIndex=0",
												type : "GET",
												beforeSend: function()
												{
													$('#loadingmessage1').show();
												},
												complete: function()
												{
													$('#loadingmessage1').hide();
												},
												success:function(response)
												{
													popup();
													var obj = JSON.parse(response);
													var str='';
													$('#loadingmessage1').hide();
													$('#liketit').html("Dislike");
													if(obj.Status==0)
													{
														str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No DisLikes</center></p></div></div></div>';
													}
													else
													{
														$.each(obj.Data, function (key, value) 
														{
														
														str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
															if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
															{
															str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
															}
															else
															{
								str+='<img src="<?php echo base_url()?>'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
															}
														str+='<span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black">'+value.User.UserName+'</a></span> </p></div></div></div>';
														
														});
													}
													$('#displaylike').html(str);
												}
												
											});
								});
								
								$('#rating').click(function()
								{
									$.ajax({
												url : "<?php echo base_url()?>Api/whoRatedVideo?videoId="+videoid+"&pageIndex=0",
												type : "GET",
												beforeSend: function()
												{
													$('#loadingmessage1').show();
												},
												complete: function()
												{
													$('#loadingmessage1').hide();
												},
												success:function(response)
												{
													popup();
													var obj = JSON.parse(response);
													var str='';
													$('#loadingmessage1').hide();
													$('#liketit').html("Rating")
													if(obj.Status==0)
													{
														str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Rating for this video</center></p></div></div></div>';
													}
													else
													{
														$.each(obj.Data, function (key, value) 
														{
														str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
															if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
															{
															str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
															}
															else
															{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
															}
														str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black"">'+value.User.UserName+'</a></span><span class="w3-right stars">'+value.Rate+'</span></p></div></div></div>';
														});
													}
													$('#displaylike').html(str);
													$.fn.stars = function() {
														return $(this).each(function() {
															// Get the value
															var val = parseFloat($(this).html());
															// Make sure that the value is in 0 - 5 range, multiply to get width
															var size = Math.max(0, (Math.min(5, val))) * 16;
															// Create stars holder
															var $span = $('<span />').width(size);
															// Replace the numerical value with stars
															$(this).html($span);
														});
													}
													  $('span.stars').stars();
												}
												
											});
								});
							}
							
							var abuse='';
							
						if(obj.Status==0)
							{
								
								abuse='<div class="w3-container w3-card w3-margin-bottom w3-center w3-text-red">No Abused Report Available</div>';
							}
						else 
						{	
							if(obj.AbuseReport.AbuseReport==null)
							{
								
								abuse='<div class="w3-container w3-card w3-margin-bottom w3-center w3-text-red">No Abused Report Available</div>';
							}
							else
							{
								abuse+='<div id="pagination1" class="row" align="center"></div><p></p>';	
								$.each(obj.AbuseReport.AbuseReport, function (abusekey, abusevalue) 
								{
									//alert(values.FullName);
									abuse+='<div class="col-sm-10"> <div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom" style="width:100%">';
										if(abusevalue.User.ProfileThumbImage=="null" || abusevalue.User.ProfileThumbImage=="")
										{
											abuse+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;padding-top:10px;">';
										}
										else
										{
											abuse+='<img src="'+abusevalue.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
										}
								
								abuse+='<span class="w3-right w3-opacity">'+abusevalue.AbuseDate+'</span><h5 class="w3-opacity w3-margin-top" style="text-transform:capitalize;">'+abusevalue.User.UserName+'</h5><hr class="w3-clear"><div class="w3-container w3-white"><p>'+abusevalue.Reason+'...</p> </div></div></div>';
								});
						}
							}
						$('#displaylanguage').html(abuse);
						
						var poststr='';
					if(obj.AbuseReport.TotalPage>1)
					{
						poststr+='<div  class="col-sm-6"><span id="postprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
						poststr+=' <div  class="col-sm-6"><span id="postnext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
								
								$('#pagination1').html(poststr);
									
										$('#postprev').click(function()
											{	
												if(pageindex>0)
												{
													pageindex--;
													videolistdetail(pageindex);
												}
												
											});
											
											$('#postnext').click(function()
											{	
												pageindex++;
												if(obj.AbuseReport.TotalPage>pageindex)
												{
													videolistdetail(pageindex);
												}
												if(obj.AbuseReport.TotalPage==pageindex)
												{
													pageindex--;
													videolistdetail(pageindex);
													document.getElementById('postnext').style.display='none'; 
												}
											});
						}	
					},
					error:function()
					{
						alert('error');
					}
			});
}
function rejectvideo(id)
{
	var userid=$('#userid').val();
	
	$('#myModal').modal('show');
	$('.modal-title').html("Reject AbuseVideo");
	$('#abusemsg').html("Are You Sure Want Reject Video");
	$('#ok').html('<button type="button" class="btn btn-primary" id="postok">OK</button>');
										
	$('#postok').click(function()
	{		
		$.ajax({
			url : "<?php echo base_url()?>Api/rejectContent?userId="+userid+"&fileId="+id+"&type=1&reason=Your video is rejected by admin",
			type : "GET",
			success:function(response)
			{
				var obj = JSON.parse(response);
				$('#myModal').modal('hide');
				window.location="<?php echo base_url('Abusereport');?>";
			},
			error:function()
			{
				alert('error');
			}
		});
	});
}
function getParameterByName(name) 
   {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }

</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

