<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reset Password</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css')?>" rel="stylesheet"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
    <!-- Custom CSS -->
    <style>
     body
    {
        padding-top: 70px;
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#dddddd">
        <div class="container" style="height:60px; width:100%;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <table style="background-color:#dddddd; width:100%; height:50px;">
      				<tr>
      					<td style="width:20%;">
      						<img src="http://dev.mobileartsme.com/holynet/assets/images/small_ic_luncher_icon.png" height="50px">
      					</td>
      					<td style="width:60%; height:50px;">
      						<center><span style="color:#009688; height:50px; font-size:40px;">HolyNet</span></center>
      					</td>
      					<td style="width:20%;"></td>
      				</tr>
      			</table>
          </br>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<blockquote class="w3-panel w3-pale-yellow   w3-border-teal w3-leftbar" id="msgforgetform">
  
</blockquote>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script>
var validationtoken = getParameterByName('validationToken');
var accesstoken = getParameterByName('accessToken');
$(document).ready(function()
{
    $.ajax({
				url : "http://dev.mobileartsme.com/holynet/Api/activeAccount?",
				type : "POST",
				data :
				{
					validationToken : validationtoken,
					accessToken   : accesstoken,
				},
				success:function(data)
				{
					var res = JSON.parse(data);
					if(res.Status==1)
					{
						var str="<center><span style='font-size:26px;color:teal;font-family:Verdana'>Your Holynet Account is Actived Successfully</span><br><br><span style='font-size:16px;color:blue'>Thanks for using Holynet!<br><br>The Holynet Team</span></center>"
						$('#forgetform').hide();
						$('#msgforgetform').html(str);
					}
				},
				error:function(data)
				{

				}
			});

});
function getParameterByName(name)
   {
	    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
</body>
</html>
