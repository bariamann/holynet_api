      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
      <style>
      	.toolbar {
      		float: left;
      	}
      </style>
      <div class="container">
      	<div class="row">
      		<!-- <div class="col-md-12 text-center"> -->
      			<!-- <div class="col-sm-5 form-box"> -->
      				
              <div class="form-top">
      					<div class="form-top-left">
      						<h3 style="color:#0197d8;">Notification Profile</h3>
      					</div>
      				</div>

      				<div class="form-bottom">
                <div class="row">
                  <center>
                    <div id="viewProfile">
                      
                    </div>
                  </center>
                </div>
      				</div>
      	</div>
      	<!-- /.row -->
      	<div class="modal fade" id="myModal" role="dialog">
      		<div class="modal-dialog">
      			<!-- Modal content-->
      			<div class="modal-content">
      				<div class="modal-header">
      					<button type="button" class="close" data-dismiss="modal">&times;</button>
      				</div>
      				<div class="modal-body">
      					<p id="Message"></p>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>

      <script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
      <script>

        function getUrlVars()
        {
          var vars = [], hash;
          var cmpId = 0;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for(var i = 0; i < hashes.length; i++)
          {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
            cmpId = hash[1];
          }
          return cmpId;
        }

        function loadNotificationProfile(notid){
          var id = notid;
          $.ajax({
            url:"<?php echo site_url('NotificationProfile/getNotificationProfile');?>/"+id,
            success:function(data){
              $("#viewProfile").html(data);
            }
          });
        }

      	$(document).ready(function() 
      	{

          var notificationId = getUrlVars();
          loadNotificationProfile(notificationId);

      		/*$('#edit').hide();
      		notifi=$('#has_notify').val();
      		$.get( "<?php echo base_url(); ?>History/getHistoryData", function( data ) {
	           //alert( data );
	           //$("#viewcategory").html(data);
	           $('#listcategory').DataTable({
		           	"bAutoWidth": false,
		           	"columnDefs": [
		           	{ "width": "25%", "targets": 0 },
		           	{ "width": "25%", "targets": 1 },
		           	{ "width": "26%", "targets": 2 },
		           	{ "width": "8%", "targets": 3 },
		           	{ "width": "8%", "targets": 4 },
		           	]
	           });
       		});*/

      		$("#imagefile").change(function () {
      			if (typeof (FileReader) != "undefined") {
      				var dvPreview = $("#imgprvw");
      				dvPreview.html("");
      				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
      				$($(this)[0].files).each(function () {
      					var file = $(this);
      					if (regex.test(file[0].name.toLowerCase())) {
      						var reader = new FileReader();
      						reader.onload = function (e) {
      							var img = $("<img />");
      							img.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
      							img.attr("src", e.target.result);
      							dvPreview.append(img);

      						}
      						reader.readAsDataURL(file[0]);
      					} else {
      						alert(file[0].name + " is not a valid image file.");
      						dvPreview.html("");
      						return false;
      					}
      				});
      			} else {
      				alert("This browser does not support HTML5 FileReader.");
      			}
      		});

      		$('#has_notify').click(function()
      		{
      			notifi=$('#has_notify').val();
      			if(document.getElementById("has_notify").checked==true)
      			{
      				notifi=1;
					//alert(notifi);
				}
				else
				{
					notifi=0;
					//	alert(notifi);
				}
			});


      		$(".close").click(function()
      		{
      			window.location.reload();
      		});

      	}); 
      
        function getcategoryData() 
        {
        	$.get( "<?php echo base_url(); ?>History/getHistoryData", function( data ) {
           		//alert( data );
           		//$("#viewcategory").html(data);
           		$('#listcategory').DataTable();
       		});
        }
        function cancelcategory()
        {
        	location.reload();
        }
    </script>
