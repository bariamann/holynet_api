<!DOCTYPE html>
<html>
<title>Holynet-abuselist</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background:white;
width:300px;
height:60%;
border:0px solid #000;
z-index: 9002;
overflow-y: scroll;
overflow-x:hidden;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
border-bottom: 6px solid;
border-color: #f44336!important;
}

.bs-example{
	margin: 20px;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	<?php
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$id=$sesscheck['id'];  
			}
  ?>
<input type="hidden" name="userid" id="userid" value="<?php echo $id;?>" />
  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>Abuse Report <!--<span class="w3-right"><a href="<?php echo base_url('Rejectedreport');?>" style="color:#FFFFFF">Rejected Report</a>--></h3>
				</div>
				</div>
				
			</div>
			<div class="w3-row">
  <a href="#" onClick="openCity(event, 'Posts');" id="londonclick">
    <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding active" id="normalusers">Abuse Posts</div>
  </a>
  <a href="#" onClick="openCity(event, 'Images');" id="parisclick">
    <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding" id="vierfiedusers">Abuse Images</div>
  </a>
   <a href="#" onClick="openCity(event, 'Videos');" id="videoclick">
    <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding" id="totalvideos">Abuse Vidoes</div>
  </a>
</div>

<div id="Posts" class="w3-container city">
    <h4>Post <span class="w3-right" id="rejectpostbtn" style="padding-left:5px;"><button type="button" class="btn-sm w3-btn w3-red open-modal" id="rejectpost">REJECT POST</button>  </span> <span class="w3-right" id="approvepostbtn"><button type="button" class="btn-sm w3-btn w3-green open-modal" id="approvepost">APPROVE POST</button>  </span> </h4>
	<hr>
	<div id="postpagination" class="row" align="center"></div>  
	 <div class="row" id="posts"></div>
	 
	</div>

<div id="Images" class="w3-container city">
  <h4>Images <span class="w3-right" style="padding-left:5px;"> <button type="button" class="btn-sm w3-btn w3-red" id="rejectimage">REJECT IMAGES</button> </span> <span class="w3-right" id="approveimagebtn"><button type="button" class="btn-sm w3-btn w3-green open-modal" id="approveimage">APPROVE IMAGES</button>  </span> </h4>
	<hr>
	<div id="imagepagination" class="row" align="center"></div>  
<div class="row" id="images"></div>

</div>

<div id="Videos" class="w3-container city">
  <h4>Videos <span class="w3-right" style="padding-left:5px;"> <button type="button" class="btn-sm w3-btn w3-red" id="rejectvideos">REJECT VIDEOS</button> </span><span class="w3-right" id="approvevideobtn"><button type="button" class="btn-sm w3-btn w3-green open-modal" id="approvevideo">APPROVE VIDEOS</button>  </span>  </h4>
	<hr>
		<div id="videopagination" class="row" align="center"></div>  
<div class="row" id="videos"></div>
</div>
<div id='loadingmessage' style='display:none'>
  <center><img src='<?php echo base_url()?>/loading.gif' width="10%" height="10%"/></center>
</div>
<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
  </div>
</div>
<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-black w3-large" align="center"><span  id="liketit">Likes </span><a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-tiny w3-blue w3-right">x</a></div>
<div class="col-sm-2"></div><div class="row"><div id="displaylike"></div>
</div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p id="abusemsg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <span id="ok"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}

function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}
var posttotalnopage=0;
var postpageindex=0;

var imagetotalnopage=0;
var imagepageindex=0;

var videototalnopage=0;
var videopageindex=0;

$(document).ready(function(event) 
{
			openCitys('Posts');
			abuseposts(postpageindex);
			
			$('#normalusers').removeClass('w3-bottombar');
			$('#normalusers').addClass('redborded');
			$('#normalusers').click(function()
			{
				abuseposts(postpageindex);
			});
			$('#vierfiedusers').click(function()
			{
				$('#normalusers').removeClass('redborded');
				$('#normalusers').addClass('w3-bottombar');
				abuseimages(imagepageindex);
			});
			$('#totalvideos').click(function()
			{
				$('#normalusers').removeClass('redborded');
				$('#normalusers').addClass('w3-bottombar');
				abusevideo(videopageindex);
			});
	
});

function abuseposts(postpageindex)
{
	
$.ajax({
					url : "<?php echo base_url()?>Api/getAbuseList?Type=3&PageIndex="+postpageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var str1='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
								posttotalnopage=obj.TotalPage;	
								var myString = 'popUpDiv';
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
										Id=value.PostId;
										 var desc=value.Description;
										str+='<div class="col-sm-6"><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom"><p></p>';
									if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
									{
										str+='<img src="<?php echo base_url()?>/img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
									}
									else
									{
										str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';
									}
								
									str+='<span class="w3-right w3-opacity">'+value.SubmitedDate+'</span><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span class="w3-text-black" style="text-transform:capitalize;">'+value.User.UserName+'</span></a><hr class="w3-clear"><div class="w3-container w3-white w3-margin-bottom"><a href="<?php echo base_url();?>Abusereport/viewabusepost?resourceid='+value.PostId+'&UserId='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;color:grey">'
									
													if(desc.length > 70)
													{
														 var shortdesc=desc.substring(0,40);
														 str+= shortdesc +'...';
													}
													else
													{
														 str+=desc;
													}
													
									str+='</a></div><hr><div class="w3-left"><input class="w3-check" type="checkbox" name="rejectabusepost[]" id="rejectabusepost'+value.PostId+'" value="'+value.PostId+'"> </div><div class="w3-right"> <button type="button" class="btn-sm w3-btn w3-theme-d1 w3-margin-bottom" id="like'+value.PostId+'" onclick="getlikes('+Id+')"><i class="fa fa-thumbs-up"></i> '+ value.Total.TotalLikes+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="dislike'+value.PostId+'" onclick="getdislikes('+Id+')"><i class="fa fa-thumbs-down"></i> '+ value.Total.TotalDislike+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="comment'+value.PostId+'" onclick="getcomment('+Id+')"><i class="fa fa-comments"></i> '+ value.Total.TotalComments+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-share-alt"></i> '+ value.Total.TotalShared+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-newspaper-o"></i> '+ value.Total.TotalAbuseReport+'</button></div></div></div>';
									
								});
								
						}
						str+='</div></div>';
						$('#posts').html(str);
						
							var postpage='';
								if(obj.TotalPage>1)
								{
									postpage+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div><p></p>';
									postpage+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#postpagination').html(postpage);
										
									$('#prev').click(function()
										{	
											if(postpageindex>0)
											{
												postpageindex--;
												abuseposts(postpageindex);
											}
											
										});
										
										$('#next').click(function()
										{	
											postpageindex++;
											if(posttotalnopage>postpageindex)
											{
												abuseposts(postpageindex);
											}
											if(posttotalnopage==postpageindex)
											{
												postpageindex--;
												abuseposts(postpageindex);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
									$('#rejectpost').click(function()
									{	
											var userid=-1;
											 if($('[type="checkbox"]').is(":checked"))
											 {
													var all_location_id = document.querySelectorAll('input[name="rejectabusepost[]"]:checked');
											
														if(all_location_id.checked)
														{
															alert('dgd');
														}
														var aIds = [];
														
														for(var x = 0, l = all_location_id.length; x < l;  x++)
														{
															aIds.push(all_location_id[x].value);
														}
														
														var str = aIds.join(',');
														
														console.log(str);
														
													$('#myModal').modal('show');
													$('.modal-title').html("Reject Abusepost");
													$('#abusemsg').html("Are You Sure Want Reject Post");
													$('#ok').html('<button type="button" class="btn btn-primary" id="postok">OK</button>');
													
													$('#postok').click(function()
													{				
														$.ajax({
																url : "<?php echo base_url()?>Api/rejectContent?userId="+userid+"&fileId="+str+"&type=3&reason=Your file is rejected by admin",
																type : "GET",
																success:function(response)
																{
																	var obj = JSON.parse(response);
																		$('#myModal').modal('hide');
																		abuseposts(postpageindex);
																},
																error:function()
																{
																	alert('error');
																}
															});
													});
											
											}
											else
											{
													alert("Please Select Post to Reject");
											}									
									});
									
									$('#approvepost').click(function()
									{	
											var userid= -1;
											
										if($('[type="checkbox"]').is(":checked"))
										{		
											var all_location_id = document.querySelectorAll('input[name="rejectabusepost[]"]:checked');
	
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(',');
											
											console.log(str);
											
											$('#myModal').modal('show');
											$('.modal-title').html("Approve Post");
											$('#abusemsg').html("Are You Sure Want Approve Post");
											$('#ok').html('<button type="button" class="btn btn-primary" id="postok">OK</button>');
											
											$('#postok').click(function()
											{				
												$.ajax({
														url : "<?php echo base_url()?>Api/approvedContent?fileId="+str+"&type=3",
														type : "GET",
														success:function(response)
														{
																var obj = JSON.parse(response);
																$('#myModal').modal('hide');
																if(obj.Status==1)
																{
																	abuseposts(postpageindex);
																}
														},
														error:function()
														{
															alert('error');
														}
													});
											});
									}
									else
									{
											alert("Please Select Post to Approve");
									}
								});
					},
					error:function()
					{
						alert('error');
					}
				
			});
}

function getlikes(id)
{
	$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=3&status=1&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("Likes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Likes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';																	                                 }
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
function getdislikes(id)
{
$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=3&status=2&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("DisLikes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No DisLikes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px;height:30px;">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';																	                                 }
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black;">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}

function abuseimages(imagepageindex)
{
	
$.ajax({
					url : "<?php echo base_url()?>Api/getAbuseList?Type=2&PageIndex="+imagepageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var str1='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
							
						var myString = 'popUpDiv';
							
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
									ID=value.ImageId;
										str+='<div class="col-sm-6"><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom"><p></p>';
									if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
									{
										str+='<img src="<?php echo base_url()?>/img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
									}
									else
									{
										str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px">';
									}
								
									str+='<span class="w3-right w3-opacity">'+value.SubmitedDate+'</span><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span class="w3-opacity" style="text-transform:capitalize;">'+value.User.UserName+'</span></a><hr class="w3-clear"><a href="<?php echo base_url();?>Abusereport/viewabuseimage?resourceid='+value.ImageId+'&UserId='+value.User.UserId+'"><p>'+value.title+'</p></a><div class="w3-container w3-white w3-margin-bottom"> <a href="<?php echo base_url();?>Abusereport/viewabuseimage?resourceid='+value.ImageId+'&UserId='+value.User.UserId+'"><img src="'+value.thumbUrl+'" alt="image" style="width:100%;height:220px;"></a><p></p></div><hr><div class="w3-left"><input class="w3-check" type="checkbox" name="rejectabuseimage[]" id="rejectabuseimage'+ID+'" value="'+ID+'"> </div> <div class="w3-right"><button type="button" class="btn-sm w3-btn w3-theme-d1 w3-margin-bottom" id="imagelike'+ID+'" onclick="getimagelikes('+ID+')"><i class="fa fa-thumbs-up"></i> '+ value.Total.TotalLikes+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="imagedislike'+ID+'" onclick="getimagedislikes('+ID+')"><i class="fa fa-thumbs-down"></i> '+ value.Total.TotalDislike+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="imagecomment'+ID+'"  onclick="getimagecount('+ID+')"><i class="fa fa-comments"></i> '+ value.Total.TotalComments+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-share-alt"></i> '+ value.Total.TotalShared+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-newspaper-o"></i> '+ value.Total.TotalAbuseReport+'</button></div></div></div>';
									
								});
								
						}	
						
						str+='</div></div>';
						$('#images').html(str);
							var imagepage='';
								if(obj.TotalPage>1)
								{
									imagepage+='<div  class="col-sm-6"><span id="imageprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div><p></p>';
									imagepage+=' <div  class="col-sm-6"><span id="imagenext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#imagepagination').html(imagepage);
										
									$('#imageprev').click(function()
										{	
											if(imagepageindex>0)
											{
												imagepageindex--;
												abuseimages(imagepageindex);
											}
											
										});
										
										$('#imagenext').click(function()
										{	
											imagepageindex++;
											if(imagetotalnopage>imagepageindex)
											{
												abuseimages(imagepageindex);
											}
											if(imagetotalnopage==imagepageindex)
											{
												imagepageindex--;
												abuseimages(imagepageindex);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
									$('#rejectimage').click(function()
									{
											var userid= -1;
											
										if($('[type="checkbox"]').is(":checked"))
										{	
											var all_location_id = document.querySelectorAll('input[name="rejectabuseimage[]"]:checked');
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(',');
											
											console.log(str);
											
										$('#myModal').modal('show');
										$('.modal-title').html("Reject AbuseImage");
										$('#abusemsg').html("Are You Sure Want Reject Image");
										$('#ok').html('<button type="button" class="btn btn-primary" id="imageok">OK</button>');
										
										$('#imageok').click(function()
										{				
											$.ajax({
													url : "<?php echo base_url()?>Api/rejectContent?userId="+userid+"&fileId="+str+"&type=2&reason=Your file is rejected by admin",
													type : "GET",
													success:function(response)
													{
														var obj = JSON.parse(response);
														$('#myModal').modal('hide');
														abuseimages(imagepageindex);
													},
													error:function()
													{
														alert('error');
													}
												});
										});
									}
									else
									{
										alert("Please Select Image to Reject");
									}
								});
									$('#approveimage').click(function()
									{	
											var userid= -1;
										if($('[type="checkbox"]').is(":checked"))
										{		
											var all_location_id = document.querySelectorAll('input[name="rejectabuseimage[]"]:checked');
	
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(',');
											
											console.log(str);
											
											$('#myModal').modal('show');
											$('.modal-title').html("Approve Image");
											$('#abusemsg').html("Are You Sure Want Approve Image");
											$('#ok').html('<button type="button" class="btn btn-primary" id="imageok">OK</button>');
											
											$('#imageok').click(function()
											{				
												$.ajax({
														url : "<?php echo base_url()?>Api/approvedContent?fileId="+str+"&type=2",
														type : "GET",
														success:function(response)
														{
																var obj = JSON.parse(response);
																$('#myModal').modal('hide');
																if(obj.Status==1)
																{
																	abuseimages(imagepageindex);
																}
														},
														error:function()
														{
															alert('error');
														}
													});
											});
										}
										else
										{
											alert("Please Select Image to Approve");
										}
									});
					},
					error:function()
					{
						alert('error');
					}
			});
}
function getimagelikes(id)
{
	$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=2&status=1&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("Likes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Likes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px">';
								}
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
function getimagedislikes(id)
{
$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=2&status=2&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("DisLikes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No DisLikes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';																	                                 }
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}

function abusevideo(videototalnopage)
{
	
$.ajax({
					url : "<?php echo base_url()?>Api/getAbuseList?Type=1&PageIndex="+videototalnopage,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var str1='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
							
						var myString = 'popUpDiv';
							
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
									ID=value.VideoId;
										str+='<div class="col-sm-6"><div class="w3-container w3-card-2 w3-white w3-round w3-margin-bottom"><p></p>';
									if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
									{
										str+='<img src="<?php echo base_url()?>/img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';
									}
									else
									{
										str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px">';
									}
								
									str+='<span class="w3-right w3-opacity">'+value.SubmitedDate+'</span><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span class=" w3-opacity">'+value.User.UserName+'</span></a><hr class="w3-clear"><p></p><div class="w3-margin-bottom"><div class="w3-container w3-white w3-padding"> <a href="'+value.fileUrl+'"  target="_blank"><img src="'+value.thumbUrl+'" alt="image" style="width:50%" class="w3-left w3-round-large w3-margin-right"></a><p><a href="<?php echo base_url();?>Abusereport/viewabusedetail?resourceid='+value.VideoId+'&UserId='+value.User.UserId+'" style="text-decoration:none;border:0;outline:none;"><span class="w3-text-black">'+value.title+'</span></a><br><hr><span  class="w3-text-grey">'+value.Description+'</span> </p></div></div><hr><div class="w3-left"><input class="w3-check" type="checkbox" name="rejectabusevideo[]" id="rejectabusevideo'+ID+'" value="'+ID+'"> </div> <div class="w3-right"><button type="button" class="btn-sm w3-btn w3-theme-d1 w3-margin-bottom" id="videolike'+ID+'" onclick="getvideolikes('+ID+')"><i class="fa fa-thumbs-up"></i> '+ value.Total.TotalLikes+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="videodislike'+ID+'" onclick="getvideodislikes('+ID+')"><i class="fa fa-thumbs-down"></i> '+ value.Total.TotalDislike+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom" id="videocomment'+ID+'" onclick="getvideocomments('+ID+')"><i class="fa fa-comments"></i> '+ value.Total.TotalComments+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-share-alt"></i> '+ value.Total.TotalShared+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-newspaper-o"></i> '+ value.Total.TotalAbuseReport+'</button> <button type="button" class="btn-sm w3-btn w3-theme-d2 w3-margin-bottom"><i class="fa fa-star-o"></i> '+ value.Total.TotalRate+'</button></div></div></div>';
									
								});
								
						}	
						
						str+='</div></div>';
						$('#videos').html(str);
						
						var videopage='';
								if(obj.TotalPage>1)
								{
									videopage+='<div  class="col-sm-6"><span id="videoprev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div><p></p>';
									videopage+=' <div  class="col-sm-6"><span id="videonext" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('videopagination').html(videopage);
										
									$('#videoprev').click(function()
										{	
											if(videopageindex>0)
											{
												videopageindex--;
												abusevideo(videopageindex);
											}
											
										});
										
										$('#videonext').click(function()
										{	
											videopageindex++;
											if(videototalnopage>videopageindex)
											{
												abusevideo(videopageindex);
											}
											if(videototalnopage==videopageindex)
											{
												postpageindex--;
												abusevideo(videopageindex);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
									$('#rejectvideos').click(function()
									{
											var userid=-1;
										if($('[type="checkbox"]').is(":checked"))
										{	
											var all_location_id = document.querySelectorAll('input[name="rejectabusevideo[]"]:checked');
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(',');
											
											console.log(str);
											
										$('#myModal').modal('show');
										$('.modal-title').html("Reject AbuseVideo");
										$('#abusemsg').html("Are You Sure Want Reject Video");
										$('#ok').html('<button type="button" class="btn btn-primary" id="videook">OK</button>');
										
										$('#videook').click(function()
										{			
											$.ajax({
													url : "<?php echo base_url()?>Api/rejectContent?userId="+userid+"&fileId="+str+"&type=1&reason=Your file is rejected by admin",
													type : "GET",
													success:function(response)
													{
														var obj = JSON.parse(response);
														$('#myModal').modal('hide');
														abusevideo(videopageindex);
													},
													error:function()
													{
														alert('error');
													}
												});
										});
									}
									else
									{
										alert("Please Select Video to Reject");
									}
									
									});
									$('#approvevideo').click(function()
									{	
											var userid=-1;
										if($('[type="checkbox"]').is(":checked"))
										{		
											var all_location_id = document.querySelectorAll('input[name="rejectabusevideo[]"]:checked');
	
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(',');
											
											console.log(str);
											
											$('#myModal').modal('show');
											$('.modal-title').html("Approve Video");
											$('#abusemsg').html("Are You Sure Want Approve Video");
											$('#ok').html('<button type="button" class="btn btn-primary" id="videook">OK</button>');
											
											$('#videook').click(function()
											{				
												$.ajax({
														url : "<?php echo base_url()?>Api/approvedContent?fileId="+str+"&type=1",
														type : "GET",
														success:function(response)
														{
																var obj = JSON.parse(response);
																$('#myModal').modal('hide');
																if(obj.Status==1)
																{
																	abusevideo(videopageindex);
																}
														},
														error:function()
														{
															alert('error');
														}
													});
											});
										}
										else
										{
											alert("Please Select Video to Approve");
										}
									});
					},
					error:function()
					{
						alert('error');
					}
			});
}

function getvideolikes(id)
{
	$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=1&status=1&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("Likes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Likes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle" style="width:30px">';																	                                 }
										str+=' <a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black"><span class="w3-opacity">'+value.User.UserName+'</span></a> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
function getvideodislikes(id)
{
$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=1&status=2&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("DisLikes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom w3-opacity w3-light-grey"><p><center>No DisLikes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin w3-opacity w3-light-grey"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px">';																	                                 }
										str+=' <span class="w3-opacity">'+value.User.UserName+'</span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>