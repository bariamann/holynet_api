<!DOCTYPE html>
<html>
<title>Holynet-Setting</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link href="<?php echo base_url('assets')?>/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>

.w3-sidenav a 
{
padding:16px
}

.navimg 
{
float:left;width:33.33% !important
}
.w3-lobster 
{
  font-family: "Lobster", serif;  
}
.city 
{
display:none;
}
.on 
{
 background:green; 
}
.off 
{
 background:red; 
}

.background
{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}

#blanket 
{
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

table.dataTable th,
table.dataTable td 
{
  white-space: nowrap;
}

#popUpDiv 
{
position:absolute;
background: teal;
width:400px;
height:100px;
border:2px solid #000;
z-index: 9002;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}

.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">
	<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
	  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
	</div>

	<header class="w3-container w3-theme w3-padding-3 w3-center">
	  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
	</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>Setting</h3>
				</div>
				</div>
				
			</div>
			<div class="w3-border">
<ul class="w3-navbar w3-white">
  <li id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Profile');" id='ptype'>Profile Types</a></li>
  <li id="proftype1"><a href="#" class="tablink" onClick="openCity(event, 'Language');" id='languages'>Language</a></li>
  <li><a href="#" class="tablink" onClick="openCity(event, 'Country');" id='countries'>Country</a></li>
  <li><a href="#" class="tablink" onClick="openCity(event, 'Video');" id='videos'>Video Types</a></li>
</ul>

<div id="Profile" class="w3-container w3-border city">

 <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Profile Type<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id02').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="profilesuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displayprofile" class="w3-container table-responsive" style="width:100%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id02" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id02').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addprofile">Add Profile Type</h4>
    </header>
    <div class="w3-container" id="addnewprofile">
     <p>
			<form class="w3-container">
			
			<p>
			<input type="text" name="peng" id="peng"  class="w3-input" placeholder="Enter Name in English" onkeydown= "if(event.keyCode == 13) document.getElementById('profiletype').click()"></p>
			
			<p><input type="text" name="parabic" id="parabic"  class="w3-input" placeholder="Enter Name in Arabic" onkeydown= "if(event.keyCode == 13) document.getElementById('profiletype').click()" onChange="CheckArabicOnly(this);"></p>
			<p><h5 style="color:red;" id="profilemsg"></h5></p>
			<p>
			<span id="editprofile"><input type="button" name="profiletype" id="profiletype" value="Save" class="w3-btn w3-blue"/> </span>
			
			</p>
			
			</form>
	</p>
    </div>
	  <div class="w3-container" id="editnewprofile"> </div>
 </div>
</div>
</div>

<div id="Language" class="w3-container w3-border city">
  <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Language<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id03').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="langugaesuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displaylanguage" class="w3-container table-responsive" style="width:80%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id03" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id03').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addlanguage">Add New Language</h4>
    </header>
    <div class="w3-container" id="addnewlanguage">
     <p>
			<form class="w3-container">
			<p>
				<input type="text" name="lang" id="lang"  class="w3-input" placeholder="Enter New Language"  onkeydown= "if(event.keyCode == 13) document.getElementById('language').click()"/>
			</p>
			<p><h5 style="color:red;" id="languagemsg"></h5></p>
			<p>
			<span id="editlanguage"><input type="button" name="language" id="language" value="Save" class="w3-btn w3-blue"/> </span>
			
			</p>
			</form>
	</p>
    </div>
	 <div class="w3-container" id="editnewlanguage">
	 </div>
 </div>
</div>
</div>


<div id="Country" class="w3-container w3-border city">
   <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Country<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id04').style.display='block'" >+</a></span></h4>
				</div>
	<p></p>
				<p id="countrysuccessmsg" style="color:red;background-color:#FFFFCC;"></p>
					<div id="displaycountry" class="w3-container table-responsive" style="width:100%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id="id04" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id04').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addcountry">Add New Country</h4>
    </header>
    <div class="w3-container" id="addnewcountry">
     <p>
			<form class="w3-container">
			<p>
			<input type="text" name="countryeng" id="countryeng"  class="w3-input" placeholder="Enter Country in English" onkeydown= "if(event.keyCode == 13) document.getElementById('countrybtn').click()" /></p>
			
			<p><input type="text" name="countryarabic" id="countryarabic"  class="w3-input" placeholder="Enter Country in Arabic" onkeydown= "if(event.keyCode == 13) document.getElementById('countrybtn').click()"  onchange="CheckArabicOnly(this);"/></p>
			<p><h5 style="color:red;" id="countrymsg"></h5></p>
			<p>
			<span id="editcountry"><input type="button" name="countrybtn" id="countrybtn" value="Save" class="w3-btn w3-blue"/> </span>
			
			</p>
			</form>
	</p>
    </div>
	 <div class="w3-container" id="editnewcountry">
	 </div>
 </div>
</div>
</div>

<div id="Video" class="w3-container w3-border city">
	<p>
		 <div class="row">
			  <div class="col-sm-8">
				<div  class="w3-card-4">
					<div class="w3-container w3-teal">
					  <h4>Video Type<span class="w3-right"><a class="w3-btn-floating w3-blue" onClick="document.getElementById('id01').style.display='block'" >+</a></span></h4>
					</div>
						<p id="successmsg" style="color:red;background-color:#FFFFCC;"></p>
							<div id="displayvideo" class="w3-container table-responsive" style="width:100%">
								<p></p>
							</div>

				</div>
			</div>
		</div>
	</p>
</div>

<div id="id01" class="w3-modal">
  <div class="w3-modal-content w3-card-8"  style="width:40%">
    <header class="w3-container w3-teal">
      <span onClick="document.getElementById('id01').style.display='none'" class="w3-closebtn">&times;</span>
       <h4 id="addvideo">Add Video Type</h4>
    </header>
   	 <div class="w3-container" id="addvideotype">
    	 <p>
			<form class="w3-container">
			
			<p>
			<input type="text" name="neng" id="neng"  class="w3-input" placeholder="Enter Name in English" onkeydown= "if(event.keyCode == 13) document.getElementById('videotype').click()"/></p>
			
			<p><input type="text" name="narabic" id="narabic"  class="w3-input" placeholder="Enter Name in Arabic" onkeydown= "if(event.keyCode == 13) document.getElementById('videotype').click()"  onchange="CheckArabicOnly(this);"/></p>
			
			<p><h5 style="color:red;" id="msg"></h5></p>
			<p>
			<span id="editvideo"><input type="button" name="videotype" id="videotype" value="Save" class="w3-btn w3-blue"/> </span>
			
			</p>	
			</form>
		</p>
    </div>
	 <div class="w3-container" id="editvideotype">
	 </div>
  </div>
</div>

<div id='loadingmessage' style='display:none'>
  <center><img src='loading.gif' width="10%" height="10%"/></center>
</div>
<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-white" align="center" id="textdisplay"></div>
<div class="col-sm-2"></div><div class="col-sm-8"  align="center"><br><button class="w3-btn w3-blue" id="ok">OK</button> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="cancel">Cancel</a> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="okcan">OK</a></div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}

function CheckArabicOnly(field)
{    
        var sNewVal = "";
        var sFieldVal = field.value;
		var f=0;
        for(var i = 0; i < sFieldVal.length; i++) 
		{
	        var ch = sFieldVal.charAt(i);;
            var c = ch.charCodeAt(0);
   
   			if (field.keyCode == '13') 
			{
            	f=1;
			}
            if(c < 1536 || c > 1791) 
			{
               f=1;
            }
            else 
			{
                sNewVal += ch;
            }
			if(f==1)
			{
				$('#msg').html("Plese Enter text in Arabic").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				//	return false;
			}
        }
		
        field.value = sNewVal;
}

$(document).ready(function(event) 
{	
			openCitys('Profile');
	
			$('#proftype').addClass('redborded');
			$('#ptype').css("color", "#ffffff");
			$('#proftype1').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#languages').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#videos').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#countries').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
	 displayvideotype();
	 displayprofiletype();
	 displaylanguage();
	 displaycountry();
	//$('#ptype').click();
		$('#videotype').click(function()
		{
			var eng=$('#neng').val();
			var arb=$('#narabic').val();
			
			if(eng=="")
			{
				$('#msg').html("Plese Enter Name in English").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
			if(arb=="")
			{
				$('#msg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#msg').delay(1000).fadeOut('slow');
				return false;
			}
			
			$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/CreateVideoType?",
					type : "POST",
					data:
					{
						NameEnglish : eng,	
						NameArabic : arb
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							$('#neng').val('');
							$('#narabic').val('');
							document.getElementById('id01').style.display='none';
							$('#successmsg').html("Video Type " + obj.Message).fadeIn('slow');
							$('#successmsg').delay(1000).fadeOut('slow');
							displayvideotype();
						}
					},
					error: function()
					{
						alert('error');
					}
				});
		});
		
		$('#profiletype').click(function()
		{
			var peng=$('#peng').val();
			var parb=$('#parabic').val();
			
			if(peng=="")
			{
				$('#profilemsg').html("Plese Enter Name in English").fadeIn('slow');
				$('#profilemsg').delay(1000).fadeOut('slow');
				return false;
			}
			if(parb=="")
			{
				$('#profilemsg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#profilemsg').delay(1000).fadeOut('slow');
				return false;
			}
			$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/CreateProfileType?",
					type : "POST",
					data:
					{
						NameEnglish : peng,	
						NameArabic : parb
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							$('#peng').val('');
							$('#parabic').val('');
							document.getElementById('id02').style.display='none';
							$('#profilesuccessmsg').html("Profile Type " + obj.Message).fadeIn('slow');
							$('#profilesuccessmsg').delay(1000).fadeOut('slow');
							displayprofiletype();
						}
					},
					error: function()
					{
						alert('error');
					}
				});
		});
		
		$('#language').click(function()
		{
			var language=$('#lang').val();
			
			if(language=="")
			{
				$('#languagemsg').html("Plese Enter Language").fadeIn('slow');
				$('#languagemsg').delay(1000).fadeOut('slow');
				return false;
			}
			$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/addLanguage?Name="+language,
					type : "POST",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#languagemsg').html(obj.Message).fadeIn('slow');
							$('#languagemsg').delay(1000).fadeOut('slow');
						}
						if(obj.Status==1)
						{
							$('#lang').val('');
							document.getElementById('id03').style.display='none';
							$('#langugaesuccessmsg').html("Language Added" + obj.Message).fadeIn('slow');
							$('#langugaesuccessmsg').delay(1000).fadeOut('slow');
							displaylanguage();
						}
					},
					error: function()
					{
						alert('error');
					}
				});
		});
		$('#countrybtn').click(function()
		{
			var coneng=$('#countryeng').val();
			var conarb=$('#countryarabic').val();
			
			if(coneng=="")
			{
				$('#countrymsg').html("Plese Enter Country in English").fadeIn('slow');
				$('#countrymsg').delay(1000).fadeOut('slow');
				return false;
			}
			if(conarb=="")
			{
				$('#countrymsg').html("Plese Enter Country in Arabic").fadeIn('slow');
				$('#countrymsg').delay(1000).fadeOut('slow');
				return false;
			}
			$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/addCountry?",
					type : "POST",
					data:
					{
						NameEnglish : coneng,	
						NameArabic : conarb
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#countrymsg').html(obj.Message).fadeIn('slow');
							$('#countrymsg').delay(1000).fadeOut('slow');
						}
						if(obj.Status==1)
						{
							$('#coneng').val('');
							$('#conarb').val('');
							document.getElementById('id04').style.display='none';
							$('#countrysuccessmsg').html("Country added " + obj.Message).fadeIn('slow');
							$('#countrysuccessmsg').delay(1000).fadeOut('slow');
							displaycountry();
						}
					},
					error: function()
					{
						alert('error');
					}
				});
		});
});

function displayvideotype()
{
		$.get( "<?php echo base_url(); ?>Videotype/displayVideotype", function( data ) {
		//	alert(data);
	  $("#displayvideo").html(data);
	  $('#listoutlet').DataTable({
		"language": {
            "zeroRecords": "No VideoType Found",
            "infoEmpty": "No VideoType Found",
        },
		});
		});
}

function displayprofiletype()
{
		$.get( "<?php echo base_url(); ?>Videotype/displayProfiletype", function( data ) {
		//alert(data);
	  $("#displayprofile").html(data);
	  $('#listprofile').DataTable({
		"language": {
            "zeroRecords": "No ProfileType Found",
            "infoEmpty": "No ProfileType Found",
        },
		})
		});
}

function displaylanguage()
{
		$.get( "<?php echo base_url(); ?>Videotype/displayLanguage", function( data ) {
	  $("#displaylanguage").html(data);
	  $('#listlaguage').DataTable({
		"language": {
            "zeroRecords": "No VideoType Found",
            "infoEmpty": "No VideoType Found",
        },
		});
		});
}

function displaycountry()
{
		$.get( "<?php echo base_url(); ?>Videotype/displayCountry", function( data ) {
	  $("#displaycountry").html(data);
	  $('#listcountry').DataTable({
		"language": {
            "zeroRecords": "No Counrty Found",
            "infoEmpty": "No Counrty Found",
        },
		});
		});
}

function editvideotype(videoid)
{
$.ajax({
			url : "<?php echo base_url('Videotype/editVideotype'); ?>/"+videoid,
			type : "GET",
			success:function(response)
			{
				$('#addvideotype').hide();
				$('#editvideotype').show();
				var obj = JSON.parse(response);
						if(obj.Status==1)
						{
							$('#id01').show();
							$('#addvideo').html("Edit Video Type");
							$('#editvideotype').html('<p><form class="w3-container"><p><input type="text" name="editneng" id="editneng"  class="w3-input" placeholder="Enter Name in English"/></p><p><input type="text" name="editnarabic" id="editnarabic"  class="w3-input" placeholder="Enter Name in Arabic" onchange="CheckeditArabicOnly(this);"/></p><p><h5 style="color:red;" id="editmsg"></h5></p><p><input type="button" name="videotypeupdate'+videoid+'" id="videotypeupdate'+videoid+'" value="Update" class="w3-btn w3-blue"/> <input type="button" name="CancelVideo'+videoid+'" id="CancelVideo'+videoid+'" value="Cancel" class="w3-btn w3-blue"/></p></form></p>');
							
							$('.w3-closebtn').hide();
							$.each(obj.Data, function (key, value) 
							{
								updatevideotype(videoid,value.nameEnglish,value.nameArabic);		
							});
						}
			},
			error: function()
			{
				alert('error');
			}
		});
}
function updatevideotype(id,neng,narbc)
{
		$('#editneng').val(neng);
		$('#editnarabic').val(narbc);
		
			$('#editneng').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('videotypeupdate'+id).click()
					}
				});	
				
		$('#editnarabic').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('videotypeupdate'+id).click()
					}
				});		
	
		$('#CancelVideo'+id).click(function()
		{
			$('#neng').val('');	
			$('#narabic').val('');	
			$('#addvideo').html("Add Video Type");
				$('#addvideotype').show();
				$('#editvideotype').hide();
			document.getElementById('id01').style.display='none';
			$('.w3-closebtn').show();
		});	
		
	$('#videotypeupdate'+id).click(function()
	{
			var eng=$('#editneng').val();
			var arb=$('#editnarabic').val();
			
			if(eng=="")
			{
				$('#editmsg').html("Plese Enter Name in English").fadeIn('slow');
				$('#editmsg').delay(1000).fadeOut('slow');
				return false;
			}
			if(arb=="")
			{
				$('#editmsg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#editmsg').delay(1000).fadeOut('slow');
				return false;
			}
	$.ajax({
			url : "<?php echo site_url('Videotype/updateVideotype');?>/"+id,
			type : "POST",
			data:
			{
				NameEnglish : eng,	
				NameArabic : arb
			},
			success:function(response)
			{
			
				var obj = JSON.parse(response);
				if(obj.Status==1)
				{
					$('#id01').hide();
					popup();
					$('#ok').hide();
					$('#cancel').hide();
					$('#textdisplay').html(obj.Message);
					$('#okcan').click(function()
					{
						displayvideotype();
						$('#neng').val('');
						$('#narabic').val('');
						$('#addvideo').html("Add Video Type");
						$('#addvideotype').show();
						$('#editvideotype').hide();
						$('.w3-closebtn').show();
					});
				}
			}
		});
	});
}
function editprofiletype(id)
{
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/editProfiletype/"+id,
			type : "GET",
			success:function(response)
			{
				$('#addnewprofile').hide();
				$('#editnewprofile').show();
				var obj = JSON.parse(response);
						if(obj.Status==1)
						{
							$('#id02').show();
							$('#addprofile').html("Edit Profile Type");
							$('#editnewprofile').html(' <p><form class="w3-container"><p><input type="text" name="proeng" id="proeng"  class="w3-input" placeholder="Enter Name in English"></p><p><input type="text" name="proarabic" id="proarabic"  class="w3-input" placeholder="Enter Name in Arabic" onchange="CheckeditArabicOnly(this);"></p><p><h5 style="color:red;" id="profileeditmsg"></h5></p><p><input type="button" name="profiletypeupdate'+id+'" id="profiletypeupdate'+id+'" value="Update" class="w3-btn w3-blue"/> <input type="button" name="CancelProfile'+id+'" id="CancelProfile'+id+'" value="Cancel" class="w3-btn w3-blue"/></p></form></p>');
							
							$('.w3-closebtn').hide();
							$.each(obj.Data, function (key, value) 
							{
								updateprofiletype(id,value.nameEnglish,value.nameArabic);
							});
							
									
						}
			},
			error: function()
			{
				alert('error');
			}
		});
}
function updateprofiletype(id,neng,narb)
{
	$('#proeng').val(neng);
	$('#proarabic').val(narb);
	
		$('#proeng').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('profiletypeupdate'+id).click()
					}
				});	
				
		$('#proarabic').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('profiletypeupdate'+id).click()
					}
				});							

	$('#CancelProfile'+id).click(function()
		{
			$('#peng').val('');	
			$('#parabic').val('');	
			$('#addprofile').html("Add Profile Type");
		//	$('#profiletypeupdate'+id).hide();
			//$('#editprofile').html('<input type="button" name="profiletype" id="profiletype" value="Save" class="w3-btn w3-blue"/>');
		//	$('#profiletype').show();
			$('#addnewprofile').show();
			$('#editnewprofile').hide();
			$('#CancelProfile'+id).hide();
			document.getElementById('id02').style.display='none';
			$('.w3-closebtn').show();
		});
	
	$('#profiletypeupdate'+id).click(function()
	{
			var peng=$('#proeng').val();
			var parb=$('#proarabic').val();
			
			if(peng=="")
			{
				$('#profileeditmsg').html("Plese Enter Name in English").fadeIn('slow');
				$('#profileeditmsg').delay(1000).fadeOut('slow');
				return false;
			}
			if(parb=="")
			{
				$('#profileeditmsg').html("Plese Enter Name in Arabic").fadeIn('slow');
				$('#profileeditmsg').delay(1000).fadeOut('slow');
				return false;
			}
				
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/updateProfiletype/"+id,
			type : "POST",
			data:
			{
				NameEnglish : peng,	
				NameArabic : parb
			},
			success:function(response)
			{
			
				var obj = JSON.parse(response);
				if(obj.Status==1)
				{
					$('#id02').hide();
					popup();
					$('#ok').hide();
					$('#cancel').hide();
					$('#textdisplay').html(obj.Message);
					$('#okcan').click(function()
					{
						displayprofiletype();
						$('#peng').val('');
						$('#parabic').val('');
						$('#addprofile').html("Add Profile Type");
						$('#addnewprofile').show();
						$('#editnewprofile').hide();
						$('.w3-closebtn').show();
					});
				}
			}
		});
	});
}
function editlanguage(id)
{
	//alert(id);
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/editLanguage/"+id,
			type : "GET",
			success:function(response)
			{
				$('#addnewlanguage').hide();
				$('#editnewlanguage').show();

				var obj = JSON.parse(response);
						if(obj.Status==1)
						{
							$('#id03').show();
							$('#addlanguage').html("Edit Language");
							$('#editnewlanguage').html('<p><form class="w3-container"><p><input type="text" name="editlang" id="langedit"  class="w3-input" placeholder="Enter New Language"/></p><p><h5 style="color:red;" id="editlanguagemsg"></h5></p><p><input type="button" name="languageupdate'+id+'" id="languageupdate'+id+'" value="Update" class="w3-btn w3-blue"/> <input type="button" name="CancelLanguage'+id+'" id="CancelLanguage'+id+'" value="Cancel" class="w3-btn w3-blue"/> </p></form></p>');
							$('.w3-closebtn').hide();
							$.each(obj.Data, function (key, value) 
							{
								updatelanguage(id,value.name);
							});		
						}
			},
			error: function()
			{
				alert('error');
			}
		});
}



function updatelanguage(id,name)
{
	$('#langedit').val(name);
	$('#langedit').keypress(function(e) 
	{
			if (e.which == '13') 
			{
				document.getElementById('languageupdate'+id).click()
			}
	});	
		$('#CancelLanguage'+id).click(function()
		{
			$('#lang').val('');	
			$('#addlanguage').html("Add language");
			$('#addnewlanguage').show();
			$('#editnewlanguage').hide();
			document.getElementById('id03').style.display='none';
			$('.w3-closebtn').show();
		});
	
		$('#languageupdate'+id).click(function()
		{
			var language=$('#langedit').val();
			
			if(language=="")
			{
				$('#editlanguagemsg').html("Plese Enter Language").fadeIn('slow');
				$('#editlanguagemsg').delay(1000).fadeOut('slow');
				return false;
			}
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/updateLangugae/"+id,
			type : "POST",
			data:
			{
				Language : language,	
			},
			success:function(response)
			{
			
				var obj = JSON.parse(response);
				if(obj.Status==1)
				{
					$('#id03').hide();
					popup();
					$('#ok').hide();
					$('#cancel').hide();
					$('#textdisplay').html(obj.Message);
					$('#okcan').click(function()
					{
						displaylanguage();
						$('#lang').val('');
						$('#addlanguage').html("Add language");
						$('#addnewlanguage').show();
						$('#editnewlanguage').hide();
						$('.w3-closebtn').show();
					});
					
				}
			}
		});
	});
}
function EditCountry(id)
{
	//alert(id);
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/editCountry/"+id,
			type : "GET",
			success:function(response)
			{
				$('#addnewcountry').hide();
				$('#editnewcountry').show();
				var obj = JSON.parse(response);
						if(obj.Status==1)
						{
							$('#id04').show();
							$('#addcountry').html("Edit Country");
							$('#editnewcountry').html('<p><form class="w3-container"><p><input type="text" name="editcountry" id="editcountryeng" class="w3-input" placeholder="Enter Country in English"/></p><p><input type="text" name="editcountryarabic" id="editcountryarabic"  class="w3-input" placeholder="Enter Country in Arabic" onchange="CheckeditArabicOnly(this);"/></p><p><h5 style="color:red;" id="editcountrymsg"></h5></p><p><input type="button" name="countryupdate'+id+'" id="countryupdate'+id+'" value="Update" class="w3-btn w3-blue"/> <input type="button" name="CancelCountry'+id+'" id="CancelCountry'+id+'" value="Cancel" class="w3-btn w3-blue"/></p></form></p>');
							$('.w3-closebtn').hide();
							
							$.each(obj.Data, function (key, value) 
							{
								updatecountry(id,value.nameEnglish,value.nameArabic);
							});		
						}
			},
			error: function()
			{
				alert('error');
			}
		});
}



function updatecountry(id,nameen,namear)
{
	$('#editcountryeng').val(nameen);
	$('#editcountryarabic').val(namear);
	
	$('#editcountryeng').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('countryupdate'+id).click()
					}
				});	
				
		$('#editcountryarabic').keypress(function(e) {
					if (e.which == '13') {
						document.getElementById('countryupdate'+id).click()
					}
				});	
				
		$('#CancelCountry'+id).click(function()
		{
			$('#addcountry').html("Add Country");
			$('#countryeng').val('');
			$('#countryarabic').val('');
			$('#addnewcountry').show();
			$('#editnewcountry').hide();
			document.getElementById('id04').style.display='none';
			$('.w3-closebtn').show();
		});
	
	
		$('#countryupdate'+id).click(function()
		{
			var nameen=$('#editcountryeng').val();
			var namear=$('#editcountryarabic').val();
			
			if(nameen=="")
			{
				$('#editcountrymsg').html("Plese Enter Country English").fadeIn('slow');
				$('#editcountrymsg').delay(1000).fadeOut('slow');
				return false;
			}
			
			if(namear=="")
			{
				$('#editcountrymsg').html("Plese Enter Country Arabic").fadeIn('slow');
				$('#editcountrymsg').delay(1000).fadeOut('slow');
				return false;
			}
	$.ajax({
			url : "<?php echo base_url(); ?>Videotype/updateCountry/"+id,
			type : "POST",
			data:
			{
				Name_en : nameen,	
				Name_arb : namear
			},
			success:function(response)
			{
			
				var obj = JSON.parse(response);
				if(obj.Status==1)
				{
					$('#id04').hide();
					popup();
					$('#ok').hide();
					$('#cancel').hide();
					$('#textdisplay').html(obj.Message);
					$('#textdisplay').html(obj.Message);
					$('#okcan').click(function()
					{
						displaycountry();
						$('#addcountry').html("Add Country");
						$('#countryeng').val('');
						$('#countryarabic').val('');
						$('#addnewcountry').show();
						$('#editnewcountry').hide();
						$('.w3-closebtn').show();	
					});
				}
			}
		});
	});
}
function CheckeditArabicOnly(field)
{    
        var sNewVal = "";
        var sFieldVal = field.value;

        for(var i = 0; i < sFieldVal.length; i++) 
		{
	        var ch = sFieldVal.charAt(i);;
            var c = ch.charCodeAt(0);
   
            if(c < 1536 || c > 1791) 
			{
              var f=1;
            }
            else 
			{
                sNewVal += ch;
            }
        }
		if(f==1)
		{
			$('#msg').html("Plese Enter text in Arabic").fadeIn('slow');
			$('#msg').delay(1000).fadeOut('slow');
			//	return false;
		}
        field.value = sNewVal;
}

function ConfirmDelete(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Video type");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
				url : "http://dev.mobileartsme.com/holynet/Api/deleteVideoType?VideoTypeId="+id,
					type : "POST",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displayvideotype();
						}
					}	
			});
		});
}
function ConfirmDeleteProfile(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Profile type");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
				url : "http://dev.mobileartsme.com/holynet/Api/deleteProfileType?ProfileId="+id,
					type : "POST",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displayprofiletype();
						}
					}	
			});
		});
}
function ConfirmDeleteLanguage(id)
{
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Language");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
					url : "http://52.203.171.109/holynet/api/deleteLanguage?Id="+id,
					type : "GET",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#textdisplay').html(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displaylanguage();	
						}
					}	
			});
		});
}
function ConfirmDeleteCountry(id)
{
	//alert(id);
	popup();
	$('#textdisplay').html("Are you Sure you want to delete Country");
	$('#okcan').hide();
	$('#ok').show();
	$('#cancel').show();
	$('#ok').click(function()
	{
		$.ajax({
					url : "http://52.203.171.109/holynet/api/deleteCountry?Id="+id,
					type : "GET",
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#textdisplay').html(obj.Message);
						}
						if(obj.Status==1)
						{
							popup('popUpDiv');
							$('#textdisplay').html(obj.Message);
							$('#ok').hide();
							displaycountry();	
						}
					}	
			});
		});
}
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

