<head>

   <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet">
	 <title>Category Approved Videos</title>
 </head> 
      <div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="col-sm-12 form-box">
					<div class="form-top">
							<div class="form-top-left"><h3 style="color:#0197d8;" id="categoryname"></h3></div>
							<div class="col-sm-3 form-group" style="padding-top:20px;">
										<input type="text" class="form-control" name="search" id="search" placeholder="Search">
								</div>
							</div>
								<div class="form-bottom col-lg-12"><div class="table-responsive col-lg-12">
									<div class="col-lg-12 text-center"></div>
								</div>
									<div class='row'>
											<div class='col-sm-2' style='background-color: #f1f1f1;height: 100%;'>
												
													</div><div id="pagination" align="center" class='col-sm-8'></div><div class='col-sm-12'><div id='display'></div></div>
											</div>
									</div>
					</div>
			</div>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">Approve Video
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		<div class="row" id="comment">

		</div>
		 <div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Title</label>
			                        	<input type="text" name="title" id="title" placeholder="New Video Title" class="form-first-name form-control">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Video Description</label>
			                        	<textarea name="description" id="description" placeholder="New Video Description" 
			                        				class="form-about-yourself form-control"></textarea>
			                        </div>
			                        <div class="form-group">
									<?php
											$this->db->select('*');
											$this->db->from('categorymaster');
											$this->db->where('Status',1);
											$query=$this->db->get();
											$result=$query->result();
											//print_r($result);
									
								?>
			                        	<select class="form-email form-control" id="category" name="category">
											<option value="none" selected="selected">-------------------------Select New Category-------------------------</option>
											<?php
											foreach($result as $row)
											{
											?>
												<option value="<?php echo $row->Id;?>"><?php echo $row->Name;?></option>
											<?php
											}
											?>
										</select>
			                        </div>
									 <div class="form-group">
										 <input type="text" name="hashtag" id="hashtag" placeholder="Edit Hashtag" class="form-first-name form-control">
									 </div>
									<div align="right">
			                        <input type="submit" class="btn btn-primary" value="Approve Video" id="Updatevideo" name="Updatevideo">
			                    </div>
        </div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>	
var categoryid = getParameterByName('Id');
var totalnopage=0;
var pageindex1=0;
var name = getParameterByName('Name');
$(document).ready(function() 
{
	//alert(categoryid);
	$('.alert').hide();
	$('#categoryname').html(name);
	approvedvideos(categoryid);
	$('#search').keypress(function()
			{
								
					if(event.keyCode == 13)
					{
						$('#pagination').hide();
						var input=$('#search').val();
									//alert(input);
						str='';
					$.ajax({
							type:'POST',
							url:'http://dev.mobileartsme.com/holytube/Apis/searchVideos?CategoryId='+catid+'&Title='+input+'&pageIndex='+pageindex1+'',
							data:{'Input':input},
							success:function(responseData)
							{	
								var d = JSON.parse(responseData);
				//alert(d.Status);
				if(d.Status==0)
				{
					$('#display').html('<div id="four-columns" class="grid-container" style="border:thick;font-size:1.8em;color:red"><center>No Videos Available for this category</center></div>');
				}
				else
				{
					//alert(d.Status);
					str='';
					count=0;
					totalnopage=d.TotalPage;
						str+="<br/><div id='four-columns' class='grid-container'><ul class='rig columns-4'>"
								$.each(d.Data, function (key, value) 
								{
									id=value.Id;
									//alert(value.Id);
									var title=value.Title;
									var desc=value.Description;
									var dur=value.Duration;
									var uplaoddate=value.UploadDate;
									var url=value.FileURL;
									var thmub=value.FileThumb;
									var userid=value.UserId;
									var username=value.UserName;
									
								
									str+='<li><a href='+value.FileURL+'><img src='+value.FileThumb+' style="padding-top:0px;height:150px"></a><div class="row"><div class="col-sm-6"><h6 style="color:#0197d8;">'+value.Title+'</h6></div><div class="col-sm-6"><a href="<?php echo base_url()?>Comment/index?Id='+value.Id+'&title='+value.Title+'&desc='+value.Description+'&url='+value.FileURL+'&thumb='+value.FileThumb+'&date='+value.UploadDate+'" style="height:20px;width:50%;"><h6 style="color:#0197d8;">View Comment</h6></a></div></div><div>Submitted Date:'+value.UploadDate+'</div><div>Duration: '+value.Duration+'</div><div>'+value.Description+'</div>';
					str+='<div style="border-top:solid 1px #d2d2d2;padding-top:3px;padding-bottom:5px;"> <div class="row"><div class="col-sm-6"><a class="btn btn-xs" style="background-color:#00CC66;height:20px;width:100%" id="Approve" href="javascript:void(0)" onclick="ConfirmApprove('+value.Id+')" title="Approve" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-ok" style="color:#FFFFFF;text-align:left;"></span></a></div> <div class="col-sm-6"><a class="btn btn-danger btn-xs" style="height:20px;width:100%" id="delete" href="javascript:void(0)" onclick="ConfirmDelete('+value.Id+')" title="Reject" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-remove"></span></a></div></div></div></li>';
										count++;
										counting=count;
										
									if(count!=counting)
									{	
										str+='</ul></div><li></li>';
										count=0;
									}	
									
								});			
								$('#display').html(str);
								$('#search').val('');
							}
							}
						});
				}
			});
 });

function approvedvideos(categoryid,pageindex)
{
	catid=categoryid;
	//var pageindex1=0;
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "http://dev.mobileartsme.com/holytube/Apis/getAcceptedVideos?CategoryId="+catid+"&pageindex="+pageindex1+"",
			crossDomain: true,
            data: "",
			success:function(responseData,textStatus,jqXHR)
			{
				var d = JSON.parse(responseData);
				//alert(d.Status);
				if(d.Status==0)
				{
					$('#display').html('<div id="four-columns" class="grid-container" style="border:thick;color:red;font-size:1.8em"><center>No Videos Available</center></div>');
				}
				else
				{
					//alert(d.Status);
					str='';
					count=0;
					totalnopage=d.TotalPage;
						str+="<br/><div id='four-columns' class='grid-container'><ul class='rig columns-4'>"
								$.each(d.Data, function (key, value) 
								{
									id=value.Id;
									//alert(value.Id);
									var title=value.Title;
									var desc=value.Description;
									var dur=value.Duration;
									var uplaoddate=value.UploadDate;
									var url=value.FileURL;
									var thmub=value.FileThumb;
									var userid=value.UserId;
									var username=value.UserName;
									
								
									str+='<li><a href='+value.FileURL+'><img src='+value.FileThumb+' style="padding-top:0px;height:150px"></a><div class="row"><div class="col-sm-6"><h6 style="color:#0197d8;">'+value.Title+'</h6></div><div class="col-sm-6"><a href="<?php echo base_url()?>Comment/index?Id='+value.Id+'&title='+value.Title+'&desc='+value.Description+'&url='+value.FileURL+'&thumb='+value.FileThumb+'&date='+value.UploadDate+'" style="height:20px;width:50%;"><h6 style="color:#0197d8;">View Comment</h6></a></div></div><div>'+value.UploadDate+'</div><div>Duration: '+value.Duration+'</div><div>'+value.Description+'</div>';
					str+='<div style="border-top:solid 1px #d2d2d2;padding-top:3px;padding-bottom:5px;"> <div class="row"><div class="col-sm-6"><a class="btn btn-xs" style="background-color:#00CC66;height:20px;width:100%" id="Approve" href="javascript:void(0)" onclick="ConfirmApprove('+value.Id+')" title="Approve" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-ok" style="color:#FFFFFF;text-align:left;"></span></a></div> <div class="col-sm-6"><a class="btn btn-danger btn-xs" style="height:20px;width:100%" id="delete" href="javascript:void(0)" onclick="ConfirmDelete('+value.Id+')" title="Reject" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-remove"></span></a></div></div></div></li>';
										count++;
										counting=count;
										
									if(count!=counting)
									{	
										str+='</ul></div><li></li>';
										count=0;
									}	
									
								});
									$('#display').html(str);
									str1='';
									if(d.TotalPage>1)
										{
											str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;padding:10px">Previous</span></div>';
											str1+='<div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;padding:10px">Next</span></div>';
											$('#pagination').html(str1);
										
										$('#prev').click(function()
										{
											
											//alert(totalnopage);
											//alert(pageindex1);	
											if(pageindex1>0)
											{
												pageindex1--;
												approvedvideos(categoryid,pageindex1);
											}
											
										});
										
										$('#next').click(function()
										{	
											pageindex1++;
											//alert(totalnopage);
											//alert(pageindex1);	
											if(totalnopage>pageindex1)
											{
												approvedvideos(categoryid,pageindex1);
											}
											if(totalnopage==pageindex1)
											{
												pageindex1--;
												approvedvideos(categoryid,pageindex1);
												document.getElementById('next').style.display='none'; 
											}
										});
									}
				}
			}
			});
}
function redirect()
{

}
function ConfirmApprove(id)
{
	//alert(id);
	if (confirm("Are you sure you want to Approve This Video!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/viewrecord');?>/"+id,
						success:function(data)
						{
								var d = JSON.parse(data);
							$.each(d, function (key, value) 
							{
								callmodal(id,value.Title,value.Description,value.CategoryId,value.Keywords);
							});
						}
			});
			}
}
function callmodal(id,tit,desc,cat,keyword)
 {
 	//alert(id);
	$("#myModal").modal('show');
	$('#title').val(tit);
	$('#description').val(desc);
	$('#category').val(cat);
	$('#hashtag').val(keyword);
	
 	$('#Updatevideo').click(function()
	{
		var title=$('#title').val();
		var description=$('#description').val();
		var category=$('#category').val();
		var keyword=$('#hashtag').val();
		
		if(title=="")
		{
			alert("please enter Title");
			return false;
		}
		if(description=="")
		{
			alert("please enter Description");
			return false;
		}
		if(category=="none")
		{
			alert("please Select Category");
			return false;
		}
		if(keyword=="")
		{
			alert("Please Enter keyword");
			return false;
		}
	
			$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewDateApproved/updateVideo');?>/"+id,
						data : {title:title,description:description,category:category,keyword:keyword},
						success:function(data)
						{
								if(data==1)
								{
									location.reload();
								}
						}
				});
				});
}

function ConfirmDelete(id)
{
	//alert(id);
	if (confirm("Are you sure you want to Delete Category!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewDateApproved/deletevideo');?>/"+id,
						success:function(data)
						{
							//alert(data);
								if(data==1)
								{
									location.reload();
								}
						}
			});
			}
}
function mycommentmodal(id,username,comment,datetime,userid)
{
	$('.form-group').hide();
	$('#Updatevideo').hide();
	$("#myModal").modal('show');
	$('#title').val(username);
	$('#description').val(comment);
	$('#hashtag').val(datetime);
}

function getParameterByName(name) 
   {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
