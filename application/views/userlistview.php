<!DOCTYPE html>
<html>
<title>User List</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background: teal;
width:400px;
height:100px;
border:2px solid #000;
z-index: 9002;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
border-bottom: 6px solid;
border-color: #f44336!important;
}
.btn-responsive {
    white-space: normal !important;
    word-wrap: break-word;
	width:100%;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	  
  

  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>User List</h3>
				</div>
				</div>
				
			</div>
			 
			<div class="w3-row">
  <a href="#" onClick="openCity(event, 'London');" id="londonclick">
    <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding active" id="normalusers">Normal Users</div>
  </a>
  <a href="#" onClick="openCity(event, 'Paris');" id="parisclick">
    <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding" id="vierfiedusers">Verified Users</div>
  </a>
</div>
<div id='loadingmessage' style='display:none'>
  <center><img src='loading.gif' width="10%" height="10%"/></center>
</div>
<div id="London" class="city">
  
</div>
<div id="Paris" class="city">
  
</div>

<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
  </div>
</div>


<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>

<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p id="abusemsg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <span id="ok"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
<script>

var totalnopage=0;
var pageindex1=0;

var usertotalnopage=0;
var userpageindex1=0;

function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}

function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}
$(document).ready(function(event) 
{
			 normaluser(pageindex1);
			specialuser(userpageindex1);
			openCitys('London');
			$('#normalusers').removeClass('w3-bottombar');
			$('#normalusers').addClass('redborded');
			$('#vierfiedusers').click(function()
			{
				$('#normalusers').removeClass('redborded');
				$('#normalusers').addClass('w3-bottombar');
			});
});
function normaluser(pageindex1)
{
		$.ajax({
					url : "<?php echo base_url()?>Api/getUsers?IsVerified=0&lang=en&PageIndex="+pageindex1,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
							totalnopage=obj.TotalPage;
							var myString = 'popUpDiv';
								str+='<p></p><div id="pagination" class="row" align="center"></div> <div class="w3-row">';
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
									str+='<div class="col-sm-4"><div class="w3-content" style="margin-top:10px"> <div class="w3-card-2 w3-round w3-white">    <div class="w3-container">   <h4 class="w3-center"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.UserId+'" style="color:black;text-transform:capitalize;">'+value.FullName+'</a><span class="w3-right"><a id="delete" href="javascript:void(0)" onclick="deleteusers('+value.UserId+')" title="Delete" class="icon-2 info-tooltip"><i class="fa fa-trash" aria-hidden="true"></i></a></span></h4> <p class="w3-center">';
									if(value.ProfileThumbImage=="null" ||value.ProfileThumbImage=="" )
									{
										str+='<img src="img_avatar3.png" class="w3-circle" style="height:80px;width:80px" alt="Avatar">';
									}
									else
									{
										str+='<span id="image1'+value.UserId+'"><img src="'+value.ProfileThumbImage+'" alt="Avatar" class="w3-circle" style="height:80px;width:80px" onError="doaction('+value.UserId+');"></span>';
									}
								
								str+='</p> <hr>';
				str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.Email+'</p>';
				str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-mobile fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.MobileNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-fax  fa-fw w3-margin-right w3-text-theme" aria-hidden="true" style="color:teal;"></i> '+ value.FaxNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i> '+ value.HomeTown+','+ value.CurrentAddress+','+value.CountryName+'</p>';
								str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-user fa-fw w3-margin-right w3-text-theme"></i> '+ value.ProfileTitle+'</p>';
								str+='<p style="font-size: 0.865em; line-height: 1em; "><a id="specialuser" class="w3-btn w3-theme  w3-hover-red btn-responsive" href="javascript:void(0)" onclick="makespecialuser('+value.UserId+')">Make Verify User</a></p>';
								str+='</div></div></div></div>';
								
								});
								str+='</div>';
						}
						$('#London').html(str);
						//$('#London').append($('#close'));
								var str1='';
								if(obj.TotalPage>1)
								{
									str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
									str1+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#pagination').html(str1);	
									$('#prev').click(function()
										{	
											if(pageindex1>0)
											{
												pageindex1--;
												normaluser(pageindex1);
											}
											
										});
										
										$('#next').click(function()
										{	
											pageindex1++;
											if(totalnopage>pageindex1)
											{
												normaluser(pageindex1);
											}
											if(totalnopage==pageindex1)
											{
												pageindex1--;
												normaluser(pageindex1);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
					},
					error:function()
					{
						alert('error');
					}
			});
}
function specialuser(userpageindex1)
{
	$.ajax({
					url : "<?php echo base_url()?>Api/getUsers?IsVerified=1&lang=en&PageIndex="+userpageindex1,
					type : "GET",
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
							
						if(obj.Status==1)
						{
								str+=' <p></p><div id="userpagination" class="row" align="center"></div><div class="w3-row">';
								$.each(obj.Data, function (key, value) 
								{
									usertotalnopage=obj.TotalPage;
									
									str+='<div class="col-sm-4"><div class="w3-content" style="margin-top:10px"> <div class="w3-card-2 w3-round w3-white">    <div class="w3-container">   <h4 class="w3-center"><i class="fa fa-check" style="color:green" aria-hidden="true"></i> <a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.UserId+'" style="color:black;text-transform:capitalize;">'+value.FullName+'</a><span class="w3-right"><a id="delete" href="javascript:void(0)" onclick="deleteusers('+value.UserId+')" title="Delete" class="icon-2 info-tooltip"><i class="fa fa-trash" aria-hidden="true"></i></a></span></h4> <p class="w3-center">';
									if(value.ProfileThumbImage=="null" || value.ProfileThumbImage=="")
									{
										str+='<img src="img_avatar3.png" class="w3-circle" style="height:80px;width:80px" alt="Avatar">';
									}
									else
									{
										str+='<span id="image'+value.UserId+'"><img src="'+value.ProfileThumbImage+'" alt="Avatar" class="w3-circle" style="height:80px;width:80px" onError="doSomething('+value.UserId+');"></span>';
									}
									
									str+='</p> <hr>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.Email+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-mobile fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.MobileNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-fax  fa-fw w3-margin-right w3-text-theme" aria-hidden="true" style="color:teal;"></i> '+ value.FaxNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i> '+ value.HomeTown+','+ value.CurrentAddress+','+value.CountryName+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-user fa-fw w3-margin-right w3-text-theme"></i> ProfileType. '+ value.ProfileTitle+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><a id="normaluser" class="w3-btn w3-theme w3-hover-red btn-responsive" href="javascript:void(0)" onclick="makenormaluser('+value.UserId+')">Make Normal User</a></p>';
								str+='</div></div></div></div>';
								
								});
								str+='</div>';
								
								$('#Paris').html(str);
								
								var str1='';
								if(obj.TotalPage>1)
								{
							str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div>';
							str1+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#userpagination').html(str1);	
									$('#prev').click(function()
										{	
											if(userpageindex1>0)
											{
												userpageindex1--;
												specialuser(userpageindex1);
											}
											
										});
										
										$('#next').click(function()
										{	
											userpageindex1++;
											if(usertotalnopage>userpageindex1)
											{
												specialuser(userpageindex1);
											}
											if(usertotalnopage==userpageindex1)
											{
												userpageindex1--;
												specialuser(userpageindex1);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
						}
					},
					error:function()
					{
						alert('error');
					}
			});

}
function doSomething(id)
{
	var str='';
	str='<img src="<?php echo base_url()?>/img_avatar3.png"  class="w3-left w3-circle w3-margin-right" style="width:30px">';
	//alert(str);	
	$('#image'+id).html(str);
}
function doaction(id)
{
	var str='';
	str='<img src="<?php echo base_url()?>/img_avatar3.png"  class="w3-left w3-circle w3-margin-right" style="width:30px">';
	//alert(str);	
	$('#image1'+id).html(str);
}
function deleteusers(userid)
{
	//popup();
		$('#myModal').modal('show');
		$('.modal-title').html("Delete User");
		$('#abusemsg').html("Are You Sure Want Delete User");
		$('#ok').html('<button type="button" class="btn btn-primary" id="postok">OK</button>');
		
	$('#postok').click(function()
	{
		$.ajax({
				url : "<?php echo base_url()?>Api/deleteUser?",
					type : "POST",
					data : 
					{
							UserId : userid,
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						else
						{
						
						}
						if(obj.Status==1)
						{
							$('#myModal').modal('hide');
							normaluser();
							specialuser()
						}
					}	
			});
		});
}

function deletespecusers(id)
{
alert(id);
}
 
 function makespecialuser(userid)
 {
 	var str='<input type="hidden" name="Userid" id="Userid" value="'+userid+'">';
	$('#special').html(str);
	var userid=$('#Userid').val();
	$.ajax({
					url : "<?php echo base_url()?>Api/makeVerifiedProfile?",
					type : "POST",
					data : 
					{
							UserId :userid,
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						
						if(obj.Status==0)
						{
							alert(obj.Message);
							$('#specialuser').show();
						}
						if(obj.Status==1)
						{
							$('#specialuser').hide();
						}
						 normaluser();
						 specialuser()
					}	
			});
 }
 function makenormaluser(userid)
 {
 	var str='<input type="hidden" name="norUserid" id="norUserid" value="'+userid+'">';
	$('#normal').html(str);
	var userid=$('#norUserid').val();
	$.ajax({
					url : "<?php echo base_url()?>Api/removeVerifledProfile?",
					type : "POST",
					data : 
					{
							UserId :userid,
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
							$('#normaluser').show();
						}
						if(obj.Status==1)
						{
							$('#normaluser').hide();
						}
						 normaluser();
						 specialuser()
					}	
			});
 }
 function pop(div) 
 {
	document.getElementById(div).style.display = 'block';
 }
 function hide(div) 
 {
	document.getElementById(div).style.display = 'none';
 }
			//To detect escape button
document.onkeydown = function(evt) 
{
		evt = evt || window.event;
	if (evt.keyCode == 27) 
	{
		hide('popDiv');
	}
};
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

