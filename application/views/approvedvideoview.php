<head>

     <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
	 <title>Approved Videos</title>
 </head> 
	   <div class="container">
		
        <div class="row" >
            <div class="col-lg-7 text-center">
				<div class="form-top">
                    <div class="form-top-left">
                        <h3 style="color:#0197d8;">Approved Vidoes</h3>
                     </div>
                 </div>        		
                           
                 <div class="form-bottom">
					<div class="table-responsive">
						<div class="col-lg-12 text-center">
							<div id="viewuser" style="float: right">
			
							</div>
						</div>
		             </div>
				</div>
            </div>
        </div>
        <!-- /.row -->

<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
	$.get( "<?php echo base_url(); ?>Viewallapprovedvideo/getapprovedVideos", function( data ) {
           $("#viewuser").html(data);
        	$('#listuser').DataTable({
				 "bAutoWidth": false,
           
			});
         });
		 

});	

$("#myModal").modal('show');
$('#header').html("Are you sure you want to Delete Category");
$('#category').hide();
$('#createcate').hide();
$('#formdata').html('<input type="button" class="btn btn-primary" value="OK" id="OK" name="OK"> <input type="button" class="btn btn-primary" value="Cancel" id="Cancel" name="Cancel">');
$('#OK').click(function()
{
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "<?php echo site_url('Category/deleteCategory');?>",
			data : {id:id},
			success:function(data)
			{
				$("#myModal").modal('hide');
				if(data==1){
					$('.alert-success') .fadeIn( function() 
				   {
						$('.alert-success').show();
						$("#Message").html("Catgeory Deleted");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							location.reload();
						  }, 1000);
				   });
				}
			}
		});
});
$('#Cancel').click(function()
{
		$("#myModal").modal('hide');
});
function ConfirmApprove(id)
{
	alert(id);
	if (confirm("Are you sure you want to Approve This Video!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/approveVideo');?>",
						data : {id:id},
						success:function(data)
						{
								alert(data);
								if(data==1)
								{
									location.reload();
								}
						}
			});
			}
}

function ConfirmDelete(id)
{
	alert(id);
	if (confirm("Are you sure you want to Delete Category!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewallVideo/deletevideo');?>/"+id,
						success:function(data)
						{
							alert(data);
								if(data==1)
								{
									location.reload();
								}
						}
			});
			}
}
</script>
