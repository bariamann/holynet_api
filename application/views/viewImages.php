<head>
<title>View Category Videos</title>
      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
</head>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
	$('#main').hide();
	$.get( "<?php echo base_url(); ?>ViewImages/viewallimages", function( data ) {
           //alert( data );
           $("#viewcategory").html(data);
        	$('#listcategory').DataTable({
				 "bAutoWidth": false,
            "columnDefs": [
            { "width": "70%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            ]
			});
			
         });	
		 
});

function ConfirmApprove(id)
{
	if (confirm("Are you sure you want to Delete Category!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewImages/deleteImages');?>",
						data : {id:id},
						success:function(data)
						{
								if(data==1)
								{
									location.reload();
								}
						}
			});
			}
}

function ConfirmDelete(id)
{
	if (confirm("Are you sure you want to Delete Category!") == true) 
		{
		$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ViewImages/deleteImages');?>",
						data : {id:id},
						success:function(data)
						{
							//alert(data);
								if(data==1)
								{
									location.reload();
								}
						}
			});
			}
}
</script>
