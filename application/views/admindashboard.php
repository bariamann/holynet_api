<!DOCTYPE html>
<html>
<title>Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }

</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>"  style="color:#FFFFFF;">Logout</a></B></h5>
</header>
<div id='loadingmessage' style='display:none'>
			  <center><img src='loading.gif' width="10%" height="10%"/></center>
			</div>
<div class="w3-container w3-padding-large w3-section w3-light-grey" id="content">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		<div class="w3-card-4" style="width:70%">
			
				<header class="w3-container w3-light-grey" style="padding:10px;">
				<!-- <div class="w3-card-4">-->
						<a href="<?php echo  base_url()."Videolist";?>" style="color:#000000;">  <ul class="w3-ul w3-border">
				  			 <div class="row">
							<div class="col-sm-12">
							 <li><p><span class="w3-jumbo w3-center"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></span></p></li>
							  <li>Total Videos <span id="video" class="w3-right w3-text-red"> </span></li>
							 </div>
						</ul></a>
				<!--  </div>-->
				 
				</header>
			</div>
	  </div>
	  
	  <div class="col-sm-4">
	  		<div class="w3-card-4" style="width:70%">
			
				<header class="w3-container w3-light-grey" style="padding:10px;">
				<!-- <div class="w3-card-4">-->
				<a href="<?php echo  base_url()."Imagelist";?>" style="color:#000000;">  <ul class="w3-ul w3-border">
				   <div class="row">
							<div class="col-sm-12">
							 
							 <li><p><span class="w3-jumbo"><i class="fa fa-picture-o fa-2x" aria-hidden="true"></i></span></p></li>
							 <li>Total Images <span id="image" class="w3-right w3-text-red"> </span> </li>
							 </div>
					</div>
				</ul></a>
				 <!-- </div>-->
				</header>
			</div>
	  </div>
	  <div class="col-sm-4">
	  		<div class="w3-card-4" style="width:70%">
			
				<header class="w3-container w3-light-grey" style="padding:10px;">
				<!-- <div class="w3-card-4">-->
				<a href="<?php echo  base_url()."Postlist";?>" style="color:#000000;"><ul class="w3-ul w3-border">
				   <div class="row">
							<div class="col-sm-12">
							 
							  <li><p><span class="w3-jumbo"><i class="fa fa-comments fa-2x" aria-hidden="true"></i></span></p></li>
							  <li>Total Posts <span id="post" class="w3-right w3-text-red"> </span></li>
							 </div>
					</div>
				</ul></a>
				<!--  </div>-->
				</header>
			</div>
			 <br>
	  </div>
	   <div class="col-sm-4">
	  		<div class="w3-card-4" style="width:70%">
			
				<header class="w3-container w3-light-grey" style="padding:10px;">
				<!-- <div class="w3-card-4">-->
				<a href="<?php echo  base_url()."Userlist";?>" style="color:#000000;"><ul class="w3-ul w3-border">
				   <div class="row">
							<div class="col-sm-12">
							
							 <li><p><span class="w3-jumbo"><i class="fa fa-group fa-2x" aria-hidden="true"></i></span></p></li>
							  <li>Total Users <span id="user" class="w3-right w3-text-red"> </span></li>
							 </div>
					</div>
				</ul></a>
				<!--  </div>-->
				</header>
			</div>
			</div>
			<div class="col-sm-4">
	  		<div class="w3-card-4" style="width:70%">
		
				<header class="w3-container w3-light-grey" style="padding:10px;">
				<!-- <div class="w3-card-4">-->
				<a href="<?php echo  base_url()."Abusereport";?>" style="color:#000000;"><ul class="w3-ul w3-border">
				   <div class="row">
							<div class="col-sm-12">
							 
							  <li><p><span class="w3-jumbo"><i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i></span></p></li>
							  <li>Total Abuse Report <span id="report" class="w3-right w3-text-red"> </span></li>
							 </div>
						 	 
					</div>
				</ul></a>
				<!--  </div>-->
				</header>
			</div>
			</div>
	
  </div>
  
  <p>

</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>

$(document).ready(function() 
{
	$.ajax({
					url : "<?php echo base_url()?>Api/getTotalCountReport",
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
						$('#content').hide();
						$('footer').hide();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
						$('#content').show();
						$('footer').show();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							alert(obj.Message);
						}
						if(obj.Status==1)
						{
							$('#content').show();
							$('#video').html(obj.Report.TotalVideo);
							$('#image').html(obj.Report.TotalImage);
							$('#post').html(obj.Report.TotalPost);
							$('#user').html(obj.Report.TotalUser);
							var img = obj.Report.TotalAbuseImage;
							var video = obj.Report.TotalAbuseVideo;
							var user = obj.Report.TotalAbuseUser;
							var tot = img + video + user ;
							$('#report').html(tot);
						}
					}	
			});
});

</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>
<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

