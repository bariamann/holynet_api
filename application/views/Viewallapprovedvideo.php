<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viewallapprovedvideo extends CI_Controller 
{
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form', 'url'));
		  $this->load->model('viewallApprovedvideos');
	}
	public function index()
	{
		$id=$this->uri->segment(3);
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$this->load->view('header');
				$this->load->view('approvedvideoview',array('error' => ' ' ));
				$this->load->view('footer');
			}
			else
			{
				redirect('login');
			} 
	}
	function getapprovedVideos()
	{
			$data=$this->viewallApprovedvideos->GetApprovedVideos();
	}
	
	function viewdateapproved()
	{
		$this->index();
		$date=$this->uri->segment(3);
		$sql="SELECT * from videomaster where Status=3";
		$query= $this->db->query($sql);
		if($query->num_rows()>0)
		{
				$count=0;
				$str ="";
			$result = $query->result_array();	
			$str='<div class="container"><div class="row"><div class="col-lg-12 text-center"><div class="col-sm-12 form-box"><div class="form-top"><div class="form-top-left"><h3 style="color:#0197d8;"> Videos</h3></div></div> <div class="form-bottom col-lg-12"><div class="table-responsive col-lg-12"><div class="col-lg-12 text-center"><div id="viewcategory" style="float: right"></div></div></div>';
			$str.="<div id='four-columns' class='grid-container'><ul class='rig columns-4'>";
			foreach($result as $key=>$value) 
			{
				$time = strtotime($value['UploadDate']);
				$approveddate=date('Y-m-d', $time);
				if($date==$approveddate)
				{
					$str.='<li><div id="overlaytop"></div><img src='.base_url().$value['FileThumb'].' style="padding-top:5px;"><div><h6 style="color:#0197d8;">'.$value['Title'].'</h6><div>Created Date: '.$value['UploadDate'].'</div><div>Duration: '.$value['Duration'].'</div><div>'.$value['Description'].'</div>';
					$str.='<div style="border-top:solid 1px #d2d2d2;padding-top:3px;padding-bottom:5px;"><a class="btn btn-danger btn-xs" id="Approve" href="javascript:void(0)" onclick="ConfirmApprove('.$value['Id'].')" title="Approve" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-ok"></span></a><a class="btn btn-danger btn-xs" id="delete" href="javascript:void(0)" onclick="ConfirmDelete('.$value['Id'].')" title="Delete" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-remove"></span></a></div></li>';
						$count++;
						$counting=$count;
					
					if($count!=$counting)
					{	
						$str.='</ul></div>';
						$count=0;
					}	
				}
			}
				$str.='</div></div></div></div></div>';		
				echo $str;
		}
	}
	
	function viewrecord($id)
	{
		$query="select * from videomaster where Id=".$id;
		$queryexe=$this->db->query($query);
		if($queryexe->num_rows()>0)
		{
			$result = $queryexe->result();	
			echo json_encode($result);
		}
			
	}	 
}
?>