<!DOCTYPE html>
<html>
<title>Holynet-abuselist</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position:absolute;
background:white;
width:300px;
height:60%;
border:0px solid #000;
z-index: 9002;
overflow-y: scroll;
overflow-x:hidden;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
border-bottom: 6px solid;
border-color: #f44336!important;
}

.bs-example{
	margin: 20px;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	<?php
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$id=$sesscheck['id'];  
			}
  ?>
<input type="hidden" name="userid" id="userid" value="<?php echo $id;?>" />
  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>Abuse User</h3>
				</div>
				</div>
				
			</div>


<div id="Posts">
    <h4>Users <span class="w3-right" id="blockuser" style="padding-left:5px;"> <button type="button" class="btn-sm w3-btn w3-red open-modal" id="blockusers">Block User</button> </span> <span class="w3-right" id="rejectpostbtn"> <button type="button" class="btn-sm w3-btn w3-green open-modal" id="approveuser">Approve User</button> </span>   </h4>
	<hr>
	<div id="postpagination" class="row" align="center"></div>  
	 <div class="row" id="posts"></div> 
</div>

<div id='loadingmessage' style='display:none'>
  <center><img src='<?php echo base_url()?>/loading.gif' width="10%" height="10%"/></center>
</div>
<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
  </div>
</div>
<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-black w3-large" align="center"><span  id="liketit">Likes </span><a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-tiny w3-blue w3-right">x</a></div>
<div class="col-sm-2"></div><div class="row"><div id="displaylike"></div>
</div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>

</div>
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p id="abusemsg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <span id="ok"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
<script>
var pageindex=0;
var baseurl= "http://dev.mobileartsme.com/holynet";
$(document).ready(function(event) 
{
		abuseposts(pageindex);	
});

function abuseposts(postpageindex)
{
	
$.ajax({
					url : "<?php echo base_url()?>Api/getAbuseUsers?lang=en&PageIndex="+pageindex,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						var str='';
						var str1='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
								posttotalnopage=obj.TotalPage;	
								var myString = 'popUpDiv';
								$.each(obj.Data, function (key, value) 
								{
									//alert(value.ProfilePicture);
										str+='<div class="col-sm-4"><div class="w3-content" style="margin-top:10px"> <div class="w3-card-2 w3-round w3-white">    <div class="w3-container">   <h4 class="w3-center"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.UserId+'" style="color:black;text-transform:capitalize;">'+value.FullName+'</a><span class="w3-left"><input class="w3-check" type="checkbox" name="aprroveabuseuser[]" id="aprroveabuseuser'+value.UserId+'" value="'+value.UserId+'"></span></h4> <p class="w3-center">';
									if(value.ProfileThumbImage=="null" ||value.ProfileThumbImage=="" )
									{
										str+='<img src="img_avatar3.png" class="w3-circle" style="height:80px;width:80px" alt="Avatar">';
									}
									else
									{
										str+='<span id="image1'+value.UserId+'"><img src="'+value.ProfileThumbImage+'" alt="Avatar" class="w3-circle" style="height:80px;width:80px" onError="doaction('+value.UserId+');"></span>';
									}
								
								str+='</p> <hr>';
				str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.Email+'</p>';
				str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-mobile fa-fw w3-margin-right w3-text-theme aria-hidden="true"></i> '+ value.MobileNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-fax  fa-fw w3-margin-right w3-text-theme" aria-hidden="true" style="color:teal;"></i> '+ value.FaxNumber+'</p>';
str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i> '+ value.HomeTown+','+ value.CurrentAddress+','+value.CountryName+'</p>';
								str+='<p style="font-size: 0.865em; line-height: 1em; "><i class="fa fa-user fa-fw w3-margin-right w3-text-theme"></i> '+ value.ProfileTitle+'</p>';
								str+='</div></div></div></div>';
								
								});
								str+='</div>';
						}
						$('#posts').html(str);
						
						
							var postpage='';
								if(obj.TotalPage>1)
								{
									postpage+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Previous</span></div><p></p>';
									postpage+=' <div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;" class="w3-btn w3-theme-d2">Next</span></div>';
									$('#postpagination').html(postpage);
										
									$('#prev').click(function()
										{	
											if(postpageindex>0)
											{
												postpageindex--;
												abuseposts(postpageindex);
											}
											
										});
										
										$('#next').click(function()
										{	
											postpageindex++;
											if(posttotalnopage>postpageindex)
											{
												abuseposts(postpageindex);
											}
											if(posttotalnopage==postpageindex)
											{
												postpageindex--;
												abuseposts(postpageindex);
												document.getElementById('next').style.display='none'; 
											}
										});
								}
									$('#approveuser').click(function()
									{	
										//	var userid=$('#userid').val();
										if($('[type="checkbox"]').is(":checked"))
										{			
											var all_location_id = document.querySelectorAll('input[name="aprroveabuseuser[]"]:checked');
	
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(', ');
											
											console.log(str);
											
										$('#myModal').modal('show');
										$('.modal-title').html("Approve AbuseUser");
										$('#abusemsg').html("Are You Sure Want Approve User");
										$('#ok').html('<button type="button" class="btn btn-primary" id="postok">OK</button>');
										
										$('#postok').click(function()
										{				
											$.ajax({
													url : "<?php echo base_url()?>Api/approvedUsers?userId="+str,
													type : "GET",
													success:function(response)
													{
														var obj = JSON.parse(response);
															$('#myModal').modal('hide');
															abuseposts(postpageindex);
													},
													error:function()
													{
														alert('error');
													}
												});
										});
									}
									else
									{
										alert("Please Select User to Approve");
									}										
									});
									
									$('#blockusers').click(function()
									{	
										//	var userid=$('#userid').val();
										if($('[type="checkbox"]').is(":checked"))
										{			
											var all_location_id = document.querySelectorAll('input[name="aprroveabuseuser[]"]:checked');
	
											var aIds = [];
											
											for(var x = 0, l = all_location_id.length; x < l;  x++)
											{
												aIds.push(all_location_id[x].value);
											}
											
											var str = aIds.join(', ');
											
											console.log(str);
											
										$('#myModal').modal('show');
										$('.modal-title').html("Block AbuseUser");
										$('#abusemsg').html("Are You Sure Want Block User");
										$('#ok').html('<button type="button" class="btn btn-primary" id="blockok">OK</button>');
										
										$('#blockok').click(function()
										{				
											$.ajax({
													url : "<?php echo base_url()?>Api/blockUsers?userId="+str+"&status=0",
													type : "GET",
													success:function(response)
													{
														var obj = JSON.parse(response);
															$('#myModal').modal('hide');
															if(obj.Status==1)
															{
																abuseposts(postpageindex);
															}
													},
													error:function()
													{
														alert('error');
													}
												});
										});
									}
									else
									{
										alert("Please Select User to Block");
									}
																			
									});
					},
					error:function()
					{
						alert('error');
					}
				
			});
}

function getlikes(id)
{
	$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=3&status=1&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("Likes")
				
				if(obj.Status==0)
				{
		str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No Likes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';																	                                 }
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
function getdislikes(id)
{
$.ajax({
			url : "<?php echo base_url()?>Api/getLikesDislikesUsers?fileId="+id+"&type=3&status=2&pageIndex=0",
			type : "GET",
			beforeSend: function()
			{
				$('#loadingmessage').show();
			},
			complete: function()
			{
				$('#loadingmessage').hide();
			},
			success:function(response)
			{
				popup();
				var obj = JSON.parse(response);
				var str='';
				$('#loadingmessage').hide();
				$('#liketit').html("DisLikes")
				
				if(obj.Status==0)
				{
					str+='<p></p><div class="col-sm-12"><br><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p><center>No DisLikes</center></p></div></div></div>';
				}
				else
				{
						$.each(obj.Data, function (key, value) 
						{
							str+='<p></p><div class="col-sm-12"><div class="col-sm-12"><div class="w3-container w3-hover-shadow  w3-margin-bottom"><p>';
								if(value.User.ProfileThumbImage=="null" || value.User.ProfileThumbImage=="")
								{
									str+='<img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-circle" style="width:30px;height:30px;">';
								}
								else
								{
								str+='<img src="'+value.User.ProfileThumbImage+'" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:30px;height:30px;">';																	                                 }
										str+=' <span class="w3-opacity"><a href="<?php echo base_url();?>Userlist/userprofile?Userid='+value.User.Id+'" style="text-decoration:none;border:0;outline:none;color:black;">'+value.User.UserName+'</a></span> </p></div></div></div>';
						});
				}
					$('#displaylike').html(str);
			}
												
		});
}
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>