      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
 	 
	    <div class="container">
		
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="col-sm-5 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3 style="color:#0197d8;">Notification Tune</h3>
                            		
                        		</div>
                        		
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="registration-form"  id="noti_tune_form">
									 <div class="form-group">
			                        	<label class="sr-only" for="form-email">Category Id</label>
										<div id="get_tuneCombo">
										</div>
			                        	
			                        </div>
									
								<div align="left" style="padding-top:5px;">
			                        <input type="button" class="btn btn-primary" value="Save" id="createtune" name="createtune" data-toggle="modal" data-target="#myModal">
			      
								</div>
								
								</form>
		                    </div>
                        </div>
						
            </div>
        </div>
        <!-- /.row -->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Data Inserted Successfully</p>
        </div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('/assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{

	$.get( "<?php echo base_url('NotificationTune/getNumTune')?>", function( data ) {
        
		   	$('#get_tuneCombo').html(data);
           
         });
		 
 
   	$('#createtune').click(function()
		{
			//alert("clicked");
			var tuneName=$('#tuneName').val();
			 if(tuneName=="none")
			 {
			 	alert("Please select Tune Name");
				return false;
			 }
			var currentdate = new Date();
			
			 $.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('NotificationTune/createNotificationTune');?>",
						data	: {tuneName},
						success:function(data)
						{

								
						}
			});
		});
		
		
}); 
</script>
