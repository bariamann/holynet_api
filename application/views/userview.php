      <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet"> 
<div class="container">
		
        <div class="row" >
            <div class="col-lg-6 text-center">
				<div class="form-top">
                    <div class="form-top-left">
                        <h3 style="color:#0197d8;">View User</h3>
                     </div>
                 </div>        		
                           
                 <div class="form-bottom">
					<div class="table-responsive">
						<div class="col-lg-12 text-center">
							<div id="viewuser" style="float: right">
				
							</div>
						</div>
		             </div>
				</div>
            </div>
        </div>
        <!-- /.row -->
 
    </div>
<script src="<?php echo base_url('/assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
	$.get( "<?php echo base_url(); ?>User/getuserdata", function( data ) {
           $("#viewuser").html(data);
        	$('#listuser').DataTable();
         });
		 

});	
</script>
