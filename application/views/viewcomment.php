<head>
      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
	   <link href="<?php echo base_url('/assets/css/w3.css')?>" rel="stylesheet"> 
	<link href='http://fonts.googleapis.com/css?family=Oswald:700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	 <title>Approve/Reject Videos</title>
<style>
.user-row {
    margin-bottom: 14px;
}

.user-row:last-child {
    margin-bottom: 0;
}

.dropdown-user {
    margin: 13px 0;
    padding: 5px;
    height: 100%;
}

.dropdown-user:hover {
    cursor: pointer;
}

.table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
}

.table-user-information > tbody > tr:first-child {
    border-top: 0;
}
.table-user-information > tbody > tr > td {
    border-top: 0;
}
.toppad
{margin-top:20px;
}
</style>
 </head> 
 <div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="col-sm-12 form-box">
				<div class="form-top">
					<div class="form-top-left">
						<h3 style="color:#0197d8;" id="title">Video Profile</h3>
					</div>
				<div>
			</div>
		</div>
				<div class="form-bottom">
					<div id="container">
						<div id="videoprofile">
							<div class="container">
     							 <div class="row">
							        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-5 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad">
									<div class="panel panel-info">
								            <div class="panel-heading w3-light-blue">
											              <h3 class="panel-title"></h3>
								            </div>
									            <div class="panel-body">
										              <div class="row">
               												 <div class="col-md-3 col-lg-12" align="center" id="image"> 
																																													
															</div>		
            												<div class=" col-md-9 col-lg-0"> 	
																	 
               												</div>
             											</div>
           										 </div>
                								 <div class="panel-footer">
                       									<div class="row table-user-information"	>	
															<div class="col-sm-4"><span class="pull-left" id="vname"></span></div>
															<div class="col-sm-4"> </span></div>
															<div class="col-sm-4">	<span class="pull-right"  id="postdt"></span></div>
															<div class="col-sm-8">	<span class="pull-left" id="desc"></span></div>
														
														</div>
                   								 </div>
									</div>
									</div>
									 <div class="col-xs-6 col-sm-6 col-md-8 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad">
										 <div class="panel panel-info">
												<div class="panel-heading w3-light-blue">
															  <h3 class="panel-title" id="panelcomment"> </h3>
												</div>
													<div class="panel-body">
														  <div class="row">
														  		<div  class="col-lg-2"></div>
																
																<div id="comment" class="col-lg-12"></div>
																 <div id="pagination" class="row" align="center"></div>  
														  </div>
													</div>		
											</div>
									</div>
							 	      
       				 </div>
   			 	  </div>
				  
   			 </div>
			</div>
			</div>
</div>
</div>	
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"> 
			<span id="header"> </span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		<span id="message"></span>
      </div>
    </div>
  </div>
</div>	
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
var videoid = getParameterByName('Id');
var title = getParameterByName('title');
var desc = getParameterByName('desc');
var date = getParameterByName('date');
var thumb = getParameterByName('thumb');
var videurl = getParameterByName('url');
var url= "<?php echo base_url()?>";

var totalnopage=0;
var pageindex1=0;
$(document).ready(function() 
{
$('.alert').hide();
displaycomment(videoid);
});


function displaycomment(id,pageindex)
{
//var pageindex=0;
$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "http://dev.mobileartsme.com/holytube/Apis/getVideoComment?VideoId="+videoid+"&UserId="+2+"&pageindex="+pageindex1+"",
			crossDomain: true,
            data: "",
			success:function(responseData,textStatus,jqXHR)
			{
				var d = JSON.parse(responseData);
				if(d.Status==0)
				{
					$('#comment').html("<div class='class='w3-card-4' style='width:100%'><header style='text-align:left'><p style='Lato, sans-serif;font-size:0.9em;padding-left:0px'>No Comments Available for " + title+"</p></header></div>");
				}
				else
				{
					str="";
					count=0;
					totalnopage=d.TotalPage;
								$.each(d.Data, function (key, value) 
								{
									id=value.Id;
									var username=value.UserName;
									var comment=value.Comment;
									var datetme=value.DateTime;
									var userid=value.UserId;
									
									$('.panel-title').html(title);
									$('#vname').html("<span style='font-family:Oswald, sans-serif; font-weight:700;'>"+title+"</span>");
									$('#desc').html("<span style='Lato, sans-serif;font-size:0.8em'>"+desc+"</span>");
									$('#line').show();
									$('#postdt').html("<span style='font-family:Oswald, sans-serif; font-weight: 700;'>"+date+"</span>");
									$('#panelcomment').html("Comments for" +" "+  title);
									$('#image').html("<a href='"+videurl+"'><img alt='Video Pic' src='"+thumb+"' width='100%' height='200px'></a>");
									str+='<div class="w3-card-4" style="width:100%"><header style="text-align:left"><div class="row"><div class="col-sm-10"><h3 style="Lato, sans-serif;font-size:1.0em;font-weight:bold;padding-left:16px">'+username+'</h3></div><div class="col-sm-2" style="margin-top:5px" align="center"><a id="delete" href="javascript:void(0)" onclick="showmodal('+id+','+videoid+','+userid+')" title="Reject" class="icon-2 info-tooltip"><img src="'+url+'images/delete-xxl.png" width="20" height="20"/></a></div></div></header><div class="w3-container"><p style="Lato, sans-serif;font-size:0.9em;padding-left:0px">'+comment+'</p><p  style="Lato, sans-serif;font-size:0.8em;text-align:right;color:#9e9e9e">'+datetme+'</p></div></div><br/>';
								});
						
									str+='<div class="col-sm=12"></div>';
									$('#comment').html(str);
									str1='';
								
									if(d.TotalPage>1)
									{
										str1+='<div  class="col-sm-6"><span id="prev" style="color:#00CC66;cursor:pointer;background:#FFFFFF;padding:10px">Previous</span></div>';
											str1+='<div  class="col-sm-6"><span id="next" style="color:#00CC66;cursor:pointer;background:#FFFFFF;padding:10px">Next</span></div>';
										
										$('#pagination').html(str1);	
										$('#prev').click(function()
										{
											
											//alert(totalnopage);
											//alert(pageindex1);	
											if(pageindex1>0)
											{
												pageindex1--;
												displaycomment(videoid,pageindex1);
											}
											
										});
										
										$('#next').click(function()
										{	
											pageindex1++;
											if(totalnopage>pageindex1)
											{
												displaycomment(videoid,pageindex1);
											}
											if(totalnopage==pageindex1)
											{
												pageindex1--;
												displaycomment(videoid,pageindex1);
												document.getElementById('next').style.display='none'; 
											}
										});
									}
							}
			}
		});

}
function showmodal(id,videoid,userid)
{
$("#myModal").modal('show');
$("#header").html("Delete Comment");
$("#message").html("<input type='button' class='btn-primary' value='OK' name='ok' id='ok'> <input type='button' value='Cancel' name='cancel' id='cancel' class='btn-primary'>");

$('#ok').click(function()
{
	deletecomment(id,videoid,userid);
	alert("Comment Deleted");
	$("#myModal").modal('hide');
});

$('#cancel').click(function()
{
	$("#myModal").modal('hide');
});
}
function deletecomment(id,videoid,uid)
{
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "http://dev.mobileartsme.com/holytube/Apis/deleteVideoComment?CommentId="+id+"&VideoId="+videoid+"&UserId="+uid+"",
            crossDomain: true,
            data: "",
			success:function(responseData)
			{
				displaycomment(id);
				
			}
		});
}

 function getParameterByName(name) 
   {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
