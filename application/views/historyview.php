      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
      <style>
      	.toolbar {
      		float: left;
      	}
      </style>
      <div class="container">
      	<div class="row">
      		<div class="col-lg-12 text-center">
      			<div class="col-sm-8 form-box">
      				<div class="form-top">
      					<div class="form-top-left">
      						<h3 style="color:#0197d8;">Notification History</h3>
      					</div>
      				</div>
      				<div class="form-bottom">
      					<form role="form" action="" method="post" class="registration-form"  id="categoryform" enctype="multipart/form-data">
      						<div class="form-group">
      							<div class="table-responsive" id="display">
								
				      				<!-- <div class="col-lg-12 text-center"> -->
				      					<div id="viewcategory" style="float: right">
											
				      					</div>
				      				<!-- </div> -->
				      			</div>
								
      						</div>
      					</form>
      				</div>
      			</div>
      		</div>
      	</div>
      	<!-- /.row -->
      	<div class="modal fade" id="myModal" role="dialog">
      		<div class="modal-dialog">
      			<!-- Modal content-->
      			<div class="modal-content">
      				<div class="modal-header">
      					<button type="button" class="close" data-dismiss="modal">&times;</button>
      				</div>
      				<div class="modal-body">
      					<p id="Message"></p>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>

      <script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
      <script>
      	$(document).ready(function() 
      	{
      		$('#edit').hide();
      		notifi=$('#has_notify').val();
      		$.get( "<?php echo base_url(); ?>History/getHistoryData", function( data ) {
	           if(data=="")
			   {
			   		  $("#display").html('<h4 style="color:#FF0000">No History Available</h1>');
			   }
	           $("#viewcategory").html(data);
	           $('#listcategory').DataTable({
		           	"bAutoWidth": false,
		           	"columnDefs": [
		           	{ "width": "5%", "targets": 0 },
					{ "width": "45%", "targets": 1 },
		           	{ "width": "35%", "targets": 2 },
					{ "width": "15%", "targets": 3 },
		           	]
	           });
       		});

      		$("#imagefile").change(function () {
      			if (typeof (FileReader) != "undefined") {
      				var dvPreview = $("#imgprvw");
      				dvPreview.html("");
      				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
      				$($(this)[0].files).each(function () {
      					var file = $(this);
      					if (regex.test(file[0].name.toLowerCase())) {
      						var reader = new FileReader();
      						reader.onload = function (e) {
      							var img = $("<img />");
      							img.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
      							img.attr("src", e.target.result);
      							dvPreview.append(img);

      						}
      						reader.readAsDataURL(file[0]);
      					} else {
      						alert(file[0].name + " is not a valid image file.");
      						dvPreview.html("");
      						return false;
      					}
      				});
      			} else {
      				alert("This browser does not support HTML5 FileReader.");
      			}
      		});

      		$('#has_notify').click(function()
      		{
      			notifi=$('#has_notify').val();
      			if(document.getElementById("has_notify").checked==true)
      			{
      				notifi=1;
					//alert(notifi);
				}
				else
				{
					notifi=0;
					
					//	alert(notifi);
				}
			});


      		$(".close").click(function()
      		{
      			window.location.reload();
      		});
			
      	}); 
      
        function getcategoryData() 
        {
        	$.get( "<?php echo base_url(); ?>History/getHistoryData", function( data ) {
           		//alert( data );
           		$("#viewcategory").html(data);
           		$('#listcategory').DataTable();
       		});
        }
        function cancelcategory()
        {
        	location.reload();
        }
		
		function ConfirmDelete(id)
		{
			if (confirm("Are you sure you want to Delete Category!") == true) {
				$.ajax({
					type :  "POST",
					datatype : "JSON",
					url: "<?php echo site_url('History/deleteHistory');?>",
					data : {id:id},
					success:function(data)
					{
						//alert(data);
						if(data==1)
						{
							location.reload();
						}
						
					}
				});
			}
		}

    </script>
