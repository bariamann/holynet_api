<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anil Labs - Codeigniter mail templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
  <div style="max-width:80%"  align="center"><div style="solid;max-width:80%;" align="center"><div style="border:1px solid;background-color:teal;font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"><img src="http://dev.mobileartsme.com/holynet/holynet.png" alt="" width="300px" height="50px" align="middle"></div><br><div align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;margin-left:260px;">Hi, Username</p></div><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 10px;Margin-bottom: 40px;">We got a request to reset your Holynet password.</p><p style="Margin-bottom: 35px;"><a href="'.base_url().'ForgetPassword?activationid='.$userid.'" style="text-decoration:none;border:solid 1px;color:teal;padding:10px;">Reset Password</a></p><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;margin-left:80px;">If you ignore this message, your password won't be changed.</p></div></div>
</body>
</html>