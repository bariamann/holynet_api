<style>
/**table.dataTable thead .sorting:after,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc:after {
    display: none;**/
}
</style>
<body class="padTop53" >

    <!-- MAIN WRAPPER -->
    	<div id="wrap">
        
        <!-- HEADER SECTION -->
        <div id="top">
        	<nav class="navbar navbar-inverse navbar-fixed-top" style="padding-top: 10px;">
      <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!--MAIN SALON LOGO SECTION -->
                <header class="navbar-header">
                    <a href="index.html" class="navbar-brand">
                    Private Document Management System
                    </a>
                </header>
                
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right">
                    
                   	<li><span style='color:blue;font-size:1.2em;font-weight:bold'><?php $sesscheck=$this->session->userdata('data');	
									if($sesscheck['loginuser']==1)
									{
										echo $sesscheck['username'];//$sesscheck['username'];
									}
					?></span></li>
                    
                    <!--ADMIN SETTINGS SECTIONS -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp;<i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                           <li>
                            <a href="<?php echo base_url('Adminloginpage/logout');?>"><i class="icon-signout"></i> Logout </a>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

         </nav>
        </div>
        <!-- END HEADER SECTION -->
        
        <!-- MENU SECTION -->
        <?php $this->load->view('leftmenu');?>
        <!--END MENU SECTION -->

        <!--PAGE CONTENT -->
        <div id="content">
             
            <div class="inner col-lg-12" style="min-height:500px;">
                <div class="row ">
                    <div class="col-lg-12">
                        <h3>Manage Users</h3>
                    </div>
                </div>
                <hr />
               	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" align="right">
							<b><span id="successmsg" style="text-align:justify:font-weight:1.8em;color:#FF0000;"></span></b>
       					 <button class="btn btn-default" data-toggle="modal" data-target="#usermodal">Add New</button>
                            </div>

                            <div class="panel-body">
                                <div class="tab-content" style="padding-top:10px">
                                    <div class="tab-pane active" id="sectionLIST">
                                        <div class="row">
                                            <div class="col-md-10 col-xs-offset-1" > 
                                            	 <div class="table-responsive">
												  <table class="table table-striped table-bordered table-hover" id="user-table">
                                                    <thead>
                                                        <th style="width:40%">User Name</th>
                                                         <th style="width:%40%">User Email</th>
           												     <th style="widht:5%;display:none"></th>
															 <th style="widht:5%;display:none"></th>                      
                                                    </thead>
                                                    <tbody id="user-body"> 

                                                    </tbody>
													</table>
           										 	
                                                    </div>
            								</div>
										</div>
									</div>
									<div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                                    <div class="modal-content">
                                    <form name="frm" id="popup-validation" role="form" class="form-horizontal"  method="post">  
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="H2">New User</h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                <div class="form-group">
                                    <label class="col-md-4 control-label"> Name </label>
                                    <div class="col-md-6">
           							 <input type="text" class="form-control" placeholder="Enter Name" name="username" id="username" autofocus onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label"> Email </label>
                                    <div class="col-md-6">
            <input type="text" class="form-control" placeholder="Enter Email" name="useremail" id="useremail" onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label"> Password </label>
                                    <div class="col-md-6">
            <input type="password" class="form-control" placeholder="Enter Password" name="userpassword" id="userpassword" onKeyDown="if(event.keyCode == 13) document.getElementById('save').click()"/>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-md-4 control-label" id="retypepassword"> Re-type Password </label>
                                    <div class="col-md-6">
            <input type="password" class="form-control" placeholder="Retype Password" name="ruserpassword" id="ruserpassword" onkeydown= "if(event.keyCode == 13) document.getElementById('save').click()"/>
                                    </div>
                                </div>
							 <label class="control-label"  id="displaymsg"></label>
                                         </div>
                                        <div class="modal-footer">
										 <span id="savebutton"><button type="button" class="btn btn-primary" onClick="formsubmit();" id="save">Save</span>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                    </div>
                        </div>
</div>
							</div>
                    	</div>
            		</div>
        		</div>
        	</div>
    	</div>	
	</div>
	</div>
	<script src="<?php echo base_url('assets/plugins/jquery-2.0.3.min.js')?>"></script>
	  <script>
    	//Code To Perform Insert
		function formsubmit()
		{
			//alert('gdfg');
			var username=$("#username").val();
			var useremail=$("#useremail").val();
			var userpassword=$("#userpassword").val();
			var reenterpwd=$('#ruserpassword').val();
			var re=/\S+@\mobileartsme+\.com$/
			if(username=="")
			{
				$('#displaymsg').html("<span class='alert-danger'>User Field cannot be blank</span>");
				return false;
			}
			if(useremail=="")
			{
				$('#displaymsg').html("<span class='alert-danger'>Email Field cannot be blank</span>");
				return false;
			}
			if(!re.test(useremail))
			{
				$("#displaymsg").html("<span class='alert-danger'>Youe Entered Email must Contain something@mobilearts.com</span>" );
				return false;
			}
			if(userpassword=="")
			{
				$('#displaymsg').html("<span class='alert-danger'>Password Field cannot be blank</span>");
				return false;
			}
			if(reenterpwd=="")
			{
				$('#displaymsg').html("<span class='alert-danger'>Re-type Password cannot be blank</span>");
				return false;
			}
			if (userpassword != reenterpwd)
			{
            	$('#displaymsg').html("<span class='alert-danger'>Your Password doesnot match</span>");
            	return false;
        	}
			else
			{
				$('#displaymsg').html("");
			}
		
			$.ajax({
				url : "http://dev.mobileartsme.com/private_documents/Apis/addNewUser?",
				type : "POST",
				data:
				{
					Name:username,
					Email:useremail,
					Password:userpassword	
				},
				success:function(Data)
				{
					var obj = $.parseJSON(Data);
					//alert("User" +" "+ obj['Message']);
					$("#usermodal").modal('toggle');
					$('#successmsg').html(obj['Message']).fadeIn('slow');
					 $('#successmsg').delay(2000).fadeOut('slow');
					filluser();
				},
				error:function()
				{
					alert('error');
				}
				});	
				
			}
		//code to Perform delete
		function deleteuser(a)
		{
			var userid=a;
			if (confirm("Are you sure you want to Delete User!") == true) 
			{
				$.ajax({
					url : "http://dev.mobileartsme.com/private_documents/Apis/deleteUser?UserId="+userid+"",
					type : "POST",
					success:function(Data)
					{	
						var obj = $.parseJSON(Data);
						//alert(obj['Message']);
						filluser();
						$('#successmsg').html(obj['Message']).fadeIn('slow');
						$('#successmsg').delay(2000).fadeOut('slow');
					},
					error:function()
					{
						alert('error');
					}
					});	
			}
		}
    	//Code to Fill User Table 
     	function filluser()
     	{
     		$.ajax({
				url : "http://dev.mobileartsme.com/private_documents/Apis/getUsers",
				type : "GET",
				success:function(data)
				{
					
					var obj= $.parseJSON(data);
					var userlist=obj['Data'];
					$('#user-body').html('');
					for(var i =0 ;i<userlist.length;i++)
					{
						var $raw= $("<tr>");
						$raw.append("<td>"+userlist[i]['Name']+"</td>")	;
						$raw.append("<td>"+userlist[i]['Email']+"</td>")	;
						$raw.append("<td><a href='#'><center><button type='button' class='btn btn-success btn-circle' onclick='edituser("+userlist[i]['Id']+")'><span class='glyphicon glyphicon-pencil'></span></button></center></td>");
						$raw.append("<td><center><button type='button' onclick='deleteuser("+userlist[i]['Id']+")' class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash'></span></button></center></td>");
						$raw.append("</tr>");
						$("#user-body").append($raw);
					}
					
				},
				error:function()
				{
					alert('error');	
				}
			});	
			
			$('#user-table').dataTable(
				{
    				"bFilter":false,
    				"bLengthChange": false,
				"aoColumns": [
					{"bSearchable": true}, 
					{"bSearchable": true}
				]
				}
			);
     	}
		
		function edituser(id)
		{
			//alert(id);
			$.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ManageUser/viewrecord');?>/"+id,
						success:function(data)
						{
								var d = JSON.parse(data);
								//alert(d);
							$.each(d, function (key, value) 
							{
								callmodal(id,value.Name,value.Email,value.Password);
							});
						}
			});
		}
		function callmodal(id,name,email,password)
		 {
			$("#usermodal").modal('show');
			$("#H2").html('<B style="color:#0197d8;">Edit'+" "+name+'</B>');
	
			$('#username').val(name);
			$('#useremail').val(email);
			//$('#userpassword').val(password);
			$('#save').hide();
			$('#savebutton').html('<button type="button" class="btn btn-primary" id="edit" name="Edit">Save</button>');
			
				$('#userpassword').on('keydown',function()
				{
					if(event.keyCode == 13) 
					{
						document.getElementById('edit').click()
					}
				});
				
				$('#username').on('keydown',function()
				{
					if(event.keyCode == 13) 
					{
						document.getElementById('edit').click()
					}
				});
				
				$('#useremail').on('keydown',function()
				{
					if(event.keyCode == 13) 
					{
						document.getElementById('edit').click()
					}
				});
				$('#ruserpassword').on('keydown',function()
				{
					if(event.keyCode == 13) 
					{
						document.getElementById('edit').click()
					}
				});
			
			$('#edit').click(function()
			{
			
				var name=$('#username').val();
				var email=$('#useremail').val();
				var password=$('#userpassword').val();
				var repassword=$('#ruserpassword').val();
				//alert(repassword);
				var re=/\S+@\mobileartsme+\.com$/
				

				if(name=="")
				{
					$("#displaymsg").html("<span class='alert-danger'>Please Enter UserName</span>");
					return false;
				}
				if(email=="")
				{
					$("#displaymsg").html("<span class='alert-danger'>Please Enter Email Address</span>");
					return false;
				}
				if(!re.test(email))
				{
					$("#displaymsg").html("<span class='alert-danger'>Youe Entered Email must Contain something@mobileartsme.com</span>" );
					return false;
				}
				if(password=="")
				{
					$("#displaymsg").html("<span class='alert-danger'>Please Enter Password</span>");
					return false;
				}
				if(repassword=="")
				{
					$('#displaymsg').html("<span class='alert-danger'>Re-type Password cannot be blank</span>");
					return false;
				}
				if (password != repassword)
				{
					$('#displaymsg').html("<span class='alert-danger'>Your Password doesnot match</span>");
					return false;
				}
				
					$.ajax({
								type :  "POST",
								datatype : "JSON",
								url: "<?php echo site_url('ManageUser/edituser');?>/"+id,
								data : {name:name,email:email,password:password},
								success:function(data)
								{
										if(data==1)
										{	
											//alert("User Updated Successfully");
											$("#usermodal").modal('toggle');
											$('#successmsg').html("User Updated Successfully").fadeIn('slow');
											$('#successmsg').delay(2000).fadeOut('slow');
											filluser();
										}
								}
						});
				});
		}

		$(document).ready(function ()
		{
				 filluser();
			$('#user-table').dataTable();
			$('#usermodal').on('hidden.bs.modal', function () 
			{
				$('.modal-body').find('textarea,input').val('');
				$('#displaymsg').html('');
				
			});

		});
        </script>
	
	<script src="<?php echo base_url('assets/plugins/validationengine/js/jquery.validationEngine.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/validationengine/js/languages/jquery.validationEngine-en.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/validationInit.js')?>"></script>
	<script>
		$(function() { formValidation(); });
	</script>