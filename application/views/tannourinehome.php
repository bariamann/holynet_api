      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
 	
	 
	    <div class="container">
		
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="col-sm-5 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3 style="color:#0197d8;">Create Notification</h3>
                            		
                        		</div>
                        		
                            </div>
                            <div class="form-bottom">
			     
								<?php 
										$attributes = array('id' =>'tannourine','method'=>'post','name'=>'tannourine');
										echo form_open_multipart('Tannourine/createNotification',$attributes);
								?>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Title</label>
			                        	<input type="text" name="title" id="title" placeholder="Notification Title" class="form-first-name form-control">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Notification Description</label>
			                        	<textarea name="description" id="description" placeholder="Notification Description" 
			                        				class="form-about-yourself form-control"></textarea>
			                        </div>
			                        <div class="form-group">
									<?php
											$this->db->where('hasNotification',1);
											$this->db->select('*');
											$this->db->from('categorymaster');
											$query=$this->db->get();
											$result=$query->result();
											//print_r($result);
									
									?>
			                        	<label class="sr-only" for="form-email">Category Id</label>
			                        	<select class="form-email form-control" id="category" name="category">
											<option value="none" selected="selected">-------------------------Select Category-------------------------</option>
											<?php
											foreach($result as $row)
											{
											?>
												<option value="<?php echo $row->categoryId;?>"><?php echo $row->category;?></option>
											<?php
											}
											?>
										</select>
			                        </div>
			                      <div>
								  
                        		</div>
								
								 <div class="row"  style="padding-bottom:3px;">
								 		<div class="col-sm-4">	
                        					<label class="control-lable" style="color:#0197d8;">Select Images:</label>
										</div>
		  								<div class="col-sm-4">
													 <input type="file" id="userfile" name="userfile[]" multiple="multiple" accept="image/*">
										
										</div>
									</div>
									 <div class="row"  style="padding-bottom:3px;"   >
								 		<div class="col-sm-12">
                        						<div id="dvPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
									  <div class="row"  style="padding-bottom:3px;">
								 		<div class="col-sm-4">
											<label class="control-lable" for="audiofile" style="color:#0197d8;">Select Audio:</label>
										</div>
		  								<div class="col-sm-4">
		                 						 <input type="file" id="audiofile" name="audiofile[]" multiple="multiple" accept="audio/*">
										</div>
									</div>
									 <div class="row"  style="padding-bottom:3px;"   >
								 		<div class="col-sm-12">
                        						<div id="audioPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
									 
									  <div class="row"  style="padding-bottom:3px;">
								 		<div class="col-sm-4">
                        						<label class="control-lable" style="color:#0197d8;">Select Video:</label>
										</div>
		  								<div class="col-sm-4">
	                             						 <input type="file" id="videofile" name="videofile[]" multiple="multiple" accept="video/*">
										</div>
									</div>
									 <div class="row"  style="padding-bottom:3px;"   >
								 		<div class="col-sm-12">
                        						<div id="videoPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
									 
								<div align="right">
			                        <input type="submit" class="btn btn-primary" value="Create Notification" id="createnotifications">
			                    </div>
								<?php echo form_close() ?>
								
		                    </div>
                        </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>

<script>
$(document).ready(function() 
{
	
     $("#userfile").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            dvPreview.html("");
            var regex = /^.*\.(jpg|jpeg|gif|JPG|png|PNG)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
						
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
	$("#audiofile").change(function () {
		//alert("hello");
        if (typeof (FileReader) != "undefined") {
            var audioPreview = $("#audioPreview");
            audioPreview.html("");
           // var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.mp3|.MP3|.mpeg|.MPEG|.m3u|.M3U)$/;
            $($(this)[0].files).each(function () {
               			//alert("hello");
						path="<?php echo base_url()?>images/mp3-icon.jpg";
						//alert(path);
                        var img1 = $("<img />");
                        img1.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
                        img1.attr("src", path);
                       
						audioPreview.append(img1);	
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
	$("#videofile").change(function () {
		//alert("hello");
        if (typeof (FileReader) != "undefined") {
            var videoPreview = $("#videoPreview");
            videoPreview.html("");
           // var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.mp3|.MP3|.mpeg|.MPEG|.m3u|.M3U)$/;
            $($(this)[0].files).each(function () {
               			//alert("hello");
						path="<?php echo base_url()?>images/mp4-icon.jpg";
						//alert(path);
                        var img2 = $("<img />");
                        img2.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
                        img2.attr("src", path);
                       
						videoPreview.append(img2);
				
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
		
		
		$('#createnotifications').click(function()
		{
			var title=$('#title').val();
			var description=$('#description').val();
			var category=$('#category').val();
			var photofile = document.getElementById("userfile");
			var audiofile = document.getElementById("audiofile");	
			var videofile = document.getElementById("videofile");	
			
			 if(title=="")
			 {
			 	alert("Please enter Title");
				return false;
			 }
			 if(description=="")
			 {
			 	alert("Please enter Description");
				return false;
			 }
			  if(category=='none')
			 {
			 	alert("Please select Category");
				return false;
			 }
			var photofilearr = new Array();
			if(photofile.files.length<=20)
			{
				$filetype=1;
				for (var i = 0; i < photofile.files.length; ++ i) 
				{
					photofilearr[i]=photofile.files[i].name;
				   if((photofile.files[i].size)>2097152)
				   {
				   		alert("Please upload photo upto 2 mb!"); //do something if file size more than 2 mb
						return false;
				   }
				   
				}
			}
			else
			{
					alert("Image limit Exceeds cannot  Upload more than 20 files");
					return false;
			}
		
			var audiofilearr = new Array();
			if(audiofile.files.length<=20)
			{
				for (var i = 0; i < audiofile.files.length; ++ i) {
					audiofilearr[i]=audiofile.files[i].name;
				   if((audiofile.files[i].size)>3145728)
				   {
				   		alert("Please upload audio upto 3 mb!"); //do something if file size more than 3 mb 
						return false;
				   }
				}
			}
			else
			{
					alert("Audio limit Exceeds cannot  Upload more than 20 Audio files");
					return false;
			}
			
			var videofilearr = new Array();
			//alert(videofile.files.length);
			if(videofile.files.length<=20)
			{
				for (var i = 0; i < videofile.files.length; ++ i) {
					videofilearr[i]=videofile.files[i].name;
				   if((videofile.files[i].size)>5242880)
				   {
				   		alert("Please upload video upto 5 mb!"); //do something if file size more than 4 mb 
						return false;
				   }
				}
			}
			else
			{
					alert("Video limit Exceeds cannot  Upload more than 20 Videos files");
					return false;
			}
		});
}); 
</script>
