<head>

      <link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
	 <title>Add Videos</title>
 </head> 
	
	  
	        <div class="container">
		
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="col-sm-5 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3 style="color:#0197d8;">Add New Videos</h3>	
                        		</div>
                            </div>
                            <div class="form-bottom">
			     
								<?php 
								//	$attributes = array('id' =>'addvideos','method'=>'post','name'=>'addvideos');
									//echo form_open_multipart($attributes);
										
								?>
								<form method="post" id="formaddvideos" name="addvideos" enctype="multipart/form-data">
								 <div class="alert alert-danger" id="warning">
											<strong id="error"></strong>
								</div>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Title</label>
			                        	<input type="text" name="Title" id="Title" placeholder="Video Title" class="form-first-name form-control">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Video Description</label>
			                        	<textarea name="Description" id="Description" placeholder="Video Description" class="form-about-yourself form-control" ></textarea>
			                        </div>
									 <div class="form-group">
					                         	<input type="text" name="Keywords" id="Keywords" placeholder="Video Keywords" class="form-first-name form-control">
							
									</div>
			                        <div class="form-group">
									<?php
											$this->db->select('*');
											$this->db->from('categorymaster');
											$this->db->where('Status',1);
											$query=$this->db->get();
											$result=$query->result();
											//print_r($result);
									
								?>
			                        	<select class="form-email form-control" id="CategoryId" name="CategoryId">
											<option value="none" selected="selected">-------------------------Select Category-------------------------</option>
											<?php
											foreach($result as $row)
											{
											?>
												<option value="<?php echo $row->Id;?>"><?php echo $row->Name;?></option>
											<?php
											}
											?>
										</select>
			                        </div>
			                      <div>
								  
                        		</div>
								
								 
									 <div class="row"  style="padding-bottom:3px;">
								 		<div class="col-sm-12">
                        						<div id="audioPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
									
									  <div class="row"  style="padding-bottom:3px;">
								 		<div class="col-sm-4">
                        						<label class="control-lable" style="color:#0197d8;">Select Video:</label>
										</div>
		  								<div class="col-sm-4">
	                             						 <input type="file" id="file" name="file" accept="video/*">
										</div>
									</div>
									<div align="right" id="videotype"></div>
									 <div class="row"  style="padding-bottom:3px;"   >
								 		<div class="col-sm-12">
                        						<div id="videoPreview" style="margin-top:5px; margin-left:5px">
												 </div>		
										</div>
									</div>
									 <?php
											$sesscheck=$this->session->userdata('data');
											$id=$sesscheck['id'];
									?>
										<input type="hidden" value="<?php echo $id;?>" name="UserId" id="UserId"/>
										
								<div align="right">
			                        <input type="submit" class="btn btn-primary" value="Add Video" id="addvideo" name="addvideo">
			                    </div>
								<div id="loader-icon" style="display:none;"><img src="<?php echo base_url()?>images/loading.gif" width="30%" height="30%"/></div>
								<?php //echo form_close() ?>
								</form>
		                    </div>
							<div><?php echo $error?></div>
							
                        </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"> 
			<span id="header"> </span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		 <div class="form-group">
			                   
        </div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
	$('.alert-danger').hide();
	$('#videotype').html("<span style='font-size:0.8em' class='alert-danger'>(Supported formats: mp4, mpg, mpeg, 3gp, mov, m4v, flv, avi, wmv, mkv)</span>");
	$("#file").change(function () {
		//alert("hello");
        if (typeof (FileReader) != "undefined") {
            var videoPreview = $("#videoPreview");
            videoPreview.html("");
           // var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.mp3|.MP3|.mpeg|.MPEG|.m3u|.M3U)$/;
            $($(this)[0].files).each(function () {
               			//alert("hello");
						path="<?php echo base_url()?>images/mp4-icon.jpg";
						//alert(path);
                        var img2 = $("<img />");
                        img2.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
                        img2.attr("src", path);
                       
						videoPreview.append(img2);
						
				
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
	  $("#formaddvideos").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "http://dev.mobileartsme.com/holytube/Apis/uploadVideo?",
            type: "POST",
            data:  new FormData(this),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(response)
            {
				if(confirm("Video added Successfully"))
				{
				 	location.reload();
    			}
				/*alert("Video added Successfully");
				$('#loader-icon').hide();
				$('#addvideo').removeClass('disabled');
				$('#Title').val('');
				$('#Description').val('');
				$('#file').val('');
				$('#Keywords').val('');*/
            },
            error: function() 
            {
            }           
       });
    }));	
		
		$('#addvideo').click(function()
		{
			var title=$('#Title').val();
			var description=$('#Description').val();
			var category=$('#CategoryId').val();
			var keyword=$('#Keywords').val();
			var videofile = $('#file').val();
			var UserId=$('#UserId').val();
			//mp4, 3gp, mov, m4v
			Reg= /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.mp4|.MP4|.3gp|.3GP|.mov|.Mov|.m4v|.M4v)$/;
			  
			 if(title=="")
			 {
			 	$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Video Title");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
				return false;
			 }
			 if(description=="")
			 {
				 $('.alert-danger') .fadeIn( function() 
				   {
						$('.alert-danger').show();
						$("#error").html("Please enter Video Description");
						  setTimeout( function()
						  {
							$('.alert-danger').fadeOut("fast");
						  }, 2000);
				   });
				return false;
			 }
			  if(category=='none')
			 {
			 	$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("Please Select Category");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
				return false;
			 }
			  if(keyword=="")
			 {
				 $('.alert-danger') .fadeIn( function() 
				   {
						$('.alert-danger').show();
						$("#error").html("Please enter Video keyword");
						  setTimeout( function()
						  {
							$('.alert-danger').fadeOut("fast");
						  }, 2000);
				   });
				return false;
			 }
			 if(videofile=='')
			 {
			 	$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("Please Select  Video to upload");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
				return false;
			 }
			 
			if(!Reg.test(videofile))
			{	
				$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("Please Select Video file");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
				return false;
      		}
			//$('#loader-icon').show();
			if($('#file').val()) 
			{
				$('#loader-icon').show();
				$('#addvideo').addClass('disabled');
			}
		});
		
}); 
/*function insertdata()
{
var formData = new FormData( $("#formaddvideos")[0] );

			$.ajax({
						type :  "POST",
						 dataType: 'json',
						url: "http://dev.mobileartsme.com/holytube/Apis/uploadVideo?",
						data : formData,
						 mimeType: "multipart/form-data",
						cache : false,
						contentType : false,
						processData : false,
						
						 success:function(response)
						{
							console.log(response);
							//		alert("video uploaded Successfully");
								var d = JSON.parse(response);
								alert(d.Message);
						}
			});
}*/
</script>
