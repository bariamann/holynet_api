<!DOCTYPE html>
<html>
<title>Holynet</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">

<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}

#popUpDiv {
position: absolute;	
background:white;
width:300px;
height:60%;
border:0px solid #000;
z-index: 9002;
overflow-y: scroll;
overflow-x:hidden;
-moz-border-radius: 10px;
-webkit-border-radius:10px;
border-radius: 10px;
margin-left: -100px;
margin-top: -100px;
}
.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Holynetlogin/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>
<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div> 	 
   <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
						<div class="w3-container w3-teal">
								<h3>Change Password</h3>
						</div>
					</div>
				</div>	
				<div class="w3-row">
					 <form class="w3-container" role="form" action="" method="post"  id="chngePwd_form">
					<p>
					<input class="w3-input" type="password" name="oldPwd" id="oldPwd" placeholder="Old Password"></p>
					
					<p>
					<input class="w3-input" type="password" name="newPwd" id="newPwd" placeholder="New Password" ></p>
					<input type="hidden" name="id" id="id" value="<?=$id;?>" />
					<p>
					<input class="w3-input" type="password" name="confirmPwd" id="confirmPwd" placeholder="Confirm Password" ></p>
					<button class="w3-btn w3-blue"  value="Save" id="changePwd" name="changePwd" >Change Password</button>
					</form>     
				 </div> 
          	</div>
		</div>
	</div>
	</p>
 </div>
</div>
	<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">HOLYNET</a> |
  <a href="/about/default.asp" target="_top">2016-17</a>
  </nav></p>
</footer>
	    <!-- /.row -->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">Error Message
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <span id="msg"></span>
        </div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('/assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
<script>
$(document).ready(function() 
{
   	$('#changePwd').click(function()
		{
			var oldPwd=$('#oldPwd').val();
			var newPwd=$('#newPwd').val();
			var confirmPwd=$('#confirmPwd').val(); 
			var id=$('#id').val();
			 if(oldPwd=="")
			 {
				$("#myModal").modal('show');
				$('#msg').html("Please Enter Old Password");
				return false;
			 }
			 if(newPwd=="")
			 {
			 	$("#myModal").modal('show');
				$('#msg').html("Please Enter new Password");
				return false;
			 }
			 if(confirmPwd=="")
			 {
			 	$("#myModal").modal('show');
				$('#msg').html("Please Enter Confirm Password");
				return false;
			 }
			 else
			 {
				 	if(confirmPwd==newPwd) {}
					 else 
					 {
						 	$("#myModal").modal('show');
							$('#msg').html("Password and Confirm Password does not match");
							return false;
					 }
			}			
			 $.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ChangePassword/chngePassword');?>",
						data	: {oldPwd,newPwd,confirmPwd,id},
						success:function(data)
						{
							if(data==1)
							{
								alert("Password successfully changed");
							}
							else
							{
								alert("Pasword cannot be Changed");
							}
						}
				});
		});
		
		$('.close').click(function()
		{
			//alert('close');
			$('#oldPwd').val("");
			$('#newPwd').val("");
			$('#confirmPwd').val(""); 
		});
}); 
</script>
