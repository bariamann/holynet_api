
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
	<title>Holynet Login</title>
		<meta charset="utf-8">
		<link href="<?php echo base_url().'/assets/css/style.css'?>" rel='stylesheet' type='text/css'/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!--webfonts-->
		<link href='//fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
		<!--//webfonts-->
</head>
<body>
	 <!-----start-main---->
	 <div class="main">
	 <!---728x90--->
		<div class="login-form">
			<h1>Admin Login</h1>
					<div class="head">
						<img src="images/user.png" alt=""/>
					</div>
				<form>
						<input type="text" class="text" placeholder="USERNAME" id="username" name="username" focus>
					<input type="password" placeholder="PASSWORD" id="password" name="password" >
						<div class="submit">
							<input type="button" id="login" name="login" value="LOGIN">
						</div>	
						<p><div class="w3-container" style="background-color:#FFCC99;"><h3 style="color:red;" id="msg"></h3></div></p>
				</form>
			</div>
			<!--//End-login-form-->
			<!---728x90--->
			 <!-----//end-main---->
		 		
</body>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
$('#username').keypress(function(e) 
{
	if (e.which == '13') 
	{
		$('#login').click();
	}
});
$('#password').keypress(function(e)
 {
	if (e.which == '13')
	 {
		$('#login').click();
	}
});
$('#login').click(function()
{
	var uname=$('#username').val();
	var password=$('#password').val();
	if(uname=="")
	{
		$('#msg').html('Please Enter UserName!').fadeIn('slow');
		$('#msg').delay(500).fadeOut('slow');
		return false;
	}
	if(password=="")
	{
		$('#msg').html('Please Enter Password!</h3>').fadeIn('slow');
		$('#msg').delay(500).fadeOut('slow');
		return false;
	}
	
		$.ajax({
					url : "<?php echo base_url('Holynetlogin/login');?>",
					type : "POST",
					data:
					{
						Name:uname,	
						password:password
					},
					success:function(response)
					{
						//alert(response);
						var obj = JSON.parse(response);
						if(obj.Status==0)
						{
							$('#msg').html(obj.Message).fadeIn('slow');
							$('#msg').delay(1000).fadeOut('slow');
						}
						
						if(obj.Status==1)
						{
							$('#login').hide();
							 $('#login').addClass('disabled');
							window.location ="<?php echo base_url('Admindashboard')?>";
						}

					},
					error:function()
					{
						alert('error');
					}
			});
});
});
</script>
</html>