<head>
<title>Category Page</title>
<link href="<?php echo base_url('/assets/css/style.css')?>" rel="stylesheet"> 
<style>
	.toolbar {
		float: left;
	}
	#response {
		background-color:#9F9;
		border:2px solid #0197d8;
	}
	#list li {
		background-color:#fff;
		color:#000;
		list-style: none;
		width:100%;
	}
</style>
</head>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"> 
			<span id="header"> </span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		 <div class="form-group">
			                    	<form role="form" action="" method="post" class="registration-form"  id="categoryform" enctype="multipart/form-data">
						<div class="form-group" id="formdata">
							<label class="sr-only" for="form-first-name">Category Name</label>
							<input type="text" name="categoryname" id="category" placeholder="Enter Category Name" class="form-first-name form-control"  autofocus onkeydown= "if(event.keyCode == 13) document.getElementById('createcate').click()">
							</div>
							<div align="left" style="padding-top:5px;">
							<input type="button" class="btn btn-primary" value="Save" id="createcate" name="createcate" data-toggle="modal" data-target="#myModal">
							<div id="edit"></div>
						</div>
					</form>
        </div>
      </div>
       <div class="alert alert-danger" id="warning">
					<strong id="error"></strong>
		</div>
    </div>
  </div>
    </div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="col-sm-5 form-box">
				<div class="form-top">
					<div class="form-top-left">
						<h3 style="color:#0197d8;" id="title">Approved Categories</h3>
					</div>
					<div style="padding-top:15px;" align="right">
						<a href="#" id="modaldisplay"><img src="<?php echo base_url()?>images/add_blue.png" width="40" height="40"/></a>
					</div>
				</div>
				<div class="form-bottom">
					<div id="container">
					 <div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong id="Message"></strong>
					</div>
					<table class="myTable table table-bordred table-striped" id="list">
							<thead>
								<tr>
									<th class="col-sm-2 text-center">Category Name</th>
									<th class="col-sm-2 text-center">Date</th>
									<th  style="display:none"></th>
									<th style="display:none"></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql  = "SELECT Id,Name,Icon,Date FROM categorymaster where Status=1 order by Id ASC";
								$query=$this->db->query($sql);
								$query_result = $query->result();   
								foreach($query_result as $row)
								{
									$id = $row->Id;
									$text = $row->Name;
									$imagePath = $row->Icon;
									$date = $row->Date;
								
								?>
									
									<tr id="domain<?php echo $id ?> ">
											<td style="width:30%;" class="well well-lg"><a href='<?php echo base_url().'viewImages/viewallimages/'.$id;?>'><?php echo $text;?></a></td>
												
												<td style="width:30%;" class="well well-lg"><?php echo $date;?></td>

												<td  style="width:10%; border:1px solid #d2d2d2;font-family: Roboto Condensed, sans-serif;font-size:9px;font-style:italic;">
													<p data-placement="top" data-toggle="tooltip" title="Edit"> 
														<a class="btn btn-primary btn-xs" id="editcon" data-title="Edit" href="javascript:void(0)" title="Edit"  onclick="editCategory(<?php echo $id ?>)"class="icon-1 info-tooltip">
															<span class="glyphicon glyphicon-pencil"></span>
														</a>                                       
													</p>	
												</td>
												<td  style=" width:10%; border:1px solid #d2d2d2;font-family: Roboto Condensed, sans-serif;font-size:9px;font-style:italic;">
													<p data-placement="top" data-toggle="tooltip" title="Delete">
														<a class="btn btn-danger btn-xs" id="delete" href="javascript:void(0)" onclick="ConfirmDelete(<?php echo $id ?>)" title="Delete" class="icon-2 info-tooltip">
															<span class="glyphicon glyphicon-trash"></span>
														</a>
													</p>
												</td>
											</tr>
											</div>
										    
									
								<?php } ?>
							
						</table>
					</div>
				</div>
			</div>
			
			<!-- <div class="table-responsive">
				<div class="col-lg-12 text-center">
					<div id="viewcategory" style="float: right"></div>
				</div>
			</div> --> <!-- /. Table Resone -->


			<div class="col-md-2">
			 </div>
			 
			<div class="col-lg-5 col-lg-8">
				<div align="right">
					<div id="container">
						<div id="list">
							
								<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="col-sm-5 form-box">
				<div class="form-top">
					<div class="form-top-left">
						<h3 style="color:#0197d8;" id="title">Pending Categories</h3>
					</div>
				</div>
				<div class="form-bottom">
					<div id="container">
						<table class="myTable table table-bordred table-striped" id="list">
							<thead>
								<tr>
									<th class="col-sm-2 text-center">Category Name</th>
									<th class="col-sm-2 text-center">Date</th>
									<th  style="display:none"></th>
									<th style="display:none"></th>
								</tr>
							</thead>
							<tbody>
							
								<?php
								$sql  = "SELECT Id,Name,Icon,Date FROM categorymaster where Status=0 order by Id ASC";
								$query=$this->db->query($sql);
								$query_result = $query->result();
								
								if($this->db->affected_rows() > 0)
								{
								
								foreach($query_result as $row)
								{
									$id = $row->Id;
									$text = $row->Name;
									$imagePath = $row->Icon;
									$date = $row->Date;
								
								?>
									
										<tr id="domain<?php echo $id ?> ">
											<td style="width:30%;" class="well well-lg"><a href='<?php echo base_url().'viewImages/viewallimages/'.$id;?>'><?php echo $text;?></a></td>
												
												<td style="width:30%;" class="well well-lg"><?php echo $date;?></td>

												<td  style="width:10%; border:1px solid #d2d2d2;font-family: Roboto Condensed, sans-serif;font-size:9px;font-style:italic;">
													<p data-placement="top" data-toggle="tooltip" title="Edit"> 
														<a class="btn btn-primary btn-xs" id="editcon" data-title="Approve" href="javascript:void(0)" title="Approve"  onclick="ApproveCategory(<?php echo $id ?>)"class="icon-1 info-tooltip">
															<span class="glyphicon glyphicon-ok"></span>
														</a>                                       
													</p>	
												</td>
												<td  style=" width:10%; border:1px solid #d2d2d2;font-family: Roboto Condensed, sans-serif;font-size:9px;font-style:italic;">
													<p data-placement="top" data-toggle="tooltip" title="Delete">
														<a class="btn btn-danger btn-xs" id="Reject" href="javascript:void(0)" onclick="rejectcategory(<?php echo $id ?>)" title="Reject" class="icon-2 info-tooltip">
															<span class="glyphicon glyphicon-remove"></span>
														</a>
													</p>
												</td>
											</tr>
											</div>
										     
									
								<?php 
									} 
									}
								?>
							
						</table>
						
					</div>
				</div>
			</div>
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>

<script type="text/javascript">

	$(document).ready(function() {
	 $('.myTable').DataTable({
	  "bAutoWidth": false,
	 });
	 $('.alert').hide();
	$('.alert-danger').hide();
	$('.alert-success').hide();
	var options = {
		'btn-loading': '<i class="fa fa-spinner fa-pulse"></i>',
		'btn-success': '<i class="fa fa-check"></i>',
		'btn-error': '<i class="fa fa-remove"></i>',
		'msg-success': 'All Good! Redirecting...',
		'msg-error': 'Wrong login credentials!',
		'useAJAX': true,
	};
	$('#modaldisplay').click(function()
	{
		$("#myModal").modal('show');
		$("#header").html('<B style="color:#0197d8;">Create New Category</B>');
	});
	$("#imagefile").change(function () {
		if (typeof (FileReader) != "undefined") {
			var dvPreview = $("#imgprvw");
			dvPreview.html("");
			var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
			$($(this)[0].files).each(function () {
				var file = $(this);
				if (regex.test(file[0].name.toLowerCase())) {
					var reader = new FileReader();
					reader.onload = function (e) {
						var img = $("<img />");
						img.attr("style", "height:100px;width: 100px;padding: 0px 0px 10px 10px");
						img.attr("src", e.target.result);
						dvPreview.append(img);
						
					}
					reader.readAsDataURL(file[0]);
				} else {
					alert(file[0].name + " is not a valid image file.");
					dvPreview.html("");
					return false;
				}
			});
		}else {
			alert("This browser does not support HTML5 FileReader.");
		}
	});
	
	$('#has_notify').click(function(){
		notifi=$('#has_notify').val();
		if(document.getElementById("has_notify").checked==true){
			notifi=1;
			//alert(notifi);
		}else{
			notifi=0;
			//	alert(notifi);
		}
	});
	$('#createcate').click(function()
	{	
		var ct_name=$('#category').val();
		
		if(ct_name=="")
		{
			$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Category Name");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
		}
		var datacomp = new FormData($('#categoryform')[0]);
		datacomp.append("category",ct_name);
	
		$.ajax({
			type :  "POST",
			//datatype : "JSON",
			url: "<?php echo site_url('Category/createCategory');?>",
			data	: datacomp,
			mimeType: "multipart/form-data",
			contentType: false,
			cache: false,
			processData: false,
			success:function(data){
				//alert(data);
				if(data==1)
				{
					$('.alert-success') .fadeIn( function() 
				   {
							getcategoryData();
						$('.alert-success').show();
						$("#Message").html("Catgeory Inserted Successfully");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							//location.reload();
						
						  }, 1000);
				   });
				}
			}
		});
	});
	$('#warning').click(function()
	{
		$('.alert').hide();
	});
	
	$(".close").click(function(){location.reload();});
});

function getcategoryData() {
	$.get( "<?php echo base_url(); ?>Category/getcategorydata", function( data ) {
		//alert(data);
		$("#list").html(data);
	//	 $('.myTable').DataTable({
		//  "bAutoWidth": false,
		 //});
	});
}

function editCategory(id)
{
		$("#myModal").modal('show');
		$("#header").html('<B style="color:#0197d8;">Edit Category</B>');
		$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "<?php echo site_url('Category/editCategory');?>",
			data : {id:id},
			success:function(data){
				//alert(data);
				var d = JSON.parse(data);
				$.each(d, function (key, value) {
					$id=value.Id;
					$('#category').val(value.Name);
					$('#createcate').hide();
					$('#cancel').show();
					$('#edit').show();
					$('#edit').html('<button type="button" class="btn btn-primary" id="edit" name="edit" onclick="updatecategory($id)">Update</button> <button type="button" class="btn btn-primary" id="cancel" name="cancel" onclick="cancelcategory()">Cancel</button>');
				});
			}
		});
}

function ConfirmDelete(id)
{
$("#myModal").modal('show');
$('#header').html("Are you sure you want to Delete Category");
$('#category').hide();
$('#createcate').hide();
$('#formdata').html('<input type="button" class="btn btn-primary" value="OK" id="OK" name="OK"> <input type="button" class="btn btn-primary" value="Cancel" id="Cancel" name="Cancel">');
$('#OK').click(function()
{
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "<?php echo site_url('Category/deleteCategory');?>",
			data : {id:id},
			success:function(data)
			{
				$("#myModal").modal('hide');
				if(data==1){
					$('.alert-success') .fadeIn( function() 
				   {
				   		getcategoryData();
						$('.alert-success').show();
						$("#Message").html("Catgeory Deleted");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							location.reload();
						  }, 1000);
				   });
				}
			}
		});
});
$('#Cancel').click(function()
{
location.reload();
});
}
function rejectcategory(id)
{
$("#myModal").modal('show');
$('#header').html("Are you sure you want to Reject Category");
$('#category').hide();
$('#createcate').hide();
$('#formdata').html('<input type="button" class="btn btn-primary" value="OK" id="OK" name="OK"> <input type="button" class="btn btn-primary" value="Cancel" id="Cancel" name="Cancel">');
$('#OK').click(function()
{
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "<?php echo site_url('Category/rejectCategory');?>",
			data : {id:id},
			success:function(data)
			{
				$("#myModal").modal('hide');
				if(data==1){
					$('.alert-success') .fadeIn( function() 
				   {
						$('.alert-success').show();
						$("#Message").html("Catgeory Rejected");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							location.reload();
						  }, 1000);
				   });
				}
			}
		});
});
$('#Cancel').click(function()
{
		$("#myModal").modal('hide');
});
}
function updatecategory($id){
	var id=$id;
	var category = $("#category").val();
	if (category==""){
		$('.alert-danger') .fadeIn( function() 
			   {
					$('.alert-danger').show();
					$("#error").html("please enter Category Name");
					  setTimeout( function()
					  {
					  	$('.alert-danger').fadeOut("fast");
					  }, 2000);
			   });
			return false;
	}
	/*if (notification=="none"){
		alert("Please select Category");
		exit;
	}*/
	var datacomp = new FormData($('#categoryform')[0]);
	//alert(data);
	datacomp.append("categoryname",category);
	//datacomp.append("notification",notification);
	//datacomp.append("photofile",photofile);

	$.ajax({
		type :  "POST",
		url: "<?php echo site_url('Category/updateCategory');?>/"+id,
		data	: datacomp,
		mimeType: "multipart/form-data",
		contentType: false,
		cache: false,
		processData: false,
		success:function(data){
			$("#myModal").modal('hide');
					$('.alert-success') .fadeIn( function() 
				   {
				   		getcategoryData();
						$('.alert-success').show();
						$("#Message").html("Catgeory updated Successfully");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							location.reload();
						  }, 1000);
				   });
		}
	});
}
function ApproveCategory(id)
{
$("#myModal").modal('show');
$('#header').html("Are you sure you want to Approve Category");
$('#category').hide();
$('#createcate').hide();
$('#formdata').html('<input type="button" class="btn btn-primary" value="OK" id="OK" name="OK"> <input type="button" class="btn btn-primary" value="Cancel" id="Cancel" name="Cancel">');
$('#OK').click(function()
{
	$.ajax({
			type :  "POST",
			datatype : "JSON",
			url: "<?php echo site_url('Category/approveCategory');?>",
			data : {id:id},
			success:function(data){
			$("#myModal").modal('hide');
				if(data==1){
					$('.alert-success') .fadeIn( function() 
				   {
				   		getcategoryData();
						$('.alert-success').show();
						$("#Message").html("Catgeory Aprroved");
						  setTimeout( function()
						  {
							$('.alert-success').fadeOut("fast");
							location.reload();
						  }, 1000);
				   });
				}
			}
		});
});
$('#Cancel').click(function()
{
		$("#myModal").modal('hide');
		location.reload();
});
}

function cancelcategory(){
	location.reload();
}
</script>
