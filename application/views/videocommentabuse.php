<!DOCTYPE html>
<html>
<title>Holynet</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/css-pop.js');?>"></script>
<link href="<?php echo base_url('assets')?>/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<style>
.w3-sidenav a {padding:16px}
.navimg {float:left;width:33.33% !important}
.w3-lobster {
  font-family: "Lobster", serif;
  
}
.city {display:none;}
 .on  { background:green; }
 .off { background:red; }
 .background{
    background-color:#cccccc;
    padding:15px;
	border-radius: 50%;
}
#blanket {
background-color:#111;
opacity: 0.65;
*background:none;
position:absolute;
z-index: 9001;
top:0px;
left:0px;
width:100%;
}
table.dataTable th,
table.dataTable td {
  white-space: nowrap;
}
#popUpDiv {
position:absolute;
background: teal;
width:400px;
height:100px;
border:2px solid #000;
z-index: 9002;
}

.redborded
{
color: #000000!important;
background-color: #f44336!important;
}
</style>
<body>

<?php $this->load->view('leftmenu');?>

<div class="w3-overlay w3-hide-large" onClick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

<div class="w3-main" style="margin-left:300px;">

<div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large w3-hide-large">
  <i class="fa fa-bars w3-opennav w3-xlarge w3-margin-left w3-margin-right" onClick="w3_open()"></i>HOLYNET 
</div>

<header class="w3-container w3-theme w3-padding-3 w3-center">
  <h5 class="w3-right"><i class="fa fa-sign-out" aria-hidden="true"></i><B><a href="<?php echo  base_url()."Admindashboard/logout";?>" style="color:#FFFFFF">Logout</a></B></h5>
</header>

<div class="w3-container w3-padding-large w3-section w3-light-grey">
  <div class="row" align="center">
	  <div class="col-sm-4">
	  		
	  </div>
  </div>
	  
  

  <p>
  <div class="w3-code">
		<div class="row">
			<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="w3-container w3-teal">
					<h3>Comment And Abuse Report</h3>
				</div>
				</div>
				
			</div>
			<div class="w3-border">
<ul class="w3-navbar w3-white">
  <li id="proftype"><a href="#" class="tablink" onClick="openCity(event, 'Profile');" id='ptype'>Comments</a></li>
  <li id="proftype1"><a href="#" class="tablink" onClick="openCity(event, 'Language');" id='languages'>Abused Report</a></li>
</ul>

<div id="Profile" class="w3-container w3-border city">

 <p>
 	<div class="row">
  		<div class="col-sm-8">
			
				<div class="w3-container w3-teal">
				  <h4>Comments</h4>
				</div>
				<p></p>
					<div id="displayprofile" class="w3-container table-responsive" style="width:100%">
						<p></p>
					</div>
			
		</div>
	</div>
</p>
</div>

<div id="Language" class="w3-container w3-border city">
  <p>
 	<div class="row">
  		<div class="col-sm-8">
			<div  class="w3-card-4">
				<div class="w3-container w3-teal">
				  <h4>Abuse Report</h4>
				</div>
						<p></p>
					<div id="displaylanguage" class="w3-container table-responsive" style="width:80%">
						<p></p>
					</div>
			</div>
		</div>
	</div>
</p>
</div>

<div id='loadingmessage' style='display:none'>
  <center><img src='<?php echo base_url();?>/loading.gif' width="10%" height="10%"/></center>
</div>
<div id="special">
</div>
<div id="normal">
</div>
			</div>
		</div>
  </div>
</div>
</div>
<div id="blanket" style="display:none"></div>
<div id="popUpDiv" style="display:none">
<div class="row"> 
<div class="col-sm-12 w3-text-white" align="center" id="textdisplay"></div>
<div class="col-sm-2"></div><div class="col-sm-8"  align="center"><br><button class="w3-btn w3-blue" id="ok">OK</button> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="cancel">Cancel</a> <a href="#" onClick="popup('popUpDiv')" style="color:white;background-color:teal;" class="w3-btn w3-blue" id="okcan">OK</a></div>
</div>
</div>

<footer class="w3-container w3-padding-large w3-light-grey w3-justify w3-opacity">
  <p><nav>
  <a href="/forum/default.asp" target="_blank">FORUM</a> |
  <a href="/about/default.asp" target="_top">ABOUT</a>
  </nav></p>
</footer>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
var videoid = getParameterByName('VideoId');
var userid = getParameterByName('UserId');
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function openCitys(cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
	 document.getElementById(cityName).style.display = "block";
  }
}

$(document).ready(function(event) 
{	
			openCitys('Profile');
	
			$('#proftype').addClass('redborded');
			$('#ptype').css("color", "#ffffff");
			$('#proftype1').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
			
			$('#languages').click(function()
			{
				$('#proftype').removeClass('redborded');
				$('#normalusers').addClass('w3-white');
				$('#ptype').removeAttr('style');
			});
});		
function displaycomment()
{
		$.ajax({
					url : "http://dev.mobileartsme.com/holynet/Api/getResourceDetails?ResourceId="+videoid+"&Type=1&UserId="+userid,
					type : "GET",
					beforeSend: function()
					{
						$('#loadingmessage').show();
					},
					complete: function()
					{
						$('#loadingmessage').hide();
					},
					success:function(response)
					{
					
						var obj = JSON.parse(response);
						alert(obj.Status);
						var str='';
						var url= "<?php echo base_url()?>";
						if(obj.Status==0)
						{
								str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
						$('#loadingmessage').hide();
						if(obj.Status==1)
						{
							
							str='<div class="w3-content" style="padding-top:5px"><div class="w3-card-4" style="width:100%"><div class="w3-container"><p></p><p><center>'+obj.Message+'</center></p></div></div></div>';
						}
							$('#displayprofile').html(str);
					},
					error:function()
					{
						alert('error');
					}
			});
}
function getParameterByName(name) 
   {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
<script>
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
openNav("nav01");
function openNav(id) {
    document.getElementById("nav01").style.display = "none";
    document.getElementById("nav02").style.display = "none";
    document.getElementById("nav03").style.display = "none";
    document.getElementById(id).style.display = "block";
}
</script>

<script src="<?php echo base_url('assets/js/w3codecolors.js')?>"></script>

</body>
</html>

