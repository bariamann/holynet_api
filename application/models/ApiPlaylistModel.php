<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiPlaylistModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

		/* Method get Total Playlist of User
			Created By: Nishit Patel
		*/
		public function getTotalPlayListOfUser($userId){

			$this->db->select("COUNT(Id) as TotalPlaylist");
			$this->db->from("playlistmaster");
			$this->db->where("userId",$userId);
			$query = $this->db->get();
			$result = $query->result();
			$totalValue = 0;
			if($result != null){
				foreach ($result as $row) {
					$totalValue = (int)$row->TotalPlaylist;
				}
			}
			return $totalValue;
		}

		/* Method to get playlist according userId
       Created By: Nishit Patel
    */
    public function getTotalPagesForGetPlayList($userId){
      $this->db->select("Id,name,createdDate");
      $this->db->from("playlistmaster");
      $this->db->where("userId",$userId);
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
    }
		    /* Method to get playlist title and id according userId
		       Created By: Manzz Baria
		    */
		    public function getPlaylistTitle($userId){
		      $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate");
		      $this->db->from("playlistmaster");
		      $this->db->where("playlistmaster.userId",$userId);
					$this->db->order_by("playlistmaster.Id","DESC");
		      $query = $this->db->get();
		      return $this->displayPlayListTitle($query->result());
		    }

				/* Method to display playlist title and id in Json
					 Created By: Manzz Baria
				*/
				public function displayPlayListTitle($result){
					$arrayObjectClient = null;
					if($result != null){
						foreach($result as $row){
							$arrayObject = null;

							$arrayObject  = array(
														"Id"=>(int)$row->Id,
														"Name" => $row->name,
														"CreatedDate" => $row->createdDate);

							$arrayObjectClient[]=$arrayObject;
						}
					}

					return $arrayObjectClient;
				}


    /* Method to get playlist according userId
       Created By: Nishit Patel
    */
    public function getPlayList($userId,$PageIndex){
      $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,usermaster.Id as userId,usermaster.userName,usermaster.profileImage,usermaster.profileThumb");
      $this->db->from("playlistmaster");
			$this->db->join("usermaster","playlistmaster.userId = usermaster.Id","inner");
      $this->db->where("playlistmaster.userId",$userId);
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->order_by("playlistmaster.Id","DESC");
			$this->db->limit(20,$pageNo);
      $query = $this->db->get();

      return $this->displayPlayList($query->result(),$userId);
    }

    /* Method to display Plalist in Json
       Created By: Nishit Patel
    */
    public function displayPlayList($result,$userId){
      $this->load->model("Utility","utility");
			$this->load->model("ApiFavouriteModel","fmodel");
			$this->load->model("ApiVideoModel","vmodel");
			$this->load->model("ApiUserModel","usmodel");
      $arrayObjectClient = null;
      if($result != null){
        foreach($result as $row){
          $arrayObject = null;
					$arrayUser = null;
          $playListId = (int)$row->Id;
          $createdDate = $this->utility->timeAgoFormat($row->createdDate);
          $playListName = $row->name;

					$profileUrl = "";
					$profileImage = $row->profileImage;
					if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
					}
					$totalVideos = (int)$this->vmodel->getTotalVideosByPlayListId($playListId);
					$totalFavourites = (int)$this->fmodel->getTotalFavouritesForFileId($playListId,PLAYLIST_TYPE);
					$videoObject = $this->vmodel->getLatestVideoFromPlayList($playListId);
					$totalObject = array("TotalVideos"=>$totalVideos,"TotalFavourites"=>$totalFavourites);
					$activities = $this->usmodel->getUserActivity($playListId,PLAYLIST_TYPE,$userId);

					$arrayUser  = array(
										"UserId"=>(int)$row->userId,
										"UserName"=>$row->userName,
										"ProfileThumbImage"=>$profileUrl
										);

          $arrayObject  = array(
                        "Id"=>$playListId,
                        "Name" => $playListName,
                        "CreatedDate" => $createdDate,
												"User"=>$arrayUser,
												"Video"=>$videoObject,
												"Total"=>$totalObject,
												"Activity"=>$activities);

					if($videoObject == null){
						//continue;
					}
          $arrayObjectClient[]=$arrayObject;
        }
      }

      return $arrayObjectClient;
    }

    /* Method to check existing playlist
       Created By: Nishit Patel
    */
    public function checkExistingPlaylist($playListName,$userId){
      $dataArray = "userId=".$userId." AND name=".$this->db->escape($playListName);
  		$this->db->where($dataArray);
  	  $query = $this->db->get('playlistmaster');
  	  $count_row = $query->num_rows();
  	  if ($count_row > 0) {
  	     return TRUE;
  	  } else {
  	     return FALSE;
  	  }
    }

    /* Method to check existing playlist for update time
       Created By: Nishit Patel
    */
    public function checkExistingPlaylistForUpdate($playlistId,$playListName,$userId){
      $dataArray = "userId=".$userId." AND name=".$this->db->escape($playListName)." AND Id <>".$playlistId;
  		$this->db->where($dataArray);
  	  $query = $this->db->get('playlistmaster');
  	  $count_row = $query->num_rows();
  	  if ($count_row > 0) {
  	     return TRUE;
  	  } else {
  	     return FALSE;
  	  }
    }
      /* Method to create playlist and get back id
       Created By: Nishit Patel
    */
    public function createPlayList($PlayListName,$UserId){
        $data = array(
          'name' => $PlayListName,
          'userId' => $UserId
          );
        $this->db->insert('playlistmaster', $data);
        $playListId = $this->db->insert_id();
        return $playListId;
    }

    /* Method to add playlist
       Created By: Nishit Patel
    */
    public function addPlayList($PlayListName,$UserId){
			$this->load->model("Utility","utility");
	    $createdDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
  			$data = array(
  				'name' => $PlayListName,
  				'userId' => $UserId,
					'createdDate' => $createdDate
  				);
  			$this->db->insert('playlistmaster', $data);
  			$playListId = $this->db->insert_id();
        return $this->getPlaylistDetailByPlaylistId($playListId);
  	}

		/* Method to get playlist detail from playlist id
			 Created By: Nishit Patel
		*/
		public function getPlaylistDetailByPlaylistId($playlistId){
			// $this->db->select("Id,name,createdDate");
			// $this->db->from("playlistmaster");
			// $this->db->where("Id",$playlistId);
			// $query = $this->db->get();
			// $result = $query->result();
			// $playListData = null;
			// if($result != null){
			// 	$this->load->model("Utility","utility");
			// 	foreach ($result as $row) {
			//
			// 		$displayDate = $this->utility->timeAgoFormat($row->createdDate);
			// 		$playListData = array('Id' => (int)$row->Id,'Name'=>$row->name,'Date'=>$displayDate);
			// 	}
			// }
			// return $playListData;

			$this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,usermaster.Id as userId,usermaster.userName,usermaster.profileImage,usermaster.profileThumb");
      $this->db->from("playlistmaster");
			$this->db->join("usermaster","playlistmaster.userId = usermaster.Id","inner");
      $this->db->where("playlistmaster.Id",$playlistId);
      $query = $this->db->get();

      return $this->displayPlayList($query->result(),0);


		}

    /* Method to edit playlist
       Created By: Nishit Patel
    */
    public function updatePlayList($playListId,$playListName){
			$this->load->model("Utility","utility");
			$createdDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
  		$dataArray = array("name"=>$playListName,"createdDate" => $createdDate);
      $this->db->where('Id', $playListId);
  		$this->db->update('playlistmaster', $dataArray);
      if($this->db->affected_rows() > 0){
        return $this->getPlaylistDetailByPlaylistId($playListId);
      }else{
        return null;
      }
    }

    /* Method to delete Playlist
       Created By: Nishit Patel
    */
    public function deletePlayList($playlistId){
      $this->db->where('Id', $playlistId);
      $this->db->delete('playlistmaster');
      if($this->db->affected_rows() > 0){

        $this->load->model("ApiVideoModel","video");
        $commentUpdateDate = $this->video->deleteVideosByPlayList($playlistId);

        return true;
      }else{
        return false;
      }
    }

		/* Method to search playlist by keyword
			 Created By: Nishit Patel
		*/
    public function getSearchPlayListByKeyword($userId,$keyword,$lang,$PageIndex){
      $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
			(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total,playlistmaster.userId,usermaster.userName,usermaster.profileImage,");
      $this->db->from("playlistmaster");
			$this->db->join('usermaster','playlistmaster.userId = usermaster.Id','inner');
			$this->db->join('likemaster','likemaster.fileId = playlistmaster.Id and likemaster.type = '.PLAYLIST_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = playlistmaster.Id and favouritemaster.type = '.PLAYLIST_TYPE,'left');
			$whereStr = "playlistmaster.userId <> ".$userId." AND playlistmaster.name like '%".$keyword."%'";
      $this->db->where($whereStr);
			$this->db->group_by("playlistmaster.Id");
			$this->db->order_by("Total","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
      return $this->displayPlayList($query->result(),$userId);
    }

		/* Method to Total page for search playlist by keyword
			 Created By: Nishit Patel
		*/
    public function getTotalPageSearchPlayListByKeyword($userId,$keyword,$lang,$PageIndex){
      $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
			(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total");
      $this->db->from("playlistmaster");
			$this->db->join('likemaster','likemaster.fileId = playlistmaster.Id and likemaster.type = '.PLAYLIST_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = playlistmaster.Id and favouritemaster.type = '.PLAYLIST_TYPE,'left');
			$whereStr = "playlistmaster.userId <> ".$userId." AND playlistmaster.name like '%".$keyword."%'";
      $this->db->where($whereStr);
			$this->db->group_by("playlistmaster.Id");
			$this->db->order_by("Total","desc");
			$query = $this->db->get();
			$result = $query->num_rows();
			$totalPages  = $result / 20;
			return round($totalPages);

    }

	}
?>
