<?php
class Videotypemodel extends CI_Model
{
	public function displayVideoType()
	{
		$query = $this->db->query("select * from  videotypemaster");
		if($query->num_rows()>0)
		{
			$str ="";
			$result = $query->result_array();


			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str.='<table id="listoutlet" width="100%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container dt-responsive">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Name English</th>
					<th class="text-center">Name Arabic</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain = "<tr id='domain1".$value['Id']."'>";
					$str.=$domain.'<td>'.$value['nameEnglish'].'</td>
					<td>'.$value['nameArabic'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)" id="editvideo'.$value['Id'].'"  onclick="editvideotype('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)"  id="deletevideo'.$value['Id'].'" onclick="ConfirmDelete('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str.='</form>';
				$str.='</tbody></table><p></p>';
			echo $str;
		}
	}

	public function displayProfileType()
	{
		$query = $this->db->query("select * from  profiletypemaster");
		if($query->num_rows()>0)
		{
			$str ="";
			$result = $query->result_array();


			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str.='<table id="listprofile" width="100%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container dt-responsive">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Name English</th>
					<th class="text-center">Name Arabic</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain = "<tr id='domain1".$value['Id']."'>";
					$str.=$domain.'<td>'.$value['nameEnglish'].'</td>
					<td>'.$value['nameArabic'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)" id="editprofile'.$value['Id'].'"  onclick="editprofiletype('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)"  id="deleteprofile'.$value['Id'].'" onclick="ConfirmDeleteProfile('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str.='</form>';
				$str.='</tbody></table><p></p>';
			echo $str;
		}
	}

	/*public function displayProfileType()
	{
		$query = $this->db->query("select * from  profiletypemaster");
		if($query->num_rows()>0)
		{
			$str ="";
			$result = $query->result_array();

			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str.='<table id="listprofile" width="100%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Name English</th>
					<th class="text-center">Name Arabic</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain = "<tr id='profile".$value['Id']."'>";
					$str.=$domain.'<td>'.$value['nameEnglish'].'</td>
					<td>'.$value['nameArabic'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)" id="editprofile'.$value['Id'].'" onclick="editprofiletype('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)"  id="deleteprofile'.$value['Id'].'" "onclick="ConfirmDeleteProfile('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str.='</form>';
				$str.='</tbody></table><p></p>';
			echo $str;
		}
	}*/
	public function DisplayCountry()
	{
		$query = $this->db->query("select * from  countrymaster");
		if($query->num_rows()>0)
		{
			$str ="";
			$result = $query->result_array();


			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str.='<table id="listcountry" width="100%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container dt-responsive">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Name English</th>
					<th class="text-center">Name Arabic</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain = "<tr id='domain1".$value['Id']."'>";
					$str.=$domain.'<td>'.$value['nameEnglish'].'</td>
					<td>'.$value['nameArabic'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)" id="editcounry'.$value['Id'].'"  onclick="EditCountry('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)"  id="deletecounrty'.$value['Id'].'" onclick="ConfirmDeleteCountry('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str.='</form>';
				$str.='</tbody></table><p></p>';
			echo $str;
		}
	}
	/*public function DisplayCountry()
	{
		$query = $this->db->query("select * from  countrymaster");
		if($query->num_rows()>0)
		{
			$str4 ="";
			$result = $query->result_array();
			$domain4="";

			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str4.='<table id="listcountry" width="100%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Name English</th>
					<th class="text-center">Name Arabic</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain4 = "<tr id='profile".$value['Id']."'>";
					$str4.=$domain4.'<td>'.$value['nameEnglish'].'</td>
					<td>'.$value['nameArabic'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)" id="editcountry'.$value['Id'].'" onclick="EditCountry('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)" id="deletecountry'.$value['Id'].'" "onclick="DeleteCountry('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str4.='</form>';
				$str4.='</tbody></table><p></p>';
			echo $str4;
		}
	}
	*/
	public function DisplayLanguage()
	{
		$query = $this->db->query("select * from  languagemaster");
		if($query->num_rows()>0)
		{
			$str2 ="";
			$result = $query->result_array();


			//$str.='<table id="listoutlet" cellspacing="0" class="container-fluid" style="width: 780px;">
			$str2.='<table id="listlaguage" width="90%"  class="w3-table w3-bordered w3-striped w3-hoverable w3-container dt-responsive">
			<thead>
			<tr class="w3-blue">
					<th class="text-center">Language</th>
					<th style="display:none;"></th>
					<th style="display:none;"></th>
				</tr>
			</thead>
			<tbody>';
				foreach($result as $key=>$value) {
					$domain2 = "<tr id='language".$value['Id']."'>";
					$str2.=$domain2.'<td>'.$value['name'].'</td>
					<td><a class="w3-btn-floating w3-blue" href="javascript:void(0)"  onclick="editlanguage('.$value['Id'].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
					<td><a class="w3-btn-floating w3-red" href="javascript:void(0)" onclick="ConfirmDeleteLanguage('.$value['Id'].')"><i class="fa fa-times"></i> </a>
				</tr>';
			}
			$str2.='</form>';
				$str2.='</tbody></table><p></p>';
			echo $str2;
		}
	}
}
