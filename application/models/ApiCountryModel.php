<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCountryModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to get all Country in asending order by name from database
      Created By: Nishit Patel
  */
  public function getCountry(){

			$this->db->select('Id,nameEnglish,nameArabic');
			$this->db->from('countrymaster');
			$this->db->order_by('nameEnglish', 'ASC');
			$query = $this->db->get();

			return $this->displayCountry($query->result());

  }
  /* Method to display Country data into proper json format
      Created By: Nishit Patel
  */
  public function displayCountry($result){
  		$arrayObjectClient = null;
  		if($result != null){
  			foreach($result as $row){
  				$arrayObject = null;
  				$nameEnglish = $row->nameEnglish;
          $nameArabic = $row->nameArabic;

  				$arrayObject  = array(
  											"Id"=>(int)$row->Id,
  											"NameEnglish"=> $nameEnglish,
                        "NameArabic" => $nameArabic);

  				$arrayObjectClient[]=$arrayObject;
  			}
  		}
  		return $arrayObjectClient;
  }

  /* Method to check is Country already exisist
      Created By: Nishit Patel
  */
  public function checkIsCountryExsist($nameEnglish,$nameArabic){

    $dataArray = "nameEnglish=".$this->db->escape($nameEnglish)." AND nameArabic=".$this->db->escape($nameArabic);

		$this->db->where($dataArray);
	  $query = $this->db->get('countrymaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }

  }

  /* Method to check is Country already exsist for update
      Creted By: Nishit Patel
  */
  public function checkIsCountryExsistforUpdate($Id,$NameEnglish,$NameArabic){
    $dataArray = "nameEnglish=".$this->db->escape($NameEnglish)." AND nameArabic=".$this->db->escape($NameArabic)." AND Id <> ".$Id;
    $this->db->where($dataArray);
	  $query = $this->db->get('countrymaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
  }


  /* Method to add new Country
      Created By: Nishit Patel
  */
  public function addNewCountry($NameEnglish,$NameArabic){

    $data = array('nameEnglish' => $NameEnglish,'nameArabic' => $NameArabic);
    $this->db->insert('countrymaster', $data);
    $insert_Id = $this->db->insert_id();

    $countryData = array('Id' => (int)$insert_Id,'NameEnglish'=> $NameEnglish,'NameArabic'=> $NameArabic);
    return $countryData;

  }

  /* Method to delete exsisting Country by language Id
      Created By: Nishit Patel
  */
  public function deleteExistsCountry($Id){

      $this->db->where('Id', $Id);
      $this->db->delete('countrymaster');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }

  }

  /* Method to update exsising Country record
      Created By: Nishit Patel
  */
  public function updateExistsCountry($Id,$NameEnglish,$NameArabic){

    $dataArray = array("nameEnglish"=>$NameEnglish,"nameArabic"=>$NameArabic);
    $this->db->where('Id', (int)$Id);
		$this->db->update('countrymaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }

  }


}


?>
