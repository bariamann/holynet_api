<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiNotificationModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

		/* Method to test sample notification
			 Created By: Nishit Patel
		*/
		public function sendSampleNotification($userId,$deviceType){
			$this->db->select("Id,deviceToken,userId,deviceType");
			$this->db->from("devicemaster");
			$this->db->where("userId",$userId);
			$this->db->where("deviceType",$deviceType);
			$query = $this->db->get();
			$result = $query->result();
			$deviceList = null;
			foreach ($result as $row) {
				$deviceList[] = $row->deviceToken;
			}
			$this->notificationmodel->sendNotificationToDistributorIphoneDevice($deviceList,"Distribution","APN Testing Message","","","","","","");
		}

		/* Method to get user name by userId
			 Created By: Nishit Patel
		*/
		public function getUserName($userId){
			$this->db->select("userName");
			$this->db->from("usermaster");
			$this->db->where("Id",$userId);
			$query = $this->db->get();
			$result = $query->result();
			$userName = "";
			if($result != null){
				foreach ($result as $row) {
					$userName = $row->userName;
				}
			}
			return $userName;
		}

		/* Method to get user profile picture by userId
			 Created By: Nishit Patel
		*/
		public function getUserProfilePicture($userId){
			$this->db->select("profileThumb");
			$this->db->from("usermaster");
			$this->db->where("Id",$userId);
			$query = $this->db->get();
			$result = $query->result();
			$profileThumb = "";
			if($result != null){
				foreach ($result as $row) {
					$profileThumb = $row->profileThumb;
				}
			}
			return $profileThumb;
		}


		/* Method to get Rejected Video
			 Created By: Nishit Patel
		*/
		public function getVideoSmallDetail($videoId){
			$this->db->select("videomaster.Id,videomaster.title,videomaster.description,videomaster.thumbUrl,videomaster.fileUrl,contentmaster.userId");
			$this->db->from("videomaster");
			$this->db->join("contentmaster","videomaster.contentId = contentmaster.Id","inner");
			$this->db->where("videomaster.Id",$videoId);
			$query = $this->db->get();
			return $query->result();
		}

		/* Method to get Rejected Image
		 	 Created By: Nishit Patel
		*/
		public function getImageSmallDetail($imageId){
			$this->db->select("imagemaster.Id,imagemaster.title,imagemaster.thumbUrl,imagemaster.fileUrl,contentmaster.userId");
			$this->db->from("imagemaster");
			$this->db->join("contentmaster","imagemaster.contentId = contentmaster.Id","inner");
			$this->db->where("imagemaster.Id",$imageId);
			$query = $this->db->get();
			return $query->result();
		}

		/* Method to get Rejected post
			 Created By: Nishit Patel
		*/
		public function getPostSmallDetail($postId){
			$this->db->select("postmaster.Id,postmaster.decription,contentmaster.userId");
			$this->db->from("postmaster");
			$this->db->join("contentmaster","postmaster.contentId = contentmaster.Id","inner");
			$this->db->where("postmaster.Id",$postId);
			$query = $this->db->get();
			return $query->result();
		}

		/* Method to get user device token
			 Created By: Nishit Patel
		*/
		public function getUserDeviceToken($userId){
			$this->db->select("Id,deviceToken,deviceType,userId");
			$this->db->from("devicemaster");
			$this->db->where("userId",$userId);
			$query = $this->db->get();
			return $query->result();
		}

		/* Method to send notification when reject
			 Created By: Nishit Patel
		*/
		public function sendNotificationForRejectContent($fileId,$type,$userId){
				$result = null;
				$notificationType = REJECT_NOTIFICATION;
				$notificationMessage = "";
				$notificationTitle = "";
				$thumbUrl = "gfhf";
				$userId = 0;

				if($type == VIDEO_TYPE){
					$result = $this->getVideoSmallDetail($fileId);
					$notificationTitle = "Your Video rejected due to abuse content.";
					$notificationMessage = "Title: '".$result->title."'";
					$thumbUrl = VIDEO_THUMB_URL.$result->thumbUrl;
					$userId = (int)$result->userId;
				}else if($type == IMAGE_TYPE){
					$result = $this->getImageSmallDetail($fileId);
					$notificationTitle = "Your Image rejected due to abuse content.";
					$notificationMessage = "Title: '".$result->title."'";
					$thumbUrl = IMAGE_THUMB_URL.$result->thumbUrl;
					$userId = (int)$result->userId;
				}else if($type == POST_TYPE){
					$result = $this->getPostSmallDetail($fileId);
					$notificationTitle = "Your Post rejected due to abuse content.";
					$notificationMessage = "Title: '".$result->description."'";
					$thumbUrl = "";
					$userId = (int)$result->userId;
				}

				$this->db->select("Id,deviceToken,deviceType,userId");
				$this->db->from("devicemaster");
				$this->db->where("userId",$userId);
				$query = $this->db->get();
				$userResult = $query->result();


				$androidGCMArray = null;
				$iosAPNArray = null;
				if($userResult != null){
					foreach ($userResult as $row) {
						$deviceType = (int)$row->deviceType;
    				if ($deviceType == ANDROID_NOTIFICATION) {
								$androidGCMArray[] = $row->deviceToken;
    				}
						if($deviceType == IOS_NOTIFICATION){
    						$iosAPNArray[] = $row->deviceToken;
    				}
					}
					//Send notification to android users
    			if ($androidGCMArray != null) {
    				$this->sendNotificationAndroid($androidGCMArray,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType);
    			}
					//Send notification to iphone users
    			if($iosAPNArray != null){
    				$this->sendDisNotificationiPhone($iosAPNArray,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType);
    			}
				}
		}

		/* Method to send notification when like / dislike
			 Created By: Manzz Baria
		*/
		public function sendNotificationForLikeDislikeContent($fileId,$type,$userId,$status){

			$userName = $this->getUserName($userId);
			$result = null;
			$notificationType = LIKE_DISLIKE_NOTIFICATION;
			$notificationMessage = "";
			$notificationTitle = "";
	    $thumbUrl = "";
			$UserId = 0;
			$reason = "";

			if($type == VIDEO_TYPE){
				$result = $this->getVideoSmallDetail($fileId);

				if ($status == 1) {
					# Like
					$notificationTitle = $userName." like your video.";
				}else if($status == 2){
						# Dislike
						$notificationTitle = $userName." dislike your video.";
				}
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = VIDEO_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;
						if($UserId == $userId){
							return;
						}
					}
				}

			}else if($type == IMAGE_TYPE){
				$result = $this->getImageSmallDetail($fileId);

				if ($status == 1) {
					# Like
						$notificationTitle = $userName." like your image";
			}else if($status == 2){
						# Dislike
							$notificationTitle = $userName." dislike your image";
				}
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = IMAGE_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;

						if($UserId == $userId){
							return;
						}

					}
				}

			}else if($type == POST_TYPE){
				$result = $this->getPostSmallDetail($fileId);

				if ($status == 1) {
					# Like
					$notificationTitle = $userName." like your post.";
				}else if($status == 2){
						# Dislike
						$notificationTitle = $userName." dislike your post.";
				}
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->decription."'";
						$thumbUrl = "";
						$UserId = (int)$row->userId;
						if($UserId == $userId){
							return;
						}
					}
				}
			}

			$this->db->select("devicemaster.userId,devicemaster.deviceToken,devicemaster.deviceType");
			$this->db->from("devicemaster");
			$this->db->where("devicemaster.userId",$UserId);
			$query = $this->db->get();
			$userResult = $query->result();

			$androidGCMArray = null;
			$iosAPNArray = null;

			if($userResult != null){
				foreach ($userResult as $row) {
					$deviceType = (int)$row->deviceType;
					if ($deviceType == ANDROID_NOTIFICATION) {
							$androidGCMArray[] = $row->deviceToken;
					}
					if($deviceType == IOS_NOTIFICATION){
							$iosAPNArray[] = $row->deviceToken;
					}
				}

			//	$this->sendNotificationAndroid($androidGCMArray,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType);
				//Send notification to android users
				if ($androidGCMArray != null) {
					$this->sendNotificationToAndroidDevice($androidGCMArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				//Send notification to iphone users
				if($iosAPNArray != null){
					if(IS_APN_DEVELOPMENT){
							$this->sendNotificationToIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}else{
							$this->sendNotificationToDistributorIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				}
			}

		}
		/* Method to send notification when comment
			 Created By: Manzz Baria
		*/
		public function sendNotificationForCommentContent($fileId,$type,$userId,$comment){

			$userName = $this->getUserName($userId);
			$result = null;
			$notificationType = COMMENT_NOTIFICATION;
			$notificationMessage = "";
			$notificationTitle = "";
	    $thumbUrl = "";
			$UserId = 0;
			$reason = "";

			if($type == VIDEO_TYPE){
				$result = $this->getVideoSmallDetail($fileId);
				$notificationTitle = $userName." comment on your video.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = VIDEO_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;
					}
				}

			}else if($type == IMAGE_TYPE){
				$result = $this->getImageSmallDetail($fileId);
				$notificationTitle = $userName." comment on your image.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = IMAGE_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;
					}
				}

			}else if($type == POST_TYPE){
				$result = $this->getPostSmallDetail($fileId);
				$notificationTitle = $userName." comment on your post.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->decription."'";
						$thumbUrl = "";
						$UserId = (int)$row->userId;
					}
				}

			}
			$notificationMessage = "Comment: '".$comment."'";

			$this->db->select("devicemaster.userId,devicemaster.deviceToken,devicemaster.deviceType");
			$this->db->from("devicemaster");
			$this->db->where("devicemaster.userId",$UserId);
			$query = $this->db->get();
			$userResult = $query->result();

			$androidGCMArray = null;
			$iosAPNArray = null;

			if($userResult != null){
				foreach ($userResult as $row) {
					$deviceType = (int)$row->deviceType;
					if ($deviceType == ANDROID_NOTIFICATION) {
							$androidGCMArray[] = $row->deviceToken;
					}
					if($deviceType == IOS_NOTIFICATION){
							$iosAPNArray[] = $row->deviceToken;
					}
				}

			//	$this->sendNotificationAndroid($androidGCMArray,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType);
				//Send notification to android users
				if ($androidGCMArray != null) {
					$this->sendNotificationToAndroidDevice($androidGCMArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				//Send notification to iphone users
				if($iosAPNArray != null){
					if(IS_APN_DEVELOPMENT){
							$this->sendNotificationToIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}else{
							$this->sendNotificationToDistributorIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				}
			}

		}


		/* Method to send notification when following
			 Created By: Manzz Baria
		*/
		public function sendNotificationForFollowingUser($fileId,$type,$userId){

			$userName = $this->getUserName($userId);
			$profilePicture = $this->getUserProfilePicture($userId);
			$result = null;
			$notificationType = FOLLOWING_NOTIFICATION;
			$notificationMessage = "";
			$notificationTitle = "";
			$thumbUrl = "";
			$reason = "";

			$notificationTitle = $userName." started following you.";
			$thumbUrl = PROFILE_THUMB_URL.$thumbUrl;


			$this->db->select("devicemaster.userId,devicemaster.deviceToken,devicemaster.deviceType");
			$this->db->from("devicemaster");
			$this->db->where("devicemaster.userId",$fileId);
			$query = $this->db->get();
			$userResult = $query->result();
			$UserId = $fileId;

			$androidGCMArray = null;
			$iosAPNArray = null;

			if($userResult != null){
				foreach ($userResult as $row) {
					$deviceType = (int)$row->deviceType;
					if ($deviceType == ANDROID_NOTIFICATION) {
							$androidGCMArray[] = $row->deviceToken;
					}
					if($deviceType == IOS_NOTIFICATION){
							$iosAPNArray[] = $row->deviceToken;
					}
				}

			//	$this->sendNotificationAndroid($androidGCMArray,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType);
				//Send notification to android users
				if ($androidGCMArray != null) {
					$this->sendNotificationToAndroidDevice($androidGCMArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				//Send notification to iphone users
				if($iosAPNArray != null){
					if(IS_APN_DEVELOPMENT){
							$this->sendNotificationToIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}else{
							$this->sendNotificationToDistributorIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				}
			}

		}


		/* Method to send notification when shared
			 Created By: Manzz Baria
		*/
		public function sendNotificationForSharedContent($fileId,$type,$myUserId,$shareUserId){

						$userName = $this->getUserName($myUserId);
						$result = null;
						$notificationType = SHARE_NOTIFICATION;
						$notificationMessage = "";
						$notificationTitle = "";
				    $thumbUrl = "";
						$reason = "";

						if($type == VIDEO_TYPE){
							$result = $this->getVideoSmallDetail($fileId);
							$notificationTitle = $userName." shared video with you.";
							if($result != null){
								foreach($result as $row){
									$notificationMessage = "Title: '".$row->title."'";
									$thumbUrl = VIDEO_THUMB_URL.$row->thumbUrl;
								}
							}

						}else if($type == IMAGE_TYPE){
							$result = $this->getImageSmallDetail($fileId);
							$notificationTitle = $userName." shared image with you.";
							if($result != null){
								foreach($result as $row){
									$notificationMessage = "Title: '".$row->title."'";
									$thumbUrl = IMAGE_THUMB_URL.$row->thumbUrl;
								}
							}

						}else if($type == POST_TYPE){
							$result = $this->getPostSmallDetail($fileId);
							$notificationTitle = $userName." shared post with you.";
							if($result != null){
								foreach($result as $row){
									$notificationMessage = "Title: '".$row->decription."'";
									$thumbUrl = "";
								}
							}

						}
						$this->db->select("devicemaster.userId,devicemaster.deviceToken,devicemaster.deviceType");
						$this->db->from("devicemaster");
						$this->db->where("devicemaster.userId",$shareUserId);
						$query = $this->db->get();
						$userResult = $query->result();
						$UserId = $shareUserId;
						$androidGCMArray = null;
						$iosAPNArray = null;

						if($userResult != null){
							foreach ($userResult as $row) {
								$deviceType = (int)$row->deviceType;
								if ($deviceType == ANDROID_NOTIFICATION) {
										$androidGCMArray[] = $row->deviceToken;
								}
								if($deviceType == IOS_NOTIFICATION){
										$iosAPNArray[] = $row->deviceToken;
								}
							}

					//		$this->sendNotificationAndroid($androidGCMArray,$notificationTitle,$notificationMessage,$thumbUrl,$myUserId,$fileId,$type,$notificationType);
							//Send notification to android users
							if ($androidGCMArray != null) {
								$this->sendNotificationToAndroidDevice($androidGCMArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
								}
							//Send notification to iphone users
							if($iosAPNArray != null){
								if(IS_APN_DEVELOPMENT){
										$this->sendNotificationToIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
								}else{
										$this->sendNotificationToDistributorIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
								}
							}
						}
		}

		/* Method to send notification when reject content
			 Created By: Manzz Baria
		*/
		public function sendNotificationForRejection($fileId,$type,$Reason){

		//	$userName = $this->getUserName($userId);
			$result = null;
			$notificationType = REJECT_NOTIFICATION;
			$notificationMessage = "";
			$notificationTitle = "";
			$thumbUrl = "";
			$UserId = 0;
			$reason = "";

			$reason = $Reason;

			if($type == VIDEO_TYPE){
				$result = $this->getVideoSmallDetail($fileId);
				$notificationTitle = "Admin has rejected your video.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = VIDEO_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;
					}
				}

			}else if($type == IMAGE_TYPE){
				$result = $this->getImageSmallDetail($fileId);
				$notificationTitle = "Admin has rejected your image.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->title."'";
						$thumbUrl = IMAGE_THUMB_URL.$row->thumbUrl;
						$UserId = (int)$row->userId;
					}
				}

			}else if($type == POST_TYPE){
				$result = $this->getPostSmallDetail($fileId);
				$notificationTitle = "Admin has rejected your post.";
				if($result != null){
					foreach($result as $row){
						$notificationMessage = "Title: '".$row->decription."'";
						$thumbUrl = "";
						$UserId = (int)$row->userId;
					}
				}

			}
			$this->db->select("devicemaster.userId,devicemaster.deviceToken,devicemaster.deviceType");
			$this->db->from("devicemaster");
			$this->db->where("devicemaster.userId",$UserId);
			$query = $this->db->get();
			$userResult = $query->result();

			$androidGCMArray = null;
			$iosAPNArray = null;

			if($userResult != null){
				foreach ($userResult as $row) {
					$deviceType = (int)$row->deviceType;
					if ($deviceType == ANDROID_NOTIFICATION) {
							$androidGCMArray[] = $row->deviceToken;
					}
					if($deviceType == IOS_NOTIFICATION){
							$iosAPNArray[] = $row->deviceToken;
					}
				}
				$userId = $UserId;
				//Send notification to android users
				if ($androidGCMArray != null) {
					$this->sendNotificationToAndroidDevice($androidGCMArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				//Send notification to iphone users
				if($iosAPNArray != null){
					if(IS_APN_DEVELOPMENT){
							$this->sendNotificationToIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}else{
							$this->sendNotificationToDistributorIphoneDevice($iosAPNArray,$notificationTitle,$notificationMessage,$UserId,$thumbUrl,$fileId,$type,$notificationType,$reason);
					}
				}
			}

		}





		/* Method to send Notification to android device
			 Created By: Manzz Baria
		*/

	public function sendNotificationToAndroidDevice($deviceId,$NotyTitle,$Message,$userId,$thumbUrl,$fileId,$type,$notificationType,$reason){

		$ANDROID_API = "AIzaSyAKKn5sKb1K6XFfh7ZZFoIN0fgEmZkTzwM";
		$ANDROID_APN_URL = "https://android.googleapis.com/gcm/send";
		$api_key = $ANDROID_API;
		//$DeviceId=$deviceId;
		$registrationIDs= $deviceId;
		$url = $ANDROID_APN_URL;

		//$Message = "Easy Market has sent you NEW OFFERS!";
		$fields = array(
			'registration_ids'  => $registrationIDs,
			'data'  => array(
				"Title" => $NotyTitle,
				"Message" => $Message,
				"UserId" => $userId,
				"ThumbUrl" => $thumbUrl,
				"FileId" => $fileId,
				"FileType" => $type,
				"Reason" => $reason,
				"NotificationType" => $notificationType
			),
		);
		$headers = array('Authorization: key=' . $api_key,'Content-Type: application/json');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
		curl_close($ch);
	}
	/* Method to send Notification to iphone device
		 Created By: Manzz Baria
	*/


	public function sendNotificationiPhone($deviceId,$notyMessage,$insert_id){
		// Put your device token here (without spaces):
		$deviceToken[] = $deviceId; //'ee5d21ff67c71819e08ce615873fec50955c70cce2cbbd8d0665f6a982b517d4';

		//echo "Device Token: ".$deviceToken."</BR>";
		$passphrase = '';
		if (DEBUG_APN_WITH_SAMPLE) {
			$passphrase = 'welcome';
		}else{
			$passphrase = 'naimnaim2';
		}
		// Put your alert message here:
		//$message = 'Easy Market has sent you NEW OFFERS!';
		$NotiTitle = "Holynet";
		$message = $notyMessage;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		//stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_vs2apn.pem');
		if (DEBUG_APN_WITH_SAMPLE) {
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_test1.pem');
		}else{
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_naiim.pem');
		}
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		//echo "</BR>FP: ".$fp."</BR>";
		if (!$fp){
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		}

		$aps = array('alert' => $message,'sound' => 'default');
		$bodyJson = array("aps"=>$aps,"notificationid"=>(int)$insert_id,"type"=>0);
		// Encode the payload as JSON
		$payload = json_encode($bodyJson);
		if ($deviceToken != null) {
			foreach ($deviceToken as $device) {
				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));
				if (!$result){
					echo 'Message not delivered' . PHP_EOL;
				}else{
					echo 'Message successfully delivered' . PHP_EOL;
				}
				// Close the connection to the server
				fclose($fp);
			}
		}
	}


	public function sendNotificationToIphoneDevice($deviceId,$notificationTitle,$notificationMessage,$userId,$thumbUrl,$fileId,$type,$notificationType,$reason){

		// Put your device token here (without spaces):
		$deviceToken = $deviceId;
		$passphrase = '';
		if (DEBUG_APN_WITH_SAMPLE) {
			$passphrase = 'welcome';
		}else{
			$passphrase = 'naimnaim2';
		}
		$NotiTitle = "Holynet";
		$message = $notificationMessage;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		if (DEBUG_APN_WITH_SAMPLE) {
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_test1.pem');
		}else{
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_naiim.pem');
		}
		//stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_test1.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		$aps = array('alert' => $notificationTitle." ".$notificationMessage,'sound' => 'default');
		$bodyJson = array("aps"=>$aps,"UserId" => $userId,"ThumbUrl" => $thumbUrl,"FileId" => $fileId,"FileType" => $type,"Reason" => $reason,"NotificationType" => $notificationType);

		// Encode the payload as JSON
		$payload = json_encode($bodyJson);

		try {
			if ($deviceId != null) {
				foreach ($deviceId as $device) {
					//echo $device."</BR>";
					// Build the binary notification
					$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
					//$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device)))) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));
					if (!$result){
						//echo 'Message not delivered' . PHP_EOL;
					}else{
						//echo 'Message successfully delivered' . PHP_EOL;
					}
					// Close the connection to the server
				}
			}
			fclose($fp);
		} catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}


	public function sendNotificationToDistributorIphoneDevice($deviceId,$notificationTitle,$notificationMessage,$userId,$thumbUrl,$fileId,$type,$notificationType,$reason){

		// Put your device token here (without spaces):
		$deviceToken = $deviceId;
		$passphrase = '';
		if (DEBUG_APN_WITH_SAMPLE) {
			$passphrase = 'welcome';
		}else{
			$passphrase = 'naimnaim2';
		}
		$NotiTitle = "Holynet";
		$message = $notificationMessage;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		if (DEBUG_APN_WITH_SAMPLE) {
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_dis_vs2.pem');
		}else{
			stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck_naiim1.pem');
		}
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		$aps = array('alert' => $notificationTitle." ".$notificationMessage,'sound' => 'default');
		$bodyJson = array("aps"=>$aps,"UserId" => $userId,"ThumbUrl" => $thumbUrl,"FileId" => $fileId,"FileType" => $type,"Reason" => $reason,"NotificationType" => $notificationType);

		// Encode the payload as JSON
		$payload = json_encode($bodyJson);

		try {
			if ($deviceId != null) {
				foreach ($deviceId as $device) {
					// Build the binary notification
					$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
					//$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device)))) . pack('n', strlen($payload)) . $payload;
					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));
					if (!$result){
						//echo 'Message not delivered' . PHP_EOL."</BR>";
					}else{
						//echo 'Message successfully delivered' . PHP_EOL."</BR>";
					}
					// Close the connection to the server
				}
			}
			fclose($fp);
		} catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}



    /* Method to send Notification to android
       Created By: Nishit Patel
    */
  	public function sendNotificationAndroid($deviceTokes,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType){

  		$ANDROID_API = "AIzaSyAGJc8nvppftYey-CQegr8BqH0KR7NVBVY";
  		$ANDROID_APN_URL = "https://android.googleapis.com/gcm/send";
  		$api_key = $ANDROID_API;

  		$url = $ANDROID_APN_URL;

			$deviceTokes = "ey25JbKxa7A:APA91bGduDq6LbMdptpcEFDidh3I3nDGq4smb7Iu-To8J4ncoKyGdZSw3moyKQmMbXJfMzpNq0uGbrs0PCzSCEm8DNkRkKG7GFItpoLFDYrwrEef3j2GFJwM7zT5a8XTFqNhZ-K-LovI";

  		$NotiTitle = $notificationTitle;
  		$Message = $notificationMessage;
  		$fields = array(
  			'registration_ids'  => $deviceTokes,
  			'data'  => array(
					"Title" => $NotiTitle,
					"Message" => $Message,
					"UserId" => $userId,
					"ThumbUrl" => $thumbUrl,
					"FileId" => $fileId,
					"FileType" => $type,
					"NotificationType" => $notificationType
				),
  		);
  		$headers = array('Authorization: key='.$api_key,'Content-Type: application/json');
  		$ch = curl_init();
  		curl_setopt($ch, CURLOPT_URL, $url);
  		curl_setopt( $ch, CURLOPT_POST, true );
  		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
  		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
  		$result = curl_exec($ch);
  		curl_close($ch);

  	}

		/* Method to send Notification to iPhone
       Created By: Nishit Patel
    */
  	public function sendDisNotificationiPhone($deviceTokes,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType){
  		// Put your device token here (without spaces):
			$deviceId = $deviceTokes;
  		$deviceToken = $deviceId;
  		$passphrase = 'naimnaim2';
  		$NotiTitle = "HolyNet";
  		$message = $notificationMessage;
  		////////////////////////////////////////////////////////////////////////////////
  		$ctx = stream_context_create();
			if(IS_DEVELOPMENT){
				stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck.pem');
			}else{
				stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/dis/ck.pem');
			}
  		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
  		// Open a connection to the APNS server
  		$fp = stream_socket_client(
  			'ssl://gateway.push.apple.com:2195', $err,
  			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

  		if (!$fp)
  			exit("Failed to connect: $err $errstr" . PHP_EOL);

			$aps = array('alert' => $notificationTitle." ".$notificationMessage,'sound' => 'default');
  		$bodyJson = array("aps"=>$aps,"UserId" => $userId,"ThumbUrl" => $thumbUrl,"FileId" => $fileId,"FileType" => $type,"NotificationType" => $notificationType);

  		// Encode the payload as JSON
  		$payload = json_encode($bodyJson);

  		if ($deviceId != null) {
  			foreach ($deviceId as $device) {
  				// Build the binary notification
  				//$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
					$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device)))) . pack('n', strlen($payload)) . $payload;
  				// Send it to the server
  				$result = fwrite($fp, $msg, strlen($msg));
  				if (!$result){
  					echo 'Message not delivered' . PHP_EOL;
  				}else{
  					echo 'Message successfully delivered' . PHP_EOL;
  				}
  			}
				// Close the connection to the server
				fclose($fp);
  		}
  	}

	}
?>
