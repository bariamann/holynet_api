<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiLanguageModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to get all languages in asending order by name from database
      Created By: Nishit Patel
  */
  public function getLanguages(){

			$this->db->select('Id,name');
			$this->db->from('languagemaster');
			$this->db->order_by('name', 'ASC');

			$query = $this->db->get();

			return $this->displayLanguages($query->result());

  }
  /* Method to display Languages data into proper json format
      Created By: Nishit Patel
  */
  public function displayLanguages($result){
  		$arrayObjectClient = null;
  		if($result != null){
  			foreach($result as $row){
  				$arrayObject = null;
  				$language = $row->name;

  				$arrayObject  = array(
  											"Id"=>(int)$row->Id,
  											"Language"=>$language,);

  				$arrayObjectClient[]=$arrayObject;
  			}
  		}
  		return $arrayObjectClient;
  }

  /* Method to check is language already exisist
      Created By: Nishit Patel
  */
  public function checkIsLanguageExsist($name){

    $dataArray = "name=".$this->db->escape($name);

		$this->db->where($dataArray);
	  $query = $this->db->get('languagemaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }

  }

  /* Method to check is language already exsist for update
      Creted By: Nishit Patel
  */
  public function checkIsLanguageExsistforUpdate($Id,$Name){
    $dataArray = "name=".$this->db->escape($Name)." AND Id <> ".$Id;
    $this->db->where($dataArray);
	  $query = $this->db->get('languagemaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
  }


  /* Method to add new language
      Created By: Nishit Patel
  */
  public function addNewLanguage($Name){

    $data = array('name' => $Name);
    $this->db->insert('languagemaster', $data);
    $insert_Id = $this->db->insert_id();

    $languageData = array('Id' => (int)$insert_Id,'Language'=> $Name);
    return $languageData;

  }

  /* Method to delete exsisting language by language Id
      Created By: Nishit Patel
  */
  public function deleteExistsLanguage($Id){

      $this->db->where('Id', $Id);
      $this->db->delete('languagemaster');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }

  }

  /* Method to update exsising Language record
      Created By: Nishit Patel
  */
  public function updateExistsLanguage($Id,$Name){

    $dataArray = array("name"=>$Name);
    $this->db->where('Id', (int)$Id);
		$this->db->update('languagemaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }

  }


}


?>
