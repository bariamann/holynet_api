<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiImageModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}


	/* Method to get Total images for User
		 Created By: Nishit Patel
	*/
	public function getTotalImagesForUser($userId){

    $this->db->select("count(contentmaster.Id) as TotalImages");
    $this->db->from("contentmaster");
    $this->db->join("imagemaster","contentmaster.Id = imagemaster.contentId","inner");
    $this->db->where("contentmaster.userId",$userId);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = (int)$row->TotalImages;
      }
    }
    return $totalValue;

  }




	public function countTotalImage(){
			$this->db->select('Id');
			$this->db->from('imagemaster');
			$query = $this->db->count_all_results();
			return ceil($query);
	}

	public function isImageAvailableForContent($contentId){
			$this->db->select('Id');
			$this->db->from('imagemaster');
			$strWhere = "contentId='".$contentId."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return true;
			}
			return false;
		}

	public function saveImage($Title,$thumbUrl,$fileUrl,$Privacy,$size,$Keyword,$isCommentable,$userId,$sharedIds){

			$this->load->model("ApiContentModel","cmodel");
		    $contentId = $this->cmodel->saveContent($userId);

			$data = array(
				'title' => urldecode($Title),
				'thumbUrl' => $thumbUrl,
				'fileUrl' => $fileUrl,
				'privacy' => $Privacy,
				'size' => $size,
				'keyword' => urldecode($Keyword),
				'isCommentable' => $isCommentable,
				'contentId' => $contentId
				);
			$this->db->insert('imagemaster', $data);
			$fileId = $this->db->insert_id();
			if ($sharedIds != null) {
			    if ($Privacy != 3 &&  $Privacy != -1) {
					$type = 2; // Post
					$this->load->model("ApiShareModel","smodel");
					$result = $this->smodel->share($userId,$fileId,$type,$sharedIds);
					return $fileId;
				}
			}
			return $fileId;

	}

	public function updateImage($Title,$Keyword,$isCommentable,$userId,$imageId,$SharedIds,$privacy){
			// check Access denied
			$this->load->model("ApiContentModel","cmodel");
			$isAccess = $this->cmodel->isAccessResource($imageId,IMAGE_TYPE,$userId);
			if ($isAccess) {
				if ($Title != '-1') {
					$data = array(
						'title' => urldecode($Title)
						);
					$this->db->where('Id', $imageId);
					$this->db->update('imagemaster', $data);
				}
				if ($Keyword != '-1') {
					$data = array(
						'keyword' => urldecode($Keyword)
						);
					$this->db->where('Id', $imageId);
					$this->db->update('imagemaster', $data);
				}
				if ($isCommentable >= 0) {
					$data = array(
						'isCommentable' => $isCommentable
						);
					$this->db->where('Id', $imageId);
					$this->db->update('imagemaster', $data);
				}
				if ($SharedIds != '-1')

				{
					if ($privacy != 3 &&  $privacy != -1)
					{
						$this->load->model("ApiShareModel","smodel");
						$result = $this->smodel->share($userId,$imageId,IMAGE_TYPE,$SharedIds);
						return $result;
					}
				}
				return true;
			}
			return false;

	}


		public function getTotalPageOfImageList($userId){
			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
				imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
				videomaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}

			$this->db->order_by("contentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}

	/* Method to get total pages for get image List
		 Crated By: Nishit Patel
	*/
	public function getTotalPagesForGetImageList($userId){
			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
				imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
				imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}

	public function getImageList($PageIndex,$userId,$lang,$myuserId = 0){
			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
				imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
				imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->where('usermaster.status',0);

			$this->db->order_by("contentmaster.Id","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			//return $this->displayImageList($query->result());
			return $this->displaySearchedImageList($query->result(),$myuserId);
	}

	public function getImageDetails($ImageId,$lang,$userId){
			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
				imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
				imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');

			$this->db->where('imagemaster.Id',$ImageId);
			$this->db->where('usermaster.status',0);

			$query = $this->db->get();
			//return $this->displayImageList($query->result());
			return $this->displaySearchedImageList($query->result(),$userId);
	}

	public function displayImageList($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					$arrayUser = null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0;
						$totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0; $totalWatchedCount=0;
						$type = 2;

						$this->load->model("ApiCommentModel","cmtmodel");
						$totalComments = $this->cmtmodel->getTotalComment($row->ImageId,$type);

						$this->load->model("ApiLikeModel","lkmodel");
						$totalLikes = $this->lkmodel->getTotalLike($row->ImageId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->ImageId,$type);

						$this->load->model("ApiShareModel","shmodel");
						$totalShared = $this->shmodel->getTotalShare($row->ImageId,$type);

						$this->load->model("ApiAbuseModel","abmodel");
						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->ImageId,$type);

						$this->load->model("ApiWatchedModel","wmodel");
						$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->ImageId,$type);

 						$this->load->model("ApiContentModel","cmodel");
						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

						$this->load->model("Utility","utility");
						$size = $this->utility->formatSizeUnits($row->size);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalWatched"=>(int)$totalWatchedCount,
											"TotalWatched"=>(int)$totalWatchedCount
											);

						$arrayObject  = array(
											"ImageId"=>(int)$row->ImageId,
											"title"=>$row->title,
											"thumbUrl"=>IMAGE_THUMB_URL.$row->thumbUrl,
											"fileUrl"=>IMAGE_URL.$row->fileUrl,
											"size"=>$size,
											"privacy"=>(int)$row->privacy,
											"keyword"=>$row->keyword,
											"isCommentable"=>(bool)$row->isCommentable,
											"SubmitedDate"=>$SubmitedDate,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal
											);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}
	/* Method to delete image file from image master and Amazon s3
		 Created By: Manzz Baria
		 Note  : You can not user this function direct,
		 Please use deleteResource function from ApiContentModel class
	*/

	public function deleteImage($imageId){

		$thumbUrl = '';
		$fileUrl = '';

		// get video file and thumbnail path for delete from s3
		$this->db->select("thumbUrl,fileUrl");
    	$this->db->from("imagemaster");
    	$this->db->where('Id', $imageId);
    	$query = $this->db->get();
    	$result = $query->result();
    	if($result != null){
    		foreach ($result as $row) {
    			$thumbUrl = $row->thumbUrl;
    			$fileUrl = $row->fileUrl;
    		}
    	}

    	//Delete from S3
    	$this->load->library('s3');
    	$filePath = DELETE_IMAGE_URL.$fileUrl;
    	$ThumbPath = DELETE_IMAGE_THUMB_URL.$thumbUrl;
    	if ($this->s3->deleteObject(BUCKET, $filePath)) {
        		//echo "Deleted file.";
    	}
    	if ($this->s3->deleteObject(BUCKET, $ThumbPath)) {
        		//echo "Deleted file.";
    	}

    	// delete from video master
		$this->db->where('Id', $imageId);
		$this->db->delete('imagemaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	/* Method to get total page searched image by keyword
		 Created By: Nishit Patel
	*/
	public function getTotalPageSearchImagedByKeyword($userId,$keyword,$lang,$PageIndex){

			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
	contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,usermaster.userName,usermaster.profileImage,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
	(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('likemaster','likemaster.fileId = imagemaster.Id and likemaster.type = '.IMAGE_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = favouritemaster.Id and favouritemaster.type = '.IMAGE_TYPE,'left');

			$whereStr = "imagemaster.title like '%".$keyword."%' OR imagemaster.keyword like '%".$keyword."%'";
			$this->db->where($whereStr);
			$this->db->group_by("imagemaster.Id");
			$this->db->order_by("Total","desc");

			$query = $this->db->get();
			$result = $query->num_rows();
			$totalPages  = $result / 20;
			return round($totalPages);
	}

	/* Method to get searched image by keyword
		 Created By: Nishit Patel
	*/
	public function getSearchImagedByKeyword($userId,$keyword,$lang,$PageIndex){

			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
	contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,usermaster.userName,usermaster.profileImage,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
	(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('likemaster','likemaster.fileId = imagemaster.Id and likemaster.type = '.IMAGE_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = favouritemaster.Id and favouritemaster.type = '.IMAGE_TYPE,'left');

			$whereStr = "usermaster.status=0 AND imagemaster.title like '%".$keyword."%' OR imagemaster.keyword like '%".$keyword."%'";
			$this->db->where($whereStr);
			$this->db->group_by("imagemaster.Id");
			$this->db->order_by("Total","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}

			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displaySearchedImageList($query->result(),$userId);
	}

	public function displaySearchedImageList($result,$userId){
			$arrayObjectClient = null;
			if($result != null){
				$this->load->model("ApiCommentModel","cmtmodel");
				$this->load->model("ApiLikeModel","lkmodel");
				$this->load->model("ApiShareModel","shmodel");
				$this->load->model("ApiAbuseModel","abmodel");
				$this->load->model("ApiWatchedModel","wmodel");
				$this->load->model("ApiContentModel","cmodel");
				$this->load->model("Utility","utility");

				$this->load->model("ApiUserModel","usmodel");

				foreach($result as $row){
					$arrayObject = null;
					$arrayUser = null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0;
						$totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0; $totalWatchedCount=0;
						$type = 2;


						$totalComments = $this->cmtmodel->getTotalComment($row->ImageId,$type);


						$totalLikes = $this->lkmodel->getTotalLike($row->ImageId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->ImageId,$type);


						$totalShared = $this->shmodel->getTotalShare($row->ImageId,$type);


						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->ImageId,$type);


						$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->ImageId,$type);


						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

						$activities = $this->usmodel->getUserActivity($row->ImageId,IMAGE_TYPE,$userId);

						$size = $this->utility->formatSizeUnits($row->size);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalWatched"=>(int)$totalWatchedCount,
											"TotalWatched"=>(int)$totalWatchedCount
											);

						$arrayObject  = array(
											"ImageId"=>(int)$row->ImageId,
											"title"=>$row->title,
											"thumbUrl"=>IMAGE_THUMB_URL.$row->thumbUrl,
											"fileUrl"=>IMAGE_URL.$row->fileUrl,
											"size"=>$size,
											"privacy"=>(int)$row->privacy,
											"keyword"=>$row->keyword,
											"isCommentable"=>(bool)$row->isCommentable,
											"SubmitedDate"=>$SubmitedDate,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal,
											"Activity"=>$activities
											);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		/* Method to get related images list by image id
			 Created By: Nishit Patel
		*/
		public function getRelatedImageList($imageId,$PageIndex){
			$this->db->select('imagemaster.Id,imagemaster.title,imagemaster.keyword,contentmaster.Id,contentmaster.userId');
			$this->db->from('imagemaster');
			$this->db->join('contentmaster','contentmaster.Id = imagemaster.contentId','inner');
			$this->db->where('imagemaster.Id',$imageId);
			$query = $this->db->get();
			$result = $query->result();

			$keyword = "";
			$userId = 0;
			if($result != null){
				foreach ($result as $row) {
					$userId = (int)$row->userId;
					$keyword = $row->title;
				}
			}

			$totalPage = 20;
			$recordCount = $totalPage;
			$resultArray = $this->getRelatedImagesBaseOnUserKeywordType($PageIndex,$userId,$keyword,$imageId,$recordCount);
			return $resultArray;
		}

		/* Method to get video list according playlist
			 Created By: Nishit Patel
		*/
		public function getRelatedImagesBaseOnUserKeywordType($PageIndex,$userId,$keyword,$imageIds,$recordCount){
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$query = $this->db->query("SELECT imagemaster.Id as ImageId,imagemaster.title,imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
			contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,usermaster.userName,usermaster.profileImage
			from imagemaster inner join contentmaster on imagemaster.contentId = contentmaster.Id
			inner join usermaster on contentmaster.userId = usermaster.Id
			where imagemaster.Id <> ".$imageIds." AND (contentmaster.userId=".$userId." OR imagemaster.title like '%".$keyword."%') order by ImageId DESC LIMIT ".$pageNo.",20");
			//return json_encode($query->result());
			return $this->displaySearchedImageList($query->result(),0);
		}


}
?>
