<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiRateModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to get Total Rating by video id
    Created By: Nishit Patel
  */
  public function getTotalRatingForFileId($videoId){
    $this->db->select("count(Id) as TotalCounts,sum(rate) as TotalRating");
    $this->db->from("videoratingmaster");
    $whereStr = "videoId=".$videoId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalCount = (int)$row->TotalCounts;
        $totalRate = (int)$row->TotalRating;
        if($totalRate > 0){
            $totalValue = $totalRate / $totalCount;
        }
      }
    }
    $totalrAtes = round($totalValue,0,PHP_ROUND_HALF_UP);
    return $totalrAtes;
  }

  /* Method to get Video id from rate id
     Created By: Nishit Patel
  */
  public function getVideoIdFromRateId($rateId){
    $this->db->select("videoId");
    $this->db->from("videoratingmaster");
    $whereStr = "Id=".$rateId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $videoId = 0;
    if($result != null){
      foreach ($result as $row) {
        $videoId = (int)$row->videoId;
      }
    }
    return $videoId;
  }



  /* Method to check is exsisting rating available for video & user
    Created By: Nishit Patel
  */
  public function checkForExistingRating($videoId,$userId,$rate){
    $dataArray = "userId=".$userId." AND videoId=".$videoId." AND rate=".$rate;
		$this->db->where($dataArray);
	  $query = $this->db->get('videoratingmaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
  }

  /* Method to add rating to video
    Created By: Nishit patel
  */
  public function addRatingToVideo($videoId,$userId,$rate){
    $this->load->model("Utility","utility");
    $ratingDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array('userId' => $userId,'videoId' => $videoId,'rate' => $rate,'rateDate'=>$ratingDate);
    $this->db->insert('videoratingmaster', $data);
    $insert_Id = $this->db->insert_id();

    $RateDate = $this->utility->timeAgoFormat($ratingDate);
    $RatingData = array('Id' => (int)$insert_Id,'UserId'=> $userId,'VideoId'=> $videoId,'Rate'=> $rate,'Date'=>$RateDate);
    return $RatingData;
  }

  /* Method to update exsising rating to video
      Created By: Nishit Patel
  */
  public function updateExistsRating($rateId,$rate){

    $dataArray = array("rate"=>$rate);
    $this->db->where('Id', (int)$rateId);
		$this->db->update('videoratingmaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }

  }

  /* Method to delete existing rating video
    Created By: Nishit Patel
  */
  public function deleteExistingRate($rateId){
    $this->db->where('Id', $rateId);
    $this->db->delete('videoratingmaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
  }

  /* Method to delete rate according video
     Created By: Nishit Patel
  */
  public function deleteVideoRating($videoId){
    $this->db->where('videoId', $videoId);
    $this->db->delete('videoratingmaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
  }
   public function getTotalPageOfwhoRatedVideo($videoId){
     $this->db->select('videoratingmaster.Id as RateId,videoratingmaster.rate,videoratingmaster.rateDate,videoratingmaster.userId,
        usermaster.userName,usermaster.profileImage');
      $this->db->from('videoratingmaster');
      $this->db->join('usermaster','videoratingmaster.userId = usermaster.Id','inner');
       $this->db->where('videoratingmaster.videoId',$videoId);
      $this->db->order_by("videoratingmaster.Id","desc");

      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
    }


  public function whoRatedVideo($PageIndex,$videoId,$userId){
      $this->db->select('videoratingmaster.Id as RateId,videoratingmaster.rate,videoratingmaster.rateDate,videoratingmaster.userId,
        usermaster.userName,usermaster.profileImage');
      $this->db->from('videoratingmaster');
      $this->db->join('usermaster','videoratingmaster.userId = usermaster.Id','inner');
      $this->db->where('videoratingmaster.videoId',$videoId);
      $this->db->order_by("videoratingmaster.Id","desc");
      $pageNo = "00";
      if ($PageIndex > 0) {
        $PageIndex = $PageIndex * 2;
        $pageNo = $PageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayWhoRatedVideo($query->result(),$userId);
    }

public function displayWhoRatedVideo($result,$userId){
      $arrayObjectClient = null;
      if($result != null){
        foreach($result as $row){
          $arrayObject = null;
          $arrayUser= null;

            $this->load->model("ApiContentModel","cmodel");
            $rateDate = $this->cmodel->timeAgo($row->rateDate);
            $profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
            $this->load->model("ApiUserModel","usmodel");
						$activities = $this->usmodel->getUserActivity($row->userId,USER_TYPE,$userId);
            $arrayUser  = array(
                      "UserId"=>(int)$row->userId,
                      "UserName"=>$row->userName,
                      "ProfileThumbImage"=>$profileUrl
                      );

            $arrayObject  = array(
                      "RateId"=>(int)$row->RateId,
                      "Rate"=>(int)$row->rate,
                      "RateDate"=>$rateDate,
                      "User"=>$arrayUser,
                      "Activity"=>$activities
                      );

          $arrayObjectClient[]=$arrayObject;
        }
      }
      return $arrayObjectClient;
    }



}

?>
