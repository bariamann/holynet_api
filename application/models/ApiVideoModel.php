<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiVideoModel extends CI_Model {

	public function _construct(){
		parent::_construct();
	}

	public function getVideosRateCount($videoId){
			$this->db->select('rate');
			$this->db->from('videoratingmaster');
			$this->db->where('videoId',(int)$videoId);
			$count = $this->db->count_all_results();
			return ceil($count);


	}
	public function getVideoRate($videoId){

		$totalRate = 0;
		$rate = 0;
		$count = 0;
		$count = $this->getVideosRateCount($videoId);
		$this->db->select('rate');
		$this->db->from('videoratingmaster');
		$this->db->where('videoId',(int)$videoId);
		$query = $this->db->get();

		$result = $query->result();
		if($result != null){
			foreach($result as $row)
			{
				$totalRate = $totalRate + (int)$row->rate;
			}
			$rate = $totalRate / $count;
		}

			return ceil($rate);
	}
	public function countTotalVideo(){
			$this->db->select('Id');
			$this->db->from('videomaster');
			$query = $this->db->count_all_results();
			return ceil($query);
	}

	public function getVideoTypes($lang){
			$this->db->select('Id,nameEnglish,nameArabic');
			$this->db->from('videotypemaster');

			if ($lang == 'ar') {
				$this->db->order_by('nameArabic', 'ASC');
			}else{
				$this->db->order_by('nameEnglish', 'ASC');
			}
			$query = $this->db->get();

			return $this->displayVideoTypes($query->result(),$lang);
	}
	public function displayVideoTypes($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					if ($lang == 'ar') {
						$videoType =$row->nameArabic;
					}else{
						$videoType = $row->nameEnglish;
					}
					$arrayObject  = array(
											"Id"=>(int)$row->Id,
											"VideoType"=>$videoType);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}
	public function createVideoType($NameEnglish,$NameArabic){
			$data = array(
				'nameEnglish' => $NameEnglish ,
				'nameArabic' => $NameArabic
				);
			$this->db->insert('videotypemaster', $data);
			return true;

		}

	public function isVideoAvailableForContent($contentId){
			$this->db->select('Id');
			$this->db->from('videomaster');
			$strWhere = "contentId='".$contentId."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return true;
			}
			return false;
		}

	public function deleteVideoType($Id){
				$this->db->where('Id', $Id);
				$this->db->delete('videotypemaster');
			   	return true;

		}

	public function saveVideo($Title,$Description,$thumbUrl,$fileUrl,$Privacy,$size,$Duration,$Keyword,$videoTypeId,$isCommentable,$playListId,$userId,$sharedIds){

			$this->load->model("ApiContentModel","cmodel");
		    $contentId = $this->cmodel->saveContent($userId);

			$data = array(
				'title' => urldecode($Title) ,
				'description' => urldecode($Description),
				'thumbUrl' => $thumbUrl,
				'fileUrl' => $fileUrl,
				'size' => $size,
				'duration' => $Duration,
				'privacy' => $Privacy,
				'keyword' => urldecode($Keyword),
				'videoTypeId' => $videoTypeId,
				'isCommenttable' => $isCommentable,
				'playListId' => $playListId,
				'contentId' => $contentId
				);
			$this->db->insert('videomaster', $data);
			$fileId = $this->db->insert_id();
			if ($sharedIds != null) {
			    if ($Privacy != 3 &&  $Privacy != -1) {
					$type = 1; // Post
					$this->load->model("ApiShareModel","smodel");
					$result = $this->smodel->share($userId,$fileId,$type,$sharedIds);
					//return $fileId;
				}
			}
			return $fileId;

	}

	public function updateVideo($Title,$description,$Keyword,$isCommentable,$videoTypeId,$playListId,$userId,$videoId,$SharedIds,$privacy){
			// check Access denied
			$this->load->model("ApiContentModel","cmodel");
			$isAccess = $this->cmodel->isAccessResource($videoId,VIDEO_TYPE,$userId);
			if ($isAccess) {
				if ($Title != '-1') {
					$data = array(
						'title' => urldecode($Title)
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($description != '-1') {
					$data = array(
						'description' => urldecode($description)
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($Keyword != '-1') {
					$data = array(
						'keyword' => urldecode($Keyword)
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($videoTypeId != -1) {
					$data = array(
						'videoTypeId' => $videoTypeId
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($isCommentable != -1) {
					$data = array(
						'isCommenttable' => $isCommentable
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($playListId != -1) {
					$data = array(
						'playListId' => $playListId
						);
					$this->db->where('Id', $videoId);
					$this->db->update('videomaster', $data);
				}
				if ($SharedIds != '-1')
				{
					if ($privacy != 3 &&  $privacy != -1)
					{
						$this->load->model("ApiShareModel","smodel");
						$result = $this->smodel->share($userId,$videoId,VIDEO_TYPE,$SharedIds);
						return $result;
					}
				}
				return true;
			}
			return false;

	}
	public function getTotalPageOfVideoList($userId){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,playlistmaster.name as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->order_by("contentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}

	/* Method to get total pages for abuse report content
		 Created By: Nishit Patel
	*/
	public function getTotalPagesForAbusedVideoList($contentType){

		$this->db->select('count(abusemaster.Id) as TotalRecords');
		$this->db->from('abusemaster');
		$this->db->join('videomaster','abusemaster.fileId = videomaster.Id','inner');
		$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
		$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','inner');
		$this->db->where('abusemaster.type',$contentType);

		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);

	}

	/* Method to get total pages for related videos
		 Created By: Nishit Patel
	*/
	public function getTotalPageOfRelatedVideoList($videoId){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			if ($videoId != -1) {
				$this->db->where('contentmaster.userId',$videoId);
			}
			$this->db->order_by("contentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}

	/* Method to get related videos
		 Created By: Nishit Patel
	*/
	public function getRelatedVideos($videoId,$PageIndex,$userId){

		$this->db->select('videomaster.Id,videomaster.keyword,videomaster.videoTypeId,contentmaster.userId');
		$this->db->from('videomaster');
		$this->db->join('contentmaster','contentmaster.Id = videomaster.contentId','inner');
		$this->db->where('videomaster.Id',$videoId);
		$query = $this->db->get();
		$result = $query->result();

		$videoTypeId = 0;
		$keyword = "";
		$userId = 0;
		if($result != null){
			foreach ($result as $row) {
				$userId = (int)$row->userId;
				$keyword = $row->keyword;
				$videoTypeId = (int)$row->videoTypeId;
			}
		}

		$totalPage = 20;

		$recordCount = $totalPage;

		$resultArray = $this->getRelatedVideoBaseOnUserKeywordType($PageIndex,$userId,$keyword,$videoTypeId,$videoId,$recordCount);



		$countVideoType = count($resultArray);
		$resultKeywordArray = array();
		if($countVideoType < $totalPage){

			$recordCount = $totalPage - $countVideoType;

			$videoIds = array();
			foreach ($resultArray as $row) {
				//array_push($videoIds,(int)$row->VideoId);
				$videoIds[] = (int)$row->VideoId;
			}
			$videoIds[] = $videoId;
			$videoIdsStr = implode(", ",$videoIds);
			$resultKeywordArray = $this->getRelatedVideoBaseOnUserKeywordType($PageIndex,$userId,$keyword,0,$videoIdsStr,$recordCount);

		}

		$combineArray = array_merge($resultArray,$resultKeywordArray);

		$count = count($combineArray);
		$resultVideos = array();
		if($count < $totalPage){
			$recordCount = $totalPage - $count;
			$videoIds = array();
			foreach ($combineArray as $row) {
				//array_push($videoIds,(int)$row->VideoId);
				$videoIds[] = (int)$row->VideoId;
			}
			$videoIds[] = $videoId;
			$videoIdsStr = implode(", ",$videoIds);
			$resultVideos = $this->getRelatedVideoBaseOnUserKeywordType($PageIndex,0,$keyword,0,$videoIdsStr,$recordCount);
		}

		$combineResultArray = array_merge($combineArray,$resultVideos);


		$lang = "en";
		//return $this->displayVideoList($combineResultArray,$lang);
		return $this->displaySearchedVideoList($combineResultArray,$lang,$userId);

	}

	/* Method to get video list according playlist
		 Created By: Nishit Patel
	*/
	public function getRelatedVideoBaseOnUserKeywordType($PageIndex,$userId,$keyword,$videoType,$videoIds,$recordCount){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,IFNULL(videotypemaster.nameEnglish,"") as nameEnglish,IFNULL(videotypemaster.nameArabic,"") as nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,IFNULL(playlistmaster.name,"") as playListName,(select count(likemaster.Id) from likemaster where fileId = videomaster.Id AND type = 1) as totalLikes');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');

			$whereStr = "";

			//Condition without video type
			if($videoType == 0){
				$whereStr = "contentmaster.userId=".$userId." AND videomaster.keyword like '%".$keyword."%' AND (usermaster.status = 0 AND videomaster.Id NOT IN (".$videoIds."))";
			}
			//Condition without user
			if($userId == 0){
				$whereStr = "videomaster.keyword like '%".$keyword."%' AND (usermaster.status = 0 AND videomaster.Id NOT IN (".$videoIds."))";
			}
			//Condition with all value
			if($videoType > 0 && $userId > 0){
					$whereStr = "usermaster.status=0 AND videomaster.videoTypeId=".$videoType." AND contentmaster.userId=".$userId." AND videomaster.keyword like '%".$keyword."%' AND videomaster.Id NOT IN (".$videoIds.")";
			}

			$this->db->where($whereStr);
			$this->db->order_by("totalLikes","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit($recordCount,$pageNo);
			$query = $this->db->get();
			$lang = "en";
			$result = $query->result();
			return $result; //$this->displayVideoList($query->result(),$lang);
	}
	/* Method to get total page of video according playlist
		 Created By: Nishit Patel
	*/
	public function getTotalPageOfVideoListForPlaylist($playlistId){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,playlistmaster.name as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			if ($playlistId != -1) {
				$this->db->where('videomaster.playListId',$playlistId);
			}
			$this->db->order_by("videomaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}

	/* Method to get video list according playlist
		 Created By: Nishit Patel
	*/
	public function getVideoListForPlaylist($PageIndex,$playlistId,$lang,$userId){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,IFNULL(playlistmaster.name,"") as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			if ($playlistId != -1) {
				$this->db->where('videomaster.playListId',$playlistId);
			}
			$this->db->order_by("videomaster.Id","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			//return $this->displayVideoList($query->result(),$lang);
			return $this->displaySearchedVideoList($query->result(),$lang,$userId);
	}

	/* Method to get total video pages for get video List
		 Created By: Nishit Patel
	*/
	public function getTotalPagesForGetVideoList($userId){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,playlistmaster.name as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
	}
	public function getVideoList($PageIndex,$userId,$lang,$myuserId = 0){
			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,IFNULL(videotypemaster.nameEnglish,"") as nameEnglish,IFNULL(videotypemaster.nameArabic,"") as nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,IFNULL(playlistmaster.name,"") as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->where('usermaster.status',0);

			$this->db->order_by("contentmaster.Id","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			//return $this->displayVideoList($query->result(),$lang);
			return $this->displaySearchedVideoList($query->result(),$lang,$myuserId);
	}

	public function displayVideoList($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					$arrayVideotype = null;
					$arrayPlayList = null;
					$arrayUser = null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0;
						$totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0; $totalRate = 0;
						$totalWatchedCount = 0;
						$type = 1;

						$totalRate = $this->getVideoRate($row->VideoId);

						$this->load->model("ApiCommentModel","cmtmodel");
						$totalComments = $this->cmtmodel->getTotalComment($row->VideoId,$type);

						$this->load->model("ApiLikeModel","lkmodel");
						$totalLikes = $this->lkmodel->getTotalLike($row->VideoId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->VideoId,$type);

						$this->load->model("ApiShareModel","shmodel");
						$totalShared = $this->shmodel->getTotalShare($row->VideoId,$type);

						$this->load->model("ApiAbuseModel","abmodel");
						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->VideoId,$type);

						$this->load->model("ApiWatchedModel","wmodel");
						$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->VideoId,$type);

 						$this->load->model("ApiContentModel","cmodel");
						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

						$this->load->model("Utility","utility");
						$size = $this->utility->formatSizeUnits($row->size);

						$videoTypeName = "";
						if ($lang == 'ar') {
							$videoTypeName = $row->nameArabic;
						}else{
							$videoTypeName = $row->nameEnglish;
						}

						$arrayVideotype  = array(
											"videoTypeId"=>(int)$row->videoTypeId,
											"videoTypeName"=>$videoTypeName
											);
						$arrayPlayList  = array(
											"playListId"=>(int)$row->playListId,
											"playListName"=>$row->playListName
											);
											$profileUrl = "";
											$profileImage = $row->profileImage;
											if($profileImage != null){
												$profileUrl = PROFILE_URL.$profileImage;
											}
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalRate"=>(int)$totalRate,
											"TotalWatched"=>(int)$totalWatchedCount
											);

						$arrayObject  = array(
											"VideoId"=>(int)$row->VideoId,
											"title"=>$row->title,
											"Description"=>$row->description,
											"thumbUrl"=>VIDEO_THUMB_URL.$row->thumbUrl,
											"fileUrl"=>VIDEO_URL.$row->fileUrl,
											"size"=>$size,
											"duration"=>$row->duration,
											"privacy"=>(int)$row->privacy,
											"keyword"=>$row->keyword,
											"isCommentable"=>(bool)$row->isCommenttable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"VideoType"=>$arrayVideotype,
											"Translation"=>$this->getTranslation((int)$row->VideoId),
											"PlayList"=>$arrayPlayList,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal
											);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

	/* Method to get translation by Id
		 Created By: Nishit Patel
	*/
	public function getTranslation($fileId){
		$this->db->select("translationmaster.Id,translationmaster.type,translationmaster.fileId,translationmaster.title,translationmaster.description,translationmaster.languageId,languagemaster.name");
		$this->db->from("translationmaster");
		$this->db->join("languagemaster","translationmaster.languageId = languagemaster.Id","inner");
		$this->db->where("translationmaster.fileId",$fileId);
		$this->db->order_by("translationmaster.Id","DESC");
		$query = $this->db->get();
		$result = $query->result();
		return $this->displayTranslation($result);
	}
	/* Method to display Translation
		 Created By: Nishit Patel
	*/
	public function displayTranslation($result){
		$translationlist = null;
		if($result != null){
			foreach ($result as $row) {
				$translation = array('Id'=>(int)$row->Id,'title'=>$row->title,'Description'=>$row->description,'languageId'=>(int)$row->languageId,'language'=>$row->name);
				$translationlist[] = $translation;
			}
		}
		return $translationlist;
	}

	public function getVideoDetails($videoId,$lang,$userId){

		$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,IFNULL(videotypemaster.nameEnglish,"") as nameEnglish,IFNULL(videotypemaster.nameArabic,"") as nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,IFNULL(playlistmaster.name,"") as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
			$whereStr = "videomaster.Id=".$videoId." AND usermaster.status=0";
			$this->db->where($whereStr);
			$query = $this->db->get();
			//return $this->displayVideoList($query->result(),$lang);
			return $this->displaySearchedVideoList($query->result(),$lang,$userId);
	}

	/* Method to delete Video
			Created By: Nishit Patel
	*/
	public function deleteVideosByPlayList($playlistId){

		//Get Video list for this playlist
		//SELECT Id FROM  where playListId=2
		$this->db->select("Id");
		$this->db->from("videomaster");
		$this->db->where("playListId",$playlistId);
		$query = $this->db->get();
		$result = $query->result();
		if($result != null){
			foreach ($result as $row) {

				$videoId = $row->Id;

				//Delete Abuse Report
				$this->load->model("ApiAbuseModel","abuse");
				$this->abuse->deleteAbouseReportByFile($videoId,VIDEO_TYPE);

				//Delete Comments
				$this->load->model("ApiCommentModel","comment");
				$this->comment->deleteCommentsByFileId($videoId,VIDEO_TYPE);

				//Delete Favourites
				$this->load->model("ApiFavouriteModel","favourite");
				$this->favourite->deleteFavouritesForFileId($videoId,VIDEO_TYPE);

				//Delete Likes/Dislikes
				$this->load->model("ApiLikeModel","likes");
				$this->likes->deleteLikesDislikesForFileId($videoId,VIDEO_TYPE);

				//Delete Likes/Dislikes
				$this->load->model("ApiRateModel","rate");
				$this->rate->deleteVideoRating($videoId);

			}
		}

		$this->db->where('playListId', $playlistId);
		$this->db->delete('videomaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}

	/* Method to delete video file from video master and Amazon s3
		 Created By: Manzz Baria
		 Note  : You can not user this function direct,
		 Please use deleteResource function from ApiContentModel class
	*/

	public function deleteVideo($videoId){

		$thumbUrl = '';
		$fileUrl = '';

		// get video file and thumbnail path for delete from s3
		$this->db->select("thumbUrl,fileUrl");
    	$this->db->from("videomaster");
    	$this->db->where('Id', $videoId);
    	$query = $this->db->get();
    	$result = $query->result();
    	if($result != null){
    		foreach ($result as $row) {
    			$thumbUrl = $row->thumbUrl;
    			$fileUrl = $row->fileUrl;
    		}
    	}

    	//Delete from S3
    	$this->load->library('s3');
    	$filePath = DELETE_VIDEO_URL.$fileUrl;
    	$ThumbPath = DELETE_VIDEO_THUMB_URL.$thumbUrl;
    	if ($this->s3->deleteObject(BUCKET, $filePath)) {
        		//echo "Deleted file.";
    	}
    	if ($this->s3->deleteObject(BUCKET, $ThumbPath)) {
        		//echo "Deleted file.";
    	}

    	// delete from video master
		$this->db->where('Id', $videoId);
		$this->db->delete('videomaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}


	/* Method to get Total Videos for UserId
		 Created By: Nishit Patel
	*/
	public function getTotalVideosByUserId($userId){
		$this->db->select("count(contentmaster.Id) as TotalVideos");
    $this->db->from("videomaster");
		$this->db->join("contentmaster","videomaster.contentId = contentmaster.Id","inner");
    $whereStr = "contentmaster.userId=".$userId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->TotalVideos;
      }
    }
    return $totalValue;
	}

	/* Method to get Total Videos for UserId
		 Created By: Nishit Patel
	*/
	public function getTotalVideosByPlayListId($playListId){
		$this->db->select("count(Id) as TotalVideos");
    $this->db->from("videomaster");
    $whereStr = "playListId=".$playListId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->TotalVideos;
      }
    }
    return $totalValue;
	}

	/* Method to get Singal latest Video for Play list
		 Created By: Nishit Patel
	*/
	public function getLatestVideoFromPlayList($playListId){
		//SELECT  FROM  where playListId=2 order by Id DESC limit 0,1
		$this->db->select("Id,title,thumbUrl,fileUrl");
		$this->db->from("videomaster");
		$this->db->where("playListId",$playListId);
		$this->db->order_by("Id","DESC");
		$this->db->limit(1,0);
		$query = $this->db->get();
		$result = $query->result();
		$videoObject = null;

		if($result != null){
			foreach ($result as $row) {
				$videoObject = array("VideoId"=>(int)$row->Id,"title"=>$row->title,"thumbUrl"=>VIDEO_THUMB_URL.$row->thumbUrl,"fileUrl"=>VIDEO_URL.$row->fileUrl);
			}
		}
		return $videoObject;
	}

	/* Method get Total Videos of User
    Created By: Nishit Patel
  */
  public function getTotalVideosOfUser($userId){

    $this->db->select("count(contentmaster.Id) as TotalVideos");
    $this->db->from("contentmaster");
    $this->db->join("videomaster","contentmaster.Id = videomaster.contentId","inner");
    $this->db->where("contentmaster.userId",$userId);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = (int)$row->TotalVideos;
      }
    }
    return $totalValue;
  }


	/* Method to get total page for video by search keyword
		 Created By: Nishit Patel
	*/
	public function getTotalPagesVideoBySearchKeyword($keyword,$lang,$PageIndex,$userId){
		$this->db->select('videomaster.Id as VideoId,videomaster.title as title,videomaster.description as description,videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
			videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
			contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,usermaster.userName,usermaster.profileImage,playlistmaster.Id as playListId,playlistmaster.name as playListName,
			COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Totals');
		$this->db->from('videomaster');
		$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
		$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
		$this->db->join('likemaster','videomaster.Id = likemaster.fileId and likemaster.type = '.VIDEO_TYPE,'left');
		$this->db->join('favouritemaster','videomaster.Id = favouritemaster.fileId and favouritemaster.type='.VIDEO_TYPE,'left');


		$whereStr = "title like '%".$keyword."%' OR description like '%".$keyword."%' OR videomaster.keyword like '%".$keyword."%'";
		$typeKeyword = "";
		if($lang == "en"){
			$typeKeyword = " OR videotypemaster.nameEnglish like '%".$keyword."%'";
		}else{
			$typeKeyword = " OR videotypemaster.nameArabic like '%".$keyword."%'";
		}
		$whereStr .= $typeKeyword;
		$this->db->where($whereStr);
		$this->db->group_by("videomaster.Id");
		$this->db->order_by("Totals","desc");

		// $query = $this->db->count_all_results();
		// $result  = $query / 20;
		// return ceil($result);

		$query = $this->db->get();
		$result = $query->num_rows();
		$totalPages  = $result / 20;
		return round($totalPages);

	}

	/* Method to get video by search keyword
		 Created By: Nishit Patel
	*/
	public function getVideoBySearchKeyword($keyword,$lang,$PageIndex,$userId){
		$this->db->select('videomaster.Id as VideoId,videomaster.title as title,videomaster.description as description,videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
			videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,videomaster.videoTypeId,IFNULL(videotypemaster.nameEnglish,"") as nameEnglish,IFNULL(videotypemaster.nameArabic,"") as nameArabic,
			contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,usermaster.userName,usermaster.profileImage,playlistmaster.Id as playListId,IFNULL(playlistmaster.name,"") as playListName,
			COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Totals');
		$this->db->from('videomaster');
		$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
		$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','left');
		$this->db->join('likemaster','videomaster.Id = likemaster.fileId and likemaster.type = '.VIDEO_TYPE,'left');
		$this->db->join('favouritemaster','videomaster.Id = favouritemaster.fileId and favouritemaster.type='.VIDEO_TYPE,'left');


		$whereStr = "usermaster.status=0 AND title like '%".$keyword."%' OR description like '%".$keyword."%' OR videomaster.keyword like '%".$keyword."%'";
		$typeKeyword = "";
		if($lang == "en"){
			$typeKeyword = " OR videotypemaster.nameEnglish like '%".$keyword."%'";
		}else{
			$typeKeyword = " OR videotypemaster.nameArabic like '%".$keyword."%'";
		}
		$whereStr .= $typeKeyword;
		$this->db->where($whereStr);
		$this->db->group_by("videomaster.Id");
		$this->db->order_by("Totals","desc");

		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		$resultContent = null;
		return $this->displaySearchedVideoList($query->result(),$lang,$userId);
	}
	/* Method to display search video list
		 Created By: Nishit Patel
	*/
	public function displaySearchedVideoList($result,$lang,$userId){
			$arrayObjectClient = null;
			if($result != null){
				$this->load->model("ApiCommentModel","cmtmodel");
				$this->load->model("ApiLikeModel","lkmodel");
				$this->load->model("ApiShareModel","shmodel");
				$this->load->model("ApiAbuseModel","abmodel");
				$this->load->model("ApiWatchedModel","wmodel");
				$this->load->model("ApiContentModel","cmodel");
				$this->load->model("Utility","utility");

				$this->load->model("ApiUserModel","usmodel");
				foreach($result as $row){
					$arrayObject = null;
					$arrayVideotype = null;
					$arrayPlayList = null;
					$arrayUser = null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0;
						$totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0; $totalRate = 0;
						$totalWatchedCount = 0;
						$type = 1;

						$totalRate = $this->getVideoRate($row->VideoId);


						$totalComments = $this->cmtmodel->getTotalComment($row->VideoId,$type);


						$totalLikes = $this->lkmodel->getTotalLike($row->VideoId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->VideoId,$type);


						$totalShared = $this->shmodel->getTotalShare($row->VideoId,$type);


						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->VideoId,$type);


						$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->VideoId,$type);


						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);


						$size = $this->utility->formatSizeUnits($row->size);

						$activities = $this->usmodel->getUserActivity($row->VideoId,VIDEO_TYPE,$userId);

						$videoTypeName = "";
						if ($lang == 'ar') {
							$videoTypeName = $row->nameArabic;
						}else{
							$videoTypeName = $row->nameEnglish;
						}

						$arrayVideotype  = array(
											"videoTypeId"=>(int)$row->videoTypeId,
											"videoTypeName"=>$videoTypeName
											);
						$arrayPlayList  = array(
											"playListId"=>(int)$row->playListId,
											"playListName"=>$row->playListName
											);
						$profileUrl = "";
						$profileImage = $row->profileImage;
						if($profileImage != null){
							$profileUrl = PROFILE_URL.$profileImage;
						}
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalRate"=>(int)$totalRate,
											"TotalWatched"=>(int)$totalWatchedCount
											);

						$arrayObject  = array(
											"VideoId"=>(int)$row->VideoId,
											"title"=>$row->title,
											"Description"=>$row->description,
											"thumbUrl"=>VIDEO_THUMB_URL.$row->thumbUrl,
											"fileUrl"=>VIDEO_URL.$row->fileUrl,
											"size"=>$size,
											"duration"=>$row->duration,
											"privacy"=>(int)$row->privacy,
											"keyword"=>$row->keyword,
											"isCommentable"=>(bool)$row->isCommenttable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"VideoType"=>$arrayVideotype,
											"Translation"=>$this->getTranslation((int)$row->VideoId),
											"PlayList"=>$arrayPlayList,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal,
											"Activity"=>$activities
											);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}


}
?>
