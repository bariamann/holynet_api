<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiContentModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

	public function timeAgo($time_ago)
	{
		    $time_ago = strtotime($time_ago);
		    $cur_time   = time();

		    $dt = new DateTime();
			$dt->format("r");
			$tz = new DateTimeZone("UTC");
			$dt->setTimezone($tz);
			$d = $dt->format("Y-m-d H:i:s");

			$cur_time = strtotime($d);

		    $time_elapsed   = $cur_time - $time_ago;
		    $seconds    = $time_elapsed ;
		    $minutes    = round($time_elapsed / 60 );
		    $hours      = round($time_elapsed / 3600);
		    $days       = round($time_elapsed / 86400 );
		    $weeks      = round($time_elapsed / 604800);
		    $months     = round($time_elapsed / 2600640 );
		    $years      = round($time_elapsed / 31207680 );
		    // Seconds
		    if($seconds <= 60){
		        return "just now";
		    }
		    //Minutes
		    else if($minutes <=60){
		        if($minutes==1){
		            return "one minute ago";
		        }
		        else{
		            return "$minutes minutes ago";
		        }
		    }
		    //Hours
		    else if($hours <=24){
		        if($hours==1){
		            return "an hour ago";
		        }else{
		            return "$hours hrs ago";
		        }
		    }
		    //Days
		    else if($days <= 7){
		        if($days==1){
		            return "yesterday";
		        }else{
		            return "$days days ago";
		        }
		    }
		    //Weeks
		    else if($weeks <= 4.3){
		        if($weeks==1){
		            return "a week ago";
		        }else{
		            return "$weeks weeks ago";
		        }
		    }
		    //Months
		    else if($months <=12){
		        if($months==1){
		            return "a month ago";
		        }else{
		            return "$months months ago";
		        }
		    }
		    //Years
		    else{
		        if($years==1){
		            return "one year ago";
		        }else{
		            return "$years years ago";
		        }
		    }
	}

	public function getWall($PageIndex,$userId,$lang){
			$this->db->select('Id,submitedDate');
			$this->db->from('contentmaster');
			$strWhere = "userId='".$userId."'";
			$this->db->where($strWhere,null,false);
			$this->db->order_by("Id","desc");
			$query = $this->db->get();

	}


	public function addPost($description,$privacy,$isCommentable,$userId,$sharedIds){
			$data = array(
				'userId' => $userId
				);
			$this->db->insert('contentmaster', $data);
			$contentId = $this->db->insert_id();
		    $this->load->model("ApiPostModel","dmodel");
		    $result = $this->dmodel->savePost($description,$privacy,$isCommentable,$userId,$sharedIds,$contentId);
			//$this->dmodel->savePost($description,$privacy,$isCommentable,$userId,$sharedIds,$contentId);
			return $result;
	}

	public function saveContent($userId){
			$data = array(
				'userId' => $userId
				);
			$this->db->insert('contentmaster', $data);
			$contentId = $this->db->insert_id();
			return $contentId;
	}

	public function isAccessContent($contentId,$userId){
			if ($userId == -1) {
				return true;
			}
			$this->db->select('Id');
			$this->db->from('contentmaster');
			$strWhere = "Id='".$contentId."' AND userId='".$userId."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return true;
			}
			return false;
		}

		public function getContentIdByFile($ResourceId,$Type){
			$contentId = 0;
			/* this is also working
			$this->db->select('contentmaster.Id');
			$this->db->from('contentmaster');
			if ($Type == VIDEO_TYPE) {
					$this->db->join('videomaster','contentmaster.Id = videomaster.contentId','inner');
					$this->db->where('videomaster.Id',$ResourceId);
			}
			if ($Type == IMAGE_TYPE) {
					$this->db->join('imagemaster','contentmaster.Id = imagemaster.contentId','inner');
					$this->db->where('imagemaster.Id',$ResourceId);
			}
			if ($Type == POST_TYPE) {
					$this->db->join('postmaster','contentmaster.Id = postmaster.contentId','inner');
					$this->db->where('postmaster.Id',$ResourceId);
			}
			*/
			if ($Type == VIDEO_TYPE) {
				$this->db->select('contentId');
				$this->db->from('videomaster');
			}
			if ($Type == IMAGE_TYPE) {
				$this->db->select('contentId');
				$this->db->from('imagemaster');
			}
			if ($Type == POST_TYPE) {
				$this->db->select('contentId');
				$this->db->from('postmaster');
			}

			$strWhere = "Id='".$ResourceId."'";
			$this->db->where($strWhere,null,false);

			$query = $this->db->get();
	    	$result = $query->result();
	    	if($result != null){
	    		foreach ($result as $row) {
	    			$contentId = $row->contentId;
	    		}
    		}
    		return $contentId;
		}
	public function isMyContent($fileId,$type,$myUserId){
			$contentId = $this->getContentIdByFile($fileId,$type);
			$this->db->select('userId');
			$this->db->from('contentmaster');
			$strWhere = "Id='".$contentId."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			$result = $query->result();
			$userId = 0;
			if($result != null){
				foreach ($result as $row) {
					$userId = $row->userId;
				}
			}
			if ($userId == $myUserId) {
				return true;
			}
			return false;
		}

		public function isAccessResource($ResourceId,$Type,$UserId){
			if ($UserId == -1) {
				return true;
			}
			$this->db->select('contentmaster.Id');
			$this->db->from('contentmaster');
			if ($Type == VIDEO_TYPE) {
					$this->db->join('videomaster','contentmaster.Id = videomaster.contentId','inner');
					$this->db->where('videomaster.Id',$ResourceId);
			}
			if ($Type == IMAGE_TYPE) {
					$this->db->join('imagemaster','contentmaster.Id = imagemaster.contentId','inner');
					$this->db->where('imagemaster.Id',$ResourceId);
			}
			if ($Type == POST_TYPE) {
					$this->db->join('postmaster','contentmaster.Id = postmaster.contentId','inner');
					$this->db->where('postmaster.Id',$ResourceId);
			}

			$this->db->where('contentmaster.userId',$UserId);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return true;
			}
			return false;
		}

		public function deleteResource($ResourceId,$Type,$UserId){
			$isAccess = $this->isAccessResource($ResourceId,$Type,$UserId);
			if ($isAccess) {

				//Get content id of file
				$contentId = 0;
				$contentId = $this->getContentIdByFile($ResourceId,$Type);

				//delete comments
				$this->load->model("ApiCommentModel","cmtmodel");
				$this->cmtmodel->deleteCommentsByFileId($ResourceId,$Type);

				//delete likes / dislikes
				$this->load->model("ApiLikeModel","lkmodel");
				$this->lkmodel->deleteLikesDislikesForFileId($ResourceId,$Type);

				//delete abuse
				$this->load->model("ApiAbuseModel","abmodel");
				$this->abmodel->deleteAbouseReportByFile($ResourceId,$Type);

				//Delete Favourites
				$this->load->model("ApiFavouriteModel","favourite");
				$this->favourite->deleteFavouritesForFileId($ResourceId,$Type);

				//Delete Share
				$this->load->model("ApiShareModel","share");
				$this->share->deleteShare($ResourceId,$Type);

				//Delete Translation
				$this->load->model("ApiTranslateModel","transmodel");
				$this->transmodel->deleteTranslationByFile($ResourceId,$Type);


				if ($Type == VIDEO_TYPE) {

					//Delete rates
					$this->load->model("ApiRateModel","rate");
					$this->rate->deleteVideoRating($ResourceId);

					//Delete watched
					$this->load->model("ApiWatchedModel","watched");
					$this->watched->deleteWatchedByFile($ResourceId,$Type);

					//Delete video
					$this->load->model("ApiVideoModel","vmodel");
					$this->vmodel->deleteVideo($ResourceId);


				}
				if ($Type == IMAGE_TYPE) {

					//Delete Image
					$this->load->model("ApiImageModel","imagemodel");
					$this->imagemodel->deleteImage($ResourceId);
				}

				if ($Type == POST_TYPE) {
					//Delete Post
					$this->load->model("ApiPostModel","postmodel");
					$this->postmodel->deletePost($ResourceId);
				}

				$this->db->where('Id', $contentId);
				$this->db->delete('contentmaster');
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}



			}
		}

	}
?>
