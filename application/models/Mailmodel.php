<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailmodel extends CI_Model 
{
	public function checkadmin($user,$password)
	{
		$this->db->select('*');
		$this->db->from('usermaster');
		$this->db->where('userName',$user);
		$this->db->where('password',$password);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			$result=$query->result();
			$id=$result[0]->userId;
			$data=array('loginDate'=>date("Y-m-d  H:i:s"));
			$this->db->where('userName',$user);
			$this->db->where('password',$password);
			$this->db->update('usermaster',$data);
			$data=array('flag'=>1,'id'=>$id);
			return $data;
		}
		else
		{
			$data=array('flag'=>0);
			return $data;
		}
		
	}
	public function chngePwd($oldPwd,$newPwd,$id)
	{
		$this->db->where('userId',$id);
		$this->db->where('password',$oldPwd);
		$data=$this->db->get('usermaster');
		if($data->num_rows()>0)
		{
			$this->db->where('userId',$id);
			$this->db->set('password',$newPwd);
			$this->db->update('usermaster');
			return TRUE;
		}		
		else
		{
			return FALSE;
		}
	}
	public function chk_email($email)
	{
		$this->db->where('email',$email);
		$data=$this->db->get('usermaster');
		if($data->num_rows()>0)
		{
			$result=$data->result();
			$password=$result[0]->password;
			$data_value=array('password'=>$password,'flag'=>1);
			return $data_value;
		}
		else 
		{
			$data_value=array('flag'=>0);
			return $data_value;
		}
	}
}








