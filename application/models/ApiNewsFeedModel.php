<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiNewsFeedModel extends CI_Model {

	public function _construct(){
		parent::_construct();
	}

	/* Method to get total pages for most popular tubes according likes
	Created By: Nishit Patel
	*/
	public function getTotalPagesPopularNewsfeeds($userId){
		//SELECT fileId,type,count(Id) as TotalLikes FROM  where status=1 group by fileId,type
		$this->db->select("Id");
		$this->db->from("likemaster");
		$this->db->where("status",LIKE);
		$this->db->group_by("fileId","type");
		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);
	}

	/* Method to get most popular tubes according likes
	Created By: Nishit Patel
	*/
	public function getPopularNewsfeeds($userId,$lang,$PageIndex){
		$this->db->select("fileId,type,IFNULL(count(Id),0) as TotalLikes");
		$this->db->from("likemaster");
		$this->db->where("status",LIKE);
		$this->db->group_by("fileId","type");
		$this->db->order_by("TotalLikes","DESC");
		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		//return $query->result();
		return $this->displayPopularNewsFeeds($query->result(),$lang,$userId);
	}

	/* Method to display popular tubes
	Created By: Nishit Patel
	*/
	private function displayPopularNewsFeeds($result,$lang,$userId){

		$this->load->model("ApiVideoModel","vmodel");
		$this->load->model("ApiPostModel","pmodel");
		$this->load->model("ApiImageModel","imodel");
		$this->load->model("ApiFavouriteModel","fvmodel");

		$this->load->model("ApiUserModel","usmodel");

		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
				$arrayObject = null;

				$fileId = (int)$row->fileId;
				$type = (int)$row->type;
				$favrouiteTubes = null;
				if($type == VIDEO_TYPE){
					$lang = "en";
					$favrouiteTubes = $this->vmodel->getVideoDetails($fileId,$lang);
				}
				if($type == POST_TYPE){
					$favrouiteTubes = $this->pmodel->getPostDetails($fileId,1);
				}
				if($type == IMAGE_TYPE){
					$lang = "en";
					$favrouiteTubes = $this->imodel->getImageDetails($fileId,$lang);
				}
				if($type == PLAYLIST_TYPE){
					$favrouiteTubes = $this->fvmodel->getFavouritePlayListByPlaylistId($fileId);
				}

				$activities = $this->usmodel->getUserActivity($fileId,$type,$userId);

				$arrayObject  = array(
					"Likes"=>(int)$row->TotalLikes,
					"Type"=>$type,
					"Tubes" => $favrouiteTubes,
					"Activity"=>$activities);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		/* Method to get total page for popular Content
		Created By: Nishit Patel
		*/
		public function getTotalPagePopularContents($userId){

			$query = $this->db->query('SELECT videomaster.Id as fileId,1 as type,count(likemaster.Id) as totalLikes FROM videomaster left join likemaster on videomaster.Id = likemaster.fileId and likemaster.type=1 group by videomaster.Id
			UNION
			SELECT imagemaster.Id as fileId,2 as type,count(likemaster.Id) as totalLikes FROM imagemaster left join likemaster on imagemaster.Id = likemaster.fileId and likemaster.type=2 group by imagemaster.Id
			UNION
			SELECT postmaster.Id as fileId,3 as type,count(likemaster.Id) as totalLikes FROM postmaster left join likemaster on postmaster.Id = likemaster.fileId and likemaster.type=3 group by postmaster.Id
			Order by totalLikes DESC');

			$count = count($query->result());
			$result  = $count / 20;
			return ceil($result);

		}

		public function getTotalPageFavouritesNewsFeedForUser($userId){
			$query = $this->db->query('
			SELECT videomaster.playListId as Id,5 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join favouritemaster on videomaster.playListId = favouritemaster.fileId and favouritemaster.type=5 inner join contentmaster on contentmaster.Id = videomaster.contentId where contentmaster.userId = '.$userId.' group by videomaster.Id
			UNION
			SELECT videomaster.Id,1 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join contentmaster on contentmaster.Id = videomaster.contentId inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where videomaster.playListId = 0 AND favouritemaster.userId = '.$userId.' group by videomaster.Id
			UNION
			SELECT imagemaster.Id,2 as type,imagemaster.title,contentmaster.submitedDate,contentmaster.userId from imagemaster inner join contentmaster on imagemaster.contentId = contentmaster.Id inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where contentmaster.userId = '.$userId.' GROUP BY imagemaster.Id
			UNION
			SELECT postmaster.Id,3 as type,postmaster.decription as title,contentmaster.submitedDate,contentmaster.userId from postmaster inner join contentmaster on postmaster.contentId = contentmaster.Id inner join favouritemaster on favouritemaster.fileId = contentmaster.userId and favouritemaster.type=4 where contentmaster.userId = '.$userId.' GROUP BY postmaster.Id
			order by submitedDate DESC');

			$count = count($query->result());
			$result  = $count / 20;
			return ceil($result);
		}

		/* Method to get popular Content
		Created By: Nishit Patel
		*/
		public function getPopularContents($userId,$lang,$PageIndex){

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}

			$query = $this->db->query('select videomaster.Id as fileId,1 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = videomaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id)  + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = videomaster.Id and favouritemaster.type = 1)) as Totals FROM videomaster left join likemaster on videomaster.Id = likemaster.fileId and likemaster.type=1 group by videomaster.Id
			UNION
			SELECT imagemaster.Id as fileId,2 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = imagemaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id) + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = imagemaster.Id and favouritemaster.type = 1)) as Totals FROM imagemaster left join likemaster on imagemaster.Id = likemaster.fileId and likemaster.type=2 group by imagemaster.Id
			UNION
			SELECT postmaster.Id as fileId,3 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = postmaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id) + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = postmaster.Id and favouritemaster.type = 1)) as Totals FROM postmaster left join likemaster on postmaster.Id = likemaster.fileId and likemaster.type=3 group by postmaster.Id
			Order by Totals DESC LIMIT '.$pageNo.',20');
			return $this->displayPopularContents($query->result(),$lang,$userId);

		}

		/* Method to display popular tubes
		Created By: Nishit Patel
		*/
		private function displayPopularContents($result,$lang,$userId){
			$this->load->model("ApiVideoModel","vmodel");
			$this->load->model("ApiPostModel","pmodel");
			$this->load->model("ApiImageModel","imodel");
			$this->load->model("ApiFavouriteModel","fvmodel");

			$this->load->model("ApiUserModel","usmodel");

			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

					$fileId = (int)$row->fileId;
					$type = (int)$row->type;

					$favrouiteTubes = null;
					if($type == VIDEO_TYPE){

						$lang = "en";
						$favrouiteTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
					}
					if($type == POST_TYPE){

						$favrouiteTubes = $this->pmodel->getPostDetails($fileId,$userId);
					}
					if($type == IMAGE_TYPE){

						$lang = "en";
						$favrouiteTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
					}
					if($type == PLAYLIST_TYPE){

						$favrouiteTubes = $this->fvmodel->getFavouritePlayListByPlaylistId($fileId,$userId);
					}

					$arrayObject  = array(
						"Likes"=>(int)$row->totalLikes,
						"Favourites"=>(int)$row->FavouriteTotal,
						"Type"=>$type,
						"Tubes" => $favrouiteTubes);

						$arrayObjectClient[]=$arrayObject;
					}
				}
				return $arrayObjectClient;
			}

			/* Method to get favourite users & playlist
			Created By: Nishit Patel
			*/
			public function getFavouritesNewsFeedForUser($userId,$lang,$PageIndex){

				$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}

				/*$query = $this->db->query('
				SELECT videomaster.playListId as Id,5 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join favouritemaster on videomaster.playListId = favouritemaster.fileId and favouritemaster.type=5 inner join contentmaster on contentmaster.Id = videomaster.contentId where contentmaster.userId = '.$userId.' group by videomaster.Id
				UNION
				SELECT videomaster.Id,1 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join contentmaster on contentmaster.Id = videomaster.contentId inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where videomaster.playListId = 0 AND favouritemaster.userId = '.$userId.' group by videomaster.Id
				UNION
				SELECT imagemaster.Id,2 as type,imagemaster.title,contentmaster.submitedDate,contentmaster.userId from imagemaster inner join contentmaster on imagemaster.contentId = contentmaster.Id inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where contentmaster.userId = '.$userId.' GROUP BY imagemaster.Id
				UNION
				SELECT postmaster.Id,3 as type,postmaster.decription as title,contentmaster.submitedDate,contentmaster.userId from postmaster inner join contentmaster on postmaster.contentId = contentmaster.Id inner join favouritemaster on favouritemaster.fileId = contentmaster.userId and favouritemaster.type=4 where contentmaster.userId = '.$userId.' GROUP BY postmaster.Id
				order by submitedDate DESC LIMIT '.$pageNo.',20');*/


				$query = $this->db->query('
				SELECT videomaster.playListId as Id,5 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join favouritemaster on videomaster.playListId = favouritemaster.fileId and favouritemaster.type=5 inner join contentmaster on contentmaster.Id = videomaster.contentId where favouritemaster.userId = '.$userId.' group by videomaster.Id
				UNION
				SELECT videomaster.Id,1 as type,videomaster.title,contentmaster.submitedDate,contentmaster.userId FROM videomaster inner join contentmaster on contentmaster.Id = videomaster.contentId inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where videomaster.playListId = 0 AND favouritemaster.userId = '.$userId.' group by videomaster.Id
				UNION
				SELECT imagemaster.Id,2 as type,imagemaster.title,contentmaster.submitedDate,contentmaster.userId from imagemaster inner join contentmaster on imagemaster.contentId = contentmaster.Id inner join favouritemaster on contentmaster.userId = favouritemaster.fileId and favouritemaster.type=4 where favouritemaster.userId = '.$userId.' GROUP BY imagemaster.Id
				UNION
				SELECT postmaster.Id,3 as type,postmaster.decription as title,contentmaster.submitedDate,contentmaster.userId from postmaster inner join contentmaster on postmaster.contentId = contentmaster.Id inner join favouritemaster on favouritemaster.fileId = contentmaster.userId and favouritemaster.type=4 where favouritemaster.userId = '.$userId.' GROUP BY postmaster.Id
				order by submitedDate DESC LIMIT '.$pageNo.',20');



				return $this->displayFavouriteNewsFeedForUser($query->result(),$lang,$userId);

			}

			/* Method to display popular tubes
			Created By: Nishit Patel
			*/
			private function displayFavouriteNewsFeedForUser($result,$lang,$userId){
				$this->load->model("ApiAbuseModel","abusemodel");
				$this->load->model("ApiVideoModel","vmodel");
				$this->load->model("ApiPostModel","pmodel");
				$this->load->model("ApiImageModel","imodel");
				$this->load->model("ApiFavouriteModel","fmodel");

				$this->load->model("ApiUserModel","usmodel");

				$arrayObjectClient = null;
				if($result != null){
					foreach($result as $row){
						$arrayObject = null;

						$fileId = (int)$row->Id;
						$type = (int)$row->type;
						$favrouiteTubes = null;
						if($type == VIDEO_TYPE){
							$lang = "en";
							$favrouiteTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
						}
						if($type == POST_TYPE){
							$favrouiteTubes = $this->pmodel->getPostDetails($fileId,$userId);
						}
						if($type == IMAGE_TYPE){
							$lang = "en";
							$favrouiteTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
						}
						if($type == PLAYLIST_TYPE){
							$favrouiteTubes = $this->fmodel->getFavouritePlayListByPlaylistId($fileId,$userId);
						}

						//$activities = $this->usmodel->getUserActivity($fileId,$type,$userId);

						$arrayObject  = array(
							"Likes"=>0,
							"Favourites"=>0,
							"Type"=>$type,
							"Tubes" => $favrouiteTubes);

							$arrayObjectClient[]=$arrayObject;
						}
					}
					return $arrayObjectClient;
				}

				/* Method to get user newsfeed
				Created By: Nishit Patel
				*/
				public function getUserNewsFeed($userId,$lang,$PageIndex,$loginUserId){

					$pageNo = "00";
					if ($PageIndex > 0) {
						$PageIndex = $PageIndex * 2;
						$pageNo = $PageIndex.'0';
					}

					$query = $this->db->query('select videomaster.Id as fileId,5 as type,videomaster.playListId,contentmaster.Id as contentId,contentmaster.submitedDate from videomaster inner join contentmaster on videomaster.contentId = contentmaster.Id AND videomaster.playListId > 0 where contentmaster.userId = '.$userId.'
					UNION
					select videomaster.Id as fileId,1 as type,videomaster.playListId,contentmaster.Id as contentId,contentmaster.submitedDate  FROM videomaster inner join contentmaster on videomaster.contentId = contentmaster.Id and videomaster.playListId = 0 where contentmaster.userId = '.$userId.'
					UNION
					select imagemaster.Id as fileId,2 as type,imagemaster.Id as playListId,contentmaster.Id as contentId,contentmaster.submitedDate from imagemaster inner join contentmaster on imagemaster.contentId = contentmaster.Id where contentmaster.userId = '.$userId.'
					UNION
					select postmaster.Id as fileId,3 as type,postmaster.Id as playListId,contentmaster.Id as contentId,contentmaster.submitedDate from postmaster INNER JOIN contentmaster on postmaster.contentId = contentmaster.Id where contentmaster.userId = '.$userId.'
					ORDER BY contentId DESC limit '.$pageNo.',20');

					return $this->displayUserNewsFeedForApp($query->result(),$lang,$loginUserId);

				}
				/* Method to display user Newsfeed
				Created By: Nishit Patel
				*/
				private function displayUserNewsFeedForApp($result,$lang,$userId){
					$this->load->model("Utility","utility");
					$this->load->model("ApiVideoModel","vmodel");
					$this->load->model("ApiPostModel","pmodel");
					$this->load->model("ApiImageModel","imodel");
					$this->load->model("ApiFavouriteModel","fmodel");

					$this->load->model("ApiUserModel","usmodel");

					$arrayObjectClient = null;
					if($result != null){
						foreach($result as $row){
							$arrayObject = null;

							$fileId = (int)$row->fileId;
							$type = (int)$row->type;
							$playListId = (int)$row->playListId;

							$userTubes = null;
							if($type == VIDEO_TYPE){
								$lang = "en";
								$userTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
							}
							if($type == POST_TYPE){
								$userTubes = $this->pmodel->getPostDetails($fileId,$userId);
							}
							if($type == IMAGE_TYPE){
								$lang = "en";
								$userTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
							}
							if($type == PLAYLIST_TYPE){
								$userTubes = $this->fmodel->getFavouritePlayListByPlaylistId($playListId,$userId);
							}

							$displayDate = $this->utility->timeAgoFormat($row->submitedDate);

							//$activities = $this->usmodel->getUserActivity($fileId,$type,$userId);

							$arrayObject  = array(
								"contentId"=>(int)$row->contentId,
								"createdDate" => $displayDate,
								"Type"=>$type,
								"Tubes" => $userTubes);

								$arrayObjectClient[]=$arrayObject;
							}
						}
						return $arrayObjectClient;
					}

					/* Method to get searched keyword popular Content
					Created By: Nishit Patel
					*/
					public function getSearchKeywordPopularContents($userId,$lang,$keyword,$PageIndex){

						$pageNo = "00";
						if ($PageIndex > 0) {
							$PageIndex = $PageIndex * 2;
							$pageNo = $PageIndex.'0';
						}

						$query = $this->db->query('select videomaster.Id as fileId,1 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = videomaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id)  + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = videomaster.Id and favouritemaster.type = 1)) as Totals FROM videomaster left join likemaster on videomaster.Id = likemaster.fileId and likemaster.type=1 group by videomaster.Id
						UNION
						SELECT imagemaster.Id as fileId,2 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = imagemaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id) + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = imagemaster.Id and favouritemaster.type = 1)) as Totals FROM imagemaster left join likemaster on imagemaster.Id = likemaster.fileId and likemaster.type=2 group by imagemaster.Id
						UNION
						SELECT postmaster.Id as fileId,3 as type,count(likemaster.Id) as totalLikes,(select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = postmaster.Id and favouritemaster.type = 1) as FavouriteTotal,(count(likemaster.Id) + (select count(favouritemaster.Id) from favouritemaster where favouritemaster.fileId = postmaster.Id and favouritemaster.type = 1)) as Totals FROM postmaster left join likemaster on postmaster.Id = likemaster.fileId and likemaster.type=3 group by postmaster.Id
						Order by Totals DESC LIMIT '.$pageNo.',20');
						return $this->displayPopularContents($query->result(),$lang,$userId);

					}


				}
				?>
