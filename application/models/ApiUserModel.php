<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiUserModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}


		/* ENCRYPT String
			Return enycrypted string
			Created By: Nishit Patel
		*/
		private function encrypt($text)
		{
			$SALT ='whateveryouwantwhateveryouwant'.'\0';
		    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
		}

		/* DECRYPT String
			Return decrypted string
			Created By: Nishit Patel
		*/
		private function decrypt($text)
		{
			$SALT ='whateveryouwantwhateveryouwant'.'\0';
		    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
		}

		/* Method to update device token
			 Created By: Nishit Patel
		*/
		public function updateDeviceToken($deviceType,$userId,$deviceToken){
			$deviceId = $this->isDeviceTokenAlreadyExsist($deviceToken);
			if($deviceId > 0){
				$data = array('deviceType' => $deviceType,'userId' => $userId,'deviceToken' => $deviceToken);
				$this->db->where('Id', $deviceId);
				$this->db->update('devicemaster', $data);
				return TRUE;
			}else{
				$data = array('deviceType' => $deviceType,'userId' => $userId,'deviceToken' => $deviceToken);
		    $this->db->insert('devicemaster', $data);
		    $insert_Id = $this->db->insert_id();
		    return TRUE;
			}
		}

		/* Method to check is device token already exsist
			 Created By: Nishit Patel
		*/
		public function isDeviceTokenAlreadyExsist($deviceToken){
			$this->db->select('Id');
			$this->db->from('devicemaster');
			$this->db->where('deviceToken',$deviceToken);
			$query = $this->db->get();
			$result = $query->result();
			$deviceId = 0;
			if($result != null){
				foreach ($result as $row) {
					$deviceId = $row->Id;
				}
			}
			return $deviceId;
		}


	public function timeAgo($time_ago)
	{
		    $time_ago = strtotime($time_ago);
		    $cur_time   = time();

		    $dt = new DateTime();
			$dt->format("r");
			$tz = new DateTimeZone("UTC");
			$dt->setTimezone($tz);
			$d = $dt->format("Y-m-d H:i:s");

			$cur_time = strtotime($d);

		    $time_elapsed   = $cur_time - $time_ago;
		    $seconds    = $time_elapsed ;
		    $minutes    = round($time_elapsed / 60 );
		    $hours      = round($time_elapsed / 3600);
		    $days       = round($time_elapsed / 86400 );
		    $weeks      = round($time_elapsed / 604800);
		    $months     = round($time_elapsed / 2600640 );
		    $years      = round($time_elapsed / 31207680 );
		    // Seconds
		    if($seconds <= 60){
		        return "just now";
		    }
		    //Minutes
		    else if($minutes <=60){
		        if($minutes==1){
		            return "one minute ago";
		        }
		        else{
		            return "$minutes minutes ago";
		        }
		    }
		    //Hours
		    else if($hours <=24){
		        if($hours==1){
		            return "an hour ago";
		        }else{
		            return "$hours hrs ago";
		        }
		    }
		    //Days
		    else if($days <= 7){
		        if($days==1){
		            return "yesterday";
		        }else{
		            return "$days days ago";
		        }
		    }
		    //Weeks
		    else if($weeks <= 4.3){
		        if($weeks==1){
		            return "a week ago";
		        }else{
		            return "$weeks weeks ago";
		        }
		    }
		    //Months
		    else if($months <=12){
		        if($months==1){
		            return "a month ago";
		        }else{
		            return "$months months ago";
		        }
		    }
		    //Years
		    else{
		        if($years==1){
		            return "one year ago";
		        }else{
		            return "$years years ago";
		        }
		    }
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}

	public function isUserExists($Email,$username){
			$this->db->select('Id');
			$this->db->from('usermaster');
			$strWhere = "email='".$Email."' OR userName='".$username."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->result() != null) {
				return true;
			}
			return false;
		}
		public function checkEmailRegisterd($Email){
				$this->db->select('Id');
				$this->db->from('usermaster');
				$strWhere = "email='".$Email."'";
				$this->db->where($strWhere,null,false);
				$query = $this->db->get();
				if ($query->result() != null) {
					return true;
				}
				return false;
			}

	public function isCurrentPasswordMatched($userId,$password){
		    $encryptedpassword = $this->encrypt($password);
			$this->db->select('Id');
			$this->db->from('usermaster');
			$strWhere = "Id='".$userId."' AND password='".$encryptedpassword."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->result() != null) {
				return true;
			}
			return false;
	}

	public function isUserFollowing($userId,$myUserId){
			$this->db->select('Id');
			$this->db->from('favouritemaster');
			$strWhere = "userId='".$myUserId."' AND fileId='".$userId."' AND type = 4";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->result() != null) {
				return true;
			}
			return false;
	}

	public function changePassword($userId,$password){
		  $encryptedpassword = $this->encrypt($password);
				$data = array(
   							'password' => $encryptedpassword
                         );
				$this->db->where('Id', $userId);
				$this->db->update('usermaster', $data);
				return true;

	}

	public function updateUserProfilePicture($userId,$profileImage,$profileThumb){
				$data = array(
   							'profileImage' => $profileImage,
								'profileThumb' => $profileThumb
                         );
				$this->db->where('Id', $userId);
				$this->db->update('usermaster', $data);
				return true;

	}

	public function changeFullName($userId,$fullName){
				if($fullName != '-1'){

					$data = array(
   							'fullName' => $fullName
                         );
					$this->db->where('Id', $userId);
					$this->db->update('usermaster', $data);
				}
				return true;

	}



	public function updateUserProfile($UserId,$fullName,$userName,$email,$mobileNumber,$homeTown,$countryId,$currentAddress,$faxNumber,$profileTypeId){

				if($fullName != '-1' && $fullName != ''){
					$data = array(
   							'fullName' => $fullName
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if($userName != '-1' && $userName != '')
		  		{
					$data = array(
   							'userName' => $userName
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if($email != '-1')
		  		{
					$data = array(
   							'email' => $email
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($mobileNumber != '-1')
		  		{
					$data = array(
   							'mobileNumber' => $mobileNumber
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($homeTown != '-1')
		  		{
					$data = array(
   							'homeTown' => $homeTown
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($countryId != -1)
		  		{
					$data = array(
   							'countryId' => $countryId
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($currentAddress != '-1')
		  		{
					$data = array(
   							'currentAddress' => $currentAddress
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($faxNumber != '-1')
		  		{
					$data = array(
   							'faxNumber' => $faxNumber
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}
				if ($profileTypeId != -1)
		  		{
					$data = array(
   							'profileTypeId' => $profileTypeId
                         );
					$this->db->where('Id', $UserId);
					$this->db->update('usermaster', $data);
				}

				return true;
	}


		public function registerUser($fullname,$username,$email,$password,$DeviceType,$DeviceToken,$lang){
			$this->load->model("ApiFavouriteModel","favouritemodel");
			$encryptedpassword = $this->encrypt($password);
			$data = array(
				'fullName' => $fullname ,
				'userName' => $username,
				'email' => $email,
				'password' => $encryptedpassword,
				'isVerified' => 0,
				'isActive' => 0
				);
			$this->db->insert('usermaster', $data);
			$userId = $this->db->insert_id();

			//Make super user by default follow
			$followingId = $this->favouritemodel->makeFollowUser($userId,SUPER_USER_ID,USER_TYPE);

			$accessToken = $this->generateRandomString(12);
			$this->saveAccessToken($accessToken,$userId);
			$this->sendActivationEmail($username,$userId,$accessToken,$email);
			$this->saveDevice($userId,$DeviceType,$DeviceToken);
			$result = $this->getUserById($userId,$lang);

			return $result;
		}

	public function isAlreadySavedDeviceToken($userId,$DeviceType,$DeviceToken){
				$this->db->select('Id');
				$this->db->from('devicemaster');
				$strWhere = "userId='".$userId."' AND deviceToken='".$DeviceToken."' AND deviceType='".$DeviceType."'";
				$this->db->where($strWhere,null,false);
				$query = $this->db->get();
				if ($query->result() != null) {
					return true;
				}
				return false;
		}

		public function saveDevice($userId,$DeviceType,$DeviceToken){
			return $this->updateDeviceToken($DeviceType,$userId,$DeviceToken);
			// $isSaved = $this->isAlreadySavedDeviceToken($userId,$DeviceType,$DeviceToken);
			// if (!$isSaved) {
			// 	$data = array(
			// 		'deviceToken' => $DeviceToken,
			// 		'deviceType' => $DeviceType,
			// 		'userId' => $userId
			// 		);
			// 	$this->db->insert('devicemaster', $data);
			// }
			// return true;
		}

		/* Method to check user status
			 Created By: Nishit Patel
		*/
		public function getUserStatusBlock($Email,$password){
			$encrypted = $this->encrypt($password);
			$this->db->select('Id,status');
			$this->db->from('usermaster');
			$strWhere = "email='".$Email."' OR userName='".$Email."' AND Password='".$encrypted."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			$result = $query->result();
			$userStatus = 0;
			if ($result != null) {
				foreach($result as $row){
					$userId = (int)$row->Id;
					$userStatus = (int)$row->status;
				}
			}
			if($userStatus == 1){
				return true;
			}
			return false;
		}

		/* Method to check user is Active or not
			 Created By: Nishit Patel
		*/
		public function checkUserIsActive($Email){
			$this->db->select('Id,isActive');
			$this->db->from('usermaster');
			$strWhere = "email='".$Email."' OR userName='".$Email."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			$result = $query->result();
			$userStatus = 0;
			if ($result != null) {
				foreach($result as $row){
					$userId = (int)$row->Id;
					$userStatus = (int)$row->isActive;
				}
			}
			if($userStatus == 1){
				return true;
			}
			return false;
		}

		public function userLogin($Email,$Password,$DeviceType,$DeviceToken){
			$this->load->model("ApiFavouriteModel","favouritemodel");
			$encrypted = $this->encrypt($Password);
			$this->db->select('Id,status');
			$this->db->from('usermaster');
			$strWhere = "email='".$Email."' OR userName='".$Email."' AND Password='".$encrypted."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			$result = $query->result();
			if ($result != null) {
				foreach($result as $row){
					$userId = (int)$row->Id;
				}
				$this->saveDevice($userId,$DeviceType,$DeviceToken);

				if(SUPER_USER_ID != $userId){
					//Make super user by default follow
					$followingId = $this->favouritemodel->makeFollowUser($userId,SUPER_USER_ID,USER_TYPE);
				}

				return $this->getUserById($userId,'en');
			}
			return $query->result();
		}


		public function getUserById($userId,$lang){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,IFNULL(profiletypemaster.nameEnglish,"") as ProfileNameEN,IFNULL(profiletypemaster.nameArabic,"") as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.Id',$userId);
			$query = $this->db->get();
			return $this->displayUser($query->result(),$lang);
		}


		public function getUsers($PageIndex,$lang,$isVerified){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			if ($isVerified != -1) {
				$this->db->where('usermaster.isVerified',$isVerified);
			}
			$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayUserList($query->result(),$lang);
		}

		public function getTotalPageOfUsers($isVerified){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			if ($isVerified != -1) {
				$this->db->where('usermaster.isVerified',$isVerified);
			}
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		/* Method to get Abuse user list
			 Created By: Nishit Patel
		*/
		public function getAbuseUsers($PageIndex,$lang){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('abusemaster','usermaster.Id = abusemaster.fileId and abusemaster.type='.USER_TYPE,'inner');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			$this->db->where('usermaster.status',0);
			$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayUserList($query->result(),$lang);
		}

		/* Method to get total abuse user page number
			 Created By: Nishit Patel
		*/
		public function getAbuseTotalPageOfUsers(){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('abusemaster','usermaster.Id = abusemaster.fileId and abusemaster.type='.USER_TYPE,'inner');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			$this->db->where('usermaster.status',0);
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		public function displayUser($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
						$countryName = 'blank';
						$ProfileTitle = '';

						if ($lang == 'ar') {
							$countryName = $row->CountryNameAR;
							$ProfileTitle = $row->ProfileNameAR;
						}else{
							$countryName = $row->CountryNameEN;
							$ProfileTitle = $row->ProfileNameEN;
						}
						$RegistrationDate = $this->timeAgo($row->registeredDate);

						$profileUrl = "";
						$profileThumbUrl = "";
						$profileImage = $row->profileImage;
						$profileThumbImage = $row->profileThumb;
						if($profileImage != null){
							$profileUrl = PROFILE_URL.$profileImage;
						}
						if($profileThumbImage != null){
							$profileThumbUrl = PROFILE_THUMB_URL.$profileThumbImage;
						}
						$arrayObject  = array(
											"UserId"=>(int)$row->Id,
											"FullName"=>$row->fullName,
											"UserName"=>$row->userName,
											"Email"=>$row->email,
											"ProfileImage"=> $profileUrl,
											"ProfileThumbImage"=> $profileThumbUrl,
											"MobileNumber"=>$row->mobileNumber,
											"HomeTown"=>$row->homeTown,
											"CurrentAddress"=>$row->currentAddress,
											"FaxNumber"=>$row->faxNumber,
											"IsVerified"=>(bool)$row->isVerified,
											"IsActive"=>(bool)$row->isActive,
											"RegistrationDate"=>$RegistrationDate,
											"CountryId"=>(int)$row->countryId,
											"CountryName"=>$countryName,
											"ProfileTypeId"=>(int)$row->profileTypeId,
											"ProfileTitle"=>$ProfileTitle
											);

					//$arrayObjectClient[]=$arrayObject;
					return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}
		public function displayUserList($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

						if ($lang == 'ar') {
							$countryName = $row->CountryNameAR;
							$ProfileTitle = $row->ProfileNameAR;

						}else{
							$countryName = $row->CountryNameEN;
							$ProfileTitle = $row->ProfileNameEN;
						}
						$RegistrationDate = $this->timeAgo($row->registeredDate);
						$profileUrl = "";
						$profileThumbUrl = "";
						$profileImage = $row->profileImage;
						$profileThumbImage = $row->profileThumb;
						if($profileImage != null){
							$profileUrl = PROFILE_URL.$profileImage;
						}
						if($profileThumbImage != null){
							$profileThumbUrl = PROFILE_THUMB_URL.$profileThumbImage;
						}

						$arrayObject  = array(
											"UserId"=>(int)$row->Id,
											"FullName"=>$row->fullName,
											"UserName"=>$row->userName,
											"Email"=>$row->email,
											"ProfileImage"=> $profileUrl,
											"ProfileThumbImage"=> $profileThumbUrl,
											"MobileNumber"=>$row->mobileNumber,
											"HomeTown"=>$row->homeTown,
											"CurrentAddress"=>$row->currentAddress,
											"FaxNumber"=>$row->faxNumber,
											"IsVerified"=>(bool)$row->isVerified,
											"IsActive"=>(bool)$row->isActive,
											"RegistrationDate"=>$RegistrationDate,
											"CountryId"=>(int)$row->countryId,
											"CountryName"=>$countryName,
											"ProfileTypeId"=>(int)$row->profileTypeId,
											"ProfileTitle"=>$ProfileTitle
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		public function makeVerifiedProfile($userId){
				$data = array(
   							'isVerified' => 1
                         );
				$this->db->where('Id', $userId);
				$this->db->update('usermaster', $data);
				return true;

		}

	public function saveAccessToken($accessToken,$userId){
						$data = array(
									'accessToken' => $accessToken
													 );
						$this->db->where('Id', $userId);
						$this->db->update('usermaster', $data);
					return true;
		}
		public function getUserId($email){
				$this->db->select('Id');
				$this->db->from('usermaster');
				$this->db->where('email',$email);
				$query = $this->db->get();
				$result = $query->result();
				$userId = null;
				if($result != null){
					foreach ($result as $row) {
									$userId = (int)$row->Id;
					}
				}
				return $userId;
		}

		public function activeAccount($validationToken,$accessToken){
			//decrypt
				//$userId = $this->decrypt($validationToken);
				$userId = $validationToken;
				$isValid = $this->isValidTokens($userId,$accessToken);
				if ($isValid) {
					$data = array(
								'isActive' => 1
												 );
					$this->db->where('Id', $userId);
					$this->db->update('usermaster', $data);
					$this->saveAccessToken('',$userId);
					return true;
				}
				return false;

			}

		public function sendActivationEmail($userName,$userId,$accessToken,$email){
			$body='



			<div style="max-width:100%"  >

			<table style="background-color:#dddddd; width:100%; height:50px; border:1px solid teal;">
				<tr>
					<td style="width:20%;">
						<img src="http://dev.mobileartsme.com/holynet/assets/images/small_ic_luncher_icon.png" height="50px">
					</td>
					<td style="width:60%; height:50px;">
						<center><span style="color:#009688; height:50px; font-size:40px;">HolyNet</span></center>
					</td>
					<td style="width:20%;"></td>
				</tr>
			</table>

	        <div style="max-width:100%;" >

	          <br>
	          <div align="left">
	            <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:20px;line-height: 25px;Margin-bottom: 25px; margin-left:5%; Margin-top: 5%;">
	              Hi, '.$userName.'
	            </p>
	          </div>
	          <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px; Margin-bottom: 5%;margin-left:5%; margin-right:5%;">
	            To active your Holynet account, click below button.
	          </p>
	          <center>
	          <p style="Margin-bottom: 5%;">
	            <a href="'.base_url().'ActiveAccount?validationToken='.$userId.'&accessToken='.$accessToken.'" style="text-decoration:none;border:solid 1px;color:teal;padding:10px;font-size: 18px;">
	              Active Holynet account
	            </a>
	          </p>
	        </center>
	          <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px; Margin-bottom: 5%;margin-left:5%; margin-right:5%;">
	            If you ignore this message, your Holynet account wont be active.
	          </p>
	          <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px; Margin-bottom: 5%;margin-left:5%; margin-right:5%;">
	            Thanks for using Holynet!
	          </p>
	        </div>
	        <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px; Margin-bottom: 5%;margin-left:5%; margin-right:5%;">
	          The Holynet Team
	        </p>
	      </div>';
			$config = Array(
											'protocol' => 'smtp',
											'smtp_host' => 'ssl://smtp.googlemail.com',
											'smtp_port' => 465,
											'smtp_user' => 'teamcontactme@gmail.com',
											'smtp_pass' => 'ashapura228',
											'mailtype' => 'html',
											'charset' => 'UTF-8',
											'wordwrap' => TRUE
										);
			$this->load->library('email', $config);
			$this->email->initialize($config);

			$this->email->set_newline("\r\n");
			$this->email->from('teamcontactme@gmail.com', 'Holynet Support');
			$this->email->to($email);
			$this->email->subject('Here is the link to active your Holynet account');
			$this->email->message($body);
			if (!$this->email->send()) {
			//	show_error($this->email->print_debugger());
				$msg='Sorry Unable to send email...'; }
			else {

				$msg='Your e-mail has been sent!';
			}
		}

		public function getUserName($email){
				$this->db->select('userName');
				$this->db->from('usermaster');
				$this->db->where('email',$email);
				$query = $this->db->get();
				$result = $query->result();
				$userName = null;
				if($result != null){
					foreach ($result as $row) {
									$userName = $row->userName;
					}
				}
				return $userName;
		}

		public function forgetPassword($email){
			$isRegistered = $this->checkEmailRegisterd($email);
			if ($isRegistered) {
				$userId = null;
				$accessToken = null;
				$userName = null;

				$userId = $this->getUserId($email);
				$userName = $this->getUserName($email);

				//$validationToken = $this->encrypt($userId);
				$accessToken = $this->generateRandomString(12);
				$this->saveAccessToken($accessToken,$userId);


					$body=' <div style="max-width:100%">
				    <div style="max-width:100%;">

							<table id="emb-email-header" style="background-color:#dddddd; width:100%; height:50px; border:1px solid teal;">
								<tr>
									<td style="width:20%;">
										<img src="http://dev.mobileartsme.com/holynet/assets/images/small_ic_luncher_icon.png" height="50px">
									</td>
									<td style="width:60%; height:50px;">
										<center><span style="color:#009688; height:50px; font-size:40px;">HolyNet</span></center>
									</td>
									<td style="width:20%;"></td>
								</tr>
							</table>

				      <br>
				      <div align="left">
				        <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:20px;line-height: 25px;Margin-bottom: 25px; margin-left:5%; Margin-top: 5%;">
				          Hi, '.$userName.'
				        </p>
				      </div>

				        <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px; Margin-bottom: 5%;margin-left:5%; margin-right:5%;">
				          We got a request to reset your Holynet password.
				        </p>
				        <center>
				      <p style="Margin: 5% 5% 5% 5%;">
				        <a href="'.base_url().'ForgetPassword?validationToken='.$userId.'&accessToken='.$accessToken.'"
				        style="text-decoration:none;border:solid 1px;color:teal;padding:10px;font-size: 18px;">
				          Reset Password
				        </a>
				      </p>
				      </center>
				      <p style="color: #565656;font-family: Georgia,serif;font-size: 18px;line-height: 25px;Margin-bottom: 5%;
				      margin-left:5%; margin-right:5%;">
				        If you ignore this message, your password wont be changed.
				      </p>
				      <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px;line-height: 25px; margin-left:5%; margin-right:5%;">
				        Thanks for using Holynet!
				      </p>

				    </div>

				      <p style="color: #565656;font-family: Georgia,serif;font-size: 18px; line-height: 25px; margin-left:5%; margin-right:5%;">The Holynet Team</p>

				  </div>';
					$config = Array(
													'protocol' => 'smtp',
													'smtp_host' => 'ssl://smtp.googlemail.com',
													'smtp_port' => 465,
													'smtp_user' => 'teamcontactme@gmail.com',
													'smtp_pass' => 'ashapura228',
													'mailtype' => 'html',
													'charset' => 'UTF-8',
												  'wordwrap' => TRUE
												);
					$this->load->library('email', $config);
					$this->email->initialize($config);

					$this->email->set_newline("\r\n");
					$this->email->from('teamcontactme@gmail.com', 'Holynet Support');
					$this->email->to($email);
					$this->email->subject('Here is the link to reset your password');
					$this->email->message($body);
					if (!$this->email->send()) {
					//	show_error($this->email->print_debugger());
						$msg='Sorry Unable to send email...'; }
					else {

						$msg='Your e-mail has been sent!';
					}
					return true;
			}
			return false;

		}
		public function isValidTokens($userId,$accessToken){
				$this->db->select('Id');
				$this->db->from('usermaster');
				$strWhere = "Id='".$userId."' AND accessToken='".$accessToken."'";
				$this->db->where($strWhere,null,false);
				$query = $this->db->get();
				if ($query->result() != null) {
					return true;
				}
				return false;
			}

		public function resetPassword($validationToken,$accessToken,$newPassword){
			//decrypt
				//$userId = $this->decrypt($validationToken);
				$userId = $validationToken;
				$isValid = $this->isValidTokens($userId,$accessToken);
				$encryptedPassword = $this->encrypt($newPassword);
				if ($isValid) {
					$data = array(
								'password' => $encryptedPassword
												 );
					$this->db->where('Id', $userId);
					$this->db->update('usermaster', $data);
					$this->saveAccessToken('',$userId);
					return true;
				}
				return false;

			}

		public function removeVerifledProfile($userId){
				$data = array(
   							'isVerified' => 0
                         );
				$this->db->where('Id', $userId);
				$this->db->update('usermaster', $data);
				return true;

		}
		public function deleteUser($userId){
				$this->db->where('Id', $userId);
				$this->db->delete('usermaster');
				return true;
		}

		// $isVerified = -1 for both , 0 for normal and 1 for special
		public function getSearchUsers($PageIndex,$lang,$isVerified,$nameKeyword,$email,$mobileNo,$countryId){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage,usermaster.profileThumb,  usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			if ($isVerified != -1) {
				$this->db->where('usermaster.isVerified',$isVerified);
			}
			if ($nameKeyword != '') {
				$this->db->like('usermaster.fullName', $nameKeyword, 'after');
				$this->db->or_not_like('usermaster.userName', $nameKeyword, 'after');
			}
			if ($email != '') {
				$this->db->like('usermaster.email', $email, 'after');
			}
			//echo $mobileNo;
			if ($mobileNo != '') {
				$this->db->like('usermaster.mobileNumber', $mobileNo, 'after');
			}
			if ($countryId != -1) {
				$this->db->where('usermaster.countryId',$countryId);
			}
			$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayUserList($query->result(),$lang);
		}


			// $isVerified = -1 for both , 0 for normal and 1 for special
		public function getTotalPageOfSearchUsers($isVerified,$nameKeyword, $email,$mobileNo,$countryId){
			$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
			 usermaster.profileImage, usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
			 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
			 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
			 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
			$this->db->from('usermaster');
			$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
			$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
			$this->db->where('usermaster.isActive',1);
			if ($isVerified != -1) {
				$this->db->where('usermaster.isVerified',$isVerified);
			}
			if ($nameKeyword != '') {
				$this->db->like('usermaster.fullName', $nameKeyword, 'after');
				$this->db->or_not_like('usermaster.userName', $nameKeyword, 'after');
			}
			if ($email != '') {
				$this->db->like('usermaster.email', $email, 'after');
			}
			if ($mobileNo != '') {
				$this->db->like('usermaster.mobileNumber', $mobileNo, 'after');
			}
			if ($countryId != -1) {
				$this->db->where('usermaster.countryId',$countryId);
			}
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}
		// $isVerified = -1 for both , 0 for normal and 1 for special
		public function countTotalUser($isVerified){
			$this->db->select('Id');
			$this->db->from('usermaster');
			$this->db->where('isActive',1);
			if ($isVerified != -1) {
				$this->db->where('isVerified',$isVerified);
			}
			$query = $this->db->count_all_results();
			return ceil($query);
		}
		public function countTotalAbuseUser(){
			$this->db->select('Id');
			$this->db->from('abusemaster');
			$this->db->where('type',0);
			$query = $this->db->count_all_results();
			return ceil($query);
	}


	/* Method to Get User Profile by UserId
		 Created By: Nishit Patel
	*/
	public function getUserProfile($userId,$pageIndex,$myUserId){
		$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
		 usermaster.profileImage,usermaster.profileThumb,  usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
		 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
		 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
		 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
		$this->db->from('usermaster');
		$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
		$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
		$this->db->where('usermaster.Id',$userId);
		$query = $this->db->get();
		return $this->displayUserProfileList($query->result(),$pageIndex,$myUserId);
	}

	/* Method to get Total Images for UserId
		 Created By: Nishit Patel
	*/
	public function getTotalImagesByUserId($userId){
		$this->db->select("count(imagemaster.Id) as totalImages");
    $this->db->from("imagemaster");
		$this->db->join("contentmaster","imagemaster.contentId = contentmaster.Id","inner");
    $whereStr = "contentmaster.userId=".$userId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->totalImages;
      }
    }
    return $totalValue;
	}

	/* Method to get Total Posts for UserId
		 Created By: Nishit Patel
	*/
	public function getTotalPostsByUserId($userId){
		$this->db->select("count(postmaster.Id) as totalPosts");
    $this->db->from("postmaster");
		$this->db->join("contentmaster","postmaster.contentId = contentmaster.Id","inner");
    $whereStr = "contentmaster.userId=".$userId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->totalPosts;
      }
    }
    return $totalValue;
	}

	/* Method to get Total Playlist for userId
		 Created By: Nishit Patel
	*/
	public function getTotalPlaylistByUserId($userId){
		$this->db->select("count(Id) as totalPlaylist");
    $this->db->from("playlistmaster");
    $whereStr = "userId=".$userId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->totalPlaylist;
      }
    }
    return $totalValue;
	}

	/* Method to display User profile in json format
		 Created By: Nishit Patel
	*/
	public function displayUserProfileList($result,$pageIndex,$myUserId){
		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
					$arrayObject = null;
					$countryName = $row->CountryNameEN;
					$ProfileTitle = $row->ProfileNameEN;
					$isFollowing = false;

					$this->load->model("Utility","utility");
					$RegistrationDate = $this->utility->timeAgoFormat($row->registeredDate);


					$countryObject = array("CountryId"=>(int)$row->countryId,"Country"=>$countryName);
					$profileTypeObject = array("ProfileTypeId"=>(int)$row->profileTypeId,"ProfileTitle"=>$ProfileTitle);

					$userId = (int)$row->Id;
					$isFollowing = $this->isUserFollowing($userId,$myUserId);
					$abuseObject = $this->getAbuseReportForUser($userId,4,$myUserId);

					$this->load->model("ApiVideoModel","vmodel");
					$this->load->model("ApiImageModel","imodel");
					$this->load->model("ApiPostModel","pmodel");
					$this->load->model("ApiPlaylistModel","plmodel");
					$this->load->model("ApiFavouriteModel","fvrtmodel");

					$totalVideos = (int)$this->vmodel->getTotalVideosByUserId($userId);
					$totalImages = (int)$this->getTotalImagesByUserId($userId);
					$totalPosts = (int)$this->getTotalPostsByUserId($userId);
					$totalPlaylists = (int)$this->getTotalPlaylistByUserId($userId);



					$totalFollowers = $this->fvrtmodel->getTotalFollowers($userId);
					$totalFollowing = $this->fvrtmodel->getTotalFollowingUsers($userId);

					$totalObjects = array("TotalFollowers"=>$totalFollowers,"TotalFollowing"=>$totalFollowing, "TotalVideos"=>$totalVideos,"TotalImages"=>$totalImages,"TotalPosts"=>$totalPosts,"TotalPlaylists"=>$totalPlaylists);


					$videoList = $this->vmodel->getVideoList($pageIndex,$userId,"en",$myUserId);
					$totalPagesVideos = $this->vmodel->getTotalPagesForGetVideoList($userId);
					$videos = array('TotalPage' =>$totalPagesVideos,'CurrentPage'=>$pageIndex+1,'Videos'=>$videoList);

					$imageList = $this->imodel->getImageList($pageIndex,$userId,"en",$myUserId);
					$totalPagesImages = $this->imodel->getTotalPagesForGetImageList($userId);
					$images = array('TotalPage' =>$totalPagesImages,'CurrentPage'=>$pageIndex+1,'Images'=>$imageList);

					$postList = $this->pmodel->getPostList($pageIndex,$userId,$myUserId);
					$totalPagesPosts = $this->pmodel->getTotalPagesForGetPostList($userId);
					$posts = array('TotalPage' =>$totalPagesPosts,'CurrentPage'=>$pageIndex+1,'Posts'=>$postList);

					$playLists = $this->plmodel->getPlayList($userId,$pageIndex);
					$totalPagesPlaylists = $this->plmodel->getTotalPagesForGetPlayList($userId);
					$playlistsobject = array('TotalPage' =>$totalPagesPlaylists,'CurrentPage'=>$pageIndex+1,'PlayList'=>$playLists);

					$profileUrl = "";
					$profileThumbUrl = "";
					$profileImage = $row->profileImage;
					$profileThumbImage = $row->profileThumb;
					if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
					}
					if($profileThumbImage != null){
						$profileThumbUrl = PROFILE_THUMB_URL.$profileThumbImage;
					}
					$arrayObject  = array(
										"UserId"=>$userId,
										"FullName"=>$row->fullName,
										"UserName"=>$row->userName,
										"Email"=>$row->email,
										"ProfileImage"=> $profileUrl,
										"ProfileThumbImage"=> $profileThumbUrl,
										"MobileNumber"=>$row->mobileNumber,
										"HomeTown"=>$row->homeTown,
										"CurrentAddress"=>$row->currentAddress,
										"FaxNumber"=>$row->faxNumber,
										"IsVerified"=>(bool)$row->isVerified,
										"IsActive"=>(bool)$row->isActive,
										"RegistrationDate"=>$RegistrationDate,
										"IsFollowing"=>$isFollowing,
										"Abuse"=>$abuseObject,
										"Country"=>$countryObject,
										"ProfileType"=>$profileTypeObject,
										"Totals"=>$totalObjects,
										"Videos"=>$videos,
										"Images"=>$images,
										"Posts"=>$posts,
										"PlayList"=>$playlistsobject
									);
				$arrayObjectClient[]=$arrayObject;
			}
		}
		return $arrayObjectClient;
	}

	/* Method to Get User Profile for App by UserId
		 Created By: Nishit Patel
	*/
	public function getUserProfileForApp($userId,$lang,$PageIndex,$loginUserId){
		$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
		 usermaster.profileImage,usermaster.profileThumb, usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
		 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
		 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
		 usermaster.profileTypeId,profiletypemaster.nameEnglish as ProfileNameEN,profiletypemaster.nameArabic as ProfileNameAR');
		$this->db->from('usermaster');
		$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
		$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
		$this->db->where('usermaster.Id',$userId);
		$query = $this->db->get();
		return $this->displayUserProfileForApp($query->result(),$lang,$PageIndex,$loginUserId);
	}

	/* Method to display User profile in json format
		 Created By: Nishit Patel
	*/
	public function displayUserProfileForApp($result,$lang,$PageIndex,$loginUserId){
		$this->load->model("ApiVideoModel","vmodel");
		$this->load->model("Utility","utility");
		$this->load->model("ApiNewsFeedModel","nfmodel");
		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
					$arrayObject = null;
					$countryName = $row->CountryNameEN;
					$ProfileTitle = $row->ProfileNameEN;


					$RegistrationDate = $this->utility->timeAgoFormat($row->registeredDate);

					$countryObject = array("CountryId"=>(int)$row->countryId,"Country"=>$countryName);
					$profileTypeObject = array("ProfileTypeId"=>(int)$row->profileTypeId,"ProfileTitle"=>$ProfileTitle);

					$userId = (int)$row->Id;

					$totalVideos = (int)$this->vmodel->getTotalVideosByUserId($userId);
					$totalImages = (int)$this->getTotalImagesByUserId($userId);
					$totalPosts = (int)$this->getTotalPostsByUserId($userId);
					$totalPlaylists = (int)$this->getTotalPlaylistByUserId($userId);

					$totalObjects = array("TotalVideos"=>$totalVideos,"TotalImages"=>$totalImages,"TotalPosts"=>$totalPosts,"TotalPlaylists"=>$totalPlaylists);


					$newFeeds = $this->nfmodel->getUserNewsFeed($userId,$lang,$PageIndex,$loginUserId);
					$profileUrl = "";
					$profileThumbUrl = "";
					$profileImage = $row->profileImage;
					$profileThumbImage = $row->profileThumb;
					if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
					}
					if($profileThumbImage != null){
						$profileThumbUrl = PROFILE_THUMB_URL.$profileThumbImage;
					}
					$arrayObject  = array(
										"UserId"=>$userId,
										"FullName"=>$row->fullName,
										"UserName"=>$row->userName,
										"Email"=>$row->email,
										"ProfileImage"=> $profileUrl,
										"ProfileThumbImage"=> $profileThumbUrl,
										"MobileNumber"=>$row->mobileNumber,
										"HomeTown"=>$row->homeTown,
										"CurrentAddress"=>$row->currentAddress,
										"FaxNumber"=>$row->faxNumber,
										"IsVerified"=>(bool)$row->isVerified,
										"IsActive"=>(bool)$row->isActive,
										"RegistrationDate"=>$RegistrationDate,
										"Country"=>$countryObject,
										"ProfileType"=>$profileTypeObject,
										"Totals"=>$totalObjects,
										"NewsFeeds"=>$newFeeds
									);
				$arrayObjectClient[]=$arrayObject;
			}
		}
		return $arrayObjectClient;
	}

	/* Method to get total page for search users by Keyword
		 Created By: Nishit Patel
	*/
	public function getTotalPageSearchUsersKeyword($userId,$keyword,$lang,$PageIndex){
		$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
		 usermaster.profileImage, usermaster.profileThumb,usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
		 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
		 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
		 usermaster.profileTypeId,IFNULL(profiletypemaster.nameEnglish,"") as ProfileNameEN,IFNULL(profiletypemaster.nameArabic,"") as ProfileNameAR,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
		 (COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
		$this->db->from('usermaster');
		$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
		$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
		$this->db->join('likemaster','likemaster.fileId = usermaster.Id and likemaster.type = '.USER_TYPE,'left');
		$this->db->join('favouritemaster','likemaster.fileId = usermaster.Id and favouritemaster.type = '.USER_TYPE,'left');

		$whereStr = "(usermaster.status = 0 AND usermaster.isActive = 1) AND (usermaster.fullName like '%".$keyword."%' OR usermaster.userName like '%".$keyword."%' OR usermaster.email like '%".$keyword."%' OR
		usermaster.mobileNumber like '%".$keyword."%' OR usermaster.homeTown like '%".$keyword."%' OR usermaster.currentAddress like '%".$keyword."%'";

		$keywordSearch = "";
		if($lang == "en"){
			$keywordSearch = " OR countrymaster.nameEnglish like '%".$keyword."%' OR profiletypemaster.nameEnglish like '%".$keyword."%')";
		}else{
			$keywordSearch = " OR countrymaster.nameArabic like '%".$keyword."%' OR profiletypemaster.nameArabic like '%".$keyword."%')";
		}

		$whereStr .= $keywordSearch;
		$this->db->where($whereStr);
		$this->db->group_by("usermaster.Id");
		$this->db->order_by("Total","desc");

		$query = $this->db->get();
		$result = $query->num_rows();
		$totalPages  = $result / 20;


		if($totalPages > 0 && $totalPages < 1){
			$totalrAtes = 1;
		}else{
			$totalrAtes = round($totalPages,0,PHP_ROUND_HALF_UP);
		}
    return $totalrAtes;

	}


	/* Method to search users by Keyword
		 Created By: Nishit Patel
	*/
	public function getSearchUsersKeyword($userId,$keyword,$lang,$PageIndex){
		$this->db->select('usermaster.Id, usermaster.fullName, usermaster.userName,usermaster.email,
		 usermaster.profileImage, usermaster.profileThumb,usermaster.mobileNumber, usermaster.homeTown, usermaster.currentAddress,
		 usermaster.faxNumber,usermaster.isVerified,usermaster.isActive,usermaster.registeredDate,
		 usermaster.countryId,IFNULL(countrymaster.nameEnglish,"") as CountryNameEN,IFNULL(countrymaster.nameArabic,"") as CountryNameAR,
		 usermaster.profileTypeId,IFNULL(profiletypemaster.nameEnglish,"") as ProfileNameEN,IFNULL(profiletypemaster.nameArabic,"") as ProfileNameAR,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
		 (COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
		$this->db->from('usermaster');
		$this->db->join('countrymaster','usermaster.countryId = countrymaster.Id','left');
		$this->db->join('profiletypemaster','usermaster.profileTypeId = profiletypemaster.Id','left');
		$this->db->join('likemaster','likemaster.fileId = usermaster.Id and likemaster.type = '.USER_TYPE,'left');
		$this->db->join('favouritemaster','likemaster.fileId = usermaster.Id and favouritemaster.type = '.USER_TYPE,'left');

		$whereStr = "(usermaster.status = 0 AND usermaster.isActive = 1) AND (usermaster.fullName like '%".$keyword."%' OR usermaster.userName like '%".$keyword."%' OR usermaster.email like '%".$keyword."%' OR
		usermaster.mobileNumber like '%".$keyword."%' OR usermaster.homeTown like '%".$keyword."%' OR usermaster.currentAddress like '%".$keyword."%'";

		$keywordSearch = "";
		if($lang == "en"){
			$keywordSearch = " OR countrymaster.nameEnglish like '%".$keyword."%' OR profiletypemaster.nameEnglish like '%".$keyword."%')";
		}else{
			$keywordSearch = " OR countrymaster.nameArabic like '%".$keyword."%' OR profiletypemaster.nameArabic like '%".$keyword."%')";
		}

		$whereStr .= $keywordSearch;
		$this->db->where($whereStr);
		$this->db->group_by("usermaster.Id");
		$this->db->order_by("Total","desc");

		$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		return $this->displaySearchUserList($query->result(),$lang,$userId);
	}

	/* Method to display Search users List
		 Created By: Nishit Patel
	*/
	public function displaySearchUserList($result,$lang,$userId){
		$arrayObjectClient = null;
		if($result != null){
			$this->load->model("ApiUserModel","usmodel");
			foreach($result as $row){

				$searchUserId = (int)$row->Id;

				if($searchUserId == $userId){
					continue;
				}

				$arrayObject = null;
					if ($lang == 'ar') {
						$countryName = $row->CountryNameAR;
						$ProfileTitle = $row->ProfileNameAR;

					}else{
						$countryName = $row->CountryNameEN;
						$ProfileTitle = $row->ProfileNameEN;
					}
					$RegistrationDate = $this->timeAgo($row->registeredDate);
					$profileUrl = "";
					$profileImage = $row->profileImage;
					if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
					}

					$activities = $this->usmodel->getUserActivity($searchUserId,USER_TYPE,$userId);

					$arrayObject  = array(
										"UserId"=>$searchUserId,
										"FullName"=>$row->fullName,
										"UserName"=>$row->userName,
										"Email"=>$row->email,
										"ProfileThumbImage"=> $profileUrl,
										"MobileNumber"=>$row->mobileNumber,
										"HomeTown"=>$row->homeTown,
										"CurrentAddress"=>$row->currentAddress,
										"FaxNumber"=>$row->faxNumber,
										"IsVerified"=>(bool)$row->isVerified,
										"IsActive"=>(bool)$row->isActive,
										"RegistrationDate"=>$RegistrationDate,
										"CountryId"=>(int)$row->countryId,
										"CountryName"=>$countryName,
										"ProfileTypeId"=>(int)$row->profileTypeId,
										"ProfileTitle"=>$ProfileTitle,
										"Activity"=>$activities
										);

				$arrayObjectClient[]=$arrayObject;
				//return $arrayObject;
			}
		}
		return $arrayObjectClient;
	}

	/* Method to get user object by user Id
		 Created By: Nishit Patel
	*/
	public function getUserSmallObject($userId){
		$this->db->select("Id,userName,profileImage,profileThumb");
		$this->db->from("usermaster");
		$this->db->where('Id',$userId);
		$query = $this->db->get();
		$result = $query->result();
		$userObject = null;
		if($result != null){
			foreach ($result as $row) {
				$profileUrl = "";
				$profileThumbUrl = "";
				$profileImage = $row->profileImage;
				$profileThumbImage = $row->profileThumb;
				if($profileImage != null){
					$profileUrl = PROFILE_URL.$profileImage;
				}
				if($profileThumbImage != null){
					$profileThumbUrl = PROFILE_THUMB_URL.$profileThumbImage;
				}
				$userObject = array("Id"=>(int)$row->Id,"UserName"=>$row->userName,
				"ProfileImage"=> $profileUrl,
				"ProfileThumbImage"=> $profileThumbUrl);
			}
		}
		return $userObject;
	}

	/****************** User Activity for HoleNet Start ************************/
	/* Method to get user activity
		 Created By: Nishit Patel
	*/
	public function getUserActivity($fileId,$type,$userId){

		$abuseObject = $this->getAbuseReportForUser($fileId,$type,$userId);
		$favouriteObject = $this->getFavouriteByUser($fileId,$type,$userId);
		$likeObject = $this->getLikesByUser($fileId,$type,$userId);
		$rateObject = $this->getRatingByUser($fileId,$type,$userId);
		$watchedObject = $this->getWatchedByUser($fileId,$type,$userId);
		$sharedObject = $this->getSharedContentByUser($fileId,$type,$userId);
		$activities = array('Abuse'=>$abuseObject,'Favourite'=>$favouriteObject,'Like'=>$likeObject,'Rate'=>$rateObject,'Watch'=>$watchedObject,'Shared'=>$sharedObject);

		return $activities;
	}
	/* Method to get shared content by user
     Created By: Nishit Patel
  */
  public function getSharedContentByUser($fileId,$type,$userId){

      $this->db->select('Id,sharedDate');
      $this->db->from('sharedmaster');
      $this->db->where('userId',$userId);
      $this->db->where('type',$type);
      $this->db->where('fileId',$fileId);
      $query = $this->db->get();
      $result = $query->result();
      $abuseObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->sharedDate);
          $abuseObject = array("Id"=>(int)$row->Id,"SubmittedDate"=>$displayDate);
        }
      }
      return $abuseObject;
  }
	/* Method to get favourite by user
     Created By: Nishit Patel
  */
  public function getFavouriteByUser($fileId,$type,$userId){

      $this->db->select('Id,favouriteDate');
      $this->db->from('favouritemaster');
      $this->db->where('userId',$userId);
      $this->db->where('type',$type);
      $this->db->where('fileId',$fileId);
      $query = $this->db->get();
      $result = $query->result();
      $abuseObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->favouriteDate);
          $abuseObject = array("Id"=>(int)$row->Id,"SubmittedDate"=>$displayDate);
        }
      }
      return $abuseObject;
  }
	/* Method to get watched content for user
		 Created By: Nishit Patel
	*/
	public function getWatchedByUser($fileId,$type,$userId){

			$this->db->select('Id,watchedDate');
			$this->db->from('videowatchedmaster');
			$this->db->where('userId',$userId);
			$this->db->where('type',$type);
			$this->db->where('fileId',$fileId);
			$query = $this->db->get();
			$result = $query->result();
			$abuseObject = null;
			if($result != null){
				$this->load->model("Utility","utility");
				foreach ($result as $row) {
					$displayDate = $this->utility->timeAgoFormat($row->watchedDate);
					$abuseObject = array("Id"=>(int)$row->Id,"SubmittedDate"=>$displayDate);
				}
			}
			return $abuseObject;
	}
	/* Method to get likes for user
		 Created By: Nishit Patel
	*/
	public function getLikesByUser($fileId,$type,$userId){

			$this->db->select('Id,status,likeDate');
			$this->db->from('likemaster');
			$this->db->where('userId',$userId);
			$this->db->where('type',$type);
			$this->db->where('fileId',$fileId);
			$query = $this->db->get();
			$result = $query->result();
			$abuseObject = null;
			if($result != null){
				$this->load->model("Utility","utility");
				foreach ($result as $row) {
					$displayDate = $this->utility->timeAgoFormat($row->likeDate);
					$abuseObject = array("Id"=>(int)$row->Id,"Status"=>(int)$row->status,"SubmittedDate"=>$displayDate);
				}
			}
			return $abuseObject;
	}
	/* Method to get rating for user
		 Created By: Nishit Patel
	*/
	public function getRatingByUser($fileId,$type,$userId){

			$this->db->select('Id,rate,rateDate');
			$this->db->from('videoratingmaster');
			$this->db->where('userId',$userId);
			$this->db->where('videoId',$fileId);
			$query = $this->db->get();
			$result = $query->result();
			$abuseObject = null;
			if($result != null){
				$this->load->model("Utility","utility");
				foreach ($result as $row) {
					$displayDate = $this->utility->timeAgoFormat($row->rateDate);
					$abuseObject = array("Id"=>(int)$row->Id,"Rate"=>(int)$row->rate,"SubmittedDate"=>$displayDate);
				}
			}
			return $abuseObject;
	}
	/* Method to get Abuse report for user
		 Created By: Nishit Patel
	*/
	public function getAbuseReportForUser($fileId,$type,$userId){

			$this->db->select('Id,reason,abuseDate');
			$this->db->from('abusemaster');
			$this->db->where('userId',$userId);
			$this->db->where('type',$type);
			$this->db->where('fileId',$fileId);
			$query = $this->db->get();
			$result = $query->result();
			$abuseObject = null;
			if($result != null){
				$this->load->model("Utility","utility");
				foreach ($result as $row) {
					$displayDate = $this->utility->timeAgoFormat($row->abuseDate);
					$abuseObject = array("Id"=>(int)$row->Id,"Reason"=>$row->reason,"SubmittedDate"=>$displayDate);
				}
			}
			return $abuseObject;
	}
	/****************** User Activity for HoleNet END ************************/
	/* Method to get total pages for users by name
		 Created By: Nishit Patel
	*/
	public function getTotalPagesForUserByName($userId,$keyword){
		$whereStr = "Id <> ".$userId." AND (fullName like '%".$keyword."%' OR userName like '%".$keyword."%')";
		$this->db->select("Id");
		$this->db->from("usermaster");
		$this->db->where($whereStr);
		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);
	}

	/* Method to get Favourite Videos by User Id
		Created By: Nishit Patel
	*/
	public function getUsersByName($userId,$keyword,$PageIndex){
		$whereStr = "Id <> ".$userId." AND (fullName like '%".$keyword."%' OR userName like '%".$keyword."%')";
		$this->db->select("Id,userName,profileImage,profileThumb");
		$this->db->from("usermaster");
		$this->db->where($whereStr);
		$this->db->order_by("userName","DESC");
		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		return $this->displayUsersByName($query->result(),$userId);
	}

	public function displayUsersByName($result,$userId){
		$this->load->model("Utility","utility");
		$this->load->model("ApiVideoModel","vModel");
		$this->load->model("ApiImageModel","iModel");
		$this->load->model("ApiPostModel","pModel");
		$this->load->model("ApiPlaylistModel","plModel");
		$this->load->model("ApiFavouriteModel","fvrtmodel");

		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
				$arrayObject = null;

				$fileId = (int)$row->Id;

				$totalVideos = $this->vModel->getTotalVideosOfUser($fileId);
				$totalImages = $this->iModel->getTotalImagesForUser($fileId);
				$totalPost = $this->pModel->getTotalPostOfUser($fileId);
				$totalPlayList = $this->plModel->getTotalPlayListOfUser($fileId);

				$totalFollowers = $this->fvrtmodel->getTotalFollowers($fileId);
				$totalFollowing = $this->fvrtmodel->getTotalFollowingUsers($fileId);

				$totalObject = array("TotalFollowers"=>$totalFollowers,"TotalFollowing"=>$totalFollowing,"TotalVideos"=>$totalVideos,"TotalImages"=>$totalImages,"TotalPost"=>$totalPost,"TotalPlayList"=>$totalPlayList);
				$profileUrl = "";
				$profileImage = $row->profileImage;
				if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
				}

				$activities = $this->getUserActivity((int)$row->Id,USER_TYPE,$userId);
				$userObject = array("Id"=>(int)$row->Id,"UserName"=>$row->userName,"ProfileThumbImage"=> $profileUrl,"Totals"=>$totalObject,"Activity"=>$activities);

				$arrayObjectClient[]=$userObject;

			}
		}

		return $arrayObjectClient;
	}

	/* Method to get Popular Users
		 Created By: Nishit Patel
	*/
	public function getPopularUsers($userId,$PageIndex){
		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}

		$query = $this->db->query('
			select usermaster.Id,usermaster.userName,usermaster.profileImage,usermaster.profileThumb,
			(select count(Id) from likemaster where likemaster.fileId = usermaster.Id AND likemaster.type =4) as TotalLikes,
			(select count(Id) from favouritemaster where favouritemaster.fileId = usermaster.Id and favouritemaster.type = 4) as TotalFavourites,
			((select count(Id) from likemaster where likemaster.fileId = usermaster.Id AND likemaster.type =4) +
			(select count(Id) from favouritemaster where favouritemaster.fileId = usermaster.Id and favouritemaster.type = 4)) as Totals
			from usermaster where usermaster.isActive = 1
			order by Totals DESC LIMIT '.$pageNo.',20');

		return $this->displayUsersByName($query->result(),$userId);
	}

	/* Method to get Total pages for Popular Users
		 Created By: Nishit Patel
	*/
	public function getTotalPagesPopularUsers($userId,$PageIndex){
		$query = $this->db->query('
			select usermaster.Id,usermaster.userName,usermaster.profileImage,usermaster.profileThumb,
			(select count(Id) from likemaster where likemaster.fileId = usermaster.Id AND likemaster.type =4) as TotalLikes,
			(select count(Id) from favouritemaster where favouritemaster.fileId = usermaster.Id and favouritemaster.type = 4) as TotalFavourites,
			((select count(Id) from likemaster where likemaster.fileId = usermaster.Id AND likemaster.type =4) +
			(select count(Id) from favouritemaster where favouritemaster.fileId = usermaster.Id and favouritemaster.type = 4)) as Totals
			from usermaster where usermaster.isActive = 1
			order by Totals DESC');
			$result = $query->num_rows();
			$totalPages  = $result / 20;
			$pages = round($totalPages);
			if($pages < 1){
				return 1;
			}
			return round($totalPages);
	}


}
?>
