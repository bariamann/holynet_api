<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiShareModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}


	public function share($userId,$fileId,$type,$sharedIds){
			$this->removeShare($fileId,$type,$userId);
			$data = array(
				'userId' => $userId,
				'fileId' => $fileId,
				'type' => $type
				);
			$this->db->insert('sharedmaster', $data);
			$shareMasterId = $this->db->insert_id();
			$result = $this->saveDetails($shareMasterId,$sharedIds,$userId,$fileId,$type);

			if($result){
				$this->load->model("ApiNotificationModel","notificationmodel");
				//$this->notificationmodel->sendNotificationForSharedContent($fileId,$type,$userId,$shareMasterId);
			}
			return $result;
		}

	public function getTotalShare($fileId,$type){
			$this->db->select('shareddetails.Id');
			$this->db->from('shareddetails');
			$this->db->join('sharedmaster','shareddetails.sharedMasterId = sharedmaster.Id','inner');
			$this->db->where('sharedmaster.fileId',$fileId);
			$this->db->where('sharedmaster.type',$type);
			$query = $this->db->count_all_results();
			return ceil($query);
	}

	public function saveDetails($sharedMasterId,$sharedIds,$myUserId,$fileId,$type){
			if ($sharedIds != null) {
				$UserIds = explode(',', $sharedIds);
				foreach ($UserIds as $UserId)
				{
					$isAlready = $this->isAlreadyShareWithUser($UserId,$sharedMasterId);
					if (!$isAlready)
					{
						$data = array(
									'userId' => $UserId,
									'sharedMasterId' => $sharedMasterId
									);
						$this->db->insert('shareddetails', $data);

						//send notification
						$this->load->model("ApiNotificationModel","notificationmodel");
						$this->notificationmodel->sendNotificationForSharedContent($fileId,$type,$myUserId,$UserId);

					}

				}
			}
			return true;
		}
		public function isAlreadyShareWithUser($userId,$sharedMasterId){
					$this->db->select('Id');
					$this->db->from('shareddetails');
					$this->db->where('userId',$userId);
					$this->db->where('sharedMasterId',$sharedMasterId);
					$query = $this->db->get();
					if ($query->num_rows() > 0){
						return true;
					}else{
						return false;
					}

				}

		public function removeShare($fileId,$type,$userId){
				$sharedmasterId =0;
				$this->db->select('Id');
				$this->db->from('sharedmaster');
				$strWhere = "type='".$type."' AND fileId='".$fileId."' AND userId='".$userId."'";
				$this->db->where($strWhere,null,false);
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					$result = $query->result();
					if ($result != null) {
						foreach($result as $row){
							$sharedmasterId = (int)$row->Id;
							$this->db->where('sharedMasterId', $sharedmasterId);
							$this->db->delete('shareddetails');
						}

					}
					//$this->db->where('sharedMasterId', $sharedmasterId);
					$strWhere = "type='".$type."' AND fileId='".$fileId."' AND userId='".$userId."'";
					$this->db->where($strWhere,null,false);
					$this->db->delete('sharedmaster');
					return true;
				}else{
					return false;
				}

		}
		public function deleteShare($fileId,$type){
				$sharedmasterId =0;
				$this->db->select('Id');
				$this->db->from('sharedmaster');
				$strWhere = "type='".$type."' AND fileId='".$fileId."' ";
				$this->db->where($strWhere,null,false);
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					$result = $query->result();
					if ($result != null) {
						foreach($result as $row){
							$sharedmasterId = (int)$row->Id;
							$this->db->where('sharedMasterId', $sharedmasterId);
							$this->db->delete('shareddetails');
						}

					}
					//$this->db->where('sharedMasterId', $sharedmasterId);
					$strWhere = "type='".$type."' AND fileId='".$fileId."' ";
					$this->db->where($strWhere,null,false);
					$this->db->delete('sharedmaster');
					return true;
				}else{
					return false;
				}

		}

		/* Method to get total pages shared content Data
			 Created By: Nishit Patel
		*/
		public function getTotalPageForSharedContent($userId){
			$query = $this->db->query('select videomaster.Id as fileId,1 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join videomaster on sharedmaster.fileId = videomaster.Id and sharedmaster.type=1 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT imagemaster.Id as fileId,2 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join imagemaster on sharedmaster.fileId = imagemaster.Id and sharedmaster.type=2 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT postmaster.Id as fileId,3 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join postmaster on sharedmaster.fileId = postmaster.Id and sharedmaster.type=3 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT usermaster.Id as fileId,4 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join usermaster on sharedmaster.fileId = usermaster.Id and sharedmaster.type=4 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT playlistmaster.Id as fileId,5 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join playlistmaster on sharedmaster.fileId = playlistmaster.Id and sharedmaster.type=5 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
ORDER BY Id DESC');
				$count = count($query->result());
				$result  = $count / 20;
				return ceil($result);
		}

		/* Method to get shared content Data
			 Created By: Nishit Patel
		*/
		public function getSharedContent($userId,$lang,$PageIndex){
				$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}
				$query = $this->db->query('select videomaster.Id as fileId,1 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join videomaster on sharedmaster.fileId = videomaster.Id and sharedmaster.type=1 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT imagemaster.Id as fileId,2 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join imagemaster on sharedmaster.fileId = imagemaster.Id and sharedmaster.type=2 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT postmaster.Id as fileId,3 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join postmaster on sharedmaster.fileId = postmaster.Id and sharedmaster.type=3 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT usermaster.Id as fileId,4 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join usermaster on sharedmaster.fileId = usermaster.Id and sharedmaster.type=4 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
UNION SELECT playlistmaster.Id as fileId,5 as type,sharedmaster.Id,sharedmaster.userId as SharedById,sharedmaster.sharedDate FROM sharedmaster inner join playlistmaster on sharedmaster.fileId = playlistmaster.Id and sharedmaster.type=5 inner join shareddetails on sharedmaster.Id = shareddetails.sharedMasterId where shareddetails.userId = '.$userId.'
ORDER BY Id DESC LIMIT '.$pageNo.',20');
				return $this->displaySharedContent($query->result(),$lang,$userId);
		}

		/* Method to display shared content
			 Created By: Nishit Patel
		*/
		public function displaySharedContent($result,$lang,$userId){
			$this->load->model("Utility","utility");
			$this->load->model("ApiVideoModel","vmodel");
			$this->load->model("ApiPostModel","pmodel");
			$this->load->model("ApiImageModel","imodel");
			$this->load->model("ApiFavouriteModel","fvmodel");
			$this->load->model("ApiUserModel","usmodel");

			$arrayObjectClient = null;
	    if($result != null){
	      foreach($result as $row){
	        $arrayObject = null;

					$sharedId = (int)$row->Id;
	        $fileId = (int)$row->fileId;
	        $type = (int)$row->type;
					$displayDate = $this->utility->timeAgoFormat($row->sharedDate);

	        $sharedTubes = null;
	        if($type == VIDEO_TYPE){
	          $lang = "en";
	          $sharedTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
	        }
	        if($type == POST_TYPE){
						$sharedTubes = $this->pmodel->getPostDetails($fileId,$userId);
	        }
	        if($type == IMAGE_TYPE){
	          $lang = "en";
	          $sharedTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
	        }
	        if($type == PLAYLIST_TYPE){
	          $sharedTubes = $this->fvmodel->getFavouritePlayListByPlaylistId($fileId,$userId);
	        }

					$userObject = $this->usmodel->getUserSmallObject((int)$row->SharedById);

					//$activities = $this->usmodel->getUserActivity($fileId,$type,$userId);

	        $arrayObject  = array(
	                      "SharedDate"=>$displayDate,
												"User"=>$userObject,
	                      "Type"=>$type,
	                      "Tubes" => $sharedTubes);

	        $arrayObjectClient[]=$arrayObject;
	      }
	    }
	    return $arrayObjectClient;
		}
	}
?>
