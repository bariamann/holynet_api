<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class Utility extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to get Current date
    Created By: Nishit Patel
  */
  public function getCurrentDate($format){
      $date = date($format, time());
      return $date;
  }

  /* Method to get Readable time format
    Created By: Nishit Patel
  */
  public function timeAgoFormat($time_ago){

      $time_ago = strtotime($time_ago);
		  $cur_time   = time();

      $d = $this->getCurrentDate("Y/m/d h:i:s");

			$cur_time = strtotime($d);

		  $time_elapsed   = $cur_time - $time_ago;
		  $seconds    = $time_elapsed ;
		  $minutes    = round($time_elapsed / 60 );
		  $hours      = round($time_elapsed / 3600);
		  $days       = round($time_elapsed / 86400 );
		  $weeks      = round($time_elapsed / 604800);
		  $months     = round($time_elapsed / 2600640 );
		  $years      = round($time_elapsed / 31207680 );
		  // Seconds
		  if($seconds <= 60){
		      return "just now";
		  }
		  //Minutes
		  else if($minutes <=60){
		      if($minutes==1){
		          return "one minute ago";
		      }else{
		          return "$minutes minutes ago";
		      }
		  }
		  //Hours
		  else if($hours <=24){
		      if($hours==1){
		          return "an hour ago";
		      }else{
		          return "$hours hrs ago";
		      }
		  }
		  //Days
		  else if($days <= 7){
		      if($days==1){
		          return "yesterday";
		      }else{
		          return "$days days ago";
		      }
		  }
		  //Weeks
		  else if($weeks <= 4.3){
		      if($weeks==1){
		          return "a week ago";
		      }else{
		          return "$weeks weeks ago";
		      }
		  }
		  //Months
		  else if($months <=12){
		      if($months==1){
		          return "a month ago";
		      }else{
		          return "$months months ago";
		      }
		  }
		  //Years
		  else{
		      if($years==1){
		          return "one year ago";
		      }else{
		          return "$years years ago";
		      }
		  }
	}

  /* Method to Convert bytes into Readable size form
     Created By: Nishit Patel
  */
  function formatSizeUnits($bytes){

      if ($bytes >= 1073741824){
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
      }elseif ($bytes >= 1048576){
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
      }elseif ($bytes >= 1024){
        $bytes = number_format($bytes / 1024, 2) . ' KB';
      }elseif ($bytes > 1){
        $bytes = $bytes . ' bytes';
      }elseif ($bytes == 1){
        $bytes = $bytes . ' byte';
      }else{
        $bytes = '0 bytes';
      }
      return $bytes;
	}


}


?>
