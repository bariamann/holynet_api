<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

	class ApiProfileModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

	public function getProfiles($lang){
			$this->db->select('Id,nameEnglish,nameArabic');
			$this->db->from('profiletypemaster');

			if ($lang == 'ar') {
				$this->db->order_by('nameArabic', 'ASC');
			}else{
				$this->db->order_by('nameEnglish', 'ASC');
			}
			$query = $this->db->get();
			
			return $this->displayProfiles($query->result(),$lang);
		}
	public function displayProfiles($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					if ($lang == 'ar') {
						$profileName =$row->nameArabic;
					}else{
						$profileName = $row->nameEnglish;
					}
					$arrayObject  = array(
											"Id"=>(int)$row->Id,
											"ProfileTitle"=>$profileName,);						

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		public function createProfileType($NameEnglish,$NameArabic){				
			$data = array(
				'nameEnglish' => $NameEnglish ,
				'nameArabic' => $NameArabic
				);
			$this->db->insert('profiletypemaster', $data);
			return true;

		}

		public function deleteProfileType($Id){
				$this->db->where('Id', $Id);
				$this->db->delete('profiletypemaster'); 
			   	return true;

		}

		

	}
?>