<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiPostModel extends CI_Model {

	public function _construct(){
		parent::_construct();
	}

	/* Method get Total Post of User
		Created By: Nishit Patel
	*/
	public function getTotalPostOfUser($userId){

		$this->db->select("count(contentmaster.Id) as TotalPosts");
		$this->db->from("contentmaster");
		$this->db->join("postmaster","contentmaster.Id = postmaster.contentId","inner");
		$this->db->where("contentmaster.userId",$userId);
		$query = $this->db->get();
		$result = $query->result();
		$totalValue = 0;
		if($result != null){
			foreach ($result as $row) {
				$totalValue = (int)$row->TotalPosts;
			}
		}
		return $totalValue;

	}

	public function countTotalPost(){
		$this->db->select('Id');
		$this->db->from('postmaster');
		$query = $this->db->count_all_results();
		return ceil($query);
	}
	public function countTotalAbusePost(){
		$this->db->select('Id');
		$this->db->from('abusemaster');
		$this->db->where('type', 3);
		$query = $this->db->count_all_results();
		return ceil($query);
	}

	public function savePost($description,$privacy,$isCommentable,$userId,$sharedIds,$contentId){

		$data = array(
			'decription' => $description,
			'privacy' => $privacy,
			'isCommentable' => $isCommentable,
			'contentId' => $contentId
			);
		$this->db->insert('postmaster', $data);
		$fileId = $this->db->insert_id();
		    if ($sharedIds != null) {
			    if ($privacy != 3 &&  $privacy != -1) {
					$type = 3; // Post
					$this->load->model("ApiShareModel","smodel");
					$result = $this->smodel->share($userId,$fileId,$type,$sharedIds);
					//return $result;
				}
			}
			return $this->getPostDetails($fileId,$userId);
		}
	/* Method to delete post  from post master only
		 Created By: Manzz Baria
		 Note  : You can not user this function direct,
		 Please use deleteResource function from ApiContentModel class
	*/
		public function deletePost($postId){
				$this->db->where('Id', $postId);
				$this->db->delete('postmaster');
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}

			}

				public function getContentIdFromPost($postId){
					$ContentId = 0;
					$this->db->select('contentId');
					$this->db->from('postmaster');
					$strWhere = "Id='".$postId."'";
					$this->db->where($strWhere,null,false);
					$query = $this->db->get();
					$result = $query->result();
					if ($result != null) {
						foreach($result as $row){
							$ContentId = (int)$row->contentId;
						}
					}
					return $ContentId;
				}
				public function isPostAvailableForContent($contentId){
					$this->db->select('Id');
					$this->db->from('postmaster');
					$strWhere = "contentId='".$contentId."'";
					$this->db->where($strWhere,null,false);
					$query = $this->db->get();
					if ($query->num_rows() > 0){
						return true;
					}
					return false;
				}

	public function updatePost($description,$privacy,$isCommentable,$SharedIds,$userId,$postId){
					$contentId = $this->getContentIdFromPost($postId);
					$this->load->model("ApiContentModel","cmodel");
					$isAccess = $this->cmodel->isAccessContent($contentId,$userId);
					if ($isAccess) {

						if ($description != '-1') {
							$data = array(
								'decription' => $description
								);
							$this->db->where('Id', $postId);
							$this->db->update('postmaster', $data);
						}
						if ($privacy != -1) {
							$data = array(
								'privacy' => $privacy
								);
							$this->db->where('Id', $postId);
							$this->db->update('postmaster', $data);
						}
						if ($isCommentable != -1) {
							$data = array(
								'isCommentable' => $isCommentable
								);
							$this->db->where('Id', $postId);
							$this->db->update('postmaster', $data);
						}

						if ($SharedIds != '-1') {
							if ($privacy != 3 &&  $privacy != -1)
							{
								$type = 3; // Post
								$this->load->model("ApiShareModel","smodel");
								$result = $this->smodel->share($userId,$postId,$type,$SharedIds);
								//return $result;
							}
						}
						return $this->getPostDetails($postId,$userId);;
					}
					return null;
	}


		public function getTotalPageOfPostList($userId){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->order_by("contentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		/* Method to get total pages for get post List
			 Created By: Nishit Patel
		*/
		public function getTotalPagesForGetPostList($userId){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			//$this->db->where('postmaster.privacy',1);
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->where('usermaster.status',0);

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		public function getPostList($PageIndex,$userId,$myUserId = 0){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			//$this->db->where('postmaster.privacy',1);
			if ($userId != -1) {
				$this->db->where('contentmaster.userId',$userId);
			}
			$this->db->order_by("contentmaster.Id","desc");
			//$this->db->order_by("contentmaster.Id","asc");
			/*if ($isVerified != -1) {
				$this->db->where('postmaster.isVerified',$isVerified);
			}*/

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayPostList($query->result(),$myUserId);
		}

	public function displayPostList($result,$userId){
			$arrayObjectClient = null;
			if($result != null){
				$this->load->model("ApiCommentModel","cmtmodel");
				$this->load->model("ApiLikeModel","lkmodel");
				$this->load->model("ApiShareModel","shmodel");
				$this->load->model("ApiAbuseModel","abmodel");
				$this->load->model("ApiContentModel","cmodel");
				$this->load->model("ApiUserModel","usmodel");
				foreach($result as $row){
					$arrayObject = null;
					$arrayUser= null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0; $totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0;
						$type = 3;



						$totalComments = $this->cmtmodel->getTotalComment($row->PostId,$type);

						$totalLikes = $this->lkmodel->getTotalLike($row->PostId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->PostId,$type);

						$totalShared = $this->shmodel->getTotalShare($row->PostId,$type);

						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->PostId,$type);

						$activities = $this->usmodel->getUserActivity($row->PostId,POST_TYPE,$userId);

						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport
											);

						$arrayObject  = array(
											"PostId"=>(int)$row->PostId,
											"Description"=>$row->decription,
											"Privacy"=>(int)$row->privacy,
											"isCommentable"=>(bool)$row->isCommentable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal,
											"Activity"=>$activities
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}



	public function getPostDetails($postId,$userId){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->where('postmaster.Id',$postId);
			$this->db->where('usermaster.status',0);

			$query = $this->db->get();
			return $this->displayPostDetails($query->result(),$userId);
	}

		public function displayPostDetails($result,$userId){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					$arrayUser= null;
					$arrayTotal = null;


						$totalComments = 0; $totalLikes =0; $totalDislikes = 0; $totalShared = 0;
						$totalAbuseReport = 0; $totalWatchedCount = 0;
						$type = 3;

						$this->load->model("ApiCommentModel","cmtmodel");
						$totalComments = $this->cmtmodel->getTotalComment($row->PostId,$type);

						$this->load->model("ApiLikeModel","lkmodel");
						$totalLikes = $this->lkmodel->getTotalLike($row->PostId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->PostId,$type);

						$this->load->model("ApiShareModel","smodel");
						$totalShared = $this->smodel->getTotalShare($row->PostId,$type);

						$this->load->model("ApiAbuseModel","abmodel");
						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->PostId,$type);

						$this->load->model("ApiWatchedModel","wmodel");
						$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->PostId,$type);

 						$this->load->model("ApiContentModel","cmodel");
						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);


						$this->load->model("ApiUserModel","usmodel");
						$activities = $this->usmodel->getUserActivity($row->PostId,POST_TYPE,$userId);

						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalWatched"=>(int)$totalWatchedCount
											);
						$arrayObject  = array(
											"PostId"=>(int)$row->PostId,
											"Description"=>$row->decription,
											"Privacy"=>(int)$row->privacy,
											"isCommentable"=>(bool)$row->isCommentable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal,
											"Activity"=>$activities
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		/* Method to get favourite post
			 Created By: Nishit Patel
		*/
		public function getFavrouitePostDetails($postId){
				$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
					contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
					usermaster.userName,usermaster.profileImage');
				$this->db->from('postmaster');
				$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
				$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
				$this->db->where('postmaster.Id',$postId);

				$query = $this->db->get();
				return $this->displayFavouritePostDetails($query->result());
		}

		/* Method to display favourite post detail
		*/
		public function displayFavouritePostDetails($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					$arrayUser= null;
					$arrayTotal = null;


						$totalComments = 0; $totalLikes =0; $totalDislikes = 0; $totalShared = 0;
						$totalAbuseReport = 0;
						$type = 3;

						$this->load->model("ApiCommentModel","cmtmodel");
						$totalComments = $this->cmtmodel->getTotalComment($row->PostId,$type);

						$this->load->model("ApiLikeModel","lkmodel");
						$totalLikes = $this->lkmodel->getTotalLike($row->PostId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->PostId,$type);

						$this->load->model("ApiShareModel","smodel");
						$totalShared = $this->smodel->getTotalShare($row->PostId,$type);

						$this->load->model("ApiAbuseModel","abmodel");
						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->PostId,$type);

 						$this->load->model("ApiContentModel","cmodel");
						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport
											);
						$arrayObject  = array(
											"PostId"=>(int)$row->PostId,
											"Description"=>$row->decription,
											"Privacy"=>(int)$row->privacy,
											"isCommentable"=>(bool)$row->isCommentable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		public function getSearchPostByKeyword($userId,$keyword,$lang,$PageIndex){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
				(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('likemaster','likemaster.fileId = postmaster.Id and likemaster.type = '.POST_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = postmaster.Id and favouritemaster.type = '.POST_TYPE,'left');
			$whereStr = "usermaster.status=0 AND postmaster.decription like '%".$keyword."%'";
			$this->db->where($whereStr);
			$this->db->group_by("postmaster.Id");
			$this->db->order_by("Total","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayPostList($query->result(),$userId);
		}

		/* Method to get total pages for search keyword for post
			 Created By: Nishit Patel
		*/
		public function getTotalPageSearchPostByKeyword($userId,$keyword,$lang,$PageIndex){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,COUNT(likemaster.Id) as totalLikes,COUNT(favouritemaster.Id) as totalFavourites,
				(COUNT(likemaster.Id) + COUNT(favouritemaster.Id)) as Total');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('likemaster','likemaster.fileId = postmaster.Id and likemaster.type = '.POST_TYPE,'left');
			$this->db->join('favouritemaster','likemaster.fileId = postmaster.Id and favouritemaster.type = '.POST_TYPE,'left');
			$whereStr = "postmaster.decription like '%".$keyword."%'";
			$this->db->where($whereStr);
			$this->db->group_by("postmaster.Id");
			$this->db->order_by("Total","desc");
			$query = $this->db->get();
			$result = $query->num_rows();
			$totalPages  = $result / 20;

			$whole = floor($totalPages);      // 1
			$fraction = $totalPages - $whole; // .25

			if($fraction > 0){
				$total =  $whole + 1;
			}else{
				$total =  $whole;
			}

			return round($total);
		}

		/* Method to get related post list by post id
			 Created By: Nishit Patel
		*/
		public function getRelatedPostList($postId,$pageIndex){
			$this->db->select('postmaster.Id,postmaster.decription,contentmaster.Id,contentmaster.userId');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','contentmaster.Id = postmaster.contentId','inner');
			$this->db->where('postmaster.Id',$postId);
			$query = $this->db->get();
			$result = $query->result();

			$userId = 0;
			if($result != null){
				foreach ($result as $row) {
					$userId = (int)$row->userId;
				}
			}

			$totalPage = 20;
			$recordCount = $totalPage;
			$resultArray = $this->getRelatedPostBaseOnUserKeywordType($pageIndex,$userId,$postId,$recordCount);
			return $resultArray;
		}

		/* Method to get video list according playlist
			 Created By: Nishit Patel
		*/
		public function getRelatedPostBaseOnUserKeywordType($PageIndex,$userId,$postIds,$recordCount){
			$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('postmaster');
			$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$whereStr = "contentmaster.userId=".$userId." AND postmaster.Id NOT IN (".$postIds.")";
			$this->db->where($whereStr);
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->order_by("postmaster.Id","DESC");
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayPostDetails($query->result(),0);
		}

}
?>
