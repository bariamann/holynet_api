<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiWatchedModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

    /* Method to check is watched video exist
       Created By: Nishit Patel
    */
    public function checkIsWatchedVideoExist($fileId,$type,$userId){
      $dataArray = "fileId=".$fileId." AND type=".$type." AND userId=".$userId;
  		$this->db->where($dataArray);
  	  $query = $this->db->get('videowatchedmaster');
  	  $count_row = $query->num_rows();
  	  if ($count_row > 0) {
  	     return TRUE;
  	  } else {
  	     return FALSE;
  	  }
    }

    /* Method to add watched Video
       Created By: Nishit Patel
    */
    public function addWatchedFile($fileId,$type,$userId){

      $this->load->model("Utility","utility");
      $watchedDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
  		$displayDate = $this->utility->timeAgoFormat($watchedDate);

  		$data = array('fileId' => $fileId,'type' => $type,'userId' => $userId,'watchedDate'=>$watchedDate);
  	  $this->db->insert('videowatchedmaster', $data);
  	  $insert_Id = $this->db->insert_id();
  	  $likeData = array('Id' => (int)$insert_Id,'UserId'=>(int)$userId,'FileId'=>(int)$fileId,'Type'=>(int)$type,'Date'=>$displayDate);
  	  return $likeData;

    }

		/* Method to get total watched count
			 Crated By: Nishit Patel
		*/
		public function getTotalWatchOut($fileId,$type){
			$this->db->select('Id');
			$this->db->from('videowatchedmaster');
			$this->db->where('fileId',$fileId);
			$this->db->where('type',$type);
			$query = $this->db->count_all_results();
			return ceil($query);
		}

		/* Method to get total pages for watched users
			 Created By: Nishit Patel
		*/
		public function getTotalPagesWatchedUser($fileId,$type){
			$this->db->select('Id');
			$this->db->from('videowatchedmaster');
			$this->db->where('fileId',$fileId);
			$this->db->where('type',$type);
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		/* Method to get list of watched users
			 Created By: Nishit Patel
		*/
		public function getWatchedUsersList($fileId,$type,$PageIndex,$userId){
			$this->db->select('videowatchedmaster.Id,videowatchedmaster.watchedDate,
				videowatchedmaster.userId,usermaster.username,usermaster.profileImage');
			$this->db->from('videowatchedmaster');
			$this->db->join('usermaster','videowatchedmaster.userId = usermaster.Id','inner');
			$this->db->where('videowatchedmaster.type',$type);
			$this->db->where('videowatchedmaster.fileId',$fileId);
			$this->db->order_by("videowatchedmaster.Id","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayWatchedUsersList($query->result(),$userId);
		}

		/* Method to display watched json format
			 Created By: Nishit Patel
		*/
		public function displayWatchedUsersList($result,$userId){
				$this->load->model("Utility","utility");
				$this->load->model("ApiUserModel","usmodel");

				$arrayObjectClient = null;
				if($result != null){
					foreach($result as $row){
						$arrayObject = null;

							$displayDate = $this->utility->timeAgoFormat($row->watchedDate);
							$profileUrl = "";
							$profileImage = $row->profileImage;
							if($profileImage != null){
								$profileUrl = PROFILE_URL.$profileImage;
							}
							$activities = $this->usmodel->getUserActivity((int)$row->userId,USER_TYPE,$userId);
							$userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->username,"ProfileThumbImage"=>$profileUrl,"Activity"=>$activities);

							$arrayObject  = array(
												"Id"=>(int)$row->Id,
												"LikeDate"=>$displayDate,
												"User"=>$userObject
												);

						$arrayObjectClient[]=$arrayObject;
						//return $arrayObject;
					}
				}
				return $arrayObjectClient;
		}

		/* Method to get user watched content
			 Created By: Nishit Patel
		*/
		public function getTotalPageWatchedContent($userId){
			$this->db->select('Id');
			$this->db->from('videowatchedmaster');
			$this->db->where('userId',$userId);
			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

		/* Method to get user watched content
			 Created By: Nishit Patel
		*/
		public function getWatchedContent($userId,$PageIndex){
			$this->db->select('videowatchedmaster.Id,videowatchedmaster.watchedDate,videowatchedmaster.fileId,videowatchedmaster.type');
			$this->db->from('videowatchedmaster');
			$this->db->join('usermaster','videowatchedmaster.userId = usermaster.Id','inner');
			$this->db->where('videowatchedmaster.userId',$userId);
			$this->db->order_by("videowatchedmaster.Id","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayUserWatched($query->result(),$userId);
		}

		/* Method to display user watched content
			 Created By: Nishit Patel
		*/
		private function displayUserWatched($result,$userId){
			$this->load->model("Utility","utility");
			$arrayObjectClient = array();
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

						$displayDate = $this->utility->timeAgoFormat($row->watchedDate);
						$fileId = (int)$row->fileId;
						$type = (int)$row->type;

						$watchedTubes = null;
						if($type == VIDEO_TYPE){
							$this->load->model("ApiVideoModel","vmodel");
							$lang = "en";
							$watchedTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
						}
						if($type == POST_TYPE){
							$this->load->model("ApiPostModel","pmodel");
							$watchedTubes = $this->pmodel->getPostDetails($fileId,$userId);
						}
						if($type == IMAGE_TYPE){
							$this->load->model("ApiImageModel","imodel");
							$lang = "en";
							$watchedTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
						}
						if($type == PLAYLIST_TYPE){
							$this->load->model("ApiFavouriteModel","fmodel");
							$watchedTubes = $this->fmodel->getFavouritePlayListByPlaylistId($fileId,$userId);
						}

						$arrayObject  = array(
											"Id"=>(int)$row->Id,
											"LikeDate"=>$displayDate,
											"Type"=>$type,
											"WatchedTube"=>$watchedTubes
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
		}

	/* Method to delete Watched by file
		 Created By: Manzz Baria
	*/
	public function deleteWatchedByFile($fileId,$type){
		$whereStr = "fileId=".$fileId." AND type=".$type;
		$this->db->where($whereStr);
		$this->db->delete('videowatchedmaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}



}
?>
