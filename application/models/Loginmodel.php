<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmodel extends CI_Model 
{
	private function encrypt($text) 
		{ 
			$SALT ='whateveryouwantwhateveryouwant'.'\0'; 
		    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
		} 

		/* DECRYPT String
			Return decrypted string
			Created By: Nishit Patel
		*/
		private function decrypt($text) 
		{ 
			$SALT ='whateveryouwantwhateveryouwant'.'\0'; 
		    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
		} 
		
			public function loginUser($user,$password){
			
			 $encryptedPass = $this->encrypt($password);
				
					$this->db->select('Id, fullName, email, userName,password');
					$this->db->from('adminmaster');			
					$this->db->where('userName',$user);
					$this->db->where('password',$encryptedPass);
					$query = $this->db->get();
					if ($query->num_rows() > 0)
					{
						return $this->displayUser($query->result());
					}
					else
					{
						return null;
					}
					
				}
				
				public function displayUser($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
						//$dateAgo = $this->timeAgo($row->DateTime);
						$arrayObject  = array(
											"Id"=>(int)$row->Id,
											"fullName"=>$row->fullName,
											"email"=>$row->email,
											"userName"=>$row->userName,
											"password"=>$row->password);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		/*public function timeAgo($time_ago)
		{
		    $time_ago = strtotime($time_ago);
		    $cur_time   = time();

		    $dt = new DateTime();
			$dt->format("r");
			$tz = new DateTimeZone("UTC");
			$dt->setTimezone($tz);
			$d = $dt->format("Y-m-d H:i:s");

			$cur_time = strtotime($d);

		    $time_elapsed   = $cur_time - $time_ago;
		    $seconds    = $time_elapsed ;
		    $minutes    = round($time_elapsed / 60 );
		    $hours      = round($time_elapsed / 3600);
		    $days       = round($time_elapsed / 86400 );
		    $weeks      = round($time_elapsed / 604800);
		    $months     = round($time_elapsed / 2600640 );
		    $years      = round($time_elapsed / 31207680 );
		    // Seconds
		    if($seconds <= 60){
		        return "just now";
		    }
		    //Minutes
		    else if($minutes <=60){
		        if($minutes==1){
		            return "one minute ago";
		        }
		        else{
		            return "$minutes minutes ago";
		        }
		    }
		    //Hours
		    else if($hours <=24){
		        if($hours==1){
		            return "an hour ago";
		        }else{
		            return "$hours hrs ago";
		        }
		    }
		    //Days
		    else if($days <= 7){
		        if($days==1){
		            return "yesterday";
		        }else{
		            return "$days days ago";
		        }
		    }
		    //Weeks
		    else if($weeks <= 4.3){
		        if($weeks==1){
		            return "a week ago";
		        }else{
		            return "$weeks weeks ago";
		        }
		    }
		    //Months
		    else if($months <=12){
		        if($months==1){
		            return "a month ago";
		        }else{
		            return "$months months ago";
		        }
		    }
		    //Years
		    else{
		        if($years==1){
		            return "one year ago";
		        }else{
		            return "$years years ago";
		        }
		    }
	}*/
	
	public function chngePwd($oldPwd,$newPwd,$id)
	{
		 $encryptedPass = $this->encrypt($oldPwd);
		 $newencryptedPass = $this->encrypt($newPwd);
		$sql="SELECT * FROM adminmaster WHERE Id ='".$id."' and BINARY password='".$encryptedPass."'";
		$data=$this->db->query($sql);
		if($data->num_rows()>0)
		{
			$this->db->where('Id',$id);
			$this->db->set('password',$newencryptedPass);
			$this->db->update('adminmaster');
			return TRUE;
		}		
		else
		{
			return FALSE;
		}
	}

}








