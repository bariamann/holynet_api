<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiSearchModel extends CI_Model {

	public function _construct(){
		parent::_construct();
	}

  /* Method to search content by keyword
     Created By: Nishit Patel
  */
  public function getSearchContentByKeyword($userId,$lang,$PageIndex,$keyword,$type){
    if ($type == VIDEO_TYPE) {
      $this->load->model("ApiVideoModel","vmodel");
      $userData = $this->vmodel->getVideoBySearchKeyword($keyword,$lang,$PageIndex,$userId);
      return $userData;
    }
    if($type == IMAGE_TYPE){
      $this->load->model("ApiImageModel","imodel");
      $userData = $this->imodel->getSearchImagedByKeyword($userId,$keyword,$lang,$PageIndex);
      return $userData;
    }
    if($type == POST_TYPE){
      $this->load->model("ApiPostModel","pmodel");
      $userData = $this->pmodel->getSearchPostByKeyword($userId,$keyword,$lang,$PageIndex);
      return $userData;
    }
    if($type == USER_TYPE){
      $this->load->model("ApiUserModel","usermodel");
      $userData = $this->usermodel->getSearchUsersKeyword($userId,$keyword,$lang,$PageIndex);
      return $userData;
    }
    if($type == PLAYLIST_TYPE){
      $this->load->model("ApiPlaylistModel","playmodel");
      $userData = $this->playmodel->getSearchPlayListByKeyword($userId,$keyword,$lang,$PageIndex);
      return $userData;
    }
  }

}
?>
