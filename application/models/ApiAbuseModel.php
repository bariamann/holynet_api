<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiAbuseModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

	public function countTotalAbuse($type){
		$this->db->select('Id');
		$this->db->from('abusemaster');
		$this->db->where('type', $type);
		$query = $this->db->count_all_results();
		return ceil($query);
	}

	public function getTotalAbuseReport($fileId,$type){
		$this->db->select('Id');
		$this->db->from('abusemaster');
		$this->db->where('fileId', $fileId);
		$this->db->where('type', $type);
		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);
	}


	public function getAbuseReportList($fileId,$type,$PageIndex){
			$this->db->select('abusemaster.Id,abusemaster.reason,abusemaster.abuseDate,
				abusemaster.userId,usermaster.userName,usermaster.profileImage');
			$this->db->from('abusemaster');
			$this->db->join('usermaster','abusemaster.userId = usermaster.Id','inner');
			$this->db->where('abusemaster.type',$type);
			$this->db->where('abusemaster.fileId',$fileId);
			$this->db->order_by("abusemaster.Id","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayAbuserList($query->result());
	}

	public function displayAbuserList($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

 						$this->load->model("ApiContentModel","cmodel");
						$abuseDate = $this->cmodel->timeAgo($row->abuseDate);

						$profileUrl = "";
						$profileImage = $row->profileImage;
						if($profileImage != null){
							$profileUrl = PROFILE_URL.$profileImage;
						}

						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);

						$arrayObject  = array(
											"AbuseId"=>(int)$row->Id,
											"Reason"=>$row->reason,
											"AbuseDate"=>$abuseDate,
											"User"=>$arrayUser,
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
	}

	/* Method to check already exist data
		 Created By: Nishit Patel
	*/
	public function checkIsAbuseExists($reason,$fileId,$type,$userId){
		$dataArray = "userId=".$userId." AND fileId=".$fileId." AND type=".$type." AND reason=".$this->db->escape($reason);
		$this->db->where($dataArray);
	  $query = $this->db->get('abusemaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
	}

	/* Method to check already exist data
		 Created By: Nishit Patel
	*/
	public function checkIsUserAlreadyReportedAbuse($fileId,$type,$userId){
		$dataArray = "userId=".$userId." AND fileId=".$fileId." AND type=".$type;
		$this->db->where($dataArray);
	  $query = $this->db->get('abusemaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
	}

	/* Method to get is approved is up to limit or not
		 Created By: Nishit Patel
	*/
	public function getIsAllowToReportAbuse($fileId,$type){
		$this->db->select('approvedAbused');
		$this->db->from('contentmaster');
		if($type == VIDEO_TYPE){
			$this->db->join('videomaster','contentmaster.Id = videomaster.contentId','inner');
			$this->db->where('videomaster.Id',$fileId);
		}else if($type == IMAGE_TYPE){
			$this->db->join('imagemaster','contentmaster.Id = imagemaster.contentId','inner');
			$this->db->where('imagemaster.Id',$fileId);
		}else if($type == POST_TYPE){
			$this->db->join('postmaster','contentmaster.Id = postmaster.contentId','inner');
			$this->db->where('postmaster.Id',$fileId);
		}
		$query = $this->db->get();
		$result = $query->result();
		$count = 0;
		if($result != null){
			foreach ($result as $row) {
				$count = (int)$row->approvedAbused;
			}
		}
		return $count;
	}

	/* Method to report abuse for holynet content
		 Created By: Nishit Patel
	*/
	public function addAbuseReportForHolyNetContent($reason,$fileId,$type,$userId){
		$this->load->model("Utility","utility");
    $abuseDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array('userId' => $userId,'fileId' => $fileId,'type' => $type,'reason'=>$reason,'abuseDate'=>$abuseDate);
    $this->db->insert('abusemaster', $data);
    $insert_Id = $this->db->insert_id();

    $displayDate = $this->utility->timeAgoFormat($abuseDate);
    $abuseData = array('Id' => (int)$insert_Id,'Reason'=>$reason,'UserId'=>(int)$userId,'FileId'=>(int)$fileId,'Type'=>(int)$type,'Date'=>$displayDate);
    return $abuseData;
	}

	/* Method to update abuse report
		 Created By: Nishit Patel
	*/
	public function updateAbuseReportHolyNetContent($reasonId,$reason){

		$this->load->model("Utility","utility");
    $abuseUpdateDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
		$displayDate = $this->utility->timeAgoFormat($abuseUpdateDate);

		$dataArray = array("reason"=>$reason,"abuseDate"=>$abuseUpdateDate);
    $this->db->where('Id', $reasonId);
		$this->db->update('abusemaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }

	}

	/* Method to delete abuse report
		 Created By: Nishit Patel
	*/
	public function deleteAbuseReportHolyNetContent($reasonId){
		$this->db->where('Id', $reasonId);
    $this->db->delete('abusemaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
	}

	/* Method to delete abuse report by file
		 Created By: Nishit Patel
	*/
	public function deleteAbouseReportByFile($fileId,$type){
		$whereStr = "fileId=".$fileId." AND type=".$type;
		$this->db->where($whereStr);
		$this->db->delete('abusemaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}


	/* Method to get total pages for abuse report content
		 Created By: Nishit Patel
	*/
	public function getTotalPagesForAbusedContent($contentType){

		$this->db->select('count(abusemaster.Id) as TotalRecords');
		$this->db->from('abusemaster');
		$this->db->join('videomaster','abusemaster.fileId = videomaster.Id','inner');
		$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','inner');
		$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','inner');

		$whereStr = "abusemaster.type = ".$contentType." AND ( NOT EXISTS (select Id from rejectcontentmaster where rejectcontentmaster.type=1))";
		$this->db->where($whereStr);

		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);

	}
	/* Method to get total page for abused post list
		 Created By: Nishit Patel
	*/
	public function getTotalPagesForPostAbusedContent($ContentType){
		$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
			contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
			usermaster.userName,usermaster.profileImage');
		$this->db->from('abusemaster');
		$this->db->join('postmaster','abusemaster.fileId = postmaster.Id and abusemaster.type=3','inner');
		$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$whereStr = "postmaster.Id NOT IN (select rejectcontentmaster.fileId from rejectcontentmaster where rejectcontentmaster.type=3)";
		$this->db->where($whereStr);
		$this->db->group_by('postmaster.Id,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
			contentmaster.Id,contentmaster.userId,contentmaster.submitedDate,
			usermaster.userName,usermaster.profileImage');
		$this->db->order_by("contentmaster.Id","desc");
		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);
	}

	/* Method to get abused post list
		 Created By: Nishit Patel
	*/
	public function getAbusedPostList($ContentType,$PageIndex){
		$this->db->select('postmaster.Id as PostId,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
			contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
			usermaster.userName,usermaster.profileImage');
		$this->db->from('abusemaster');
		$this->db->join('postmaster','abusemaster.fileId = postmaster.Id and abusemaster.type=3','inner');
		$this->db->join('contentmaster','postmaster.contentId = contentmaster.Id','inner');
		$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
		$whereStr = "postmaster.Id NOT IN (select rejectcontentmaster.fileId from rejectcontentmaster where rejectcontentmaster.type=3)";
		$this->db->where($whereStr);
		$this->db->group_by('postmaster.Id,postmaster.decription,postmaster.privacy,postmaster.isCommentable,
			contentmaster.Id,contentmaster.userId,contentmaster.submitedDate,
			usermaster.userName,usermaster.profileImage');
		$this->db->order_by("contentmaster.Id","desc");

		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		return $this->displayAbusedPostList($query->result());
	}

	/* Method to display Abused post List
		 Created By: Nishit Patel
	*/
	public function displayAbusedPostList($result){
		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
				$arrayObject = null;
				$arrayUser= null;
				$arrayTotal = null;

					$totalComments = 0; $totalLikes =0; $totalDislikes = 0;$totalShared = 0;
					$totalAbuseReport = 0;
					$type = 3;

					$this->load->model("ApiCommentModel","cmtmodel");
					$totalComments = $this->cmtmodel->getTotalComment($row->PostId,$type);

					$this->load->model("ApiLikeModel","lkmodel");
					$totalLikes = $this->lkmodel->getTotalLike($row->PostId,$type);
					$totalDislikes = $this->lkmodel->getTotalDislike($row->PostId,$type);

					$this->load->model("ApiShareModel","smodel");
					$totalShared = $this->smodel->getTotalShare($row->PostId,$type);

					$this->load->model("ApiAbuseModel","abmodel");
					$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->PostId,$type);

					$this->load->model("ApiContentModel","cmodel");
					$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

					$profileUrl = "";
					$profileImage = $row->profileImage;
					if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
					}
					$arrayUser  = array(
										"UserId"=>(int)$row->userId,
										"UserName"=>$row->userName,
										"ProfileThumbImage"=>$profileUrl
										);
					$arrayTotal  = array(
										"TotalComments"=>(int)$totalComments,
										"TotalLikes"=>(int)$totalLikes,
										"TotalDislike"=>(int)$totalDislikes,
										"TotalShared"=>(int)$totalShared,
										"TotalAbuseReport"=>(int)$totalAbuseReport
										);

					$arrayObject  = array(
										"PostId"=>(int)$row->PostId,
										"Description"=>$row->decription,
										"Privacy"=>(int)$row->privacy,
										"isCommentable"=>(bool)$row->isCommentable,
										"ContentId"=>(int)$row->ContentId,
										"SubmitedDate"=>$SubmitedDate,
										"User"=>$arrayUser,
										"Total"=>$arrayTotal
										);

				$arrayObjectClient[]=$arrayObject;
				//return $arrayObject;
			}
		}
		return $arrayObjectClient;
	}


	/* Method to get abuse Report content
		 Created By: Nishit Patel
	*/
	public function getAbousedContentList($contentType,$lang,$PageIndex){

			$this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,IFNULL(usermaster.profileImage,"") as profileImage,
				playlistmaster.Id as playListId,playlistmaster.name as playListName');
			$this->db->from('abusemaster');
			$this->db->join('videomaster','abusemaster.fileId = videomaster.Id','inner');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','left');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','inner');
			$whereStr = "abusemaster.type = ".$contentType." AND ( NOT EXISTS (select fileId from rejectcontentmaster where rejectcontentmaster.type=1))";
			$this->db->where($whereStr);
			$this->db->group_by('videomaster.Id,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,playlistmaster.Id,playlistmaster.name');
			$this->db->order_by("contentmaster.Id","desc");

			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayAbusedList($query->result(),$lang);

	}
	/* Method to display abuse list
		 Created By: Nishit Patel
	*/
	public function displayAbusedList($result,$lang){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;
					$arrayVideotype = null;
					$arrayPlayList = null;
					$arrayUser = null;
					$arrayTotal = null;

						$totalComments = 0; $totalLikes =0;
						$totalDislikes = 0;$totalShared = 0;
						$totalAbuseReport = 0; $totalRate = 0;
						$type = 1;

						$this->load->model("ApiRateModel","rtmodel");
						$totalRate = $this->rtmodel->getTotalRatingForFileId($row->VideoId);

						$this->load->model("ApiCommentModel","cmtmodel");
						$totalComments = $this->cmtmodel->getTotalComment($row->VideoId,$type);

						$this->load->model("ApiLikeModel","lkmodel");
						$totalLikes = $this->lkmodel->getTotalLike($row->VideoId,$type);
						$totalDislikes = $this->lkmodel->getTotalDislike($row->VideoId,$type);

						$this->load->model("ApiShareModel","smodel");
						$totalShared = $this->smodel->getTotalShare($row->VideoId,$type);

						$this->load->model("ApiAbuseModel","abmodel");
						$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->VideoId,$type);

						$this->load->model("ApiContentModel","cmodel");
						$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

						$this->load->model("Utility","utility");
						$size = $this->utility->formatSizeUnits($row->size);

						if ($lang == 'ar') {
							$videoTypeName = $row->nameArabic;
						}else{
							$videoTypeName = $row->nameEnglish;
						}

						$arrayVideotype  = array(
											"videoTypeId"=>(int)$row->videoTypeId,
											"videoTypeName"=>$videoTypeName
											);
						$arrayPlayList  = array(
											"playListId"=>(int)$row->playListId,
											"playListName"=>$row->playListName
											);

					  $profileUrl = "";
						$profileImage = $row->profileImage;
						if($profileImage != null){
								$profileUrl = PROFILE_URL.$profileImage;
						}

						$arrayUser  = array(
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl
											);
						$arrayTotal  = array(
											"TotalComments"=>(int)$totalComments,
											"TotalLikes"=>(int)$totalLikes,
											"TotalDislike"=>(int)$totalDislikes,
											"TotalShared"=>(int)$totalShared,
											"TotalAbuseReport"=>(int)$totalAbuseReport,
											"TotalRate"=>(int)$totalRate
											);

						$arrayObject  = array(
											"VideoId"=>(int)$row->VideoId,
											"title"=>$row->title,
											"Description"=>$row->description,
											"thumbUrl"=>VIDEO_THUMB_URL.$row->thumbUrl,
											"fileUrl"=>VIDEO_URL.$row->fileUrl,
											"size"=>$size,
											"duration"=>$row->duration,
											"privacy"=>(int)$row->privacy,
											"keyword"=>$row->keyword,
											"isCommenttable"=>(bool)$row->isCommenttable,
											"ContentId"=>(int)$row->ContentId,
											"SubmitedDate"=>$SubmitedDate,
											"VideoType"=>$arrayVideotype,
											"PlayList"=>$arrayPlayList,
											"User"=>$arrayUser,
											"Total"=>$arrayTotal
											);

					$arrayObjectClient[]=$arrayObject;
				}
			}
			return $arrayObjectClient;
		}

		/* Method to get total pages for abuse report image
			 Created By: Nishit Patel
		*/
		public function getTotalPagesForImageontent($contentType){

			$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
				imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
				imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage');
			$this->db->from('abusemaster');
			$this->db->join('imagemaster','imagemaster.Id = abusemaster.fileId and abusemaster.type=2','inner');
			$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->where('( NOT EXISTS (select Id from rejectcontentmaster where rejectcontentmaster.type=2))');
			$this->db->order_by("contentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);

		}

		/* Method to get Abuse Image List
			 Created By: Nishit Patel
		*/
		public function getAbuseImageList($PageIndex,$userId){
				$this->db->select('imagemaster.Id as ImageId,imagemaster.title,
					imagemaster.thumbUrl,imagemaster.fileUrl,imagemaster.size,
					imagemaster.privacy,imagemaster.keyword,imagemaster.isCommentable,
					contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
					usermaster.userName,usermaster.profileImage');
				$this->db->from('abusemaster');
				$this->db->join('imagemaster','imagemaster.Id = abusemaster.fileId and abusemaster.type=2','inner');
				$this->db->join('contentmaster','imagemaster.contentId = contentmaster.Id','inner');
				$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
				$this->db->where('( NOT EXISTS (select Id from rejectcontentmaster where rejectcontentmaster.type=2))');
				$this->db->order_by("contentmaster.Id","desc");

				$pageNo = "00";
				if ($PageIndex > 0) {
					$PageIndex = $PageIndex * 2;
					$pageNo = $PageIndex.'0';
				}
				$this->db->limit(20,$pageNo);
				$query = $this->db->get();
				//return $this->displayImageList($query->result());
				return $this->displayAbusedImageList($query->result(),$userId);
		}
		/* Method to display abuse image List
			 Created By: Nishit Patel
		*/
		public function displayAbusedImageList($result,$userId){
				$arrayObjectClient = null;
				if($result != null){
					$this->load->model("ApiCommentModel","cmtmodel");
					$this->load->model("ApiLikeModel","lkmodel");
					$this->load->model("ApiShareModel","shmodel");
					$this->load->model("ApiAbuseModel","abmodel");
					$this->load->model("ApiWatchedModel","wmodel");
					$this->load->model("ApiContentModel","cmodel");
					$this->load->model("Utility","utility");

					$this->load->model("ApiUserModel","usmodel");

					foreach($result as $row){
						$arrayObject = null;
						$arrayUser = null;
						$arrayTotal = null;

							$totalComments = 0; $totalLikes =0;
							$totalDislikes = 0;$totalShared = 0;
							$totalAbuseReport = 0; $totalWatchedCount=0;
							$type = 2;


							$totalComments = $this->cmtmodel->getTotalComment($row->ImageId,$type);


							$totalLikes = $this->lkmodel->getTotalLike($row->ImageId,$type);
							$totalDislikes = $this->lkmodel->getTotalDislike($row->ImageId,$type);


							$totalShared = $this->shmodel->getTotalShare($row->ImageId,$type);


							$totalAbuseReport = $this->abmodel->getTotalAbuseReport($row->ImageId,$type);


							$totalWatchedCount = $this->wmodel->getTotalWatchOut($row->ImageId,$type);


							$SubmitedDate = $this->cmodel->timeAgo($row->submitedDate);

							$activities = $this->usmodel->getUserActivity($row->ImageId,IMAGE_TYPE,$userId);

							$size = $this->utility->formatSizeUnits($row->size);
							$profileUrl = "";
							$profileImage = $row->profileImage;
							if($profileImage != null){
									$profileUrl = PROFILE_URL.$profileImage;
							}
							$arrayUser  = array(
												"UserId"=>(int)$row->userId,
												"UserName"=>$row->userName,
												"ProfileThumbImage"=>$profileUrl
												);
							$arrayTotal  = array(
												"TotalComments"=>(int)$totalComments,
												"TotalLikes"=>(int)$totalLikes,
												"TotalDislike"=>(int)$totalDislikes,
												"TotalShared"=>(int)$totalShared,
												"TotalAbuseReport"=>(int)$totalAbuseReport,
												"TotalWatched"=>(int)$totalWatchedCount,
												"TotalWatched"=>(int)$totalWatchedCount
												);

							$arrayObject  = array(
												"ImageId"=>(int)$row->ImageId,
												"title"=>$row->title,
												"thumbUrl"=>IMAGE_THUMB_URL.$row->thumbUrl,
												"fileUrl"=>IMAGE_URL.$row->fileUrl,
												"size"=>$size,
												"privacy"=>(int)$row->privacy,
												"keyword"=>$row->keyword,
												"isCommentable"=>(bool)$row->isCommentable,
												"SubmitedDate"=>$SubmitedDate,
												"User"=>$arrayUser,
												"Total"=>$arrayTotal,
												"Activity"=>$activities
												);

						$arrayObjectClient[]=$arrayObject;
					}
				}
				return $arrayObjectClient;
			}

		/* Method to get content id by file id & type
			 Created By: Nishit Patel
		*/
		public function getContentId($fileId,$type){
			$this->db->select('contentmaster.Id');
			$this->db->from('contentmaster');
			if($type == VIDEO_TYPE){
				$this->db->join('videomaster','contentmaster.Id = videomaster.contentId','inner');
				$this->db->where('videomaster.Id',$fileId);
			}else if($type == IMAGE_TYPE){
				$this->db->join('imagemaster','contentmaster.Id = imagemaster.contentId','inner');
				$this->db->where('imagemaster.Id',$fileId);
			}else if($type == POST_TYPE){
				$this->db->join('postmaster','contentmaster.Id = postmaster.contentId','inner');
				$this->db->where('postmaster.Id',$fileId);
			}
			$query = $this->db->get();
			$result = $query->result();
			$contentId = 0;
			if($result != null){
				foreach ($result as $row) {
					$contentId = (int)$row->Id;
				}
			}
			return $contentId;
		}

		/* Method to approved abused content
			 Created By: Nishit Patel
		*/
		public function approvedContentByAdmin($fileId,$type){

			$count = $this->getIsAllowToReportAbuse($fileId,$type);
			$contentId = (int)$this->getContentId($fileId,$type);
			$dataArray = array("approvedAbused"=>$count+1);
	    $this->db->where('Id', $contentId);
			$this->db->update('contentmaster', $dataArray);
	    if($this->db->affected_rows() > 0){
				$this->deleteAbouseReportByFile($fileId,$type);
	      return true;
	    }else{
	      return false;
	    }
		}


		/* Method to get is approved is up to limit or not
			 Created By: Nishit Patel
		*/
		public function getIsAllowToReportAbuseUser($userId){
			$this->db->select('approvedAbused');
			$this->db->from('usermaster');
			$this->db->where('Id',$userId);
			$query = $this->db->get();
			$result = $query->result();
			$count = 0;
			if($result != null){
				foreach ($result as $row) {
					$count = (int)$row->approvedAbused;
				}
			}
			return $count;
		}

		/* Method to approved abused user
			 Created By: Nishit Patel
		*/
		public function approvedUsersByAdmin($userId){
			$count = $this->getIsAllowToReportAbuseUser($userId);
			$dataArray = array("approvedAbused"=>$count+1);
	    $this->db->where('Id', $userId);
			$this->db->update('usermaster', $dataArray);
	    if($this->db->affected_rows() > 0){
				$this->deleteAbouseReportByFile($userId,USER_TYPE);
	      return true;
	    }else{
	      return false;
	    }
		}

		/******* Reject Function Start ************/
		/* Method to reject content
			 Created By: Nishit Patel
		*/
		public function rejectContent($userId,$fileId,$type,$reason){
			$this->load->model("Utility","utility");
			$rejectDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
			$isCheck = (int)$this->checkIsRejectExists($reason,$fileId,$type,$userId);
			if($isCheck == 0){
				//insert
		    $data = array('userId' => $userId,'fileId' => $fileId,'type' => $type,'reason'=>$reason,'rejectDate'=>$rejectDate);
		    $this->db->insert('rejectcontentmaster', $data);
		    $insert_Id = (int)$this->db->insert_id();
		    $displayDate = $this->utility->timeAgoFormat($rejectDate);

				$this->load->model("ApiNotificationModel","notificationmodel");
				//$this->notificationmodel->sendNotificationForRejectContent($fileId,$type,$userId);
		    return $insert_Id;
			}else if($isCheck < -1){
				//Exsist
				return 0;
			}else if($isCheck > 0){
				//update
				$this->updateRejectHolyNetContent($reason,$fileId,$type,$userId,$isCheck);
				$this->load->model("ApiNotificationModel","notificationmodel");
				//$this->notificationmodel->sendNotificationForRejectContent($fileId,$type,$userId);
				return 1;
			}
		}

		/* Method to block user by admin
			 Created By: Nishit Patel
		*/
		public function blockUsers($userId,$status){
			$dataArray = array("status"=>$status);
	    $this->db->where('Id', $userId);
			$this->db->update('usermaster', $dataArray);
	    if($this->db->affected_rows() > 0){
	      return true;
	    }else{
	      return false;
	    }
		}

		/* Method to check rejected content data
			 Created By: Nishit Patel
		*/
		public function checkIsRejectExists($reason,$fileId,$type,$userId){
			$this->db->select('Id,reason');
			$dataArray = "fileId=".$fileId." AND type=".$type;
			$this->db->where($dataArray);
		  $query = $this->db->get('rejectcontentmaster');
			$result = $query->result();
		  if ($result != null) {
				foreach ($result as $row) {
					if($row->reason == $reason){
						return -1;
					}else{
						return (int)$row->Id;
					}
				}
		  } else {
		     return 0;
		  }
		}
		/* Method to update rejected content
			 Created By: Nishit Patel
		*/
		public function updateRejectHolyNetContent($reason,$fileId,$type,$userId,$rejectId){

			$this->load->model("Utility","utility");
	    $abuseUpdateDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
			$displayDate = $this->utility->timeAgoFormat($abuseUpdateDate);

			$dataArray = array("reason"=>$reason,"rejectDate"=>$abuseUpdateDate);
	    $this->db->where('Id', $rejectId);
			$this->db->update('rejectcontentmaster', $dataArray);

	    if($this->db->affected_rows() > 0){
	      return true;
	    }else{
	      return false;
	    }

		}
		/******* Reject Function End ************/



}

?>
