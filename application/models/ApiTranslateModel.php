<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiTranslateModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}
	public function saveTranslate($type,$fileId,$langId,$title,$description){
			$data = array(
				'type' => $type ,
				'fileId' => $fileId,
				'languageId' => $langId,
				'title' => urldecode($title),
				'description' => urldecode($description)
				);
			$this->db->insert('translationmaster', $data);
			return true;
	}
	/* Method to is translation available
	  Created By: Manzz Baria
	*/
	public function isTranslationAvailable($type,$fileId){
			$this->db->select('Id');
			$this->db->from('translationmaster');
			$strWhere = "type='".$type."' AND fileId='".$fileId."'";
			$this->db->where($strWhere,null,false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return true;
			}
			return false;
	}

	/* Method to delete translation by file
		 Created By: Manzz Baria
	*/
	public function deleteTranslationByFile($fileId,$type){
		$whereStr = "fileId=".$fileId." AND type=".$type;
		$this->db->where($whereStr);
		$this->db->delete('translationmaster');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	/* Method to update translation by file
		 Created By: Manzz Baria
	*/
	public function updateTranslate($type,$fileId,$langId,$title,$description){

		$isAvailable = $this->isTranslationAvailable($type,$fileId);
		if ($isAvailable) {
			if ($langId == -2) {

				$this->deleteTranslationByFile($type,$fileId);

			}else if($langId != -1){
				$data = array(
					'languageId' => $langId
					);
				$this->db->where('type', $type);
				$this->db->where('fileId', $fileId);
				$this->db->update('translationmaster', $data);
			}
			if($title != '-1'){
				$data = array(
					'title' => urldecode($title)
					);
				$this->db->where('type', $type);
				$this->db->where('fileId', $fileId);
				$this->db->update('translationmaster', $data);
			}

			if ($description != '-1') {
				$data = array(
					'description' => urldecode($description)
					);
				$this->db->where('type', $type);
				$this->db->where('fileId', $fileId);
				$this->db->update('translationmaster', $data);
			}
		}else{
			$this->saveTranslate($type,$fileId,$langId,$title,$description);
		}

		return true;

	}



	}
?>
