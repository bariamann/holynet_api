<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCommentModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

		public function getTotalComment($fileId,$type){
			$this->db->select('Id');
			$this->db->from('commentmaster');
			$this->db->where('fileId',$fileId);
			$this->db->where('type',$type);
			$query = $this->db->count_all_results();
			return ceil($query);
		}

	public function getTotalPageOfCommentList($fileId,$type){
			$this->db->select('commentmaster.Id,commentmaster.comment,commentmaster.commentDate,
				commentmaster.userId,usermaster.userName,usermaster.profileImage');
			$this->db->from('commentmaster');
			$this->db->join('usermaster','commentmaster.userId = usermaster.Id','inner');
			$this->db->where('commentmaster.type',$type);
			$this->db->where('commentmaster.fileId',$fileId);
			$this->db->order_by("commentmaster.Id","desc");

			$query = $this->db->count_all_results();
			$result  = $query / 20;
			return ceil($result);
		}

	public function getCommentList($fileId,$type,$PageIndex){
			$this->db->select('commentmaster.Id,commentmaster.comment,commentmaster.commentDate,
				commentmaster.userId,usermaster.userName,usermaster.profileImage');
			$this->db->from('commentmaster');
			$this->db->join('usermaster','commentmaster.userId = usermaster.Id','inner');
			$this->db->where('commentmaster.type',$type);
			$this->db->where('commentmaster.fileId',$fileId);
			$this->db->order_by("commentmaster.Id","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayCommentList($query->result());
	}

	public function displayCommentList($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

 						$this->load->model("Utility","utility");
						$commentDate = $this->utility->timeAgoFormat($row->commentDate);
						$profileUrl = "";
						$profileImage = $row->profileImage;
						if($profileImage != null){
								$profileUrl = PROFILE_URL.$profileImage;
						}
						$userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->userName,"ProfileThumbImage"=>$profileUrl);
						$arrayObject  = array(
							"CommentId"=>(int)$row->Id,
							"Comment"=>$row->comment,
							"CommentDate"=>$commentDate,
							"User" => $userObject
						);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
	}

	/* Method to check is exsisting comment available for file
    Created By: Nishit Patel
  */
  public function checkForExistingComment($userId,$fileId,$type,$comment){
    $dataArray = "userId=".$userId." AND fileId=".$fileId." AND type=".$type." AND comment=".$this->db->escape($comment);
		$this->db->where($dataArray);
	  $query = $this->db->get('commentmaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
  }

	/* Method to Add Comments to content (Video,image,post)
		Created By: Nishit Patel
	*/
	public function addComment($userId,$fileId,$type,$comment){
		$this->load->model("Utility","utility");
    $commentDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array('userId' => $userId,'fileId' => $fileId,'type' => $type,'comment'=>$comment,'commentDate'=>$commentDate);
    $this->db->insert('commentmaster', $data);
    $insert_Id = $this->db->insert_id();

		//send notification
		$this->load->model("ApiContentModel","cmmodel");
		$isYour = $this->cmmodel->isMyContent($fileId,$type,$userId);
		if(!$isYour){
			$this->load->model("ApiNotificationModel","notificationmodel");
		  $this->notificationmodel->sendNotificationForCommentContent($fileId,$type,$userId,$comment);
		}

    $commDate = $this->utility->timeAgoFormat($commentDate);
    $CommentData = $this->getCommentDetailByCommentId($insert_Id); //array('Id' => (int)$insert_Id,'Comment'=>$comment,'UserId'=> $userId,'FileId'=> $fileId,'Type'=> $type,'Date'=>$commDate);
    return $CommentData;
	}

	/* Method to get comment detail by comment id
		 Created By: Nishit Patel
	*/
	public function getCommentDetailByCommentId($commentId){
		$this->db->select("commentmaster.Id,commentmaster.fileId,commentmaster.userId,commentmaster.comment,commentmaster.type,commentmaster.commentDate,usermaster.userName,usermaster.profileImage");
		$this->db->from("commentmaster");
		$this->db->join("usermaster","commentmaster.userId = usermaster.Id","inner");
		$this->db->where("commentmaster.Id",$commentId);
		$query = $this->db->get();
		$result = $query->result();
		$CommentData_1 = null;
		if($result != null){
			$this->load->model("Utility","utility");
			foreach ($result as $row) {
				$displayDate = $this->utility->timeAgoFormat($row->commentDate);
				$profileUrl = "";
				$profileImage = $row->profileImage;
				if($profileImage != null){
						$profileUrl = PROFILE_URL.$profileImage;
				}
				$userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->userName,"ProfileThumbImage"=>$profileUrl);
				$CommentData_1 = array('Id' => (int)$row->Id,'Comment'=>$row->comment,'FileId'=>(int)$row->fileId,'Type'=>(int)$row->type,'Date'=>$displayDate,'User'=>$userObject);
			}
		}
		return $CommentData_1;
	}

	/* Method to update existing comment
		Created By: Nishit Patel
	*/
	public function updateComment($commentId,$comment){

		$this->load->model("Utility","utility");
    $commentUpdateDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
		$commDate = $this->utility->timeAgoFormat($commentUpdateDate);

		$dataArray = array("comment"=>$comment,"commentDate"=>$commentUpdateDate);
    $this->db->where('Id', $commentId);
		$this->db->update('commentmaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return $this->getCommentDetailByCommentId($commentId);
    }else{
      return null;
    }

	}

	/* Method to delete existing comment by comment Id
		Created By: Nishit Patel
	*/
	public function deleteCommentForFile($commentID){
		$this->db->where('Id', $commentID);
    $this->db->delete('commentmaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
	}


	/* Method to delete comments for file & type
		 Created By: Nishit Patel
	*/
	public function deleteCommentsByFileId($fileId,$type){
		$whereStr = "fileId=".$fileId." AND type=".$type;
		$this->db->where($whereStr);
    $this->db->delete('commentmaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
	}




}
?>
