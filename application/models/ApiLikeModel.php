<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiLikeModel extends CI_Model {

		public function _construct(){
			parent::_construct();
		}

		public function getTotalLike($fileId,$type){
			$this->db->select('Id');
			$this->db->from('likemaster');
			$this->db->where('fileId',$fileId);
			$this->db->where('status',1);
			$this->db->where('type',$type);
			$query = $this->db->count_all_results();
			return ceil($query);
		}

		public function getTotalDislike($fileId,$type){
			$this->db->select('Id');
			$this->db->from('likemaster');
			$this->db->where('fileId',$fileId);
			$this->db->where('status',2);
			$this->db->where('type',$type);
			$query = $this->db->count_all_results();
			return ceil($query);
		}


	public function getCommentList($fileId,$type,$PageIndex){
			$this->db->select('commentmaster.Id,commentmaster.comment,commentmaster.commentDate,
				commentmaster.userId,usermaster.userName,usermaster.profileImage');
			$this->db->from('commentmaster');
			$this->db->join('usermaster','commentmaster.userId = usermaster.Id','inner');
			$this->db->where('commentmaster.type',$type);
			$this->db->where('commentmaster.fileId',$fileId);
			$this->db->order_by("commentmaster.Id","desc");
			$pageNo = "00";
			if ($PageIndex > 0) {
				$PageIndex = $PageIndex * 2;
				$pageNo = $PageIndex.'0';
			}
			$this->db->limit(20,$pageNo);
			$query = $this->db->get();
			return $this->displayCommentList($query->result());
	}

	public function displayCommentList($result){
			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

 						$this->load->model("ApiContentModel","cmodel");
						$commentDate = $this->cmodel->timeAgo($row->commentDate);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }
						$arrayObject  = array(
											"CommentId"=>(int)$row->Id,
											"Comment"=>$row->comment,
											"UserId"=>(int)$row->userId,
											"UserName"=>$row->userName,
											"ProfileThumbImage"=>$profileUrl,
											"CommentDate"=>$commentDate
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
	}

	/* Method To delete existing likes by file
     Created By: Nishit Patel
  */
  public function deleteLikesDislikesForFileId($fileId,$type){
    $whereStr = "fileId=".$fileId." AND type=".$type;
    $this->db->where($whereStr);
    $this->db->delete('likemaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
  }

	/* Method to check already exist data
		 Created By: Nishit Patel
	*/
	public function checkIsLikeDislikeExists($status,$fileId,$type,$userId){
		$dataArray = "userId=".$userId." AND fileId=".$fileId." AND type=".$type." AND status=".$status;
		$this->db->where($dataArray);
	  $query = $this->db->get('likemaster');
		$result = $query->result();
		if($result != null){
			foreach ($result as $row) {
				$likeId = (int)$row->Id;
				return $likeId;
			}
		}
		return 0;
	}

	/* Method to check is status chagned
		 Created By: Nishit patel
	*/
	public function checkStatusIsChanged($status,$fileId,$type,$userId){
		$this->db->select("Id,status");
		$dataArray = "userId=".$userId." AND fileId=".$fileId." AND type=".$type;
		$this->db->where($dataArray);
	  $query = $this->db->get('likemaster');
		$result = $query->result();
		if($result != null){
			foreach ($result as $row) {
				$rowStatus = (int)$row->status;
				$likeId = (int)$row->Id;
				if($rowStatus != $status){
					return $likeId;
				}
			}
		}
		return 0;
	}

	/* Method to like/Dislike Holynet contents.
		 Created By: Nishit Patel
	*/
	public function addLikeDislikeHolyNets($status,$fileId,$type,$userId){
		$this->load->model("Utility","utility");
    $likeDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
		$displayDate = $this->utility->timeAgoFormat($likeDate);
		$likeId = $this->checkStatusIsChanged($status,$fileId,$type,$userId);

		//send notification
			$this->load->model("ApiContentModel","cmmodel");
			$isYour = $this->cmmodel->isMyContent($fileId,$type,$userId);
			if(!$isYour){
				$this->load->model("ApiNotificationModel","notificationmodel");
				$this->notificationmodel->sendNotificationForLikeDislikeContent($fileId,$type,$userId,$status);
			}

		if($likeId > 0){
			$updateStatus = $this->updateLikeDislikeHolyNets($likeId,$status,$fileId,$type,$userId);
			$likeData = array('Id' => (int)$likeId,'Status'=>$status,'UserId'=>(int)$userId,'FileId'=>(int)$fileId,'Type'=>(int)$type,'Date'=>$displayDate);
	    return $likeData;
		}else{
			$data = array('userId' => $userId,'fileId' => $fileId,'type' => $type,'status'=>$status,'likeDate'=>$likeDate);
	    $this->db->insert('likemaster', $data);
	    $insert_Id = $this->db->insert_id();
	    $likeData = array('Id' => (int)$insert_Id,'Status'=>$status,'UserId'=>(int)$userId,'FileId'=>(int)$fileId,'Type'=>(int)$type,'Date'=>$displayDate);
	    return $likeData;
		}
	}

	/* Method to update Like/Dislike Holynet contents
		 Created By: Nishit Patel
	*/
	public function updateLikeDislikeHolyNets($likeId,$status,$fileId,$type,$userId){
		$this->load->model("Utility","utility");
    $likeUpdateDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
		$displayDate = $this->utility->timeAgoFormat($likeUpdateDate);

		$dataArray = array("status"=>$status,"likeDate"=>$likeUpdateDate);
    $this->db->where('Id', $likeId);
		$this->db->update('likemaster', $dataArray);

    if($this->db->affected_rows() > 0){

			//send notification
			$this->load->model("ApiNotificationModel","notificationmodel");
			$this->notificationmodel->sendNotificationForLikeDislikeContent($fileId,$type,$userId,$status);
      return true;
    }else{
      return false;
    }
	}

	/* Method to delete likes/dislikes holynet contents
		 Created By: Nishit Patel
	*/
	public function deleteLikeDislikeHolyNets($likeId){
		$this->db->where('Id', $likeId);
    $this->db->delete('likemaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
	}

	/* Method to get list of likes/dislikes users
		 Created By: Nishit Patel
	*/
	public function getLikesDislikesUsersList($fileId,$type,$status,$PageIndex,$userId){
		$this->db->select('likemaster.Id,likemaster.status,likemaster.likeDate,
			likemaster.userId,usermaster.username,usermaster.profileImage');
		$this->db->from('likemaster');
		$this->db->join('usermaster','likemaster.userId = usermaster.Id','inner');
		$this->db->where('likemaster.type',$type);
		$this->db->where('likemaster.fileId',$fileId);
		$this->db->where('likemaster.status',$status);
		$this->db->order_by("likemaster.Id","desc");
		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		return $this->displayLikeUsersList($query->result(),$userId);
	}

	/* Method to display likes json format
		 Created By: Nishit Patel
	*/
	public function displayLikeUsersList($result,$userId){
			$this->load->model("Utility","utility");
			$this->load->model("ApiUserModel","usmodel");

			$arrayObjectClient = null;
			if($result != null){
				foreach($result as $row){
					$arrayObject = null;

						$displayDate = $this->utility->timeAgoFormat($row->likeDate);
						$profileUrl = "";
		        $profileImage = $row->profileImage;
		        if($profileImage != null){
		            $profileUrl = PROFILE_URL.$profileImage;
		        }

						$activities = $this->usmodel->getUserActivity((int)$row->userId,USER_TYPE,$userId);
						$userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->username,"ProfileThumbImage"=>$profileUrl,"Activity"=>$activities);

						$arrayObject  = array(
											"Id"=>(int)$row->Id,
											"Status"=>(int)$row->status,
											"LikeDate"=>$displayDate,
											"User"=>$userObject
											);

					$arrayObjectClient[]=$arrayObject;
					//return $arrayObject;
				}
			}
			return $arrayObjectClient;
	}

	public function getTotalPageLikedContent($userId,$status){
		$this->db->select('Id');
		$this->db->from('likemaster');
		$this->db->where('userId',$userId);
		$this->db->where('status',$status);
		$query = $this->db->count_all_results();
		$result  = $query / 20;
		return ceil($result);
	}

	/* Method to get total notification page for like / dislike content
		 Created By: Nishit Patel
	*/
	public function getNotificationTotalPageLikedContent($userId,$status){
		$query = $this->db->query('
		SELECT likemaster.fileId,likemaster.status,likemaster.type,COUNT(likemaster.Id) as totalCount,likemaster.Id,likemaster.likeDate
			FROM likemaster inner join videomaster on videomaster.Id = likemaster.fileId AND likemaster.type = '.VIDEO_TYPE.'
			inner join contentmaster on videomaster.contentId = contentmaster.Id
			where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type
	UNION
		SELECT likemaster.fileId,likemaster.status,likemaster.type,COUNT(likemaster.Id) as totalCount,likemaster.Id,likemaster.likeDate
			FROM likemaster inner join imagemaster on imagemaster.Id = likemaster.fileId AND likemaster.type = '.IMAGE_TYPE.'
			inner join contentmaster on imagemaster.contentId = contentmaster.Id
			where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type
	UNION
		SELECT likemaster.fileId,likemaster.status,likemaster.type,COUNT(likemaster.Id) as totalCount,likemaster.Id,likemaster.likeDate
			FROM likemaster inner join postmaster on postmaster.Id = likemaster.fileId AND likemaster.type = '.POST_TYPE.'
			inner join contentmaster on postmaster.contentId = contentmaster.Id
			where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type

	order by Id DESC');

		$count = count($query->result());
		$result  = $count / 20;
		return ceil($result);
	}

	/* Method to get liked content by user
		 Created By: Nishit Patel
	*/
	public function getNotificationLikedContentByUser($userId,$status,$PageIndex){

		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		/*$query = $this->db->query('
		SELECT likemaster.Id,likemaster.fileId,likemaster.userId,likemaster.status,likemaster.type,likemaster.likeDate,usermaster.username,usermaster.profileImage
			FROM likemaster inner join videomaster on videomaster.Id = likemaster.fileId AND likemaster.type = '.VIDEO_TYPE.'
			inner join contentmaster on videomaster.contentId = contentmaster.Id inner join usermaster on likemaster.userId = usermaster.Id where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.'
		UNION
		SELECT likemaster.Id,likemaster.fileId,likemaster.userId,likemaster.status,likemaster.type,likemaster.likeDate,usermaster.username,usermaster.profileImage
			FROM likemaster inner join imagemaster on imagemaster.Id = likemaster.fileId AND likemaster.type = '.IMAGE_TYPE.'
			inner join contentmaster on imagemaster.contentId = contentmaster.Id inner join usermaster on likemaster.userId = usermaster.Id where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.'
		UNION
		SELECT likemaster.Id,likemaster.fileId,likemaster.userId,likemaster.status,likemaster.type,likemaster.likeDate,usermaster.username,usermaster.profileImage
			FROM likemaster inner join postmaster on postmaster.Id = likemaster.fileId AND likemaster.type = '.POST_TYPE.'
			inner join contentmaster on postmaster.contentId = contentmaster.Id inner join usermaster on likemaster.userId = usermaster.Id where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.'
		ORDER BY Id DESC LIMIT '.$pageNo.',20');*/

		$query = $this->db->query('
		SELECT DISTINCT likemaster.fileId,likemaster.status,likemaster.type,MAX(likemaster.Id) as Id,MAX(likemaster.likeDate) as likeDate
		FROM likemaster inner join videomaster on videomaster.Id = likemaster.fileId AND likemaster.type = '.VIDEO_TYPE.'
		inner join contentmaster on videomaster.contentId = contentmaster.Id
		where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type DESC
			UNION
		SELECT DISTINCT likemaster.fileId,likemaster.status,likemaster.type,MAX(likemaster.Id) as Id,MAX(likemaster.likeDate) as likeDate
		FROM likemaster inner join imagemaster on imagemaster.Id = likemaster.fileId AND likemaster.type = '.IMAGE_TYPE.'
		inner join contentmaster on imagemaster.contentId = contentmaster.Id
		where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type
			UNION
		SELECT DISTINCT likemaster.fileId,likemaster.status,likemaster.type,MAX(likemaster.Id) as Id,MAX(likemaster.likeDate) as likeDate
		FROM likemaster inner join postmaster on postmaster.Id = likemaster.fileId AND likemaster.type = '.POST_TYPE.'
		inner join contentmaster on postmaster.contentId = contentmaster.Id
		where contentmaster.userId = '.$userId.' AND likemaster.status = '.$status.' group by likemaster.fileId,likemaster.type 
		order by Id DESC LIMIT '.$pageNo.',20');


		//return $this->displayLikeUsersList($query->result(),$userId);
		return $this->displayLikedContentByUser($query->result(),$userId);
	}

	/* Method to get liked content by user
		 Created By: Nishit Patel
	*/
	public function getLikedContentByUser($userId,$status,$PageIndex){
		$this->db->select('Id,status,likeDate,fileId,type');
		$this->db->from('likemaster');
		$this->db->where('userId',$userId);
		$this->db->where('status',$status);
		$this->db->order_by("Id","desc");
		$pageNo = "00";
		if ($PageIndex > 0) {
			$PageIndex = $PageIndex * 2;
			$pageNo = $PageIndex.'0';
		}
		$this->db->limit(20,$pageNo);
		$query = $this->db->get();
		return $this->displayLikedContentByUser($query->result(),$userId);
	}

	/* Method to display liked content by user
		 Created By: Nishit patel
	*/
	private function displayLikedContentByUser($result,$userId){
		$this->load->model("Utility","utility");
		$arrayObjectClient = null;
		if($result != null){
			foreach($result as $row){
				$arrayObject = null;

					$displayDate = $this->utility->timeAgoFormat($row->likeDate);
					$fileId = $row->fileId;
					$type = $row->type;

					$likedTube = null;
					if($type == VIDEO_TYPE){
						$this->load->model("ApiVideoModel","vmodel");
						$lang = "en";
						$likedTube = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
					}
					if($type == POST_TYPE){
						$this->load->model("ApiPostModel","pmodel");
						$likedTube = $this->pmodel->getPostDetails($fileId,$userId);
					}
					if($type == IMAGE_TYPE){
						$this->load->model("ApiImageModel","imgmodel");
						$lang = "en";
						$likedTube = $this->imgmodel->getImageDetails($fileId,$lang,$userId);
					}
					if($type == PLAYLIST_TYPE){
						$this->load->model("ApiFavouriteModel","fmodel");
						$likedTube = $this->fmodel->getFavouritePlayListByPlaylistId($fileId,$userId);
					}

					$arrayObject  = array(
										"Id"=>(int)$row->Id,
										"Status"=>(int)$row->status,
										"LikeDate"=>$displayDate,
										"Type"=>(int)$type,
										"LikedTube"=>$likedTube
										);

				$arrayObjectClient[]=$arrayObject;
				//return $arrayObject;
			}
		}
		return $arrayObjectClient;
	}




}
?>
