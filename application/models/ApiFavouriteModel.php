<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiFavouriteModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to get Total Favourites pages
     Created By: Nishit Patel
  */
  public function getTotalPagesForFavouritesUsers($userId){
    $whereStr = "userId=".$userId." AND type=".USER_TYPE;
    $this->db->select("favouritemaster.Id");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.fileId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }

  /* Method to get Total following users
     Created By: Nishit Patel
  */
  public function getTotalFollowingUsers($userId){
    $whereStr = "userId=".$userId." AND type=".USER_TYPE;
    $this->db->select("favouritemaster.Id");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.fileId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    return $query;
  }

  /* Method to get Total Favourites followers page
     Created By: Nishit Patel
  */
  public function getTotalPagesForFollowersUsers($userId){
    $whereStr = "favouritemaster.fileId=".$userId." AND favouritemaster.type=".USER_TYPE;
    $this->db->select("favouritemaster.Id");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.userId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }

  /* Method to get Total Followers
     Created By: Nishit Patel
  */
  public function getTotalFollowers($userId){
    $whereStr = "favouritemaster.fileId=".$userId." AND favouritemaster.type=".USER_TYPE;
    $this->db->select("favouritemaster.Id");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.userId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    return $query;
  }

  /* Method to get Total Favourites pages
     Created By: Nishit Patel
  */
  public function getTotalPagesForFavouritesContent($userId,$type){

    $whereStr = "userId=".$userId." AND type=".$type;
    $this->db->select("favouritemaster.Id");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.fileId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);

  }

  /* Method to get followers users list by user Id
     Created By: Nishit Patel
  */
  public function getFollowersUsers($userId,$PageIndex){
    $whereStr = "favouritemaster.fileId=".$userId." AND favouritemaster.type=".USER_TYPE." AND usermaster.status=0";
    $this->db->select("favouritemaster.Id,favouritemaster.fileId,favouritemaster.type,favouritemaster.favouriteDate,usermaster.Id as userId,usermaster.userName,usermaster.profileImage");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.userId = usermaster.Id","inner");
    $this->db->where($whereStr);
    $this->db->order_by("favouritemaster.Id","DESC");
    $pageNo = "00";
    if ($PageIndex > 0) {
      $PageIndex = $PageIndex * 2;
      $pageNo = $PageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();

    return $this->displayFavouriteUsers($query->result(),$userId);
  }

  /* Method to get Favourite Videos by User Id
    Created By: Nishit Patel
  */
  public function getFavouriteUsers($userId,$PageIndex){

    //"select favouritemaster.Id,favouritemaster.fileId,favouritemaster.type,favouritemaster.favouriteDate,usermaster.Id as userId,usermaster.userName,usermaster.profileImage from favouritemaster inner join usermaster on favouritemaster.fileId = usermaster.Id where favouritemaster.userId = 52 and favouritemaster.type = 4 and usermaster.status = 0 order by FIELD(favouritemaster.fileId, 81) DESC,favouritemaster.Id DESC"

    $orderStr = "FIELD(favouritemaster.fileId, ".SUPER_USER_ID.") DESC,favouritemaster.Id DESC";

    $whereStr = "userId=".$userId." AND type=".USER_TYPE." AND usermaster.status=0";
    $this->db->select("favouritemaster.Id,favouritemaster.fileId,favouritemaster.type,favouritemaster.favouriteDate,usermaster.Id as userId,usermaster.userName,usermaster.profileImage");
    $this->db->from("favouritemaster");
    $this->db->join("usermaster","favouritemaster.fileId = usermaster.Id","inner");
    $this->db->where($whereStr);
    //$this->db->order_by("favouritemaster.Id","DESC");
    $this->db->order_by($orderStr);
    $pageNo = "00";
    if ($PageIndex > 0) {
      $PageIndex = $PageIndex * 2;
      $pageNo = $PageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();

    return $this->displayFavouriteUsers($query->result(),$userId);
  }

  public function displayFavouriteUsers($result,$userId){
    $this->load->model("Utility","utility");
    $this->load->model("ApiVideoModel","vModel");
    $this->load->model("ApiImageModel","iModel");
    $this->load->model("ApiPostModel","pModel");
    $this->load->model("ApiPlaylistModel","plModel");
    $this->load->model("ApiUserModel","usmodel");
    $arrayObjectClient = null;
    if($result != null){
      foreach($result as $row){
        $arrayObject = null;

        $favrouriteId = (int)$row->Id;
        $favouriteDate = $this->utility->timeAgoFormat($row->favouriteDate);
        $fileId = (int)$row->fileId;


        $totalVideos = $this->vModel->getTotalVideosOfUser((int)$row->userId);
        $totalImages = $this->iModel->getTotalImagesForUser((int)$row->userId);
        $totalPost = $this->pModel->getTotalPostOfUser((int)$row->userId);
        $totalPlayList = $this->plModel->getTotalPlayListOfUser((int)$row->userId);

        $totalObject = array("TotalVideos"=>$totalVideos,"TotalImages"=>$totalImages,"TotalPost"=>$totalPost,"TotalPlayList"=>$totalPlayList);
        $profileUrl = "";
        $profileImage = $row->profileImage;
        if($profileImage != null){
            $profileUrl = PROFILE_URL.$profileImage;
        }

        $activities = $this->usmodel->getUserActivity((int)$row->userId,USER_TYPE,$userId);

        $userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->userName,"ProfileThumbImage"=> $profileUrl,"Totals"=>$totalObject,"Activity"=>$activities);

        $arrayObject  = array(
                      "FavrouiteId"=>$favrouriteId,
                      "FavouriteDate" => $favouriteDate,
                      "FavouriteUsers" => $userObject);

        $arrayObjectClient[]=$arrayObject;
      }
    }

    return $arrayObjectClient;
  }

  /* Method get Total Videos of User
    Created By: Nishit Patel
  */
  public function getTotalFavouritesforPlayList($playListId){

    $this->db->select("count(contentmaster.Id) as TotalVideos");
    $this->db->from("contentmaster");
    $this->db->join("videomaster","contentmaster.Id = videomaster.contentId","inner");
    $this->db->where("contentmaster.userId",$userId);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = (int)$row->TotalVideos;
      }
    }
    return $totalValue;

  }

  /* Method to get total pages for favrouites tubes
     Created By: Nishit Patel
  */
  public function getTotalPagesForFavouriteTubes($userId){
    $whereStr = "userId=".$UserId." AND type<>4";
    $this->db->select("Id,fileId,type,favouriteDate");
    $this->db->from("favouritemaster");
    $this->db->where($whereStr);
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }

  /* Method to get Favourites HolyTubes for Users
     Created By: Nishit Patel
  */
  public function getFavouritesContentByUserIdType($UserId,$PageIndex,$type){

    $whereStr = "userId=".$UserId." AND type=".$type;
    $this->db->select("Id,fileId,type,favouriteDate");
    $this->db->from("favouritemaster");
    $this->db->where($whereStr);
    $this->db->order_by("Id","DESC");
    $pageNo = "00";
    if ($PageIndex > 0) {
      $PageIndex = $PageIndex * 2;
      $pageNo = $PageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();

    return $this->displayFavouriteTubes($query->result(),$UserId);

  }

  /* Method to get Favourites HolyTubes for Users
     Created By: Nishit Patel
  */
  public function getFavouritesTubesByUserId($UserId,$PageIndex){

    $whereStr = "userId=".$UserId." AND type<>4";
    $this->db->select("Id,fileId,type,favouriteDate");
    $this->db->from("favouritemaster");
    $this->db->where($whereStr);
    $this->db->order_by("Id","DESC");
    $pageNo = "00";
    if ($PageIndex > 0) {
      $PageIndex = $PageIndex * 2;
      $pageNo = $PageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();

    return $this->displayFavouriteTubes($query->result(),$UserId);

  }


  /* Method to display favourites tubes in format
    Created By: Nishit Patel
  */
  private function displayFavouriteTubes($result,$userId){

    $this->load->model("Utility","utility");
    $arrayObjectClient = null;
    if($result != null){
      foreach($result as $row){
        $arrayObject = null;

        $favrouriteId = (int)$row->Id;
        $favouriteDate = $this->utility->timeAgoFormat($row->favouriteDate);
        $fileId = (int)$row->fileId;
        $type = (int)$row->type;

        $favrouiteTubes = null;
        if($type == VIDEO_TYPE){
          $this->load->model("ApiVideoModel","vmodel");
          $lang = "en";
          $favrouiteTubes = $this->vmodel->getVideoDetails($fileId,$lang,$userId);
        }
        if($type == POST_TYPE){
          $this->load->model("ApiPostModel","pmodel");
					$favrouiteTubes = $this->pmodel->getPostDetails($fileId,$userId);
        }
        if($type == IMAGE_TYPE){
          $this->load->model("ApiImageModel","imodel");
          $lang = "en";
          $favrouiteTubes = $this->imodel->getImageDetails($fileId,$lang,$userId);
        }
        if($type == PLAYLIST_TYPE){
          $favrouiteTubes = $this->getFavouritePlayListByPlaylistId($fileId,$userId);
        }

        $arrayObject  = array(
                      "FavrouiteId"=>$favrouriteId,
                      "FavouriteDate" => $favouriteDate,
                      "Type"=>$type,
                      "FavouriteTubes" => $favrouiteTubes);

        $arrayObjectClient[]=$arrayObject;
      }
    }
    return $arrayObjectClient;
  }

  /* Method to get Favourite video by Video Id
    Created By: Nishit Patel
  */
  public function getFavouriteVideoByVideoId($videoId,$type){

    $this->db->select('videomaster.Id as VideoId,videomaster.title,videomaster.description,
				videomaster.thumbUrl,videomaster.fileUrl,videomaster.size,
				videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.isCommenttable,
				videomaster.videoTypeId,videotypemaster.nameEnglish,videotypemaster.nameArabic,
				contentmaster.Id as ContentId,contentmaster.userId,contentmaster.submitedDate,
				usermaster.userName,usermaster.profileImage,
				playlistmaster.Id as playListId,playlistmaster.name as playListName');
			$this->db->from('videomaster');
			$this->db->join('contentmaster','videomaster.contentId = contentmaster.Id','inner');
			$this->db->join('usermaster','contentmaster.userId = usermaster.Id','inner');
			$this->db->join('playlistmaster','videomaster.playListId = playlistmaster.Id','inner');
			$this->db->join('videotypemaster','videomaster.videoTypeId = videotypemaster.Id','inner');
			$this->db->where('videomaster.Id',$videoId);
			$query = $this->db->get();
			return $this->displayVideoObject($query->result(),$type);

  }

  /* Method to display Video in proper format
    Created By: Nishit Patel
  */
  public function displayVideoObject($result,$type){

    $this->load->model("Utility","utility");
    $this->load->model("ApiCommentModel","cModel");
    $this->load->model("ApiLikeModel","ikModel");
    $this->load->model("ApiAbuseModel","abModel");
    $this->load->model("ApiRateModel","rModel");
    $arrayObjectClient = null;
    if($result != null){
      foreach($result as $row){

        $profileUrl = "";
        $profileImage = $row->profileImage;
        if($profileImage != null){
            $profileUrl = PROFILE_URL.$profileImage;
        }

        $arrayObject = null;
        $userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->userName,"ProfileThumbImage"=> $profileImage);
        $videoType = array("videoTypeId"=>(int) $row->videoTypeId,"videoTypeName"=> $row->nameEnglish);
        $playListObject = array("playListId"=>(int) $row->playListId,"playListName"=> $row->playListName);

        $videoId = (int)$row->VideoId;

        $totalComments = (int)$this->cModel->getTotalComment($videoId,VIDEO_TYPE);
        $totalLikes = (int)$this->ikModel->getTotalLike($videoId,VIDEO_TYPE);
        $totalDisLike = (int)$this->ikModel->getTotalDislike($videoId,VIDEO_TYPE);
        $totalFavourites = (int)$this->getTotalFavouritesForFileId($videoId,VIDEO_TYPE);
        $totalAbuseReport = (int)$this->abModel->getTotalAbuseReport($videoId,VIDEO_TYPE);
        $totalRating = $this->rModel->getTotalRatingForFileId($videoId);


        $totalsObject = array("TotalComments"=>$totalComments,"TotalLikes"=>$totalLikes,"TotalDislike"=>$totalDisLike,"TotalShared"=>0,"TotalAbuseReport"=>$totalAbuseReport,"TotalRate"=>$totalRating,"TotalFavourites"=>$totalFavourites);

        $submitDate = $this->utility->timeAgoFormat($row->submitedDate);

        $arrayObject  = array(
          "VideoId"=>$videoId,
          "title"=> $row->title,
          "Description"=> $row->description,
          "thumbUrl"=> VIDEO_THUMB_URL.$row->thumbUrl,
          "fileUrl"=> VIDEO_URL.$row->fileUrl,
          "size"=> (double)$row->size,
          "duration"=> $row->duration,
          "privacy"=> (int)$row->privacy,
          "keyword"=> $row->keyword,
          "isCommenttable"=> (bool)$row->isCommenttable,
          "SubmitedDate"=> $submitDate,
          "ContentId"=>(int) $row->ContentId,
          "VideoType"=>$videoType,
          "Totals"=>$totalsObject,
          "User"=>$userObject,
          "PlayList"=>$playListObject
        );

        $arrayObjectClient[]=$arrayObject;

      }
    }
    return $arrayObjectClient;
  }

  /* Method to get Favourite playlist by UserId
    Created By: Nishit Patel
  */
  public function getFavouritePlayListByPlaylistId($playlistId,$userId){
    $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,playlistmaster.userId,usermaster.userName,usermaster.profileImage");
    $this->db->from("playlistmaster");
    $this->db->join("usermaster","playlistmaster.userId = usermaster.Id","inner");
    $whereStr = "playlistmaster.Id=".$playlistId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    return $this->displayPlaylistObject($query->result(),$userId);
  }

  /* Method to display Playlist object in format
    Created By: Nishit Patel
  */
  private function displayPlaylistObject($result,$userId){
    $this->load->model("Utility","utility");
    $this->load->model("ApiVideoModel","vmodel");
    $this->load->model("ApiUserModel","usmodel");

    $arrayObjectClient = null;
    if($result != null){
      foreach($result as $row){
        $arrayObject = null;
        $profileUrl = "";
        $profileImage = $row->profileImage;
        if($profileImage != null){
            $profileUrl = PROFILE_URL.$profileImage;
        }
        $userObject = array("UserId"=>(int)$row->userId,"UserName"=>$row->userName,"ProfileThumbImage"=> $profileUrl);
        $playListDate = $this->utility->timeAgoFormat($row->createdDate);

        $videoObject = $this->vmodel->getLatestVideoFromPlayList((int)$row->Id);

        $totalVideos = $this->vmodel->getTotalVideosByPlayListId((int)$row->Id);

        $totalFavourites = $this->getTotalFavouritesForFileId((int)$row->Id,PLAYLIST_TYPE);

        $totalObject = array('TotalVideos'=>$totalVideos,'TotalFavourites'=>$totalFavourites);

        $activities = $this->usmodel->getUserActivity((int)$row->Id,PLAYLIST_TYPE,$userId);

        $arrayObject  = array(
                      "Id"=>(int)$row->Id,
                      "Name"=> $row->name,
                      "createdDate" => $playListDate,
                      "Video"=>$videoObject,
                      "User" => $userObject,
                      "Total" => $totalObject,
											"Activity"=>$activities);
        $arrayObjectClient[]=$arrayObject;
      }
    }
    return $arrayObjectClient;
  }


  /* Method to get Total Favourites Users for Playlist
    Created By: Nishit Patel
  */
  public function getTotalFavouritesForFileId($fileId,$type){
    //SELECT  FROM  where fileId = 2 AND type=5
    $this->db->select("count(Id) as TotalFavourites");
    $this->db->from("favouritemaster");
    $whereStr = "fileId=".$fileId." AND type=".$type;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->TotalFavourites;
      }
    }
    return $totalValue;
  }

  /* Method to get Total Comment by File Id
    Created By: Nishit Patel
  */
  public function getTotalCommentsForFileId($fileId,$type){
    $this->db->select("count(Id) as TotalComments");
    $this->db->from("commentmaster");
    $whereStr = "fileId=".$fileId." AND type=".$type;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $totalValue = 0;
    if($result != null){
      foreach ($result as $row) {
        $totalValue = $row->TotalComments;
      }
    }
    return $totalValue;
  }


  /* Method to get Favourite videos by user Id
      Created By: Nishit Patel
  */
  public function getFavouriteVideos($userId,$pageIndex){


    //select favouritemaster.Id,favouritemaster.favouriteDate,videomaster.Id,videomaster.title,videomaster.description,videomaster.thumbUrl,videomaster.fileUrl,videomaster.duration,videomaster.privacy,videomaster.keyword,videomaster.videoTypeId,videomaster.playListId,videomaster.contentId from favouritemaster inner join videomaster on favouritemaster.fileId = videomaster.Id where favouritemaster.type=1

			$this->db->select('Id,nameEnglish,nameArabic');
			$this->db->from('countrymaster');
			$this->db->order_by('nameEnglish', 'ASC');
			$query = $this->db->get();

			return $this->displayCountry($query->result());

  }

  /* Method to get Favourite playlist by UserId
    Created By: Nishit Patel
  */
  public function getFavouritePlayList($userId,$pageIndex){

    $this->db->select("playlistmaster.Id,playlistmaster.name,playlistmaster.createdDate,usermaster.Id as UserId,usermaster.userName,favouritemaster.Id as favouriteId,favouritemaster.favouriteDate");
    $this->db->from("favouritemaster");
    $this->db->join("playlistmaster","favouritemaster.fileId = playlistmaster.Id","inner");
    $this->db->join("usermaster","playlistmaster.userId = usermaster.Id","inner");
    $whereStr = "favouritemaster.type=5 AND favouritemaster.userId =".$userId;
    $this->db->where($whereStr);
    $this->db->order_by('playlistmaster.name', 'ASC');
    $query = $this->db->get();

    return $this->displayPlayList($query->result());

  }

  /* Method to display Playlist data into proper json format
      Created By: Nishit Patel
  */
  public function displayPlayList($result){
  		$arrayObjectClient = null;
  		if($result != null){
  			foreach($result as $row){
  				$arrayObject = null;

          $userObject = array("UserId"=>(int)$row->UserId,"UserName"=>$row->userName);

          $this->load->model("Utility","utility");
          $playListDate = $this->utility->timeAgoFormat($row->createdDate);
          $favouriteDate = $this->utility->timeAgoFormat($row->favouriteDate);

  				$arrayObject  = array(
  											"PlayListId"=>(int)$row->Id,
  											"Name"=> $row->name,
                        "PlaylistDate" => $playListDate,
                        "User" => $userObject);

          $favouriteObject = array("FavouriteId"=>(int)$row->favouriteId,"FavouriteDate" => $favouriteDate,"PlayList"=>$arrayObject);

  				$arrayObjectClient[]=$favouriteObject;
  			}
  		}
  		return $arrayObjectClient;
  }

  /* Method to check is Country already exisist
      Created By: Nishit Patel
  */
  public function checkIsFavouriteExsist($UserId,$FileId,$FileType){

    $dataArray = "userId=".$UserId." AND fileId=".$FileId." AND type=".$FileType;
		$this->db->where($dataArray);
	  $query = $this->db->get('favouritemaster');
    $result = $query->result();
		if($result != null){
			foreach ($result as $row) {
				$favouriteId = (int)$row->Id;
				return $favouriteId;
			}
		}
		return 0;

  }

  /* Method to check is Country already exsist for update
      Creted By: Nishit Patel
  */
  public function checkIsCountryExsistforUpdate($Id,$NameEnglish,$NameArabic){
    $dataArray = "nameEnglish=".$this->db->escape($NameEnglish)." AND nameArabic=".$this->db->escape($NameArabic)." AND Id <> ".$Id;
    $this->db->where($dataArray);
	  $query = $this->db->get('countrymaster');
	  $count_row = $query->num_rows();
	  if ($count_row > 0) {
	     return TRUE;
	  } else {
	     return FALSE;
	  }
  }


  /* Method to add items to favourite
      Created By: Nishit Patel
  */
  public function addFavourite($UserId,$FileId,$FileType){
    $this->load->model("Utility","utility");
    $submitDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array('userId' => $UserId,'fileId' => $FileId,'type' => $FileType,'favouriteDate'=>$submitDate);
    $this->db->insert('favouritemaster', $data);
    $insert_Id = $this->db->insert_id();

    //send notification
    $this->load->model("ApiNotificationModel","notificationmodel");
    $this->notificationmodel->sendNotificationForFollowingUser($FileId,$FileType,$UserId);

    $favouriteDate = $this->utility->timeAgoFormat($submitDate);
    $favouriteData = array('Id' => (int)$insert_Id,'UserId'=> $UserId,'FileId'=> $FileId,'Type'=> $FileType,'Date'=>$favouriteDate);
    return $favouriteData;
  }

  /* Method to follow user by Super User
     Created By: Nishit Patel
  */
  public function makeFollowUser($userId,$fileId,$fileType){
    $isFollowing = $this->checkIsFavouriteExsist($userId,$fileId,$fileType);
    if($isFollowing == 0){
      $this->load->model("Utility","utility");
      $submitDate = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $data = array('userId' => $userId,'fileId' => $fileId,'type' => $fileType,'favouriteDate'=>$submitDate);
      $this->db->insert('favouritemaster', $data);
      $insert_Id = $this->db->insert_id();
      return $insert_id;
    }
    return 0;
  }

  /* Method to delete exsisting favourite
      Created By: Nishit Patel
  */
  public function deleteExistsFavourite($Id){

      $this->db->where('Id', $Id);
      $this->db->delete('favouritemaster');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }

  }

  /* Method To delete existing favourites by file
     Created By: Nishit Patel
  */
  public function deleteFavouritesForFileId($fileId,$type){
    $whereStr = "fileId=".$fileId." AND type=".$type;
    $this->db->where($whereStr);
    $this->db->delete('favouritemaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
  }


  /* Method to update exsising Country record
      Created By: Nishit Patel
  */
  public function updateExistsCountry($Id,$NameEnglish,$NameArabic){

    $dataArray = array("nameEnglish"=>$NameEnglish,"nameArabic"=>$NameArabic);
    $this->db->where('Id', (int)$Id);
		$this->db->update('countrymaster', $dataArray);

    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }

  }


}


?>
